#!/bin/bash

export RUSTFLAGS="-D warnings"

cargo check $@ --manifest-path src/Cargo.toml  --target-dir src/target && \
cargo run -p uni_publisher --manifest-path src/Cargo.toml  --target-dir src/target -- --working-dir /tmp/publisher --json-file $PWD/CI/publish_crates.json  --base-directory $PWD --allow-dirty

