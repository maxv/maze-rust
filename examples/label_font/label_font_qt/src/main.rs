extern crate label_font;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	label_font::linux::main();
}
