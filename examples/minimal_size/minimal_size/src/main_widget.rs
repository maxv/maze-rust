use uniui_core::SlotImpl;

use uniui_gui::prelude::*;

use uniui_widget_button::prelude::*;
use uniui_layout_linear_layout::prelude::*;
use uniui_wrapper_minimalsize::prelude::*;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(on_small_button_clicked)]
	slot_small_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_big_button_clicked)]
	slot_big_button_clicked: SlotImpl<()>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		let slot_small_button_clicked = SlotImpl::new();
		let slot_big_button_clicked = SlotImpl::new();
		
		let layout = u_linear_layout!{
			orientation: Orientation::Vertical,
			widgets: {
				u_minimalsize!{u_button!{
					text: "small button".to_owned(),
					signal_clicked: &slot_small_button_clicked,
				}},
				u_button!{
					text: "big button".to_owned(),
					signal_clicked: &slot_big_button_clicked,
				},
			}
		};
		
		return MainWidget {
			layout,
			slot_small_button_clicked,
			slot_big_button_clicked,
		};
	}
	
	fn on_small_button_clicked(&mut self, _:()) {
		log::info!("small button clicked!!!");
	}
	
	fn on_big_button_clicked(&mut self, _:()) {
		log::info!("big button clicked!!!");
	}
}
