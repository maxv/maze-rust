extern crate checkbox;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	checkbox::linux::main();
}
