use uniui_core::SlotImpl;
use uniui_core::SlotProxy;

use uniui_gui::prelude::*;

use uniui_widget_button::Button;
use uniui_layout_linear_layout::prelude::*;
use uniui_widget_checkbox::CheckBox;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(on_button_clicked)]
	slot_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_checkstate1_changed)]
	slot_checkstate1_changed: SlotImpl<bool>,

	#[uprocess_last(on_checkstate2_changed)]
	slot_checkstate2_changed: SlotImpl<bool>,
	
	slotproxy_set_title: SlotProxy<String>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		log::info!("test");
		let slot_button_clicked = SlotImpl::new();
		let slot_checkstate1_changed = SlotImpl::new();
		let slot_checkstate2_changed = SlotImpl::new();
		let slotproxy_set_title;
		
		let mut layout = LinearLayout::new(Orientation::Vertical);
		layout.push_widget({
			let mut checkbox = CheckBox::new(format!("CheckMe1 (title will change)"), false);
			checkbox.signal_checkstate_changed().connect_slot(&slot_checkstate1_changed);
			slotproxy_set_title = checkbox.slot_set_title().proxy();
			checkbox
		});
		layout.push_widget({
			let mut checkbox = CheckBox::new(format!("CheckMe2 (title stay the same)"), true);
			checkbox.signal_checkstate_changed().connect_slot(&slot_checkstate2_changed);
			checkbox
		});
		layout.push_widget({
			let mut button = Button::new("Button".to_string());
			button.signal_clicked().connect_slot(&slot_button_clicked);
			button
		});
		
		return MainWidget {
			layout,
			slot_button_clicked,
			slot_checkstate1_changed,
			slot_checkstate2_changed,
			slotproxy_set_title,
		};
	}
	
	fn on_button_clicked(&mut self, _:()) {
		log::info!("button clicked!!!");
	}
	
	fn on_checkstate1_changed(&mut self, b:bool) {
		log::info!("checked state#1 changed:{}", b);
		match b {
			true => self.slotproxy_set_title.exec_for(format!("#1 Checked")),
			false => self.slotproxy_set_title.exec_for(format!("#1 UnChecked")),
		};
	}

	fn on_checkstate2_changed(&mut self, b:bool) {
		log::info!("checked state#2 changed:{}", b);
	}
}
