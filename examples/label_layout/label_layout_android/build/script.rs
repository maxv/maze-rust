
uni_components::define_ui_page!{
	name: MAIN,
	parameter: (),
	path: "/",
}

fn main() {
	let mut builder = uniui_build::AndroidBuilder::new(
		"path/to/icon".to_owned(),
		"com.uniuirust.examples.button_layout".to_owned(),
		std::env::var("ANDROID_SDK_ROOT").expect("ANDROID_SDK_ROOT not set"),
		"21".to_owned(),
		"30".to_owned(),
		std::env::var("ANDROID_NDK").expect("ANDROID_NDK not set"),
		"Label Layout".to_owned(),
	);
	builder.add_launcher(MAIN, "label_layout");
	builder.execute();
}
