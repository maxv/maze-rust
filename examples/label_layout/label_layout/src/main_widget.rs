use uniui_core::SlotImpl;
use uniui_core::Signal;

use uniui_gui::Widget;

use uniui_widget_button::prelude::*;
use uniui_layout_linear_layout::prelude::*;
use uniui_widget_label::prelude::*;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(on_left_button_clicked)]
	slot_left_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_right_button_clicked)]
	slot_right_button_clicked: SlotImpl<()>,
	
	signal_text_updated: Signal<String>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		log::info!("test");
		let slot_left_button_clicked = SlotImpl::new();
		let slot_right_button_clicked = SlotImpl::new();
		let mut signal_text_updated = Signal::new();
		
		let layout = u_linear_layout!{
			orientation: Orientation::Vertical,
			widgets: {
				u_linear_layout!{
					orientation: Orientation::Horizontal,
					widgets: {
						u_button!{
							text: "left button".to_owned(),
							signal_clicked: &slot_left_button_clicked,
						},
						u_button!{
							text: "right button".to_owned(),
							signal_clicked: &slot_right_button_clicked,
						},
					}
				},
				u_label!{
					text: "Click button to update text".to_owned(),
					slot_set_text: signal_text_updated,
					text_alignment: TextAlignment::Center,
				},
			}
		};
		
		return MainWidget {
			layout,
			slot_right_button_clicked,
			slot_left_button_clicked,
			signal_text_updated,
		};
	}
	
	fn on_left_button_clicked(&mut self, _:()) {
		log::info!("left button clicked!!!");
		self.signal_text_updated.emit("Left clicked".to_owned());
	}
	
	fn on_right_button_clicked(&mut self, _:()) {
		log::info!("right button clicked!!!");
		self.signal_text_updated.emit("Right clicked".to_owned());
	}
}
