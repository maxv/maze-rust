pub use uniui_gui::Application;

mod main_widget;
use main_widget::MainWidget;


#[uniui_gui::u_main]
pub fn app_main(app: &mut dyn Application) {
	uniui_gui::utils::set_logger_tag("UniL-L".to_owned());
	uniui_gui::utils::init_logger(app);

	app.set_title("Label+Layout");
	app.set_main_widget(Box::new(MainWidget::new()));
}
