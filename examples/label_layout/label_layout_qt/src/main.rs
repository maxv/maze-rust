extern crate label_layout;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	label_layout::linux::main();
}
