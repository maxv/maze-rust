use uniui_core::Signal;
use uniui_core::SlotImpl;
use uniui_core::SlotProxy;

use uniui_gui::prelude::*;

use uniui_widget_datepicker::prelude::*;
use uniui_widget_timepicker::prelude::*;
use uniui_layout_linear_layout::prelude::*;
use uniui_widget_label::prelude::*;
use uniui_wrapper_minimalsize::prelude::*;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(date_changed)]
	slot_date_changed: SlotImpl<NaiveDate>,
	
	signal_date_changed: Signal<NaiveDate>,
	slotproxy_set_date: SlotProxy<NaiveDate>,
	slotproxy_set_date_text: SlotProxy<String>,
	
	#[uprocess_last(time_changed)]
	slot_time_changed: SlotImpl<NaiveTime>,
	
	signal_time_changed: Signal<NaiveTime>,
	slotproxy_set_time: SlotProxy<NaiveTime>,
	slotproxy_set_time_text: SlotProxy<String>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		log::error!("Hello");
		
		let slotproxy_set_date;
		let slotproxy_set_date_text;
		
		let mut signal_date_changed = Signal::new();
		let slot_date_changed = SlotImpl::new();
		
		let slotproxy_set_time;
		let slotproxy_set_time_text;
		
		let mut signal_time_changed = Signal::new();
		let slot_time_changed = SlotImpl::new();
		
		let layout = u_linear_layout!{
			orientation: Orientation::Vertical,
			widgets: {
				u_minimalsize!{u_datepicker!{
					signal_date_selected: &slot_date_changed,
				}},
				u_minimalsize!{u_label!{
					text: "Date appears there".to_owned(),
					slot_set_text_proxy: slotproxy_set_date_text,
				}},
				u_minimalsize!{u_datepicker!{
					slot_set_date_proxy: slotproxy_set_date,
				}},
				u_minimalsize!{u_datepicker!{
					slot_set_date: signal_date_changed,
				}},
				
				u_minimalsize!{u_timepicker!{
					signal_time_selected: &slot_time_changed,
				}},
				u_minimalsize!{u_label!{
					text: "Time appears there".to_owned(),
					slot_set_text_proxy: slotproxy_set_time_text,
				}},
				u_minimalsize!{u_timepicker!{
					slot_set_time_proxy: slotproxy_set_time,
				}},
				u_minimalsize!{u_timepicker!{
					slot_set_time: signal_time_changed,
				}},
			}
		};
		
		return MainWidget {
			layout,
			slot_date_changed,
			signal_date_changed,
			slotproxy_set_date,
			slotproxy_set_date_text,
			
			
			slot_time_changed,
			signal_time_changed,
			slotproxy_set_time,
			slotproxy_set_time_text,
			
		};
	}
	
	fn date_changed(&mut self, date: NaiveDate) {
		let string = format!("Year:{} Month:{} Day:{}", date.year(), date.month(), date.day());
		self.slotproxy_set_date_text.exec_for(string);
		self.slotproxy_set_date.exec_for(date.clone());
		self.signal_date_changed.emit(date);
	}
	
	fn time_changed(&mut self, time: NaiveTime) {
		let string = format!("Hour:{} Minute:{} Second:{}", time.hour(), time.minute(), time.second());
		self.slotproxy_set_time_text.exec_for(string);
		self.slotproxy_set_time.exec_for(time.clone());
		self.signal_time_changed.emit(time);
	}
}
