extern crate datetimepicker;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	datetimepicker::linux::main();
}
