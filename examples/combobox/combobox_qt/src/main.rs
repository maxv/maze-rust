extern crate combobox;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	combobox::linux::main();
}
