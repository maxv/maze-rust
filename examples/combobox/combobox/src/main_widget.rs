use uniui_core::Property;

use uniui_gui::prelude::*;

use uniui_widget_combobox::prelude::*;
use uniui_layout_linear_layout::prelude::*;
use uniui_wrapper_minimalsize::prelude::*;
use uniui_wrapper_backgroundcolor::prelude::*;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: BackgroundColor<LinearLayout>,
	
	#[uproperty_process]
	color: Property<Option<Color>>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		let mut color = Property::new(None);
		
		let slotproxy_set_items;
		let layout = u_backgroundcolor!{
			widget: u_linear_layout!{
				orientation: Orientation::Vertical,
				widgets: {
					u_minimalsize!{u_combobox!{
						slot_set_items_proxy: slotproxy_set_items,
						signal_selection_changed: color.slot(),
						
					}},
				}
			},
			slot_set_color: color.signal(),
		};
		
		slotproxy_set_items.exec_for({
			let mut v = Vec::new();
			v.push((None, "<Empty>".to_owned()));
			v.push((Some(Color{red: 255, green: 255, blue: 0}), "Yellow".to_owned()));
			v.push((Some(Color{red:   0, green:   0, blue: 0}), "Black".to_owned()));
			v
		});
		
		return MainWidget {
			layout,
			color,
		};
	}
}
