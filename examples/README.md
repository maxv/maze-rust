# Examples.

It's better to review it one after another:
1. button_generic
1. button_layout
1. label_layout
1. textedit_button
1. checkbox
1. combobox
1. groupbox
1. label_font
1. mouseclicklistener
1. datetimepicker
1. simple_service
1. theme
1. ugl

Each example contains one or more business-logic crates and 2 or 3 platform-crates.

Platform-crates are used to build the example for particular platform.

Usually it has 
1. *_wasm crate for WASM platform, 
1. *_qt crate for desktop and
1. *_android crate for android
1. *_ios coming soon

