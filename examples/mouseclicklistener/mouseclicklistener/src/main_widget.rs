use uniui_core::SlotImpl;
use uniui_core::SlotProxy;

use uniui_gui::prelude::*;

use uniui_wrapper_mouseclicklistener::prelude::*;
use uniui_widget_label::prelude::*;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	widget: MouseClickListener<Label>,
	
	#[uprocess_last(on_clicked)]
	slot_clicked: SlotImpl<(i32, i32)>,
	
	slotproxy_text_updated: SlotProxy<String>,
	
	click_count: usize,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		log::info!("test");
		let slot_clicked = SlotImpl::new();
		let slotproxy_text_updated;
		
		let widget = u_mouse_click_listener!{
			widget: u_label!{
				text: "Click there to update text".to_owned(),
				slot_set_text_proxy: slotproxy_text_updated,
				text_alignment: TextAlignment::Center,
			},
			signal_clicked: &slot_clicked,
		};
		
		return MainWidget {
			widget,
			slot_clicked,
			slotproxy_text_updated,
			click_count: 0,
		};
	}
	
	fn on_clicked(&mut self, (x, y):(i32, i32)) {
		log::info!("clicked!!!");
		self.click_count +=1;
		self.slotproxy_text_updated.exec_for(
			format!(
				"Clicked: {} times. Last time at (x={}, y={})", 
				self.click_count,
				x,
				y,
			)
		);
	}
}
