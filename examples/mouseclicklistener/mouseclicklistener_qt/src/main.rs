extern crate mouseclicklistener;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	mouseclicklistener::linux::main();
}
