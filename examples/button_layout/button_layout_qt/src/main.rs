extern crate button_layout;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	button_layout::linux::main();
}
