pub use uniui_gui::Application;
use uniui_gui::u_main;

mod main_widget;
use main_widget::MainWidget;


#[u_main]
pub fn app_main(app: &mut dyn Application) {
	uniui_gui::utils::set_logger_tag("UniB-L".to_owned());
	uniui_gui::utils::init_logger(app);

	app.set_title("button layout");
	app.set_main_widget(Box::new(MainWidget::new()));
}
