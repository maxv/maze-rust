extern crate groupbox;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	groupbox::linux::main();
}
