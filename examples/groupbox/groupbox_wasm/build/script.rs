fn main() {
	let mut builder = uniui_build::WasmBuilder::for_framework(
		uniui_build::Framework::Rocket
	);
	builder.add_page(groupbox_static_map::INDEX, "groupbox");
	builder.execute();
}

