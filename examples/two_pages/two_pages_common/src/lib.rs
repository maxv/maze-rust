use uni_components::{
	define_ui_page,
};

pub const TAG: &'static str = "Uni2Pages";

#[derive(Clone, Debug, PartialEq, Eq, serde::Deserialize, serde::Serialize)]
pub struct Int {
	pub data: u32,
}

define_ui_page!{
	name: INDEX,
	parameter: Int,
	path: "/",
}

define_ui_page!{
	name: SECOND,
	parameter: Int,
	path: "/second/page/",
}
