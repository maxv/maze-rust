mod generated {
	include!(concat!(env!("OUT_DIR"), "/uni_build_generated.rs"));
}

fn main() -> Result<(), std::io::Error> {
	env_logger::builder().filter_level(log::LevelFilter::Debug).init();

	return async_std::task::block_on(async {
		log::info!("started");

		let mut app = tide::new();
			
		generated::attach(&mut app);
		log::info!("frontent ready");
		
		let listen_on = std::env::var("LISTEN_ON").unwrap_or("localhost:8080".to_owned());
		log::info!("listen:{}", listen_on);
		app.listen(listen_on).await?;
		Ok(())
	});
}
