

fn main() {
	let mut builder = uniui_build::WasmBuilder::for_framework(
		uniui_build::Framework::Tide
	);
	builder.add_page(
		two_pages_common::INDEX, 
		"two_pages_page_one"
	);
	builder.add_page(
		two_pages_common::SECOND, 
		"two_pages_page_two"
	);
	builder.execute();
}
