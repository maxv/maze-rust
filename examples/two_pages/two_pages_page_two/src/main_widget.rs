use uniui_core::SlotImpl;

use uniui_gui::prelude::*;
use uniui_gui::UiPageContext;

use uniui_widget_button::prelude::*;
use uniui_layout_linear_layout::prelude::*;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(on_left_button_clicked)]
	slot_left_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_right_button_clicked)]
	slot_right_button_clicked: SlotImpl<()>,
	
	#[uprocess_last_app(on_central_button_clicked)]
	slot_central_button_clicked: SlotImpl<()>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		log::info!("test");
		let slot_left_button_clicked = SlotImpl::new();
		let slot_right_button_clicked = SlotImpl::new();
		let slot_central_button_clicked = SlotImpl::new();
		
		let layout = u_linear_layout!{
			orientation: Orientation::Vertical,
			widgets: {
				u_button!{
					text: "First page".to_owned(),
					signal_clicked: &slot_central_button_clicked,
				},
				u_linear_layout!{
					orientation: Orientation::Horizontal,
					widgets: {
						u_button!{
							text: "left button".to_owned(),
							signal_clicked: &slot_left_button_clicked,
						},
						u_button!{
							text: "right button".to_owned(),
							signal_clicked: &slot_right_button_clicked,
						},
					}
				},
			}
		};
		
		return MainWidget {
			layout,
			slot_right_button_clicked,
			slot_left_button_clicked,
			slot_central_button_clicked,
		};
	}
	
	fn on_left_button_clicked(&mut self, _:()) {
		log::info!("left button clicked!!!");
	}
	
	fn on_right_button_clicked(&mut self, _:()) {
		log::info!("right button clicked!!!");
	}
	
	fn on_central_button_clicked(&mut self, app:&dyn UiPageContext, _:()) {
		let mut num = UiPageContext::get_launch_parameter(app, two_pages_common::INDEX).unwrap_or(two_pages_common::Int{data:0});
		num.data += 1;
		log::error!("num:{}", num.data);
		if let Err(e) = UiPageContext::open_page_with_data(app, two_pages_common::INDEX, &num) {
			log::error!("Can't open page:{:?}", e);
		}
	}
}
