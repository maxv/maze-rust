
fn main() {
	let mut builder = uniui_build::AndroidBuilder::new(
		"path/to/icon".to_owned(),
		"com.uniuirust.examples.two_pages".to_owned(),
		std::env::var("ANDROID_SDK_ROOT").expect("ANDROID_SDK_ROOT not set"),
		"21".to_owned(),
		"30".to_owned(),
		std::env::var("ANDROID_NDK").expect("ANDROID_NDK not set"),
		"TwoPages".to_owned(),
	);
	builder.add_launcher(two_pages_common::INDEX, "two_pages_page_one");
	builder.add_page(two_pages_common::SECOND, "two_pages_page_two");
	builder.execute();
}
