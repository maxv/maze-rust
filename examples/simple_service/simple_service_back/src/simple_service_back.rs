#[derive(Clone)]
pub struct Back<T: 'static + Sized + Sync + Send + Clone> {
	_t: T,
}

impl<T: 'static + Sized + Sync + Send + Clone> Back<T> {
	pub fn new(t: T) -> Self {
		return Back{
			_t: t,
		};
	}
	
	pub async fn answer(&mut self, data: simple_service_common::number_to_string::In, _env: &()) -> simple_service_common::number_to_string::Out {
		use simple_service_common::TestMe::*;
		return match data.data {
			0 => Ok(vec!["Zero".to_owned(), "test".to_owned()]),
			1 => Ok(vec!["One".to_owned(), "test".to_owned()]),
			_ => NumberNotFound,
		}
	}
}

uni_localservice::u_service_impl!{
	generic: <T: 'static + Sized + Sync + Send + Clone>,
	base: crate::Back<T>,
	service: simple_service_common::number_to_string,
	method: answer,
	env: (),
}

