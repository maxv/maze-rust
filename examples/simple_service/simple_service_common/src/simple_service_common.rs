use uni_components::{
	define_service,
	define_ui_page,
	define_type,
	Kind,
};

define_ui_page!{
	name: INDEX,
	parameter: (),
	path: "/",
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, serde::Deserialize, serde::Serialize)]
pub struct U32 {
	pub data: u32,
}

impl U32 {
	pub fn new(data: u32) -> U32 {
		return U32{
			data,
		};
	}
}

define_type!{
	doc:"
	The type to test `define_type!` macro.

	Looks like it works.
	",
	name: TestMe<T>  where T: 'static + serde::de::DeserializeOwned + serde::Serialize + Clone + Send + Sync {},
	variants: [ 
		Ok{
			code: 200,
			body: Vec<T>,
			doc: "String representation found",
		},
		NumberNotFound{
			code: 404,
			doc: "String representation not found",
		},
	],
}

define_service!{
	doc: "
	The service converts number to string
	
	It may be usefull if you need string representation of the number.
	The limitation is it only supports `0` and `1` as an input.
	",
	name: number_to_string,
	input: U32,
	output_type: TestMe<String>,
	path: "/api/n2s/",
	kind: Kind::Get,
}

// for macros tests only

define_type!{
	name: TestVec,
	variants: [
		RegularVariant{
			code: 200,
			body: String,
			doc: "Regular variant",
		},
		VecVariant {
			code: 800,
			doc: "Will test `Vec<String>` whenever it'll be ready to work",
			// TODO: body: Vec<String>,
		},
	],
}

define_type!{
	name: TestVec1<T> where T: 'static + Send + Sync + serde::Serialize + serde::de::DeserializeOwned{},
	variants: [
		RegularVariant {
			code: 200,
			body: T,
			doc: "Regular variant",
		},
		VecVariant {
			code: 800,
			doc: "Will test `Vec<String>` whenever it'll be ready to work",
			body: Vec<T>,
		},
	],
}


define_type!{
	name: TestVec2<T, K> 
	where 
		T: 'static + Send + Sync + serde::Serialize + serde::de::DeserializeOwned,
		K: 'static + Send + Sync + serde::Serialize + serde::de::DeserializeOwned,
		{},
	variants: [
		RegularVariant {
			code: 200,
			body: T,
			doc: "Regular variant",
		},
		AnotherVariant {
			code: 201,
			body: K,
			doc: "Regular variant",
		},
	],
}

define_service!{
	name: string_to_number,
	input: Vec<String>,
	outputs:[
		Ok{code: 200, body: U32, doc: "OK",},
		NumberNotFound{code: 404, doc: "Number not found"},
	],
	path: "/api/n2s/",
	kind: Kind::Get,
}

