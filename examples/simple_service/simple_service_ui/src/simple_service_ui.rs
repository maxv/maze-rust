use uniui_gui::prelude::*;

mod main_widget;

#[uniui_gui::u_main]
pub fn app_main(app: &mut dyn Application) {
	uniui_gui::utils::init_logger(app);

	app.set_title("Simple Service");
	app.set_main_widget(Box::new(main_widget::MainWidget::new()));
}

