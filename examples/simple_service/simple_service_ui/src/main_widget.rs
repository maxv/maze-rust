use uniui_core::SlotImpl;
use uniui_core::SlotProxy;

use uniui_widget_button::prelude::*;
use uniui_layout_linear_layout::prelude::*;
use uniui_widget_label::prelude::*;


type Answer = simple_service_common::number_to_string::Out;

#[derive(uniui_gui::Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(on_left_button_clicked)]
	slot_left_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_right_button_clicked)]
	slot_right_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_central_button_clicked)]
	slot_central_button_clicked: SlotImpl<()>,
	
	#[uprocess_each(on_response)]
	slot_response: SlotImpl<Result<Answer, uni_components::Response>>,
	
	slotproxy_set_text: SlotProxy<String>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		let slot_left_button_clicked = SlotImpl::new();
		let slot_right_button_clicked = SlotImpl::new();
		let slot_central_button_clicked = SlotImpl::new();
		
		let slotproxy_set_text;
		
		let layout = u_linear_layout!{
			orientation: Orientation::Vertical,
			widgets: {
				u_label!{
					text: "<Press any button>".to_owned(),
					slot_set_text_proxy: slotproxy_set_text,
				},
				u_linear_layout!{
					orientation: Orientation::Horizontal,
					widgets: {
						u_button!{
							text: "left button".to_owned(),
							signal_clicked: &slot_left_button_clicked,
						},
						u_button!{
							text: "right button".to_owned(),
							signal_clicked: &slot_right_button_clicked,
						},
					}
				},
				u_button!{
					text: "central button".to_owned(),
					signal_clicked: &slot_central_button_clicked,
				},
			}
		};
		
		return MainWidget {
			layout,
			slot_right_button_clicked,
			slot_left_button_clicked,
			slot_central_button_clicked,
			slotproxy_set_text,
			slot_response: SlotImpl::new(),
		};
	}
	
	fn on_left_button_clicked(&mut self, _:()) {
		log::info!("left button clicked!!!");
		match simple_service_common::number_to_string::REF.exec(&simple_service_common::U32::new(0), &self.slot_response) {
			Ok(_) => log::trace!("request send"),
			Err(e) => log::error!("request send error:{:?}", e),
		}
	}
	
	fn on_right_button_clicked(&mut self, _:()) {
		log::info!("right button clicked!!!");
		match simple_service_common::number_to_string::REF.exec(&simple_service_common::U32::new(1), &self.slot_response) {
			Ok(_) => log::trace!("request send"),
			Err(e) => log::error!("request send error:{:?}", e),
		}
	}
	
	fn on_central_button_clicked(&mut self, _:()) {
		log::info!("central button clicked!!!");
		match simple_service_common::number_to_string::REF.exec(&simple_service_common::U32::new(2), &self.slot_response) {
			Ok(_) => log::trace!("request send"),
			Err(e) => log::error!("request send error:{:?}", e),
		}
	}
	
	fn on_response(&self, result: Result<Answer, uni_components::Response>) {
		match result {
			Ok(Answer::Ok(n)) => {
				self.slotproxy_set_text.exec_for(n.join("-"));
			},
			Ok(Answer::NumberNotFound) => {
				self.slotproxy_set_text.exec_for("Number not found".to_owned());
			},
			Err(r) => {
				log::error!("unexpected response:{:?}", r);
				self.slotproxy_set_text.exec_for("Error. Try again.".to_owned());
			},
		};
	}
}
