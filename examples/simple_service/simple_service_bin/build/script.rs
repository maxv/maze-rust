fn main() {
	let mut build = uniui_build::WasmBuilder::for_framework(uniui_build::Framework::Tide);
	build.add_page(simple_service_common::INDEX, "simple_service_ui");
	build.execute();
}
