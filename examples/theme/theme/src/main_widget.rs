use uniui_core::SlotImpl;

use uniui_gui::Widget;

use uniui_widget_button::prelude::*;
use uniui_layout_linear_layout::prelude::*;
use uniui_widget_groupbox::prelude::*;
use uniui_wrapper_themeclass::prelude::*;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(on_left_button_clicked)]
	slot_left_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_right_button_clicked)]
	slot_right_button_clicked: SlotImpl<()>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		log::info!("test");
		let slot_left_button_clicked = SlotImpl::new();
		let slot_right_button_clicked = SlotImpl::new();
		
		let layout = u_linear_layout!{
			orientation: Orientation::Vertical,
			widgets: {
				u_groupbox!{
					text: "Hello groupbox11".to_owned(),
					widget: u_linear_layout!{
						orientation: Orientation::Horizontal,
						widgets: {
							u_themeclass_replace!{
								classes: {"hello".to_owned()},
								widget: u_button!{
									text: "left button".to_owned(),
									signal_clicked: &slot_left_button_clicked,
								},
							},
							u_themeclass_add!{
								classes: {"hello".to_owned()},
								widget: u_button!{
									text: "right button".to_owned(),
									signal_clicked: &slot_right_button_clicked,
								},
							},
						}
					},
				},
			}
		};
		
		return MainWidget {
			layout,
			slot_right_button_clicked,
			slot_left_button_clicked,
		};
	}
	
	fn on_left_button_clicked(&mut self, _:()) {
		log::info!("left button clicked!!!");
	}
	
	fn on_right_button_clicked(&mut self, _:()) {
		log::info!("right button clicked!!!");
	}
}
