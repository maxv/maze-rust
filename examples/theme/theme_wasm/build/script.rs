use uniui_theme_material::application_theme::*;
use uniui_theme_material::default_attribute::*;

use uniui_widget_button::THEME_CLASS_NAME as BUTTON_CLASS;

fn main() {
	let mut theme = ApplicationTheme::new();
	uniui_theme_material::prepare_theme(&mut theme);
	
	theme.set_default(PRIMARY_COLOR.to_owned(), "#ffc0ad".to_owned());
	theme.set_default(PRIMARY_VARIANT_COLOR.to_owned(), "#ffc0ad".to_owned());
	theme.set_default(ON_PRIMARY_COLOR.to_owned(), "#271c19".to_owned());
	
	theme.set_default(SECONDARY_COLOR.to_owned(), "#9656a1".to_owned());
	theme.set_default(SECONDARY_VARIANT_COLOR.to_owned(), "#e78fb3".to_owned());
	theme.set_default(ON_SECONDARY_COLOR.to_owned(), "#140d0b".to_owned());
	
	theme.set_default(BACKGROUND_COLOR.to_owned(), "#55423d".to_owned());
	theme.set_default(ON_BACKGROUND_COLOR.to_owned(), "#fff3ec".to_owned());
	
	theme.set_default(SURFACE_COLOR.to_owned(), "#fff3ec".to_owned());
	theme.set_default(ON_SURFACE_COLOR.to_owned(), "#140d0b".to_owned());
	
	theme.set_default(BORDER_COLOR.to_owned(), "#fff3ec".to_owned());
	theme.set_default(BORDER_WIDTH.to_owned(), "3px".to_owned());
	
	theme.add_class(
		ClassTheme{
			name: BUTTON_CLASS.to_owned(),
			attributes: vec![
				Attribute {
					name: "background-color".to_owned(),
					value: AttributeValue::Default(PRIMARY_COLOR.to_owned())
				},
				Attribute {
					name: "border-color".to_owned(),
					value: AttributeValue::Default(ON_PRIMARY_COLOR.to_owned())
				},
				Attribute {
					name: "color".to_owned(),
					value: AttributeValue::Default(ON_PRIMARY_COLOR.to_owned())
				},
				Attribute {
					name: "margin".to_owned(),
					value: AttributeValue::Custom("5px".to_owned()),
				},
				Attribute {
					name: "line-height".to_owned(),
					value: AttributeValue::Custom("27pt".to_owned()),
				},
			],
		},
	);
	
	
	let mut build = uniui_build::WasmBuilder::for_framework(uniui_build::Framework::Rocket);
	build.add_page(theme_static_map::INDEX, "theme");
	build.default_css_theme(theme.css_string().unwrap());
	build.execute();
}

