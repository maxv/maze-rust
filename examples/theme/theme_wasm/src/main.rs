#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

mod generated {
	include!(concat!(env!("OUT_DIR"), "/uni_build_generated.rs"));
}

fn main() {

	let rocket = rocket::ignite();
	let rocket = generated::mount_to(rocket);
	rocket.launch();
}
