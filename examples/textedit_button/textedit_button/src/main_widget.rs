use uniui_core::SlotImpl;

use uniui_gui::Widget;

use uniui_widget_button::prelude::*;
use uniui_layout_linear_layout::prelude::*;
use uniui_widget_textedit::prelude::*;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(on_button_clicked)]
	slot_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_text_changed)]
	slot_text_changed: SlotImpl<String>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		log::info!("test");
		let slot_button_clicked = SlotImpl::new();
		let slot_text_changed = SlotImpl::new();
		
		let layout = u_linear_layout!{
			orientation: Orientation::Vertical,
			alignment_horizontal: { uniui_gui::AlignmentHorizontal::Start },
			alignment_vertical: { uniui_gui::AlignmentVertical::Center },
			widgets: {
				u_textedit!{
					placeholder: "put your name here".to_string(),
					signal_text_changed: &slot_text_changed,
				},
				u_button!{
					text: "Button".to_string(),
					signal_clicked: &slot_button_clicked,
				},
			}
		};
		
		return MainWidget {
			layout,
			slot_button_clicked,
			slot_text_changed
		};
	}
	
	fn on_button_clicked(&mut self, _:()) {
		log::info!("button clicked!!!");
	}
	
	fn on_text_changed(&mut self, s:String) {
		log::info!("text changed:{}", s);
	}
}
