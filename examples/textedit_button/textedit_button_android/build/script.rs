uni_components::define_ui_page!{
	name: MAIN,
	parameter: (),
	path: "/",
}

fn main() {
	let mut builder = uniui_build::AndroidBuilder::new(
		"path/to/icon".to_owned(),
		"com.uniuirust.examples.textedit_button".to_owned(),
		std::env::var("ANDROID_SDK_ROOT").expect("ANDROID_SDK_ROOT not set"),
		"21".to_owned(),
		"30".to_owned(),
		std::env::var("ANDROID_NDK").expect("ANDROID_NDK not set"),
		"Textedit Button".to_owned(),
	);
	builder.add_launcher(MAIN, "textedit_button");
	builder.execute();
}
