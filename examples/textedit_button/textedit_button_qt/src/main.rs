extern crate textedit_button;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	textedit_button::linux::main();
}
