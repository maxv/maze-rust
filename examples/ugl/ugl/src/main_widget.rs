use uniui_core::SlotImpl;
use uniui_core::SlotProxy;

use uniui_gui::Widget;

use uniui_widget_button::Button;

use uniui_widget_ugl::UGL;
use uniui_widget_ugl::UGLColor;

use uniui_layout_linear_layout::prelude::*;


mod painter;
use self::painter::Painter;

#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	layout: LinearLayout,
	
	#[uprocess_last(on_left_button_clicked)]
	slot_left_button_clicked: SlotImpl<()>,
	
	#[uprocess_last(on_right_button_clicked)]
	slot_right_button_clicked: SlotImpl<()>,
	
	slotproxy_set_color: SlotProxy<UGLColor>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		let slot_left_button_clicked = SlotImpl::new();
		let slot_right_button_clicked = SlotImpl::new();
		let slotproxy_set_color;
		
		let mut layout = LinearLayout::new(Orientation::Horizontal);
		layout.push_widget({
			let painter = Painter::new(UGLColor{red: 0.1, green:0.8, blue: 0.8, alpha: 1.0});
			slotproxy_set_color = painter.slot_set_color().proxy();
			let mut ugl = UGL::new();
			ugl.set_painter(Some(painter));
			ugl
		});
		
		layout.push_widget({
			let mut layout = LinearLayout::new(Orientation::Vertical);
			layout.push_widget({
				let mut button = Button::new("left button".to_string());
				button.signal_clicked().connect_slot(&slot_left_button_clicked);
				button
			});
			layout.push_widget({
				let mut button = Button::new("right button".to_string());
				button.signal_clicked().connect_slot(&slot_right_button_clicked);
				button
			});
			
			layout
		});
		
		return MainWidget {
			layout,
			slot_right_button_clicked,
			slot_left_button_clicked,
			slotproxy_set_color,
		};
	}
	
	fn on_left_button_clicked(&mut self, _:()) {
		log::info!("left button clicked!!!");
		self.slotproxy_set_color.exec_for(UGLColor{ red: 0.8, green: 0.1, blue: 0.1, alpha: 1.0});
	}
	
	fn on_right_button_clicked(&mut self, _:()) {
		log::info!("right button clicked!!!");
		self.slotproxy_set_color.exec_for(UGLColor{ red: 0.1, green: 0.8, blue: 0.1, alpha: 1.0});
	}
}
