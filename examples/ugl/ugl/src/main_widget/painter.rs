use uni_uglhelpers::ProgramBuilder;

use uniui_widget_ugl::UGLPainter;
use uniui_widget_ugl::UGLColor;
use uniui_widget_ugl::UGLContext;
use uniui_widget_ugl::shader::UGLProgram;
use uniui_widget_ugl::shader::UGLAttribLocation;
// use uniui_widget_ugl::shader::UGLUniformLocation;
use uniui_widget_ugl::shader::UGLBuffer;
use uniui_widget_ugl::shader::BindBufferTarget;
use uniui_widget_ugl::shader::BufferDataUsage;
use uniui_widget_ugl::shader::UGLType;
use uniui_widget_ugl::shader::DrawMode;
use uniui_widget_ugl::shader::DepthFunc;

use uniui_gui::prelude::*;


struct PainterGL {
	pub program: UGLProgram,
	pub vertex_position: UGLAttribLocation,
	// pub projection_matrix: UGLUniformLocation,
	// pub model_view_matrix: UGLUniformLocation,
	pub buffer: UGLBuffer,
}

#[derive(DataProcessor)]
pub struct Painter {
	#[uproperty_process]
	#[uproperty_public_slot(slot_set_color)]
	color: Property<UGLColor>,
	gl: Option<PainterGL>
}


impl Painter {
	pub fn new(default_color: UGLColor) -> Painter {
		return Painter {
			color: Property::new(default_color),
			gl: None,
		};
	}
}

impl UGLPainter for Painter {
	fn draw(
		&mut self,
		context: &mut dyn UGLContext,
		_width: u32,
		_height: u32,
	) {
		if !self.gl.is_some() {
			let mut pb = ProgramBuilder::new(context);
			pb.add_fragment_shader(&vec!{include_str!("shaders/simple_fragment.fsh")}[..]).unwrap();
			pb.add_vertex_shader(&vec!{include_str!("shaders/simple_vertex.vsh")}[..]).unwrap();
			let program = pb.build().unwrap();
			let vertex_position = context.get_attrib_location(&program, "aVertexPosition").unwrap();
			// let projection_matrix = 0; //context.get_uniform_location(&program, "uProjectionMatrix").unwrap();
			// let model_view_matrix = 0; //context.get_uniform_location(&program, "uModelViewMatrix").unwrap();
			
			
			
			let points = [
				-0.5,  0.5,
				0.5,  0.5, 
				-0.5, -0.5,
				0.5, -0.5, 
			];
			
			let target = BindBufferTarget::ArrayBuffer;
			
			let buffer = context.create_buffer().unwrap();
			context.bind_buffer(target, Some(&buffer));
			context.buffer_data_f32(target, &points, BufferDataUsage::StaticDraw);
			
			self.gl = Some(PainterGL{
				program,
				vertex_position,
				// projection_matrix,
				// model_view_matrix,
				buffer,
			});
		}
		
		context.clear_color(self.color.as_ref());
		
		if let Some(gl) = self.gl.as_ref() {
			
			let num_components = 2;
			let normalize = false;
			let stride = 0;
			let offset = 0;
			
			context.depth_func(DepthFunc::Always);
			
			
			
			context.bind_buffer(BindBufferTarget::ArrayBuffer, Some(&gl.buffer));
			context.vertex_attrib_pointer(
				&gl.vertex_position,
				num_components,
				UGLType::Float,
				normalize,
				stride,
				offset,
			);
			context.enable_vertex_attrib_array(&gl.vertex_position);
			
			context.use_program(&gl.program);
			// context.uniform_matrix4fv(&gl.model_view_matrix, &model_view_matrix); 
			// context.uniform_matrix4fv(&gl.projection_matrix, &projection_matrix);
			
			let mode = DrawMode::TriangleStrip;
			let first = 0;
			let count = 3;
			context.draw_arrays(mode, first, count);
		}
	}	
}

