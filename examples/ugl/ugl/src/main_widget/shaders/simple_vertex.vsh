attribute vec2 aVertexPosition;

// uniform mat4 uModelViewMatrix;
// uniform mat4 uProjectionMatrix;

void main() {
  gl_Position = vec4(aVertexPosition, 0.0, 1.0); //uProjectionMatrix * uModelViewMatrix * aVertexPosition;
}

