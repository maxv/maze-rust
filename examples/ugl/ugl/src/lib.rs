pub use uniui_gui::Application;
use uniui_gui::u_main;

mod main_widget;


#[u_main]
pub fn app_main(app: &mut dyn Application) {
	uniui_gui::utils::init_logger(app);
	log::warn!("Hello");
	
	app.set_title("UGL");
	app.set_main_widget(Box::new(main_widget::MainWidget::new()));
}
