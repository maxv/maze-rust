extern crate ugl;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	ugl::linux::main();
}
