extern crate button_generic;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	button_generic::linux::main();
}
