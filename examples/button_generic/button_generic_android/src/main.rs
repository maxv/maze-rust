mod generated {
	include!(concat!(env!("OUT_DIR"), "/uni_build_generated.rs"));
}

fn main() {
	generated::start(Some("UniBTN"));
}
