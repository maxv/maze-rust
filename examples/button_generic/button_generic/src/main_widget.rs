use uniui_widget_button::prelude::*;

use uniui_gui::prelude::*;

use uniui_core::SlotImpl;


#[derive(Widget)]
pub struct MainWidget {
	#[base_widget]
	button: Button,
	
	#[uprocess_last(on_button_clicked)]
	slot_button_clicked: SlotImpl<()>,
}

impl MainWidget {
	pub fn new() -> Self {
		log::info!("test");
		let slot_button_clicked = SlotImpl::new();
		
		let button = u_button!{
			text: "UniButton".to_owned(),
			signal_clicked: &slot_button_clicked,
		};
		
		return MainWidget {
			button,
			slot_button_clicked,
		};
	}
	
	fn on_button_clicked(&mut self, _:()) {
		log::info!("button clicked!!!");
	}
}
