extern crate uniui_build;

fn main() {
	let mut builder = uniui_build::WasmBuilder::for_framework(
		uniui_build::Framework::Rocket
	);
	builder.add_path("/", "simple");
	builder.execute();
}
