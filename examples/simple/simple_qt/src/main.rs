extern crate simple;
extern crate simple_logger;

fn main() {
	simple_logger::SimpleLogger::new().init().unwrap();
	simple::linux::main();
}
