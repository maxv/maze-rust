pub use uniui_gui::Application;

mod main_widget;
use main_widget::MainWidget;


#[uniui_gui::u_main]
pub fn app_main(app: &mut dyn Application) {
	uniui_gui::utils::init_logger(app);

	app.set_title("simple");
	app.set_main_widget(Box::new(MainWidget::new()));
}
