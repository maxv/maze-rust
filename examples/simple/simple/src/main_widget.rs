use uniui_core::SlotImpl;

use uniui_gui::prelude::*;

use uniui_widget_button::Button;


#[derive(Widget)]
pub struct MainWidget{
	#[base_widget]
	button: Button,
	
	#[uprocess_last(on_button_clicked)]
	slot_button_clicked: SlotImpl<()>,
}

impl MainWidget {
	pub fn new() -> MainWidget {
		log::info!("test");
		let mut button = Button::new("just a button".to_string());
		let slot_button_clicked = SlotImpl::new();
		button.signal_clicked().connect_slot(&slot_button_clicked);
		return MainWidget {
			button,
			slot_button_clicked,
		};
	}
	
	fn on_button_clicked(&mut self, _:()) {
		log::info!("button clicked!!!");
	}
}
