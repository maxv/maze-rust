use std::{
	fs::File,
	path::Path,
	process::Command,
};
use structopt::StructOpt;

use reqwest::blocking::get as reqwest_get;

const DELAY: u64 = 5;

#[derive(StructOpt, Debug)]
struct CliOptions {
	#[structopt(short, long)]
	pub manifest_path: Option<String>,

	#[structopt(short, long)]
	pub working_dir: String,

	#[structopt(long)]
	pub json_file: Option<String>,

	#[structopt(short, long)]
	pub token: Option<String>,

	#[structopt(long)]
	pub allow_dirty: bool,

	#[structopt(short, long)]
	pub build_target: Option<String>,

	#[structopt(long)]
	pub base_directory: Option<String>,
}


fn unpack_crate<V: std::fmt::Display>(
	file_name: &str,
	dir: &str,
	name: &str,
	version: &V,
) {
	let old_crate_content = dir.to_owned();
	let old_crate_path = file_name.to_owned();

	std::fs::create_dir_all(&old_crate_content)
		.expect(&format!("Content directory creation error. Path:{}", old_crate_content));

	let old_file =
		File::open(&old_crate_path).expect(&format!("Couldn't open:{}", old_crate_path));
	let old_tar = flate2::read::GzDecoder::new(old_file);
	let mut old_archive = tar::Archive::new(old_tar);
	old_archive.unpack(&old_crate_content).expect(&format!(
		"Couldn't extract `{}` to `{}`",
		old_crate_path, old_crate_content
	));

	let file_with_git_sha = format!("{}/{}-{}/.cargo_vcs_info.json", dir, name, version);
	if let Err(_) = std::fs::remove_file(&file_with_git_sha) {
		println!("Couldn't remove file with commit sha {}", file_with_git_sha);
	}

	let lock_file = format!("{}/{}-{}/Cargo.lock", dir, name, version);
	let _ = std::fs::remove_file(&lock_file);
}

fn is_crates_content_the_same<V: std::fmt::Display>(
	old_crate_path: &str,
	new_crate_path: &str,
	target: &str,
	name: &str,
	version: &V,
) -> bool {
	let old_crate_content = format!("{}/old/{}-{}", target, name, version);
	unpack_crate(&old_crate_path, &old_crate_content, &name, &version);
	println!("Crate.io's version downloaded and unpacked to {}", old_crate_content);

	let new_crate_content = format!("{}/new/{}-{}", target, name, version);
	unpack_crate(&new_crate_path, &new_crate_content, &name, &version);
	println!("Local version builded and unpacked to {}", new_crate_content);

	let is_diff =
		dir_diff::is_different(&old_crate_content, &new_crate_content).expect(&format!(
			"Couldn't calculate difference between `{}` and `{}`",
			old_crate_content, new_crate_content,
		));

	if is_diff {
		println!(
			"Difference found between {} and {}",
			old_crate_content, new_crate_content
		);
	}

	return is_diff;
}


fn main() {
	let cli_options = CliOptions::from_args();
	let directory_list = match (cli_options.manifest_path, cli_options.json_file) {
		(Some(_), Some(_)) => {
			panic!("Both `--manifest-path` and `--json-file` was specified");
		},
		(None, None) => {
			panic!("None of `--manifest_path` or `--json-file` was specified");
		},
		(Some(manifest_path), None) => vec![manifest_path],
		(None, Some(json_file)) => {
			let file = File::open(&json_file)
				.expect(&format!("Couldn't open provided json file:{}", json_file));
			serde_json::from_reader(file)
				.expect(&format!("Couldn't parse provided json file:{}", json_file))
		},
	};

	if directory_list.is_empty() {
		panic!("Publishing crates list is empty");
	}

	let target = &cli_options.working_dir;
	let build_target =
		cli_options.build_target.unwrap_or(format!("{}/build-target", target));

	for directory in directory_list {
		let directory = match cli_options.base_directory.as_ref() {
			None => directory,
			Some(d) => format!("{}/{}", d, directory),
		};

		let path = Path::new(&directory);


		let source_id = cargo::core::SourceId::for_directory(path.clone())
			.expect("SourceId generation error");

		let config =
			cargo::util::config::Config::default().expect("Default config loading error");

		let manifest_result =
			cargo::util::toml::read_manifest(path.clone(), source_id, &config);

		let manifest = {
			use cargo::core::manifest::EitherManifest::*;
			match manifest_result {
				Ok((Real(m), _)) => m,
				Ok((Virtual(_), _)) => {
					panic!("Virtual manifest is not supported. Yet?");
				},
				Err(e) => {
					panic!("Manifest parsing error:{}", e);
				},
			}
		};

		let name = manifest.name().as_str();
		let version = manifest.version();

		let metadata = manifest.metadata();

		let mut missing_fields = Vec::new();
		if metadata.description.is_none() {
			missing_fields.push("description");
		}

		if metadata.license.is_none() {
			missing_fields.push("license");
		}

		if metadata.authors.is_empty() {
			missing_fields.push("authors");
		}

		if !missing_fields.is_empty() {
			panic!("missed fields: {}.", missing_fields.join(", "));
		}


		std::fs::create_dir_all(&build_target).expect(&format!(
			"Downloads directory creation error. Path:{}",
			build_target
		));

		let new_crate_path = {
			println!("\n==== PACKAGING START:{} =====\n", directory);
			let mut command = Command::new("cargo");
			command
				.arg("publish")
				.arg("--dry-run")
				.arg("--no-verify")
				.arg("--manifest-path")
				.arg(&directory)
				.arg("--target-dir")
				.arg(build_target.clone());
			if cli_options.allow_dirty {
				command.arg("--allow-dirty");
			};
			let status = command.status().expect("cargo failed to publish-dry-run");

			if !status.success() {
				panic!("cargo publish-dry-run failed:{}", status);
			}

			println!("\n==== PACKAGING DONE =====\n");

			format!("{}/package/{}-{}.crate", build_target, name, version)
		};
		println!("Crate generated:{}", new_crate_path);


		let url =
			format!("https://crates.io/api/v1/crates/{}/{}/download", name, version);

		let downloads = format!("{}/downloads", target);
		std::fs::create_dir_all(&downloads)
			.expect(&format!("Couldn't create directory {}", downloads));

		let mut response =
			reqwest_get(&url).expect(&format!("Couldn't download file {}", url));

		match response.status().as_u16() {
			404 => {
				match cli_options.token.as_ref() {
					None => {
						println!("Publishing skiped since token was not been provided");
					},
					Some(token) => {
						println!("==== PUBLISHING START =====");
						let status = Command::new("cargo")
							.arg("publish")
							.arg("--token")
							.arg(&token)
							.arg("--manifest-path")
							.arg(directory.clone())
							.arg("--target-dir")
							.arg(build_target.clone())
							.status()
							.expect("cargo failed to publish");

						if !status.success() {
							println!(
								"cache invalidation is hard, let's sleep a little bit"
							);
							std::thread::sleep(std::time::Duration::from_secs(DELAY));
							let status = Command::new("cargo")
								.arg("publish")
								.arg("--token")
								.arg(&token)
								.arg("--manifest-path")
								.arg(directory)
								.arg("--target-dir")
								.arg(build_target.clone())
								.status()
								.expect("cargo failed to publish");

							if !status.success() {
								panic!("cargo publish failed:{}", status);
							}
						}

						for _ in 0..5 {
							let response = reqwest_get(&url)
								.expect(&format!("Couldn't download file {}", url));
							match response.status().as_u16() {
								200 => {
									println!("==== CHECK DONE =====");
									break;
								},
								code => {
									println!(
										"Verification response code:{}. Retrying in {} \
										 seconds",
										code, DELAY,
									);

									std::thread::sleep(std::time::Duration::from_secs(
										DELAY,
									));
								},
							}
						}

						println!("==== PUBLISHING DONE =====");
					},
				}
			},
			200 => {
				let old_crate_path = format!("{}/old.crate", downloads);

				let mut file = File::create(&old_crate_path)
					.expect(&format!("Couldn't create file {}", old_crate_path));

				response.copy_to(&mut file).expect(&format!(
					"Couldn't save downloaded file `{}` to `{}`",
					url, old_crate_path,
				));
				println!("Crate downloaded:{}", old_crate_path);

				let is_diff = is_crates_content_the_same(
					&old_crate_path,
					&new_crate_path,
					&target,
					&name,
					&version,
				);

				match is_diff {
					false => {
						println!("The content is the same so uploading not required")
					},
					true => {
						panic!(
							"The content is different for the same verion. Update \
							 version for:{}.\nold_crate_path:{}\nnew_crate_path:{}\n",
							directory, old_crate_path, new_crate_path,
						)
					},
				};
			},
			unexpected_code => {
				panic!(
					"Unexpected response from crates.io with code:{}",
					unexpected_code
				);
			},
		}
	}
}
