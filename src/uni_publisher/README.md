The utility to simplify crates publishing from CI scripts

# How it works
```
$ uni_publisher --token <CRATESIO_ACCESS_TOKEN> --manifest-path <path/to/Cargo.toml> --working-dir=</tmp/dir/>
```

* (required) <path/to/Cargo.toml> - crate's manifest file (doesn't work for workspaces. yet?)
* (required) </tmp/dir/>  - temprorary directory for working files
* (optional) CRATESIO_ACCESS_TOKEN - the token to access 
  crates.io [doc](https://doc.rust-lang.org/cargo/reference/publishing.html#before-your-first-publish)
  if the token is not provided then the utility will check everything but skip uploading

# Why not using tags?
It's common solution to use tags to trigger crates publishing. It doesn't looks
good for me since the information about crate version now duplicated: you
have one version in manifest and another in tags.

For example if you're downloaded main branch and check the version then you can't
be sure if that version was published or not. And you can't be sure if the API
from the repository will be ever published to crates.io.

Special branches may be used to reflect crate version. That approach doesn't
fit well for repositories with multiple crates.

The other solution is to run uni_uploader for every crate from the repository
for every commit. For development branches (without --token parameter) it'll just 
check that crate's version is reflecting crate's changes (bumped if crate content was changed).
For main branch (with --token parameter), uni_uploader will automatically upload a
crate to crates.io if the crate version was changed.

