use maze_uniui_components::ticker::Ticker;

use maze_core::{
	buttons::Button,
	Game,
	LocalGame,
	Maze,
	TickAction,
	User,
	UserId,
};

use maze_ws_proto::{
	protocol::Resource,
	GameSettings,
	Protocol,
};

use uniws::{
	Format,
	State,
	UniWs,
	UniWsTrait,
};

use uniui_base::prelude::*;

use std::collections::{
	HashMap,
	LinkedList,
};


pub struct WsGame {
	game: LocalGame,
	server_game: LocalGame,
	tick_slot: SlotImpl<Button>,

	ticker: Ticker,
	time_shift: u64,

	user_id: Option<UserId>,
	scheduled_actions: LinkedList<(UserId, TickAction)>,
	uniws: UniWs<Protocol>,

	signal_new_resource: Signal<Resource>,
}

impl WsGame {
	pub fn new(server: &str) -> WsGame {
		let game = LocalGame::empty();

		let uniws = UniWs::<Protocol>::connect(server, Format::Binary);

		return WsGame {
			server_game: game.clone(),
			game,
			tick_slot: SlotImpl::new(),
			ticker: Ticker::new(),
			time_shift: 20,
			user_id: None,
			scheduled_actions: LinkedList::new(),
			uniws,
			signal_new_resource: Signal::new(),
		};
	}

	pub fn signal_new_resource(&mut self) -> &mut Signal<Resource> {
		return &mut self.signal_new_resource;
	}

	pub fn tick_slot(&self) -> &dyn Slot<Button> {
		return &self.tick_slot;
	}

	fn cleanup_queues(
		&mut self,
		last_tick: u64,
	) {
		let tmp = self.scheduled_actions.split_off(0);
		for (a, t) in tmp {
			if t.tick_num >= last_tick {
				self.scheduled_actions.push_back((a, t));
			}
		}
	}

	fn process_server_side_state(
		&mut self,
		mut game: LocalGame,
	) {
		trace!("tick:next_game:ok");
		self.server_game = game.clone();

		let last_tick_num = self.ticker.last_tick_num();
		if last_tick_num < (game.tick_num() + (self.time_shift / 10)) {
			error!("my_tick:{}, game_tick:{}", last_tick_num, game.tick_num());
			self.ticker.set_last_tick_num(game.tick_num() + self.time_shift);
		} else if last_tick_num > (game.tick_num() + self.time_shift * 2) {
			error!("my_tick:{} game_tick:{}", last_tick_num, game.tick_num());
			self.ticker.set_last_tick_num(game.tick_num() + self.time_shift);
		} else {
			trace!("OK my_tick:{} game_tick:{}", last_tick_num, game.tick_num());
		}

		{
			game.tick(self.scheduled_actions.iter(), last_tick_num);
			maze_core::print_game_diff_as_error(&self.game, &game);
		}

		self.cleanup_queues(self.server_game.tick_num());
	}

	fn apply_new_settings(
		&mut self,
		_app: &mut dyn Application,
		game_settings: GameSettings,
	) {
		info!("game_serrings:{:?}", game_settings);
		self.ticker.set_tick_length_msec(game_settings.tick_length_msec);
		self.user_id = Some(game_settings.user_id);
		self.time_shift = game_settings.time_shift;

		// FIXME(#168)
		// Decide if Application::push_state_with_new_env_var
		// * needed for all platforms
		// * needed for web platform
		// * not needed at all
		//
		// app.push_state_with_new_env_var(
		// maze_ws_proto::GAME_ID_VAR,
		// &game_settings.game_id.to_string(),
		// "The Game begins right now",
		// );
	}

	fn process_resources(
		&mut self,
		res: Vec<Resource>,
	) {
		for r in res {
			self.signal_new_resource.emit(r);
		}
	}

	fn process(
		&mut self,
		protocol: Protocol,
		app: &mut dyn Application,
	) {
		trace!("on_message:protocol_from_json:{:?}", protocol);
		match protocol {
			Protocol::Next(game) => {
				self.process_server_side_state(game);
			},
			Protocol::GameSettings(game_settings) => {
				self.apply_new_settings(app, game_settings);
			},
			Protocol::TockAction(user_id, tick_action) => {
				self.scheduled_actions.push_back((user_id, tick_action));
			},
			Protocol::Resources(res) => self.process_resources(res),
			p => error!("process:unexpected_protocol:{:?}", p),
		}
	}

	fn process_incoming_data(
		&mut self,
		app: &mut dyn Application,
	) {
		while let Ok(p) = self.uniws.try_recv() {
			self.process(p, app);
		}
	}

	fn get_self_tick_action(
		&mut self,
		tick_num: u64,
	) -> Option<TickAction> {
		return self.tick_slot.data_iter().last().map(|button| {
			TickAction {
				button,
				tick_num,
			}
		});
	}

	fn send_action_to_server(
		&mut self,
		action: &TickAction,
	) -> Result<(), ()> {
		return match self.uniws.ready_state() {
			State::Open | State::Connecting => {
				let message = Protocol::TickAction(*action);
				match self.uniws.send(&message) {
					Ok(()) => {
						trace!("tick:action_send_to_server:ok");
						Ok(())
					},
					Err(e) => {
						error!("tick:action_send_to_server:error:{:?}", e);
						Err(())
					},
				}
			},
			State::Closing | State::Closed => {
				error!("socket closed");
				Err(())
			},
			State::Unknown => {
				error!("Unexpected WebSocket state");
				Err(())
			},
		};
	}
}

impl DataProcessor for WsGame {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.process_incoming_data(app);

		match self.ticker.next_tick(msec_since_app_start) {
			Some(new_tick) => {
				if let Some(action) = self.get_self_tick_action(new_tick) {
					match self.send_action_to_server(&action) {
						Ok(()) => trace!("send server ok"),
						Err(e) => error!("send server err:{:?}", e),
					}

					match self.user_id {
						Some(user_id) => {
							self.scheduled_actions.push_back((user_id, action))
						},
						None => {},
					}
				}

				let mut game = self.server_game.clone();
				game.tick(self.scheduled_actions.iter(), new_tick);
				self.game = game;
			},
			None => {},
		}
	}
}

impl Game for WsGame {
	fn maze(&self) -> &Maze {
		return self.game.maze();
	}

	fn opponents(&self) -> &HashMap<UserId, User> {
		return self.game.opponents();
	}

	fn tick_num(&self) -> u64 {
		return self.game.tick_num();
	}

	fn respawn_points(&self) -> &Vec<User> {
		return self.game.respawn_points();
	}
}
