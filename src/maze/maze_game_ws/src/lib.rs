#[macro_use]
extern crate log;

use maze_uniui_components::{
	gamepad_controller::GamepadController,
	keyboard_controller::KeyboardController,
	renderer::ImageProvider,
	touch_screen_controller::DirectionController,
	Renderer,
};

use uniui_base::{
	Color,
	UiPageContext,
};
use uniui_gamepad::Factory as GamepadFactory;
use uniui_scene::{
	FlexPosition,
	FlexSize,
	PositionRequest,
	Scene as GraphicScene,
};
use uniui_wrapper_keyboardlistener::KeyboardListener;


mod ws_game;
use ws_game::WsGame;

#[uniui_gui::u_main]
pub fn app_main(app: &mut dyn UiPageContext) {
	let url = maze_uniui_components::utils::get_ws_url(app, "tracer/");
	unitracer_ws_client::init(url);
	unitracer::service_global!(web_client);

	uniui_base::utils::init_logger(app);

	unitracer::start_trace!(init_application);
	let ws_url = maze_uniui_components::utils::get_ws_url(app, "ws/v1/");
	info!("ws url:{:?}", ws_url);

	let mut ws_game = WsGame::new(&ws_url);

	let image_provider = ImageProvider::new();
	ws_game.signal_new_resource().connect_slot(image_provider.slot_add_image());
	let image_provider = UiPageContext::add_data_processor(app, image_provider);

	let game_tick_slot = ws_game.tick_slot().proxy();

	let ws_game = UiPageContext::add_data_processor(app, ws_game);

	let renderer = Renderer::new(ws_game, image_provider);
	match renderer.viewport().upgrade() {
		Some(viewport) => {
			match viewport.write() {
				Ok(mut viewport) => {
					viewport.cell_width = 30;
					viewport.cell_height = 30;
					viewport.left_cell = 0;
					viewport.top_cell = 0;
					viewport.width_cell_count = 15;
					viewport.height_cell_count = 15;
				},
				Err(e) => error!("viewport lock failed:{:?}", e),
			}
		},
		None => error!("viewport missed"),
	};

	let mut scene = GraphicScene::new(Color {
		red: 33,
		green: 33,
		blue: 33,
	});
	scene.add_item(renderer, PositionRequest::full());

	let mut touch_direction_controller = DirectionController::new();
	touch_direction_controller.game_action_signal().connect_slot(&game_tick_slot);
	scene.add_item(touch_direction_controller, PositionRequest {
		x: FlexPosition::Min,
		y: FlexPosition::Max,
		width: FlexSize::Exact(100.0),
		height: FlexSize::Exact(100.0),
	});

	let scene_widget = scene.to_canvas_widget();

	let mut keyboard_controller = KeyboardController::new();
	keyboard_controller.signal_button_pressed().connect_slot(&game_tick_slot);

	let mut key_listener = KeyboardListener::new(scene_widget);
	key_listener.keydown_signal().connect_slot(keyboard_controller.slot_key_pressed());

	app.set_main_widget(Box::new(key_listener));

	let receiver = match GamepadFactory::receiver() {
		Ok(r) => r,
		Err(e) => {
			log::warn!("Couldn't obtain gamepad receiver. Default to empty. Err:{:?}", e);
			let (_, receiver) = std::sync::mpsc::channel();
			receiver
		},
	};

	let mut gamepad_controller = GamepadController::new(receiver);
	gamepad_controller.game_action_signal().connect_slot(&game_tick_slot);
	UiPageContext::add_data_processor(app, gamepad_controller);
	UiPageContext::add_data_processor(app, keyboard_controller);
}
