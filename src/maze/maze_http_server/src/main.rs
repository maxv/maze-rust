mod generated {
	include!(concat!(env!("OUT_DIR"), "/uni_build_generated.rs"));
}

fn main() -> Result<(), std::io::Error> {
	env_logger::init();

	async_std::task::block_on(async {
		let mut app = tide::new();
		app.with(tide::log::LogMiddleware::new());
		generated::attach(&mut app);
		app.listen("127.0.0.1:8000").await?;
		Ok(())
	})
}
