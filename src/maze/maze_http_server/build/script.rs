extern crate uniui_build;

fn main() {
	let mut build = uniui_build::WasmBuilder::for_framework(uniui_build::Framework::Tide);
	build.add_page(maze_ws_proto::links::SERVER_GAME, "maze_game_ws");
	build.add_page(maze_ws_proto::links::LOCAL_GAME, "maze_game_local");
	build.add_page(maze_ws_proto::links::ROOT, "maze_index");
	build.add_page(maze_ws_proto::links::PIXELEDITOR, "maze_pixeleditor_wasm");
	build.add_page(maze_ws_proto::links::EDITOR, "maze_editor_wasm");
	build.execute();
}
