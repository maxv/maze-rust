use log::{
	LevelFilter,
	Metadata,
	Record,
};

struct SimpleLogger;

impl log::Log for SimpleLogger {
	fn enabled(
		&self,
		_metadata: &Metadata,
	) -> bool {
		return true;
	}

	fn log(
		&self,
		record: &Record,
	) {
		println!("{} - {}", record.level(), record.args());
	}

	fn flush(&self) {
	}
}

static LOGGER: SimpleLogger = SimpleLogger;

pub fn init() {
	let _ = log::set_logger(&LOGGER).map(|()| log::set_max_level(LevelFilter::Trace));
}
