use crate::Attack;

use serde::{
	Deserialize,
	Serialize,
};

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct Weapon {
	pub strike_power: i64,
}

impl Weapon {
	pub fn default() -> Weapon {
		return Weapon {
			strike_power: 1,
		};
	}

	pub fn attack(&self) -> Attack {
		return Attack {
			strength: self.strike_power,
		};
	}
}
