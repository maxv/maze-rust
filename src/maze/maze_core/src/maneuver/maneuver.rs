use crate::{
	buttons::Button,
	Direction,
	DrawInfo,
};

use serde::{
	Deserialize,
	Serialize,
};

use std::collections::HashMap;


#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
pub struct Maneuver {
	pub draw_info: DrawInfo,
	pub duration: u64,
	pub next_direction: Option<Direction>,
	pub accepted_next_maneuver: usize,
	pub rejected_next_maneuver: usize,
	pub actions: HashMap<Button, usize>,
}
