use crate::Obstacle;

use serde::{
	Deserialize,
	Serialize,
};

use std::slice::IterMut;


#[cfg(test)]
mod test;

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct Maze {
	cells: Vec<Obstacle>,
	width: usize,
	height: usize,
}

impl Maze {
	pub fn empty() -> Maze {
		return Maze {
			cells: Vec::new(),
			width: 0,
			height: 0,
		};
	}

	pub fn new(
		width: usize,
		height: usize,
		obstacle: Obstacle,
	) -> Maze {
		let cells = vec![obstacle; width * height];
		let result = Maze {
			cells,
			width,
			height,
		};
		return result;
	}

	pub fn width(&self) -> usize {
		return self.width;
	}

	pub fn height(&self) -> usize {
		return self.height;
	}

	pub fn size(&self) -> (usize, usize) {
		return (self.width, self.height);
	}

	pub fn iter(&mut self) -> IterMut<Obstacle> {
		return self.cells.iter_mut();
	}

	pub fn for_each<T: FnMut(usize, usize, &Obstacle)>(
		&self,
		mut f: T,
	) {
		let height = self.height;
		self.cells.iter().enumerate().for_each(move |(id, o)| {
			let x = id / height;
			let y = id % height;
			f(x, y, o);
		});
	}

	pub fn get(
		&self,
		x: usize,
		y: usize,
	) -> Result<&Obstacle, ()> {
		let coordinate = self.coordinate(x, y);
		let value = &self.cells[coordinate?];
		return Ok(value);
	}

	pub fn get_mut(
		&mut self,
		x: usize,
		y: usize,
	) -> Result<&mut Obstacle, ()> {
		let coordinate = self.coordinate(x, y);
		let value = &mut self.cells[coordinate?];
		return Ok(value);
	}

	pub fn set(
		&mut self,
		x: usize,
		y: usize,
		obstacle: Obstacle,
	) -> Result<(), ()> {
		let coordinate = self.coordinate(x, y);
		self.cells[coordinate?] = obstacle;
		return Ok(());
	}

	fn coordinate(
		&self,
		x: usize,
		y: usize,
	) -> Result<usize, ()> {
		let result;
		if (x < self.width) && (y < self.height) {
			result = Ok(x * self.height + y);
		} else {
			result = Err(());
		}
		return result;
	}

	pub fn is_coordinate_correct(
		&self,
		x: usize,
		y: usize,
	) -> bool {
		return (x < self.width) && (y < self.height);
	}
}
