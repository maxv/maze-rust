use super::*;

#[test]
fn test_new() {
	let width = 2;
	let height = 3;
	let maze = Maze::new(width, height);

	assert_eq!(maze.width(), width);
	assert_eq!(maze.height(), height);

	for x in 0..width {
		for y in 0..height {
			let v = maze.get(x, y).unwrap();
			assert_eq!(*v, Obstacles::Empty);
		}
	}
}

#[test]
fn test_set_correct() {
	let width = 2;
	let height = 3;
	let mut maze = Maze::new(width, height);

	let set_x = width - 1;
	let set_y = height - 1;
	maze.set(set_x, set_y, Obstacles::Wall).unwrap();

	for x in 0..width {
		for y in 0..height {
			let v = maze.get(x, y).unwrap();

			if (x == set_x) && (y == set_y) {
				assert_eq!(*v, Obstacles::Wall);
			} else {
				assert_eq!(*v, Obstacles::Empty);
			}
		}
	}
}

#[test]
fn test_get_incorrect() {
	let maze = Maze::new(2, 2);
	let incorrect_cell = maze.get(3, 3);
	assert_eq!(incorrect_cell, Err(()));
}

#[test]
fn test_set_incorrect() {
	let width = 2;
	let height = 3;
	let mut maze = Maze::new(width, height);

	let err = maze.set(width, height, Obstacles::Wall);

	assert_eq!(err, Err(()));
}

#[test]
fn test_replace_if_true() {
	let mut maze = Maze::new(2, 2);
	maze.set(1, 1, Obstacles::Wall).unwrap();

	let result = maze.replace_if(1, 1, Obstacles::Empty, &|_| true);
	assert_eq!(result, Ok(Obstacles::Wall));
	assert_eq!(*maze.get(1, 1).unwrap(), Obstacles::Empty);
}

#[test]
fn test_replace_if_false() {
	let mut maze = Maze::new(2, 2);
	maze.set(1, 1, Obstacles::Wall).unwrap();

	let result = maze.replace_if(1, 1, Obstacles::Empty, &|_| false);
	assert_eq!(result, Err(()));
	assert_eq!(*maze.get(1, 1).unwrap(), Obstacles::Wall);
}
