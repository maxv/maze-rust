use crate::{
	buttons::Button,
	maneuver::Maneuver,
	Attack,
	DrawInfo,
	Drawable,
	Obstacle,
	TeamId,
	Weapon,
};

use serde::{
	Deserialize,
	Serialize,
};


#[cfg(test)]
mod test;

#[derive(Default, Serialize, Deserialize, PartialEq, Eq, Debug, Clone, Copy)]
pub struct NextTickAction {
	requested_position: (usize, usize),

	accepted_next_maneuver: usize,
	rejected_next_maneuver: usize,

	tick: u64,
}


#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct User {
	x: usize,
	y: usize,
	inventory: Vec<Obstacle>,

	#[serde(default = "Weapon::default")]
	weapon: Weapon,

	#[serde(default)]
	health: i64,

	#[serde(default)]
	team: TeamId,

	#[serde(default)]
	maneuvers: Vec<Maneuver>,

	#[serde(default)]
	current_maneuver_id: usize,

	#[serde(default)]
	scheduled_button_maneuver_id: Option<usize>,

	#[serde(default)]
	next_tick_action: NextTickAction,
}

impl User {
	pub fn new(
		x: usize,
		y: usize,
		health: i64,
		team: TeamId,
		maneuvers: Vec<Maneuver>,
	) -> User {
		return User {
			x,
			y,
			inventory: Vec::new(),
			weapon: Weapon {
				strike_power: 1,
			},
			health,
			team,
			maneuvers,
			current_maneuver_id: 0,
			scheduled_button_maneuver_id: None,
			next_tick_action: NextTickAction {
				requested_position: (x, y),
				accepted_next_maneuver: 0,
				rejected_next_maneuver: 0,
				tick: 0,
			},
		};
	}

	pub fn maneuvers(&self) -> &Vec<Maneuver> {
		return &self.maneuvers;
	}

	pub fn maneuvers_mut(&mut self) -> &mut Vec<Maneuver> {
		return &mut self.maneuvers;
	}

	pub fn tick_button(
		&mut self,
		button: &Button,
	) {
		self.scheduled_button_maneuver_id = self
			.maneuvers
			.get(self.current_maneuver_id)
			.and_then(|m| m.actions.get(button).cloned());
	}

	pub fn requested_position(&self) -> (usize, usize) {
		return self.next_tick_action.requested_position;
	}

	pub fn next_maneuver(
		&mut self,
		direction_accepted: bool,
		tick_num: u64,
	) {
		let time_for_action = self.next_tick_action.tick < tick_num;
		let updated = match (
			self.scheduled_button_maneuver_id.take(),
			time_for_action,
			direction_accepted,
		) {
			(Some(id), ..) => {
				self.current_maneuver_id = id;
				true
			},
			(None, true, true) => {
				self.current_maneuver_id = self.next_tick_action.accepted_next_maneuver;
				let (x, y) = self.next_tick_action.requested_position;
				self.update_coordinates(x, y);
				true
			},
			(None, true, false) => {
				self.current_maneuver_id = self.next_tick_action.rejected_next_maneuver;
				true
			},
			(None, false, _) => false,
		};

		if updated {
			self.next_tick_action = match self.maneuvers.get(self.current_maneuver_id) {
				Some(maneuver) => {
					let requested_position = maneuver
						.next_direction
						.and_then(|d| d.modify_point(self.x, self.y))
						.unwrap_or((self.x, self.y));

					NextTickAction {
						accepted_next_maneuver: maneuver.accepted_next_maneuver,
						rejected_next_maneuver: maneuver.rejected_next_maneuver,
						tick: tick_num + maneuver.duration,
						requested_position,
					}
				},
				None => {
					error!("no maneuver for next_maneuver:{}", self.current_maneuver_id);
					NextTickAction {
						accepted_next_maneuver: 0,
						rejected_next_maneuver: 0,
						tick: 0,
						requested_position: (self.x, self.y),
					}
				},
			};
		}
	}

	pub fn x(&self) -> usize {
		return self.x;
	}

	pub fn y(&self) -> usize {
		return self.y;
	}

	pub fn coord(&self) -> (usize, usize) {
		return (self.x, self.y);
	}

	pub fn update_coordinates(
		&mut self,
		x: usize,
		y: usize,
	) {
		self.x = x;
		self.y = y;
	}

	pub fn attack(&self) -> Attack {
		return self.weapon.attack();
	}

	pub fn on_attacked(
		&mut self,
		attack: &Attack,
	) {
		self.health -= attack.strength;
	}

	pub fn health(&self) -> i64 {
		return self.health;
	}

	pub fn inventory(&self) -> std::slice::Iter<Obstacle> {
		return self.inventory.iter();
	}

	pub fn put_to_inventory(
		&mut self,
		item: Obstacle,
	) {
		self.inventory.push(item);
	}

	pub fn get_from_inventory(
		&self,
		id: usize,
	) -> Option<&Obstacle> {
		return self.inventory.get(id);
	}
}

impl Drawable for User {
	fn resources(&self) -> Vec<&String> {
		return self.maneuvers.iter().map(|m| &m.draw_info.resource).collect();
	}

	fn current_draw_info(
		&self,
		_: u64,
	) -> Option<&DrawInfo> {
		return self.maneuvers.get(self.current_maneuver_id).map(|m| &m.draw_info);
	}
}
