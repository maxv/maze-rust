use super::*;
use crate::*;

#[test]
fn test_update_coordinate() {
	let mut user = User::new(0, 0);
	user.update_coordinates(1, 1);

	assert_eq!(user, User::new(1, 1));
}

#[test]
fn test_inventory_empty_by_default() {
	let user = User::new(0, 0);
	assert_eq!(user.inventory.len(), 0);
}

#[test]
fn test_put_to_inventory() {
	let mut user = User::new(0, 0);
	let key = Obstacles::Key(Key::new(Color::Red));
	user.put_to_inventory(key.clone());

	assert_eq!(user.inventory.len(), 1);
	assert_eq!(user.get_from_inventory(0).unwrap(), &key);
}
