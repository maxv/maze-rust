use super::{
	event::Event,
	Obstacle,
};

use crate::DrawInfo;

use serde::{
	Deserialize,
	Serialize,
};

pub type StateId = usize;

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct Pick {
	pub next_state: StateId,
	pub result: (Event, Option<Obstacle>),
}


#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone, Default)]
pub struct State {
	pub name: String,
	pub after_activate: StateId,
	pub after_deactivate: StateId,
	pub after_triggered: StateId,
	pub is_movable: bool,
	pub pick: Option<Pick>,
	pub all_killed: StateId,

	pub draw_infos: Vec<DrawInfo>,
	pub tick_length: u64,
	pub next: Option<(StateId, Event)>,
}
