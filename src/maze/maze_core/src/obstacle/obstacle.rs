use super::{
	Event,
	State,
};

use crate::{
	DrawInfo,
	Drawable,
};

use serde::{
	Deserialize,
	Serialize,
};


#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct Obstacle {
	states: Vec<State>,
	current_state: usize,
	change_state_tick: u64,
}

impl Obstacle {
	pub fn new_static(
		default_draw_info: DrawInfo,
		default_is_movable: bool,
	) -> Obstacle {
		let state = State {
			name: "default".to_string(),
			after_activate: 0,
			after_deactivate: 0,
			after_triggered: 0,
			is_movable: default_is_movable,
			pick: None,
			all_killed: 0,

			draw_infos: vec![default_draw_info],
			tick_length: 0,
			next: None,
		};
		return Obstacle::new(vec![state], 0, 0);
	}

	pub fn new(
		states: Vec<State>,
		current_state: usize,
		change_state_tick: u64,
	) -> Obstacle {
		return Obstacle {
			states,
			current_state,
			change_state_tick,
		};
	}

	pub fn states(&self) -> &Vec<State> {
		return &self.states;
	}

	pub fn states_mut(&mut self) -> &mut Vec<State> {
		return &mut self.states;
	}

	pub fn tick(
		&mut self,
		tick_num: u64,
	) -> Option<&Event> {
		let tick_diff = self.tick_diff(tick_num) as u64;
		return self.with_current_state(tick_num, |s| {
			let time_ready = match s.tick_length > 0 {
				true => s.tick_length <= tick_diff,
				false => false,
			};
			match (time_ready, s.next.as_ref()) {
				(true, Some((next_state, event))) => Some((*next_state, event)),
				_ => None,
			}
		});
	}

	pub fn activate(
		&mut self,
		tick_num: u64,
	) {
		self.with_current_state(tick_num, |s| Some((s.after_activate, &())));
	}

	pub fn deactivate(
		&mut self,
		tick_num: u64,
	) {
		self.with_current_state(tick_num, |s| Some((s.after_deactivate, &())));
	}

	pub fn trigger(
		&mut self,
		tick_num: u64,
	) {
		self.with_current_state(tick_num, |s| Some((s.after_triggered, &())));
	}

	pub fn is_movable(&self) -> bool {
		return self.from_current_state(|s| s.is_movable).unwrap_or(true);
	}

	pub fn is_pickable(&self) -> bool {
		return self.from_current_state(|s| s.pick.is_some()).unwrap_or(false);
	}

	pub fn pick(
		&mut self,
		tick_num: u64,
	) -> Option<&(Event, Option<Obstacle>)> {
		return self.with_current_state(tick_num, |s| {
			s.pick.as_ref().map(|p| (p.next_state, &p.result))
		});
	}

	fn from_current_state<R, F: FnMut(&State) -> R>(
		&self,
		mut f: F,
	) -> Option<R> {
		return match self.states.get(self.current_state) {
			Some(state) => Some(f(state)),
			None => None,
		};
	}

	fn with_current_state<R, F: FnMut(&State) -> Option<(usize, &R)>>(
		&mut self,
		tick_num: u64,
		mut f: F,
	) -> Option<&R> {
		return match self.states.get(self.current_state) {
			Some(state) => {
				match f(state) {
					Some((next_state, result)) => {
						if self.current_state != next_state {
							self.current_state = next_state;
							self.change_state_tick = tick_num;
						}
						Some(&result)
					},
					None => None,
				}
			},
			None => None,
		};
	}

	fn tick_diff(
		&self,
		tick_num: u64,
	) -> usize {
		return tick_num.checked_sub(self.change_state_tick).unwrap_or(0) as usize;
	}
}

impl Drawable for Obstacle {
	fn resources(&self) -> Vec<&String> {
		return Vec::new();
	}

	fn current_draw_info(
		&self,
		tick_num: u64,
	) -> Option<&DrawInfo> {
		let tick_diff = self.tick_diff(tick_num);
		return self.states.get(self.current_state).map(|s| {
			let id = tick_diff % s.draw_infos.len();
			&s.draw_infos[id]
		});
	}
}
