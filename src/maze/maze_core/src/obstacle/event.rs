use crate::{
	Attack,
	User,
};

use serde::{
	Deserialize,
	Serialize,
};

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct Event {
	pub activate: Vec<(usize, usize)>,
	pub deactivate: Vec<(usize, usize)>,
	pub born: Vec<User>,
	pub attack: Vec<(usize, usize, Attack)>,
}

impl Event {
	pub fn empty() -> Event {
		return Event {
			activate: Vec::new(),
			deactivate: Vec::new(),
			born: Vec::new(),
			attack: Vec::new(),
		};
	}
}
