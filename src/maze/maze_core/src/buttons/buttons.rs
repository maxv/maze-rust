use serde::{
	Deserialize,
	Serialize,
};

#[repr(usize)]
#[derive(Serialize, Deserialize, Hash, PartialEq, Eq, Clone, Debug, Copy)]
pub enum Button {
	Up,
	Down,
	Left,
	Right,

	A,
	B,
	X,
	Y,

	Start,
	Select,
}
