use serde::{
	Deserialize,
	Serialize,
};

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone, Default)]
pub struct DrawInfo {
	pub resource: String,
	pub left: u32,
	pub top: u32,
	pub width: u32,
	pub height: u32,

	#[serde(default)]
	pub dx: i32,

	#[serde(default)]
	pub dy: i32,
}

pub trait Drawable {
	fn resources(&self) -> Vec<&String>;
	fn current_draw_info(
		&self,
		tick_num: u64,
	) -> Option<&DrawInfo>;
}
