use crate::{
	Maze,
	User,
};

use std::collections::HashMap;


pub type UserId = i64;

pub trait Game {
	fn maze(&self) -> &Maze;
	fn opponents(&self) -> &HashMap<UserId, User>;
	fn tick_num(&self) -> u64;
	fn respawn_points(&self) -> &Vec<User>;
}

pub fn print_game_diff_as_error(
	game_left: &dyn Game,
	game_right: &dyn Game,
) {
	let left_tick = game_left.tick_num();
	let right_tick = game_right.tick_num();
	if left_tick != right_tick {
		error!("left_tick:{} != right_tick:{}", left_tick, right_tick);
	}


	let left_maze = game_left.maze();
	let right_maze = game_right.maze();
	if left_maze.size() != right_maze.size() {
		error!("left_size:{:?}, right_size:{:?}", left_maze.size(), right_maze.size());
	} else {
		for i in 0..left_maze.width() {
			for j in 0..left_maze.height() {
				let left = left_maze.get(i, j);
				let right = right_maze.get(i, j);
				if left != right {
					error!(
						"i:{}, j:{}, left_cell:{:?} != right_cell:{:?}",
						i, j, left, right
					);
				}
			}
		}
	}

	let left_ops = game_left.opponents().clone();
	let mut right_ops = game_right.opponents().clone();
	left_ops.iter().for_each(|(id, lop)| {
		match right_ops.get(id) {
			Some(rop) => {
				if *rop != *lop {
					error!("left_user:{:?} != right_user:{:?}", lop, rop);
				}
			},
			None => {
				error!("right_user not found for left id:{}, user:{:?}", id, lop);
			},
		};

		right_ops.remove(id);
	});

	right_ops.iter().for_each(|(id, op)| {
		error!("left_user not found for right id:{}, user:{:?}", id, op);
	});
}
