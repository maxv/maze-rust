use serde::{
	Deserialize,
	Serialize,
};


#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct Attack {
	pub strength: i64,
}
