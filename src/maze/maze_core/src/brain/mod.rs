use crate::{
	buttons::Button,
	Maze,
	User,
	UserId,
};

use serde::{
	Deserialize,
	Serialize,
};

use std::collections::HashMap;

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct Brain {
	me: UserId,
	move_forward: bool,
}

impl Brain {
	pub fn new(me: UserId) -> Brain {
		return Brain {
			me,
			move_forward: true,
		};
	}

	pub fn my_id(&self) -> UserId {
		return self.me;
	}

	pub fn think(
		&mut self,
		_: &Maze,
		_: &HashMap<UserId, User>,
		_: u64,
	) -> Option<Button> {
		return None;
	}
}
