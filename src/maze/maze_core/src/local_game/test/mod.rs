use crate::*;

mod door_openable;
mod pick;

#[test]
fn test_game_new() {
	let maze = Maze::new(2, 2);
	let user = User::new(1, 1);
	let game = LocalGame::new(maze.clone(), user.clone());

	assert_eq!(maze, *game.maze());
	assert_eq!(user, *game.user());
}

#[test]
fn test_move() {
	let maze = Maze::new(2, 2);
	let mut user = User::new(1, 1);
	let mut game = LocalGame::new(maze.clone(), user.clone());

	let result = game.tick(&GameActions::Move(MoveDirection::North), 1);
	assert_eq!(result, Ok(()));

	user.update_coordinates(1, 0);
	assert_eq!(game, LocalGame::new(maze, user));
}
