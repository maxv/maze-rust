use crate::*;
use test_utils::*;


#[test]
fn test_door_openable() {
	init_test_logger();

	let mut maze = Maze::new(1, 2);

	let door_instance = Door::new(Color::Red);
	maze.set(0, 1, Obstacles::Door(door_instance)).unwrap();

	let mut user = User::new(0, 0);
	let key = Key::new(Color::Red);
	user.put_to_inventory(Obstacles::Key(key));

	let mut game = LocalGame::new(maze, user);

	let mut target_state = game.clone();
	if let Obstacles::Door(door) = target_state.maze.get_mut(0, 1).unwrap() {
		door.open();
	} else {
		panic!("Door expected");
	}

	let result = game.tick(&GameActions::Interact(MoveDirection::South), 0);
	assert_eq!(result, Ok(()));
	assert_eq!(target_state, game);
}
