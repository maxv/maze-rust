use crate::*;
use test_utils::*;


#[test]
fn pick_works() {
	init_test_logger();

	let mut maze = Maze::new(1, 2);
	let key = Obstacles::Key(Key::new(Color::Red));
	maze.set(0, 0, key.clone()).unwrap();

	let user = User::new(0, 0);
	let mut game = LocalGame::new(maze, user);


	let new_maze = Maze::new(1, 2);
	let mut new_user = User::new(0, 0);
	new_user.put_to_inventory(key);
	let expected_game = LocalGame::new(new_maze, new_user);

	let result = game.tick(&GameActions::Pick, 0);
	assert_eq!(result, Ok(()));
	assert_eq!(game, expected_game);
}

#[test]
fn can_t_pick_unpickable() {
	init_test_logger();

	let maze = Maze::new(1, 2);
	let user = User::new(0, 0);
	let mut game = LocalGame::new(maze, user);
	let expected_game = game.clone();

	let result = game.tick(&GameActions::Pick, 0);
	assert_eq!(result, Err(()));
	assert_eq!(game, expected_game);
}
