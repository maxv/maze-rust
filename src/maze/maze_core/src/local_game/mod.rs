use crate::{
	brain::Brain,
	Event,
	Game,
	Maze,
	TickAction,
	User,
	UserId,
};

use serde::{
	Deserialize,
	Serialize,
};

use std::collections::{
	HashMap,
	LinkedList,
};

#[cfg(test)]
mod test;

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct LocalGame {
	maze: Maze,

	#[serde(default = "HashMap::new")]
	persons_map: HashMap<UserId, User>,

	#[serde(default)]
	last_tick_num: u64,

	#[serde(default)]
	respawn_points: Vec<User>,

	#[serde(default)]
	brain_barn: LinkedList<Brain>,

	#[serde(default)]
	next_user_id: i64,
}

impl LocalGame {
	pub fn empty() -> LocalGame {
		return LocalGame::new(Maze::empty());
	}

	pub fn new(maze: Maze) -> LocalGame {
		return LocalGame {
			maze,
			persons_map: HashMap::new(),
			last_tick_num: 0,
			respawn_points: Vec::new(),
			brain_barn: LinkedList::new(),
			next_user_id: 0,
		};
	}

	pub fn opponents_mut(&mut self) -> &mut HashMap<UserId, User> {
		return &mut self.persons_map;
	}

	pub fn respawn_points_mut(&mut self) -> &mut Vec<User> {
		return &mut self.respawn_points;
	}

	pub fn add_respawn_point(
		&mut self,
		u: User,
	) {
		self.respawn_points.push(u);
	}

	pub fn remove_user(
		&mut self,
		user_id: &UserId,
	) {
		self.persons_map.remove(user_id);
	}

	pub fn add_user(
		&mut self,
		user: User,
	) -> UserId {
		let user_id = self.next_user_id;
		self.next_user_id += 1;
		self.persons_map.insert(user_id, user);
		return user_id;
	}

	pub fn maze_mut(&mut self) -> &mut Maze {
		return &mut self.maze;
	}

	pub fn replace_maze(
		&mut self,
		maze: Maze,
	) -> Maze {
		let tmp = self.maze.clone();
		self.maze = maze;
		return tmp;
	}

	fn apply_user_action(
		&mut self,
		tick_action: &TickAction,
		user_id: &UserId,
	) {
		self.persons_map
			.get_mut(user_id)
			.iter_mut()
			.for_each(|u| u.tick_button(&tick_action.button));
	}

	fn apply_event(
		&mut self,
		event: &Event,
		tick_num: u64,
	) {
		for (x, y) in event.activate.iter() {
			match self.maze.get_mut(*x, *y) {
				Ok(o) => o.activate(tick_num),
				_ => {},
			}
		}

		for (x, y) in event.deactivate.iter() {
			match self.maze.get_mut(*x, *y) {
				Ok(o) => o.deactivate(tick_num),
				_ => {},
			}
		}

		for user in event.born.iter() {
			let id = self.add_user(user.clone());
			self.brain_barn.push_back(Brain::new(id));
		}
	}

	pub fn tick<'a, T: Iterator<Item = &'a (UserId, TickAction)>>(
		&mut self,
		actions: T,
		new_tick_num: u64,
	) {
		if self.last_tick_num > new_tick_num {
			trace!("incorrect tick_num:{}, local:{}", new_tick_num, self.last_tick_num);
		} else {
			let mut actions: Vec<&(UserId, TickAction)> = actions
				.filter(|(_, a)| {
					self.last_tick_num <= a.tick_num && a.tick_num < new_tick_num
				})
				.collect();

			actions.sort_unstable_by_key(|(_, tick_action)| tick_action.tick_num);

			let mut actions_iter = actions.iter();
			let mut user_action = actions_iter.next();
			for tick in self.last_tick_num..new_tick_num {
				while match user_action.as_ref() {
					Some((u, a)) if a.tick_num == tick => {
						self.apply_user_action(a, u);
						user_action = actions_iter.next();
						true
					},
					_ => false,
				} {}

				let mut brain_actions = Vec::new();
				for b in self.brain_barn.iter_mut() {
					match b.think(&self.maze, &self.persons_map, tick) {
						Some(button) => {
							brain_actions.push((b.my_id(), TickAction {
								button,
								tick_num: tick,
							}));
						},
						None => {},
					};
				}
				brain_actions.iter().for_each(|(id, a)| self.apply_user_action(a, id));

				for (_, user) in self.persons_map.iter_mut() {
					let position = user.requested_position();
					let position_correct =
						self.maze.is_coordinate_correct(position.0, position.1);
					user.next_maneuver(position_correct, tick);
				}

				let v: Vec<_> = self
					.maze
					.iter()
					.filter_map(|o| o.tick(tick))
					.map(|e| e.clone())
					.collect();
				v.iter().for_each(|e| self.apply_event(e, tick));
			}
			self.last_tick_num = new_tick_num;
		};
	}
}

impl Game for LocalGame {
	fn maze(&self) -> &Maze {
		return &self.maze;
	}

	fn opponents(&self) -> &HashMap<UserId, User> {
		return &self.persons_map;
	}

	fn tick_num(&self) -> u64 {
		return self.last_tick_num;
	}

	fn respawn_points(&self) -> &Vec<User> {
		return &self.respawn_points;
	}
}
