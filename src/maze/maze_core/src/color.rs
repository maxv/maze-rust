use serde::{
	Deserialize,
	Serialize,
};

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub enum Color {
	Red,
	Green,
	Blue,
}
