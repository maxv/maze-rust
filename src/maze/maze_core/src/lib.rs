#[macro_use]
extern crate log;

extern crate serde;

mod maze;
pub use maze::Maze;

mod user;
pub use user::User;

mod direction;
pub use direction::Direction;

mod game;
pub use game::{
	print_game_diff_as_error,
	Game,
	UserId,
};

mod local_game;
pub use local_game::LocalGame;

mod color;
pub use color::Color;

#[cfg(test)]
mod test_utils;

mod weapon;
pub use weapon::Weapon;

mod attack;
pub use attack::Attack;

mod tick_action;
pub use tick_action::TickAction;

mod drawable;
pub use drawable::{
	DrawInfo,
	Drawable,
};

pub type TeamId = u64;

mod obstacle;
pub use obstacle::{
	Event,
	Obstacle,
	State,
};

pub mod buttons;
pub mod maneuver;

mod brain;
