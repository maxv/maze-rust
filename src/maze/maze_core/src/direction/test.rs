use super::*;

#[test]
fn modify_point_north_correct() {
	let x = 0;
	let y = 1;
	let (d_x, d_y) = Direction::North.modify_point(x, y).unwrap();
	assert_eq!((x, y - 1), (d_x, d_y));
}

#[test]
fn modify_point_north_incorrect() {
	let x = 0;
	let y = 0;
	let res = Direction::North.modify_point(x, y);
	assert_eq!(None, res);
}

#[test]
fn modify_point_east_correct() {
	let x = 0;
	let y = 1;
	let (d_x, d_y) = Direction::East.modify_point(x, y).unwrap();
	assert_eq!((x + 1, y), (d_x, d_y));
}

#[test]
fn modify_point_south_correct() {
	let x = 0;
	let y = 1;
	let (d_x, d_y) = Direction::South.modify_point(x, y).unwrap();
	assert_eq!((x, y + 1), (d_x, d_y));
}

#[test]
fn modify_point_west_correct() {
	let x = 1;
	let y = 0;
	let (d_x, d_y) = Direction::West.modify_point(x, y).unwrap();
	assert_eq!((x - 1, y), (d_x, d_y));
}

#[test]
fn modify_point_west_incorrect() {
	let x = 0;
	let y = 0;
	let res = Direction::West.modify_point(x, y);
	assert_eq!(None, res);
}
