use serde::{
	Deserialize,
	Serialize,
};

#[cfg(test)]
mod test;

#[derive(Serialize, Deserialize, Hash, PartialEq, Eq, Clone, Debug, Copy)]
pub enum Direction {
	North,
	East,
	South,
	West,
}

impl Direction {
	pub fn modify_point(
		&self,
		x: usize,
		y: usize,
	) -> Option<(usize, usize)> {
		return match self {
			Direction::North if y > 0 => Some((x, y - 1)),
			Direction::East => Some((x + 1, y)),
			Direction::South => Some((x, y + 1)),
			Direction::West if x > 0 => Some((x - 1, y)),
			_ => None,
		};
	}
}
