use crate::buttons::Button;

use serde::{
	Deserialize,
	Serialize,
};

#[derive(Serialize, Deserialize, PartialEq, Clone, Copy, Debug)]
pub struct TickAction {
	pub button: Button,
	pub tick_num: u64,
}
