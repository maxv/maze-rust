use crate::{
	protocol::Resource,
	saved_level::SavedLevel,
};

use maze_core::LocalGame;

use std::collections::HashMap;

pub fn prepare_images(images: HashMap<String, Vec<u8>>) -> Vec<Resource> {
	let mut result = Vec::new();

	for (s, v) in images {
		let ri = png::Decoder::new(&*v).read_info();
		match ri {
			Ok((info, mut reader)) => {
				let width = info.width;
				let height = info.height;
				let mut data = Vec::new();
				data.resize((width * height * 4) as usize, 0);
				match reader.next_frame(&mut *data) {
					Ok(_) => {
						result.push(Resource(s, data, width, height));
					},
					Err(e) => log::error!("decode err:{:?}", e),
				}
			},
			Err(e) => log::error!("decode error2:{:?}", e),
		};
	}

	return result;
}

pub fn load_game(vec: &[u8]) -> Option<(LocalGame, Vec<Resource>)> {
	let saved_level = SavedLevel::from_tar(vec);
	log::info!("saved_level is_some:{}", saved_level.is_some());
	return saved_level.map(|s| (s.game, prepare_images(s.images)));
}
