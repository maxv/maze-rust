use crate::GameSettings;

use serde::{
	Deserialize,
	Serialize,
};

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct Resource(pub String, pub Vec<u8>, pub u32, pub u32);

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub enum Protocol {
	Next(maze_core::LocalGame),
	Resources(Vec<Resource>),
	IncorrectActoin,
	TickAction(maze_core::TickAction),
	TockAction(maze_core::UserId, maze_core::TickAction),
	GameSettings(GameSettings),
	Closed,
}
