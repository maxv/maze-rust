use uni_components::define_ui_page;

define_ui_page! {
	name: SERVER_GAME,
	parameter: (),
	path: "/server_game/",
}

define_ui_page! {
	name: EDITOR,
	parameter: (),
	path: "/editor/",
}

define_ui_page! {
	name: LOCAL_GAME,
	parameter: (),
	path: "/local_game/",
}

define_ui_page! {
	name: PIXELEDITOR,
	parameter: (),
	path: "/pixeleditor/",
}

define_ui_page! {
	name: ROOT,
	parameter: (),
	path: "/",
}
