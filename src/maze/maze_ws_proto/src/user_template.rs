use tar::{
	Archive as TarArchive,
	Builder as TarBuilder,
	Header as TarHeader,
};

use serde::{
	de::DeserializeOwned,
	Deserialize,
	Serialize,
};

use std::{
	io::Read,
	path::Path,
};

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
pub struct UserTemplate<T> {
	pub png: Vec<u8>,
	pub maneuvers: Vec<T>,
}

impl<T: 'static + Serialize + DeserializeOwned + Clone + Eq + PartialEq> UserTemplate<T> {
	pub fn to_tar(&self) -> Vec<u8> {
		let mut result: Vec<u8> = Vec::new();
		{
			let mut builder = TarBuilder::new(&mut result);
			{
				let mut image_header = TarHeader::new_gnu();
				image_header.set_size(self.png.len() as u64);
				image_header.set_mode(511);
				match image_header.set_path("./image.png") {
					Ok(()) => {
						match builder.append_data(
							&mut image_header,
							"./image.png",
							&*self.png,
						) {
							Ok(()) => log::trace!("png ok"),
							Err(e) => log::error!("append err:{:?}", e),
						}
					},
					Err(e) => log::error!("set_path err:{:?}", e),
				};
			}

			{
				let json = match serde_json::to_string_pretty(&self.maneuvers) {
					Ok(j) => j,
					Err(e) => {
						log::error!("conversion to json err:{:?}", e);
						String::new()
					},
				};

				let mut json_header = TarHeader::new_gnu();
				json_header.set_size(json.as_bytes().len() as u64);
				json_header.set_mode(511);
				match json_header.set_path("./maneuvers.json") {
					Ok(()) => {
						match builder.append_data(
							&mut json_header,
							"./maneuvers.json",
							&*json.as_bytes(),
						) {
							Ok(()) => log::trace!("json ok"),
							Err(e) => log::error!("append err:{:?}", e),
						}
					},
					Err(e) => log::error!("set_path err:{:?}", e),
				};
			}

			match builder.finish() {
				Ok(()) => log::trace!("builder ok"),
				Err(e) => log::error!("finish err:{:?}", e),
			}
		}
		return result;
	}

	pub fn from_tar(data: &[u8]) -> Option<UserTemplate<T>> {
		let mut archive = TarArchive::new(data);
		return match archive.entries() {
			Ok(entries) => {
				let mut png_found = false;
				let mut maneuvers_found = false;
				let mut result = UserTemplate {
					png: Vec::new(),
					maneuvers: Vec::new(),
				};

				for entry in entries {
					match entry {
						Ok(mut entry) => {
							if let Ok(path) = entry.path() {
								if path == Path::new("image.png") {
									match entry.read_to_end(&mut result.png) {
										Ok(_) => {
											log::trace!("png readed");
											png_found = true;
										},
										Err(e) => log::error!("png reading err:{:?}", e),
									};
								} else if path == Path::new("maneuvers.json") {
									let mut json = Vec::new();
									match entry.read_to_end(&mut json) {
										Ok(_) => {
											log::trace!("json readed");
											match std::str::from_utf8(&json) {
												Ok(json) => {
													match serde_json::from_str::<Vec<T>>(
														&json,
													) {
														Ok(v) => {
															maneuvers_found = true;
															result.maneuvers = v;
														},
														Err(e) => {
															log::error!(
																"json coversion err:{:?}",
																e
															)
														},
													};
												},
												Err(e) => {
													log::error!(
														"json to string err:{:?}",
														e
													)
												},
											};
										},
										Err(e) => log::error!("json reading err:{:?}", e),
									};
								}
							};
						},
						Err(e) => log::error!("entry err:{:?}", e),
					}
				}

				if png_found && maneuvers_found {
					Some(result)
				} else {
					None
				}
			},
			Err(e) => {
				log::error!("archive entries err:{:?}", e);
				None
			},
		};
	}
}
