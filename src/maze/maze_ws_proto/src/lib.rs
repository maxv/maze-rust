use serde::{
	Deserialize,
	Serialize,
};

use url::Url;

pub mod links;
pub mod saved_level;
pub mod user_template;
pub mod utils;

#[derive(Serialize, Deserialize, PartialEq, Clone, Copy, Debug)]
pub struct GameSettings {
	pub game_id: GameId,
	pub tick_length_msec: u64,
	pub user_id: maze_core::UserId,
	pub time_shift: u64,
}

pub const GAME_ID_VAR: &str = "game_id";

pub type GameId = i64;

pub fn add_game_id_to_url(
	mut url: Url,
	game_id: GameId,
) -> Url {
	url.query_pairs_mut().append_pair(GAME_ID_VAR, &game_id.to_string());
	return url;
}

pub fn game_id_from_url(url: &Url) -> Option<GameId> {
	let mut game_id = None;
	for (var_name, var_value) in url.query_pairs() {
		match var_name.as_ref() {
			GAME_ID_VAR => {
				match var_value.parse::<i64>() {
					Ok(val) => game_id = Some(val),
					Err(e) => {
						log::warn!(
							"couldn't parse as i64 game_id:{} error:{:?}",
							var_value,
							e
						)
					},
				}
				break;
			},
			_ => {
				log::trace!("unknown var:{} val:{}", var_name, var_value);
			},
		}
	}

	return game_id;
}

pub mod protocol;
pub use protocol::Protocol;
