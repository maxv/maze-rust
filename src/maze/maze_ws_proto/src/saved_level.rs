use maze_core::LocalGame;

use tar::{
	Archive as TarArchive,
	Builder as TarBuilder,
	Header as TarHeader,
};

use serde::{
	Deserialize,
	Serialize,
};

use std::{
	collections::HashMap,
	io::Read,
	path::Path,
};

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct SavedLevel {
	pub game: LocalGame,
	pub images: HashMap<String, Vec<u8>>,
}

impl SavedLevel {
	pub fn to_tar(self) -> Vec<u8> {
		let mut result: Vec<u8> = Vec::new();
		{
			let mut builder = TarBuilder::new(&mut result);
			{
				for (name, png) in self.images.iter() {
					let mut image_header = TarHeader::new_gnu();
					image_header.set_size(png.len() as u64);
					image_header.set_mode(511);
					let png: &[u8] = &png;
					match image_header.set_path(&name) {
						Ok(()) => {
							match builder.append_data(&mut image_header, &name, png) {
								Ok(()) => log::trace!("png ok"),
								Err(e) => {
									log::error!("append err:{:?} image:{}", e, name)
								},
							}
						},
						Err(e) => log::error!("set_path err:{:?}", e),
					};
				}
			}

			{
				let json = match serde_json::to_string_pretty(&self.game) {
					Ok(j) => j,
					Err(e) => {
						log::error!("conversion to json err:{:?}", e);
						String::new()
					},
				};

				let mut json_header = TarHeader::new_gnu();
				json_header.set_size(json.as_bytes().len() as u64);
				json_header.set_mode(511);
				let file_name = "./game.json";
				match json_header.set_path(file_name) {
					Ok(()) => {
						match builder.append_data(
							&mut json_header,
							file_name,
							&*json.as_bytes(),
						) {
							Ok(()) => log::trace!("json ok"),
							Err(e) => log::error!("append err:{:?}", e),
						}
					},
					Err(e) => log::error!("set_path err:{:?}", e),
				};
			}

			match builder.finish() {
				Ok(()) => log::trace!("builder ok"),
				Err(e) => log::error!("finish err:{:?}", e),
			}
		}
		return result;
	}

	pub fn from_tar(data: &[u8]) -> Option<SavedLevel> {
		let mut archive = TarArchive::new(&*data);
		return match archive.entries() {
			Ok(entries) => {
				log::info!("archive ok");
				let mut result_game = None;
				let mut images = HashMap::new();

				for entry in entries {
					match entry {
						Ok(mut entry) => {
							log::info!("entry path:{:?}", entry.path());
							match entry.path() {
								Ok(path) => {
									if path == Path::new("game.json") {
										log::info!("path game");
										match SavedLevel::game_from_entry(entry) {
											Some(g) => {
												log::info!("game decoded");
												result_game = Some(g);
											},
											None => log::error!("game decode error"),
										}
									} else {
										match std::str::from_utf8(&entry.path_bytes()) {
											Ok(name) => {
												log::info!("path {}", name);
												let name = name.to_string();
												let mut png = Vec::new();
												match entry.read_to_end(&mut png) {
													Ok(_) => {
														images.insert(name, png);
													},
													Err(e) => {
														log::error!(
															"entry read error:{:?}",
															e
														);
													},
												}
											},
											Err(e) => {
												log::error!(
													"path to string conversion err:{:?}",
													e
												);
											},
										};
									}
								},
								Err(e) => {
									log::error!("path err:{:?}", e);
								},
							};
						},
						Err(e) => log::error!("entry err:{:?}", e),
					};
				}

				match result_game {
					Some(game) => {
						Some(SavedLevel {
							game,
							images,
						})
					},
					None => {
						log::error!("result game is none");
						None
					},
				}
			},
			Err(e) => {
				log::error!("archive entries err:{:?}", e);
				None
			},
		};
	}

	fn game_from_entry<'a, R: Read>(mut entry: tar::Entry<'a, R>) -> Option<LocalGame> {
		let mut bytes = Vec::new();
		return match entry.read_to_end(&mut bytes) {
			Ok(_) => {
				match std::str::from_utf8(&bytes) {
					Ok(string) => {
						match serde_json::from_str(&string) {
							Ok(g) => Some(g),
							Err(e) => {
								log::error!("serde failed:{:?}", e);
								None
							},
						}
					},
					Err(e) => {
						log::error!("not a string:{:?}", e);
						None
					},
				}
			},
			Err(e) => {
				log::error!("game reading err:{:?}", e);
				None
			},
		};
	}
}
