#[macro_use]
extern crate log;

extern crate env_logger;
extern crate unitracer;

fn main() {
	env_logger::init();
	// unitracer_jaeger::init("127.0.0.1".to_string(), "14268".to_string());
	// unitracer::service_global!(ws_service);

	info!("main:start");
	maze_websockets::start_server("127.0.0.1:8080");
}
