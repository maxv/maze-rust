use super::Viewport;

use uniui_scene::Position;


pub fn calculate_left_top(
	viewport: &Viewport,
	pos: &Position,
) -> (f64, f64) {
	let x_diff = (pos.width - viewport.pixel_width() as f64) / 2.0;
	let y_diff = (pos.height - viewport.pixel_height() as f64) / 2.0;

	let x = pos.x + x_diff;
	let y = pos.y + y_diff;

	return (x, y);
}

pub fn calculate_cell_left_top_width_height(
	viewport: &Viewport,
	pos: &Position,
	i: usize,
	j: usize,
) -> Option<(f64, f64, f64, f64)> {
	return match viewport.inside_viewport(i as u32, j as u32) {
		false => None,
		true => {
			let (left, top) = calculate_left_top(viewport, pos);
			let cell_width = viewport.cell_width as f64;
			let cell_height = viewport.cell_height as f64;
			let x = i as f64 * cell_width;
			let y = j as f64 * cell_height;
			Some((x + left, y + top, cell_width, cell_height))
		},
	};
}
