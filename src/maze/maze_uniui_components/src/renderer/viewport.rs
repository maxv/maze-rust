#[derive(Debug, Copy, Clone)]
pub struct Viewport {
	pub width_cell_count: u32,
	pub height_cell_count: u32,
	pub left_cell: u32,
	pub top_cell: u32,
	pub cell_width: u32,
	pub cell_height: u32,
}

impl Viewport {
	pub fn inside_viewport(
		&self,
		i: u32,
		j: u32,
	) -> bool {
		let i_ok = (self.left_cell <= i) && (i < self.left_cell + self.width_cell_count);
		let j_ok = (self.top_cell <= j) && (j < self.top_cell + self.height_cell_count);
		return i_ok && j_ok;
	}

	pub fn pixel_width(&self) -> u32 {
		return self.width_cell_count * self.cell_width;
	}

	pub fn pixel_height(&self) -> u32 {
		return self.height_cell_count * self.cell_height;
	}

	pub fn coord_to_cell(
		&self,
		x: u32,
		y: u32,
	) -> Option<(usize, usize)> {
		let c_x = x.checked_sub(self.left_cell).map(|v| v / self.cell_width);
		let c_y = y.checked_sub(self.top_cell).map(|v| v / self.cell_height);
		return match (c_x, c_y) {
			(Some(x), Some(y)) => Some((x as usize, y as usize)),
			_ => None,
		};
	}
}

impl Default for Viewport {
	fn default() -> Viewport {
		return Viewport {
			width_cell_count: 0,
			height_cell_count: 0,
			left_cell: 0,
			top_cell: 0,
			cell_width: 1,
			cell_height: 1,
		};
	}
}
