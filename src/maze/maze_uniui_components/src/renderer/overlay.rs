use super::{
	ImageProvider,
	Viewport,
};

use maze_core::Game;
use uniui_scene::Position as GraphicPosition;
use uniui_widget_canvas::Context as CanvasContext;


pub trait Overlay {
	fn draw(
		&mut self,
		context: &mut dyn CanvasContext,
		pos: &GraphicPosition,
		game: &dyn Game,
		viewport: &Viewport,
		image_provider: &mut ImageProvider,
	);
}
