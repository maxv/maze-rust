use maze_ws_proto::protocol::Resource;

use uniui_base::prelude::*;

use uniui_widget_canvas::ImageData;

use std::collections::HashMap;


#[derive(uniui_gui::DataProcessor)]
pub struct ImageProvider {
	results: HashMap<String, ImageData>,

	#[public_slot]
	#[uprocess_each(on_raw_data_added)]
	slot_add_image: SlotImpl<Resource>,
}

impl ImageProvider {
	pub fn new() -> ImageProvider {
		return ImageProvider {
			results: HashMap::new(),
			slot_add_image: SlotImpl::new(),
		};
	}

	pub fn get(
		&mut self,
		s: &str,
	) -> Option<&mut ImageData> {
		return self.results.get_mut(s);
	}

	fn on_raw_data_added(
		&mut self,
		r: Resource,
	) {
		let Resource(s, d, w, h) = r;
		self.results.insert(s, ImageData::new(d, w, h));
	}
}
