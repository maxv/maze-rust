use super::{
	helpers,
	ImageProvider,
	Overlay,
	Viewport,
};

use maze_core::*;

use uniui_base::prelude::*;
use uniui_scene::{
	Item as GraphicItem,
	Position as GraphicPosition,
};
use uniui_widget_canvas::Context as CanvasContext;

use uniui_wrapper_touchlistener::Touch;

use std::{
	f64,
	sync::{
		Arc,
		RwLock,
		Weak,
	},
};


pub struct Renderer {
	game: Weak<RwLock<dyn Game>>,
	viewport: Arc<RwLock<Viewport>>,
	touch: Option<(usize, usize)>,
	signal_cell_clicked: Signal<(usize, usize)>,
	last_position: (f64, f64),
	overlays: Vec<Box<dyn Overlay>>,
	image_provider: Weak<RwLock<ImageProvider>>,
}

impl Renderer {
	pub fn new(
		game: Weak<RwLock<dyn Game>>,
		image_provider: Weak<RwLock<ImageProvider>>,
	) -> Renderer {
		return Renderer {
			game,
			viewport: Arc::new(RwLock::new(Viewport::default())),
			touch: None,
			signal_cell_clicked: Signal::new(),
			last_position: (0.0, 0.0),
			overlays: Vec::new(),
			image_provider,
		};
	}

	pub fn add_overlay<O: 'static + Overlay>(
		&mut self,
		overlay: O,
	) {
		self.overlays.push(Box::new(overlay));
	}

	pub fn viewport(&self) -> Weak<RwLock<Viewport>> {
		return Arc::downgrade(&self.viewport);
	}

	pub fn signal_cell_clicked(&mut self) -> &mut Signal<(usize, usize)> {
		return &mut self.signal_cell_clicked;
	}

	fn draw_cell(
		&mut self,
		context: &dyn CanvasContext,
		viewport: &Viewport,
		image_provider: &mut ImageProvider,
		pos: &GraphicPosition,
		i: usize,
		j: usize,
		draw_info: &DrawInfo,
	) {
		match image_provider.get(&draw_info.resource) {
			None => {
				// FIXME(#126)
			},
			Some(mut image) => {
				match helpers::calculate_cell_left_top_width_height(viewport, pos, i, j) {
					Some((x, y, ..)) => {
						context.draw_imagebitmap_part(
							&mut image,
							x + draw_info.dx as f64,
							y + draw_info.dy as f64,
							draw_info.left as f64,
							draw_info.top as f64,
							draw_info.width as f64,
							draw_info.height as f64,
						);
					},
					None => {},
				};
			},
		};
	}

	fn position_to_cell(
		&self,
		x: f64,
		y: f64,
	) -> Option<(usize, usize)> {
		let x = x - self.last_position.0;
		let y = y - self.last_position.1;
		return match (self.viewport.read(), 0.0 <= x, 0.0 <= y) {
			(Ok(viewport), true, true) => viewport.coord_to_cell(x as u32, y as u32),
			(Err(e), ..) => {
				error!("no viewport:{:?}", e);
				None
			},
			_ => None,
		};
	}
}

impl GraphicItem for Renderer {
	fn accept_start_touch(
		&mut self,
		touch: &Touch,
		_: &GraphicPosition,
	) -> bool {
		return if self.touch.is_some() {
			false
		} else {
			self.touch = self.position_to_cell(touch.x, touch.y);
			self.touch.is_some()
		};
	}

	fn end_touch(
		&mut self,
		touch: &Touch,
		_: &GraphicPosition,
	) {
		let touched_cell = self.position_to_cell(touch.x, touch.y);
		match (touched_cell, self.touch.take()) {
			(Some((x, y)), Some((x1, y1))) if (x1 == x) && (y1 == y) => {
				self.signal_cell_clicked.emit((x, y))
			},
			_ => {},
		};
	}

	fn draw(
		&mut self,
		context: &mut dyn CanvasContext,
		pos: &GraphicPosition,
	) {
		let viewport = match self.viewport.read() {
			Ok(v) => Some(v.clone()),
			Err(e) => {
				error!("no viewport:{:?}", e);
				None
			},
		};

		match (self.game.upgrade(), viewport, self.image_provider.upgrade()) {
			(Some(game), Some(viewport), Some(image_provider)) => {
				match (game.read(), image_provider.write()) {
					(Ok(game), Ok(mut image_provider)) => {
						let tick_num = game.tick_num();

						self.last_position = helpers::calculate_left_top(&viewport, &pos);

						game.maze().for_each(|i, j, o| {
							match o.current_draw_info(tick_num) {
								Some(di) => {
									self.draw_cell(
										context,
										&viewport,
										&mut image_provider,
										pos,
										i,
										j,
										di,
									)
								},
								None => {
									error!("None draw_info for obstacle:{:?}", (i, j));
								},
							}
						});

						game.opponents().iter().for_each(|(_, op)| {
							match op.current_draw_info(tick_num) {
								Some(di) => {
									let (i, j) = op.coord();
									self.draw_cell(
										context,
										&viewport,
										&mut image_provider,
										pos,
										i,
										j,
										di,
									);
								},
								None => {
									error!("None draw_info for op:{:?}", op.coord());
								},
							}
						});

						self.overlays.iter_mut().for_each(|o| {
							o.draw(context, pos, &*game, &viewport, &mut image_provider)
						});
					},
					(Err(e), _) => error!("can't obtain game e:{:?}", e),
					(_, Err(e)) => error!("can't obtain image_provider e:{:?}", e),
				}
			},
			(None, ..) => error!("game doesn't exist anymore"),
			(_, None, _) => error!("viewport trouble"),
			(_, _, None) => error!("image_provider upgrade failed"),
		};
	}
}
