use std::sync::mpsc::{
	Receiver,
	TryRecvError,
};

use uniui_base::prelude::*;
use uniui_gamepad::{
	Gamepad,
	AXES_COUNT,
	BUTTONS_COUNT,
};

use maze_core::buttons::Button;


pub struct GamepadController {
	game_action_signal: Signal<Button>,
	gamepad_provider: Receiver<Gamepad>,
	gamepad: Option<Gamepad>,
	buttons_layout: [Option<Button>; BUTTONS_COUNT],
	axes_layout: [(Option<Button>, Option<Button>); AXES_COUNT],
}

impl GamepadController {
	pub fn new(gamepad_provider: Receiver<Gamepad>) -> GamepadController {
		let mut buttons_layout = [None; BUTTONS_COUNT];
		buttons_layout[0] = Some(Button::X);
		buttons_layout[1] = Some(Button::Y);

		let mut axes_layout = [(None, None); AXES_COUNT];

		axes_layout[7] = (Some(Button::Up), Some(Button::Down));

		axes_layout[6] = (Some(Button::Left), Some(Button::Right));


		return GamepadController {
			game_action_signal: Signal::new(),
			gamepad_provider,
			gamepad: None,
			buttons_layout,
			axes_layout,
		};
	}

	pub fn game_action_signal(&mut self) -> &mut Signal<Button> {
		return &mut self.game_action_signal;
	}
}

impl DataProcessor for GamepadController {
	fn process_data(
		&mut self,
		_msec_since_app_start: i64,
		_: &mut dyn Application,
	) {
		let gamepad = match self.gamepad.as_ref() {
			Some(gamepad) => Some(gamepad),
			None => {
				match self.gamepad_provider.try_recv() {
					Ok(gamepad) => {
						self.gamepad = Some(gamepad);
						self.gamepad.as_ref()
					},
					Err(e) => {
						match e {
							TryRecvError::Empty => {},
							TryRecvError::Disconnected => {
								warn!("gamepad_provider disconnected")
							},
						};
						None
					},
				}
			},
		};

		match gamepad {
			Some(gamepad) => {
				let state = gamepad.state();

				let mut button = None;
				state.buttons.iter().zip(self.buttons_layout.iter()).for_each(
					|(button_pressed, related_action)| {
						if *button_pressed {
							button = *related_action;
						}
					},
				);

				state.axes.iter().zip(self.axes_layout.iter()).for_each(
					|(axe_strength, related_action)| {
						match (-0.5 < *axe_strength, *axe_strength < 0.5) {
							(false, _) => {
								button = related_action.0;
							},
							(true, true) => {},
							(true, false) => {
								button = related_action.1;
							},
						}
					},
				);

				match button {
					Some(button) => self.game_action_signal.emit(button),
					None => {},
				};
			},
			None => {},
		};
	}
}
