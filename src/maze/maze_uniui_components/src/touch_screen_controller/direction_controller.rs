use maze_core::buttons::Button;

use uniui_base::prelude::*;

use uniui_scene::{
	Item,
	Position,
};
use uniui_widget_canvas::Context as CanvasContext;

use uniui_wrapper_touchlistener::{
	Touch,
	TouchId,
};


pub struct DirectionController {
	touch: Option<TouchId>,
	game_action_signal: Signal<Button>,
	direction: Button,
}

impl DirectionController {
	pub fn new() -> DirectionController {
		return DirectionController {
			touch: None,
			game_action_signal: Signal::new(),
			direction: Button::Up,
		};
	}

	pub fn game_action_signal(&mut self) -> &mut Signal<Button> {
		return &mut self.game_action_signal;
	}

	fn update_direction(
		&mut self,
		x: f64,
		y: f64,
		pos: &Position,
	) {
		let (center_x, center_y) = pos.center();
		let (diff_x, diff_y) = (x - center_x, y - center_y);
		let pi_3 = std::f64::consts::PI / 3.0;
		let pi_6 = std::f64::consts::PI / 6.0;
		match diff_y != 0.0 {
			true => {
				let k = diff_x / diff_y;
				let angle = k.atan().abs();
				match (diff_y > 0.0, diff_x > 0.0, angle < pi_6, angle < pi_3) {
					(false, _, true, _) => self.direction = Button::Up,
					(false, true, false, true) => {}, // North-East
					(_, true, false, false) => self.direction = Button::Right,
					(true, true, false, true) => {}, // SouthEast
					(true, _, true, _) => self.direction = Button::Down,
					(true, false, false, true) => {}, // SouthWest
					(_, false, false, false) => self.direction = Button::Left,
					(false, false, false, true) => {}, // North-West
				};
			},
			false => {
				match diff_x > 0.0 {
					true => self.direction = Button::Right,
					false => self.direction = Button::Left,
				}
			},
		}
	}
}

impl Item for DirectionController {
	fn draw(
		&mut self,
		context: &mut dyn CanvasContext,
		position: &Position,
	) {
		let r_x = position.width / 2.0;
		let r_y = position.height / 2.0;
		let (x, y) = position.center();
		context.draw_ellipse(x, y, r_x, r_y, 0.0, 0.0, 2.0 * std::f64::consts::PI);
	}

	fn accept_start_touch(
		&mut self,
		touch: &Touch,
		position: &Position,
	) -> bool {
		return match self.touch.is_none() {
			true => {
				self.touch = Some(touch.id);
				self.update_direction(touch.x, touch.y, position);
				true
			},
			false => false,
		};
	}

	fn move_touch(
		&mut self,
		touch: &Touch,
		position: &Position,
	) {
		match self.touch == Some(touch.id) {
			true => self.update_direction(touch.x, touch.y, position),
			false => {},
		}
	}

	fn end_touch(
		&mut self,
		touch: &Touch,
		_: &Position,
	) {
		match self.touch == Some(touch.id) {
			true => self.touch = None,
			false => {},
		}
	}

	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		match self.touch.is_some() {
			true => {
				self.game_action_signal.emit(self.direction);
			},
			false => {},
		}
	}
}
