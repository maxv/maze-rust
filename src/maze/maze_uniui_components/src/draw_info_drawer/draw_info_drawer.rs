use crate::renderer::ImageProvider;

use maze_core::DrawInfo;

use uniui_widget_canvas::{
	Context,
	Drawer,
};

use uniui_gui::prelude::*;

use std::sync::{
	RwLock,
	Weak,
};

#[derive(uniui_gui::DataProcessor)]
pub struct DrawInfoDrawer {
	#[uproperty_process]
	#[uproperty_public_slot(slot_set_draw_info)]
	property_draw_info: Property<DrawInfo>,

	image_provider: Weak<RwLock<ImageProvider>>,
}

impl DrawInfoDrawer {
	pub fn new(
		image_provider: Weak<RwLock<ImageProvider>>,
		default_draw_info: DrawInfo,
	) -> DrawInfoDrawer {
		return DrawInfoDrawer {
			property_draw_info: Property::new(default_draw_info),
			image_provider,
		};
	}
}

impl Drawer for DrawInfoDrawer {
	fn draw(
		&mut self,
		context: &mut dyn Context,
		_: u32,
		_: u32,
	) {
		let draw_info = self.property_draw_info.as_ref();
		match self.image_provider.upgrade() {
			Some(lock) => {
				match lock.write() {
					Ok(mut image_provider) => {
						match image_provider.get(&draw_info.resource) {
							None => {
								// FIXME(#126)
							},
							Some(mut image) => {
								context.draw_imagebitmap_part(
									&mut image,
									0.0,
									0.0,
									draw_info.left as f64,
									draw_info.top as f64,
									draw_info.width as f64,
									draw_info.height as f64,
								);
							},
						};
					},
					Err(e) => error!("error2:{:?}", e),
				}
			},
			None => error!("weak lock upgrade error"),
		};
	}
}
