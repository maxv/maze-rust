use maze_core::buttons::Button;

use uniui_base::prelude::*;

use std::collections::HashMap;

#[derive(uniui_gui::DataProcessor)]
pub struct KeyboardController {
	#[public_slot]
	#[uprocess_last(on_keydown)]
	slot_key_pressed: SlotImpl<u32>,

	#[public_signal]
	signal_button_pressed: Signal<Button>,

	keycode_bindings: HashMap<u32, Button>,
}

impl KeyboardController {
	pub fn new() -> KeyboardController {
		let mut keycode_bindings = HashMap::new();
		keycode_bindings.insert(87, Button::Up);
		keycode_bindings.insert(38, Button::Up);

		keycode_bindings.insert(65, Button::Left);
		keycode_bindings.insert(37, Button::Left);

		keycode_bindings.insert(83, Button::Down);
		keycode_bindings.insert(40, Button::Down);

		keycode_bindings.insert(68, Button::Right);
		keycode_bindings.insert(39, Button::Right);

		keycode_bindings.insert(32, Button::X);
		keycode_bindings.insert(17, Button::Y);


		return KeyboardController {
			slot_key_pressed: SlotImpl::new(),
			signal_button_pressed: Signal::new(),
			keycode_bindings,
		};
	}

	fn on_keydown(
		&mut self,
		value: u32,
	) {
		match self.keycode_bindings.get(&value) {
			Some(button) => self.signal_button_pressed.emit(*button),
			None => {},
		}
	}
}
