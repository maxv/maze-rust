pub struct Ticker {
	last_tick_num: u64,
	tick_length_msec: u64,
	previous_processed_msec: Option<u64>,
	collected_tick_diff: u64,
}

impl Ticker {
	pub fn new() -> Ticker {
		Ticker {
			last_tick_num: 0,
			tick_length_msec: 20,
			previous_processed_msec: None,
			collected_tick_diff: 0,
		}
	}

	pub fn set_last_tick_num(
		&mut self,
		tick: u64,
	) {
		self.last_tick_num = tick;
	}

	pub fn set_tick_length_msec(
		&mut self,
		tick_length_msec: u64,
	) {
		self.tick_length_msec = tick_length_msec;
	}

	pub fn last_tick_num(&self) -> u64 {
		return self.last_tick_num;
	}

	pub fn next_tick(
		&mut self,
		msec_since_app_start: i64,
	) -> Option<u64> {
		let unsigned_msec = std::cmp::max(0, msec_since_app_start) as u64;
		match self.previous_processed_msec {
			Some(msec) => {
				self.collected_tick_diff += unsigned_msec.checked_sub(msec).unwrap_or(0);
			},
			None => {
				self.collected_tick_diff = self.tick_length_msec;
			},
		};
		self.previous_processed_msec = Some(unsigned_msec);

		return match self.collected_tick_diff >= self.tick_length_msec {
			false => None,
			true => {
				let tick_count = self.collected_tick_diff / self.tick_length_msec;
				self.collected_tick_diff %= self.tick_length_msec;

				self.last_tick_num += tick_count;
				Some(self.last_tick_num)
			},
		};
	}
}
