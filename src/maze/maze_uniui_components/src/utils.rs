pub fn get_ws_url(
	app: &dyn uniui_base::Application,
	path: &str,
) -> String {
	let game_id = app.get_env_var(maze_ws_proto::GAME_ID_VAR);

	let host = app.get_host();
	let base_url = format!("ws://{}/{}", host, path);
	let mut url = url::Url::parse(&base_url).unwrap();
	match game_id {
		None => warn!("no game_id"),
		Some(game_id) => {
			info!("game_id:{}", game_id);
			url.query_pairs_mut().append_pair(maze_ws_proto::GAME_ID_VAR, &game_id);
		},
	};
	return url.to_string();
}
