use crate::{
	reticle::Reticle,
	Frame,
};

use uniui_base::{
	prelude::*,
	Color,
	SizeUnit,
};
use uniui_scene::*;
use uniui_wrapper_fixedsize::prelude::*;
use uniui_wrapper_mouseclicklistener::prelude::*;

use std::sync::{
	Arc,
	RwLock,
};

#[derive(uniui_gui::Widget)]
pub struct DataWidget {
	#[base_widget]
	inner: MouseClickListener<LinearLayout>,

	signal_frame_id_updated: Signal<usize>,
	current_frame_id: usize,
	sender_frame_selected: SlotProxy<usize>,

	#[uprocess_last(on_self_click)]
	receiver_self_click: SlotImpl<(i32, i32)>,
	signal_frame_id_text_updated: Signal<String>,
}

impl DataWidget {
	pub fn new(
		width: SizeUnit,
		height: SizeUnit,
		sender_frame_selected: SlotProxy<usize>,
		frames: Arc<RwLock<Vec<Frame>>>,
	) -> DataWidget {
		let mut signal_frame_id_updated = Signal::new();
		let mut signal_frame_id_text_updated = Signal::new();

		let mut layout = LinearLayout::new(Orientation::Horizontal);
		layout.push_widget({
			let label = Label::new("".to_string());
			signal_frame_id_text_updated.connect_slot(label.slot_set_text());
			label
		});
		layout.push_widget({
			let reticle = Reticle::new(frames, false);
			signal_frame_id_updated.connect_slot(reticle.slot_set_frame_id());

			let mut scene = Scene::new(Color {
				red: 33,
				green: 33,
				blue: 33,
			});
			scene.add_item(reticle, PositionRequest::full());

			u_fixedsize! {
				widget: scene.to_canvas_widget(),
				width_strict: width,
				height_strict: height,
			}
		});

		let mut inner = MouseClickListener::new(layout);

		let receiver_self_click = SlotImpl::new();
		inner.signal_clicked().connect_slot(&receiver_self_click);

		return DataWidget {
			inner,
			signal_frame_id_updated,
			current_frame_id: 0,
			sender_frame_selected,
			receiver_self_click,
			signal_frame_id_text_updated,
		};
	}

	pub fn fill(
		&mut self,
		t: &usize,
	) {
		self.current_frame_id = *t;
		self.signal_frame_id_updated.emit(*t);
		self.signal_frame_id_text_updated.emit(t.to_string());
	}

	fn on_self_click(
		&mut self,
		_: (i32, i32),
	) {
		self.sender_frame_selected.exec_for(self.current_frame_id);
	}
}
