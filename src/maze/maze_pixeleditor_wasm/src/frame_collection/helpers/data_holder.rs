use super::data_widget::DataWidget;

use crate::Frame;

use uniui_base::{
	prelude::*,
	SizeUnit,
};
use uniui_layout_recyclerlayout::{
	prelude::*,
	DataHolder as DataHolderBase,
};

use std::sync::{
	Arc,
	RwLock,
};

#[derive(uniui_gui::DataProcessor)]
pub struct DataHolder {
	frames: Arc<RwLock<Vec<Frame>>>,
	feedback: SlotProxy<DataHolderUpdate>,

	#[public_slot]
	#[uprocess_each(add_new_frame)]
	slot_add_new_frame: SlotImpl<()>,

	#[public_slot]
	#[uprocess_last(reload_collection)]
	slot_collection_reloaded: SlotImpl<()>,

	width: SizeUnit,
	height: SizeUnit,
	slot_frame_selected: SlotProxy<usize>,
}

impl DataHolder {
	pub fn new(
		frames: Arc<RwLock<Vec<Frame>>>,
		width: SizeUnit,
		height: SizeUnit,
		slot_frame_selected: SlotProxy<usize>,
	) -> DataHolder {
		return DataHolder {
			frames,
			feedback: SlotProxy::empty(),
			slot_add_new_frame: SlotImpl::new(),
			slot_collection_reloaded: SlotImpl::new(),
			width,
			height,
			slot_frame_selected,
		};
	}

	fn add_new_frame(
		&mut self,
		_: (),
	) {
		match self.frames.write() {
			Ok(mut v) => {
				v.push(Frame::new());
				self.feedback.exec_for(DataHolderUpdate::WholeData);
			},
			Err(_) => {},
		};
	}

	fn reload_collection(
		&mut self,
		_: (),
	) {
		self.feedback.exec_for(DataHolderUpdate::WholeData);
	}
}

impl DataHolderBase<DataWidget> for DataHolder {
	fn count(&self) -> usize {
		return match self.frames.read() {
			Ok(v) => v.len(),
			Err(_) => 0,
		};
	}

	fn setup_update_slot(
		&mut self,
		slot: &dyn Slot<DataHolderUpdate>,
	) {
		self.feedback = slot.proxy();
	}

	fn fill(
		&self,
		id: usize,
		dw: &mut DataWidget,
	) {
		dw.fill(&id);
	}

	fn new_widget(&mut self) -> DataWidget {
		return DataWidget::new(
			self.width,
			self.height,
			self.slot_frame_selected.clone(),
			self.frames.clone(),
		);
	}
}
