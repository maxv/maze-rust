use crate::Frame;

use std::sync::{
	Arc,
	RwLock,
};

pub struct CollectionReader {
	frames: Arc<RwLock<Vec<Frame>>>,
}

impl CollectionReader {
	pub fn new(frames: Arc<RwLock<Vec<Frame>>>) -> CollectionReader {
		return CollectionReader {
			frames,
		};
	}

	pub fn get(
		&mut self,
		i: usize,
	) -> Option<Frame> {
		return match self.frames.read() {
			Ok(v) => v.get(i).map(|f| f.clone()),
			Err(_) => None,
		};
	}

	pub fn len(&self) -> usize {
		return match self.frames.read() {
			Ok(v) => v.len(),
			Err(_) => 0,
		};
	}
}
