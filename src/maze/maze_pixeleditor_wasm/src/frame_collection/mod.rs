use crate::Frame;

use uniui_base::{
	prelude::*,
	SizeUnit,
};
use uniui_layout_recyclerlayout::prelude::*;

use std::sync::{
	Arc,
	RwLock,
};

mod helpers {
	pub mod data_holder;
	pub mod data_widget;
}

use self::helpers::{
	data_holder::DataHolder,
	data_widget::DataWidget,
};

mod collection_reader;
pub use self::collection_reader::CollectionReader;


#[derive(uniui_gui::Widget)]
pub struct FrameCollection {
	#[base_widget]
	layout: RecyclerLayout<DataWidget, DataHolder>,

	#[uprocess_last(signal_frame_selected.emit)]
	slot_frame_selected: SlotImpl<usize>,

	#[public_signal]
	signal_frame_selected: Signal<usize>,

	data: Arc<RwLock<Vec<Frame>>>,
}

impl FrameCollection {
	pub fn new(
		width: SizeUnit,
		height: SizeUnit,
		frames: Arc<RwLock<Vec<Frame>>>,
	) -> FrameCollection {
		let slot_frame_selected = SlotImpl::new();
		let data_holder =
			DataHolder::new(frames.clone(), width, height, slot_frame_selected.proxy());

		return FrameCollection {
			layout: RecyclerLayout::new(data_holder, Orientation::Vertical),
			slot_frame_selected,
			signal_frame_selected: Signal::new(),
			data: frames,
		};
	}

	pub fn slot_add_new_frame(&mut self) -> &dyn Slot<()> {
		return self.layout.data_holder_mut().slot_add_new_frame();
	}

	pub fn slot_collection_reloaded(&mut self) -> &dyn Slot<()> {
		return self.layout.data_holder_mut().slot_collection_reloaded();
	}

	pub fn collection_reader(&self) -> CollectionReader {
		return CollectionReader::new(self.data.clone());
	}
}
