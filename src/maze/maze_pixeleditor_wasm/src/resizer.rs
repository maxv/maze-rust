use uniui_base::prelude::*;
use uniui_widget_groupbox::prelude::*;

#[derive(uniui_gui::Widget)]
pub struct Resizer {
	#[base_widget]
	layout: GroupBox<LinearLayout>,

	#[public_signal]
	signal_resize: Signal<(usize, usize)>,

	#[uprocess_last(update_width)]
	slot_width_changed: SlotImpl<String>,

	#[uprocess_last(update_height)]
	slot_height_changed: SlotImpl<String>,

	#[uprocess_last(send_resize)]
	slot_button_clicked: SlotImpl<()>,

	width: usize,
	height: usize,
}

impl Resizer {
	pub fn new() -> Resizer {
		let mut layout = LinearLayout::new(Orientation::Vertical);

		let mut slot_width_changed = SlotImpl::new();
		let mut width = TextEdit::new("width".to_string());
		width.signal_text_changed().connect_slot(&mut slot_width_changed);
		layout.push_widget(MinimalSize {
			widget: width,
		});

		let mut slot_height_changed = SlotImpl::new();
		let mut height = TextEdit::new("height".to_string());
		height.signal_text_changed().connect_slot(&mut slot_height_changed);
		layout.push_widget(MinimalSize {
			widget: height,
		});

		let mut slot_button_clicked = SlotImpl::new();
		let mut button = Button::new("R".to_string());
		button.signal_clicked().connect_slot(&mut slot_button_clicked);
		layout.push_widget(MinimalSize {
			widget: button,
		});

		return Resizer {
			layout: GroupBox::new("Resize".to_string(), layout),
			signal_resize: Signal::new(),
			slot_height_changed,
			slot_width_changed,
			slot_button_clicked,
			width: 1,
			height: 1,
		};
	}

	fn update_width(
		&mut self,
		s: String,
	) {
		match s.parse::<usize>() {
			Ok(w) => self.width = w,
			Err(_) => {},
		}
	}

	fn update_height(
		&mut self,
		s: String,
	) {
		match s.parse::<usize>() {
			Ok(h) => self.height = h,
			Err(_) => {},
		}
	}

	fn send_resize(
		&mut self,
		_: (),
	) {
		self.signal_resize.emit((self.width, self.height));
	}
}
