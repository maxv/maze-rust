use uniui_base::prelude::*;

#[derive(uniui_gui::DataProcessor)]
pub struct FrameIdsExtractor {
	#[public_slot]
	#[uprocess_last(process_text)]
	slot_text_changed: SlotImpl<String>,

	#[public_signal]
	signal_frame_ids: Signal<Vec<usize>>,
}

impl FrameIdsExtractor {
	pub fn new() -> FrameIdsExtractor {
		return FrameIdsExtractor {
			slot_text_changed: SlotImpl::new(),
			signal_frame_ids: Signal::new(),
		};
	}

	fn process_text(
		&mut self,
		mut s: String,
	) {
		let mut result = Vec::new();

		s.retain(|c| !c.is_whitespace());
		s.split(',').for_each(|s| {
			match s.find('*') {
				Some(n) => {
					let id = &s[..n].parse::<usize>();
					let count = &s[n + 1..].parse::<usize>();
					match (id, count) {
						(Ok(id), Ok(count)) => {
							for _ in 0..*count {
								result.push(*id);
							}
						},
						_ => {},
					};
				},
				None => {
					match s.parse::<usize>() {
						Ok(id) => result.push(id),
						Err(_) => {},
					}
				},
			};
		});

		self.signal_frame_ids.emit(result);
	}
}
