use std::sync::{
	Arc,
	RwLock,
};

use uniui_base::{
	prelude::*,
	Color,
};
use uniui_scene::{
	Item as GraphicItem,
	Position as GraphicPosition,
};
use uniui_widget_canvas::Context as CanvasContext;

use uniui_wrapper_touchlistener::{
	Touch,
	TouchCoord,
};

use crate::Frame;


pub struct Reticle {
	slot_resize: SlotImpl<(usize, usize)>,
	slot_current_color: SlotImpl<Option<Color>>,
	slot_clear: SlotImpl<()>,
	current_color: Option<Color>,
	grid_color: Color,
	background_color: Color,
	slot_background_color: SlotImpl<Color>,
	drawable: bool,
	frame_height: f64,
	frame_width: f64,
	frame_top: f64,
	frame_left: f64,
	frames: Arc<RwLock<Vec<Frame>>>,
	current_frame_id: Property<usize>,
}

impl Reticle {
	pub fn new(
		frames: Arc<RwLock<Vec<Frame>>>,
		drawable: bool,
	) -> Reticle {
		return Reticle {
			slot_resize: SlotImpl::new(),
			slot_current_color: SlotImpl::new(),
			current_color: None,
			slot_clear: SlotImpl::new(),
			grid_color: Color {
				red: 100,
				green: 100,
				blue: 100,
			},
			background_color: Color {
				red: 170,
				green: 140,
				blue: 90,
			},
			slot_background_color: SlotImpl::new(),
			drawable,
			frame_height: 0.0,
			frame_width: 0.0,
			frame_top: 0.0,
			frame_left: 0.0,
			frames,
			current_frame_id: Property::new(0),
		};
	}

	pub fn slot_set_frame_id(&self) -> &dyn Slot<usize> {
		return self.current_frame_id.slot();
	}

	pub fn slot_current_color(&mut self) -> &mut dyn Slot<Option<Color>> {
		return &mut self.slot_current_color;
	}

	pub fn slot_clear(&mut self) -> &mut dyn Slot<()> {
		return &mut self.slot_clear;
	}

	pub fn slot_resize(&mut self) -> &mut dyn Slot<(usize, usize)> {
		return &mut self.slot_resize;
	}

	fn resize(
		&mut self,
		width: usize,
		height: usize,
	) {
		let vec_size = width * height;
		self.for_frame(&mut |f| {
			f.pixels.resize(vec_size, None);
			f.width = width;
			f.height = height;
		});
	}

	fn for_frame(
		&mut self,
		func: &mut dyn FnMut(&mut Frame),
	) {
		match self.frames.write() {
			Ok(mut frames) => {
				match frames.get_mut(*self.current_frame_id.as_ref()) {
					Some(mut f) => func(&mut f),
					None => info!("frame for id not found"),
				}
			},
			Err(e) => error!("couldn't lock collection e:{:?}", e),
		};
	}

	fn width(&mut self) -> usize {
		let mut result = 1;
		self.for_frame(&mut |f| result = f.width);
		return result;
	}

	fn height(&mut self) -> usize {
		let mut result = 1;
		self.for_frame(&mut |f| result = f.height);
		return result;
	}

	fn calculate_cell_size(
		&mut self,
		position: &GraphicPosition,
	) -> (f64, f64) {
		let sw = self.width();
		let sh = self.height();
		return if (0 < sw) && (0 < sh) {
			let width = position.width as usize / sw;
			let height = position.height as usize / sh;
			if width > height {
				(height as f64, height as f64)
			} else {
				(width as f64, width as f64)
			}
		} else {
			(0.0, 0.0)
		};
	}

	fn position_to_cell(
		&mut self,
		position: &GraphicPosition,
		x: TouchCoord,
		y: TouchCoord,
	) -> (usize, usize) {
		let (w, h) = self.calculate_cell_size(position);
		return if (w > 0.0) && (h > 0.0) {
			((x / w) as usize, (y / h) as usize)
		} else {
			(0, 0)
		};
	}

	fn put_color_for_touch(
		&mut self,
		touch: &Touch,
		position: &GraphicPosition,
	) {
		if self.drawable {
			let (i, j) = self.position_to_cell(
				position,
				touch.x - self.frame_left,
				touch.y - self.frame_top,
			);
			let width = self.width();
			let height = self.height();
			if (i < width) && (j < height) {
				let id = j * self.width() + i;
				let color = self.current_color.clone();
				self.for_frame(&mut |f| f.pixels[id] = color);
			}
		}
	}
}

impl GraphicItem for Reticle {
	fn draw(
		&mut self,
		context: &mut dyn CanvasContext,
		pos: &GraphicPosition,
	) {
		context.draw_rect(&self.background_color, pos.x, pos.y, pos.width, pos.height);


		let (cell_width, cell_height) = self.calculate_cell_size(pos);

		let height = self.height();
		let width = self.width();

		let (mut frame_left, mut frame_top, mut frame_width, mut frame_height) =
			(0.0, 0.0, 0.0, 0.0);
		self.for_frame(&mut |f| {
			let (fl, ft, fw, fh) = f.draw_centered(
				pos.x,
				pos.y,
				pos.width,
				pos.height,
				cell_width,
				cell_height,
				false,
				context,
			);
			frame_left = fl;
			frame_top = ft;
			frame_width = fw;
			frame_height = fh;
		});

		self.frame_left = frame_left;
		self.frame_top = frame_top;
		self.frame_width = frame_width;
		self.frame_height = frame_height;

		for i in 0..=width {
			let left = i as f64 * cell_width + self.frame_left - (i / width) as f64;
			context.draw_rect(
				&self.grid_color,
				left,
				self.frame_top,
				1.0,
				self.frame_height,
			);
		}

		for j in 0..=height {
			let top = j as f64 * cell_height + self.frame_top - (j / height) as f64;
			context.draw_rect(
				&self.grid_color,
				self.frame_left,
				top,
				self.frame_width,
				1.0,
			);
		}
	}

	fn accept_start_touch(
		&mut self,
		touch: &Touch,
		position: &GraphicPosition,
	) -> bool {
		self.put_color_for_touch(touch, position);
		return true;
	}

	fn move_touch(
		&mut self,
		touch: &Touch,
		position: &GraphicPosition,
	) {
		self.put_color_for_touch(touch, position);
	}

	fn end_touch(
		&mut self,
		_: &Touch,
		_: &GraphicPosition,
	) {
	}

	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		if let Some(c) = self.slot_background_color.data_iter().last() {
			self.background_color = c;
		}

		if let Some((w, h)) = self.slot_resize.data_iter().last() {
			self.resize(w, h);
		}

		if let Some(c) = self.slot_current_color.data_iter().last() {
			self.current_color = c;
		}

		if let Some(()) = self.slot_clear.data_iter().last() {
			self.for_frame(&mut |f| f.clear());
		}

		self.current_frame_id.try_update();
	}
}
