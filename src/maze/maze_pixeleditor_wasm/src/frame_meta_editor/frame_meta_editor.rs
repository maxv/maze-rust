use crate::{
	png_creator,
	Frame,
};

use maze_core::Direction as MoveDirection;

use maze_ws_proto::user_template::UserTemplate;

use uniui_base::prelude::*;
use uniui_file::File;
use uniui_widget_combobox::prelude::*;
use uniui_widget_fileselector::FileSelector;
use uniui_widget_groupbox::prelude::*;

use std::sync::{
	Arc,
	RwLock,
};

#[derive(uniui_gui::Widget)]
pub struct FrameMetaEditor {
	#[base_widget]
	layout: LinearLayout,

	frames: Arc<RwLock<Vec<Frame>>>,

	#[uproperty_process(on_new_frame)]
	#[uproperty_public_slot(slot_set_frame)]
	current_frame: Property<usize>,

	#[uprocess_last(on_dx_updated)]
	slot_dx_updated: SlotImpl<String>,
	signal_set_dx: Signal<String>,

	#[uprocess_last(on_dy_updated)]
	slot_dy_updated: SlotImpl<String>,
	signal_set_dy: Signal<String>,

	#[uprocess_last(on_duration_updated)]
	slot_duration_updated: SlotImpl<String>,
	signal_set_duration: Signal<String>,

	#[uprocess_last(on_direction_updated)]
	slot_direction_updated: SlotImpl<Option<MoveDirection>>,
	signal_set_direction: Signal<Option<MoveDirection>>,

	#[uprocess_last(on_next_accepted_updated)]
	slot_next_accepted_updated: SlotImpl<String>,
	signal_set_next_accepted: Signal<String>,

	#[uprocess_last(on_next_rejected_updated)]
	slot_next_rejected_updated: SlotImpl<String>,
	signal_set_next_rejected: Signal<String>,

	#[uprocess_last(on_save_collection)]
	slot_save_collection: SlotImpl<()>,

	#[uprocess_last(on_load_archive)]
	slot_load_archive: SlotImpl<Vec<File>>,

	#[uprocess_last(on_archive_readed)]
	slot_archive_readed: SlotImpl<Result<Vec<u8>, ()>>,

	#[public_signal]
	signal_frames_loaded: Signal<Vec<Frame>>,
}

impl FrameMetaEditor {
	pub fn new(frames: Arc<RwLock<Vec<Frame>>>) -> FrameMetaEditor {
		let slot_dx_updated = SlotImpl::new();
		let slot_dy_updated = SlotImpl::new();
		let mut signal_set_dx = Signal::new();
		let mut signal_set_dy = Signal::new();

		let mut layout = LinearLayout::new(Orientation::Vertical);
		layout.push_widget({
			let mut l = LinearLayout::new(Orientation::Vertical);

			let mut dx = TextEdit::new("dx".to_string());
			dx.signal_text_changed().connect_slot(&slot_dx_updated);
			signal_set_dx.connect_slot(dx.slot_set_text());
			l.push_widget(dx);

			let mut dy = TextEdit::new("dy".to_string());
			dy.signal_text_changed().connect_slot(&slot_dy_updated);
			signal_set_dy.connect_slot(dy.slot_set_text());
			l.push_widget(dy);

			GroupBox::new("displacement".to_string(), l)
		});

		let slot_duration_updated = SlotImpl::new();
		let mut signal_set_duration = Signal::new();
		layout.push_widget({
			let mut duration = TextEdit::new("duration".to_string());
			signal_set_duration.connect_slot(duration.slot_set_text());
			duration.signal_text_changed().connect_slot(&slot_duration_updated);

			GroupBox::new("duration".to_string(), duration)
		});

		let slot_direction_updated = SlotImpl::new();
		let mut signal_set_direction = Signal::new();
		let slot_next_accepted_updated = SlotImpl::new();
		let mut signal_set_next_accepted = Signal::new();
		let slot_next_rejected_updated = SlotImpl::new();
		let mut signal_set_next_rejected = Signal::new();
		layout.push_widget({
			let mut l = LinearLayout::new(Orientation::Vertical);

			l.push_widget({
				let mut direction_combo = ComboBox::new();
				direction_combo.add_item(None, "None".to_string());
				direction_combo.add_item(Some(MoveDirection::North), "North".to_string());
				direction_combo.add_item(Some(MoveDirection::East), "East".to_string());
				direction_combo.add_item(Some(MoveDirection::South), "South".to_string());
				direction_combo.add_item(Some(MoveDirection::West), "West".to_string());

				signal_set_direction
					.connect_slot(direction_combo.slot_set_selected_value());
				direction_combo
					.signal_selection_changed()
					.connect_slot(&slot_direction_updated);

				MinimalSize {
					widget: direction_combo,
				}
			});

			l.push_widget({
				let mut next_accepted = TextEdit::new("accepted".to_string());
				signal_set_next_accepted.connect_slot(next_accepted.slot_set_text());
				next_accepted
					.signal_text_changed()
					.connect_slot(&slot_next_accepted_updated);
				next_accepted
			});

			l.push_widget({
				let mut next_rejected = TextEdit::new("rejected".to_string());
				signal_set_next_rejected.connect_slot(next_rejected.slot_set_text());
				next_rejected
					.signal_text_changed()
					.connect_slot(&slot_next_rejected_updated);
				next_rejected
			});

			GroupBox::new("next".to_string(), l)
		});

		let slot_save_collection = SlotImpl::new();
		layout.push_widget({
			let mut button = Button::new("export".to_string());
			button.signal_clicked().connect_slot(&slot_save_collection);
			button
		});

		let slot_load_archive = SlotImpl::new();
		layout.push_widget({
			let mut files_selector = FileSelector::new();
			files_selector.signal_files_selected().connect_slot(&slot_load_archive);
			MinimalSize {
				widget: files_selector,
			}
		});

		return FrameMetaEditor {
			frames,
			current_frame: Property::new(0),
			layout,

			slot_dx_updated,
			signal_set_dx,

			slot_dy_updated,
			signal_set_dy,

			slot_duration_updated,
			signal_set_duration,

			slot_direction_updated,
			signal_set_direction,

			slot_next_accepted_updated,
			signal_set_next_accepted,

			slot_next_rejected_updated,
			signal_set_next_rejected,

			slot_save_collection,
			slot_load_archive,
			slot_archive_readed: SlotImpl::new(),
			signal_frames_loaded: Signal::new(),
		};
	}

	fn on_new_frame(&mut self) {
		self.slot_dx_updated.discard_pending();
		self.slot_dy_updated.discard_pending();
		self.slot_direction_updated.discard_pending();
		self.slot_duration_updated.discard_pending();
		self.slot_next_accepted_updated.discard_pending();
		self.slot_next_rejected_updated.discard_pending();
		match self.frames.read() {
			Ok(frames) => {
				match frames.get(*self.current_frame.as_ref()) {
					Some(f) => {
						self.signal_set_dx.emit(f.dx.to_string());
						self.signal_set_dy.emit(f.dy.to_string());
						self.signal_set_direction.emit(f.next_direction);
						self.signal_set_duration.emit(f.duration.to_string());
						self.signal_set_next_accepted.emit(f.next_accepted.to_string());
						self.signal_set_next_rejected.emit(f.next_rejected.to_string());
					},
					None => info!("id not found"),
				};
			},
			Err(e) => error!("couldn't lock frames e:{:?}", e),
		};
	}

	fn for_current_frame(
		&mut self,
		func: &dyn Fn(&mut Frame),
	) {
		match self.frames.write() {
			Ok(mut frames) => {
				match frames.get_mut(*self.current_frame.as_ref()) {
					Some(f) => {
						func(f);
					},
					None => error!("current frame is None"),
				};
			},
			Err(e) => error!("frames write err:{:?}", e),
		};
	}

	fn on_dx_updated(
		&mut self,
		dx: String,
	) {
		match dx.parse::<i32>() {
			Ok(dx) => self.for_current_frame(&|f| f.dx = dx),
			Err(_) => {},
		};
	}

	fn on_dy_updated(
		&mut self,
		dy: String,
	) {
		match dy.parse::<i32>() {
			Ok(dy) => self.for_current_frame(&|f| f.dy = dy),
			Err(_) => {},
		};
	}

	fn on_duration_updated(
		&mut self,
		duration: String,
	) {
		match duration.parse::<u64>() {
			Ok(duration) => self.for_current_frame(&|f| f.duration = duration),
			Err(_) => {},
		};
	}

	fn on_direction_updated(
		&mut self,
		d: Option<MoveDirection>,
	) {
		self.for_current_frame(&|f| f.next_direction = d);
	}

	fn on_next_accepted_updated(
		&mut self,
		n: String,
	) {
		match n.parse::<usize>() {
			Ok(n) => self.for_current_frame(&|f| f.next_accepted = n),
			Err(_) => {},
		};
	}

	fn on_next_rejected_updated(
		&mut self,
		n: String,
	) {
		match n.parse::<usize>() {
			Ok(n) => self.for_current_frame(&|f| f.next_accepted = n),
			Err(_) => {},
		};
	}

	fn on_save_collection(
		&mut self,
		_: (),
	) {
		match self.frames.read() {
			Ok(frames) => {
				match png_creator::collection_to_vec(
					&frames,
					&mut png_creator::new_maneuver_from_frame,
				) {
					Some(v) => {
						let data = UserTemplate {
							png: v.0,
							maneuvers: v.1,
						}
						.to_tar();
						uniui_gui::utils::save_file(&data, "user_template.tar");
					},
					None => error!("frames->vec error"),
				};
			},
			Err(e) => error!("frame lock failed e:{:?}", e),
		}
	}

	fn on_load_archive(
		&mut self,
		v: Vec<File>,
	) {
		match v.get(0) {
			Some(file) => {
				file.read_all(self.slot_archive_readed.proxy());
			},
			None => error!("empty file list"),
		}
	}

	fn on_archive_readed(
		&mut self,
		r: Result<Vec<u8>, ()>,
	) {
		match r {
			Ok(v) => {
				match UserTemplate::from_tar(&v) {
					Some(ut) => {
						let f = crate::png_creator::to_vec(&ut.png, &ut.maneuvers);
						self.signal_frames_loaded.emit(f);
					},
					None => error!("untar failed"),
				};
			},
			Err(()) => error!("archive reading error"),
		}
	}
}
