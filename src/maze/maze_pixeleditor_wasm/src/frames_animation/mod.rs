use std::sync::{
	Arc,
	RwLock,
};

use uniui_base::{
	prelude::*,
	Color,
};
use uniui_widget_canvas::prelude::*;

use crate::{
	frame_collection::CollectionReader,
	Frame,
};

pub struct FrameAnimation {
	frames: CollectionReader,
	frame_ids: Arc<RwLock<Vec<usize>>>,
	frame_length: Arc<RwLock<usize>>,
	first_frame_time: Option<i64>,
	frame: Frame,
	default_frame: Frame,
	background: Color,
	slot_set_frame_ids: SlotImpl<Vec<usize>>,
}

impl FrameAnimation {
	pub fn new(
		frames: CollectionReader,
		frame_ids: Arc<RwLock<Vec<usize>>>,
		frame_length: Arc<RwLock<usize>>,
		default_frame: Frame,
		background: Color,
	) -> FrameAnimation {
		return FrameAnimation {
			frames,
			frame_ids,
			frame_length,
			first_frame_time: None,
			frame: default_frame.clone(),
			default_frame,
			background,
			slot_set_frame_ids: SlotImpl::new(),
		};
	}

	pub fn slot_set_frame_ids(&mut self) -> &mut dyn Slot<Vec<usize>> {
		return &mut self.slot_set_frame_ids;
	}
}

impl Drawer for FrameAnimation {
	fn draw(
		&mut self,
		context: &mut dyn Context,
		width: u32,
		height: u32,
	) {
		context.draw_rect(&self.background, 0.0, 0.0, width as f64, height as f64);
		self.frame.draw_centered(
			0.0,
			0.0,
			width as f64,
			height as f64,
			1.0,
			1.0,
			true,
			context,
		);
	}
}

impl DataProcessor for FrameAnimation {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		_: &mut dyn Application,
	) {
		if let Some(t) = self.slot_set_frame_ids.data_iter().last() {
			match self.frame_ids.write() {
				Ok(mut frame_ids) => *frame_ids = t,
				Err(e) => {
					error!("coudn't lock frame_ids e:{:?}", e);
				},
			};
		}

		let frame = match (self.frame_ids.read(), self.frame_length.read()) {
			(Ok(frame_ids), Ok(frame_length)) => {
				let frame_id = if let Some(t) = self.first_frame_time.as_ref() {
					let ticks = msec_since_app_start - t;
					ticks as usize / *frame_length
				} else {
					self.first_frame_time = Some(msec_since_app_start);
					0
				};

				match frame_ids.len() {
					0 => None,
					n => {
						let id = frame_id % n;
						match frame_ids.get(id) {
							Some(i) => self.frames.get(*i),
							None => {
								error!("no frame with id:{}", id);
								None
							},
						}
					},
				}
			},
			_ => {
				error!("lock failed");
				None
			},
		};

		match frame {
			Some(f) => self.frame = f.clone(),
			None => self.frame = self.default_frame.clone(),
		}
	}
}
