use maze_core::buttons::Button;

mod frame_actions_editor;
pub use self::frame_actions_editor::FrameActionsEditor;

mod data_holder;
mod data_widget;

fn button_as_string(button: &Button) -> String {
	return match button {
		Button::Up => String::from("[ U ]"),
		Button::Down => String::from("[ D ]"),
		Button::Left => String::from("[ L ]"),
		Button::Right => String::from("[ R ]"),

		Button::A => String::from("[ A ]"),
		Button::B => String::from("[ B ]"),
		Button::X => String::from("[ X ]"),
		Button::Y => String::from("[ Y ]"),

		Button::Start => String::from("[ Start ]"),
		Button::Select => String::from("[ Select ]"),
	};
}
