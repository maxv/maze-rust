use super::data_holder::DataHolder;

use crate::Frame;

use maze_core::buttons::Button as GameButton;

use uniui_base::prelude::*;
use uniui_layout_recyclerlayout::prelude::*;
use uniui_widget_combobox::prelude::*;

use std::sync::{
	Arc,
	RwLock,
};

#[derive(uniui_gui::Widget)]
pub struct FrameActionsEditor {
	#[base_widget]
	layout: LinearLayout,

	#[uproperty_process]
	input_button: Property<GameButton>,

	#[uproperty_process]
	input_frame_id: Property<String>,

	signal_new_action: Signal<(GameButton, usize)>,

	#[public_slot]
	slot_collection_updated: SlotProxy<()>,

	#[public_slot]
	slot_set_current_frame: SlotProxy<usize>,

	#[uprocess_last(add_or_replace_action)]
	slot_add_clicked: SlotImpl<()>,
}

impl FrameActionsEditor {
	pub fn new(frames: Arc<RwLock<Vec<Frame>>>) -> FrameActionsEditor {
		let input_button = Property::new(GameButton::Select);
		let input_frame_id = Property::new("".to_string());
		let slot_add_clicked = SlotImpl::new();
		let mut signal_new_action = Signal::new();


		let mut layout = LinearLayout::new(Orientation::Vertical);

		layout.push_widget({
			let mut frame_id = TextEdit::new("frame_id".to_string());
			frame_id.signal_text_changed().connect_slot(input_frame_id.slot());

			MinimalSize {
				widget: frame_id,
			}
		});

		layout.push_widget({
			let mut layout = LinearLayout::new(Orientation::Horizontal);

			layout.push_widget({
				let mut combo_box = ComboBox::new();
				for button in Self::available_buttons() {
					let description = super::button_as_string(&button);
					combo_box.add_item(button, description);
				}

				combo_box.signal_selection_changed().connect_slot(input_button.slot());

				combo_box
			});


			layout.push_widget({
				let mut button = Button::new("V".to_string());
				button.signal_clicked().connect_slot(&slot_add_clicked);
				button
			});

			MinimalSize {
				widget: layout,
			}
		});

		let slot_collection_updated;
		let slot_set_current_frame;
		layout.push_widget({
			let data_holder = DataHolder::new(frames, 0);
			signal_new_action.connect_slot(data_holder.slot_new_action());
			slot_collection_updated = data_holder.slot_collection_updated().proxy();
			slot_set_current_frame = data_holder.slot_set_current_frame().proxy();
			let recycler_layout = RecyclerLayout::new(data_holder, Orientation::Vertical);
			recycler_layout
		});

		return FrameActionsEditor {
			layout,
			input_button,
			input_frame_id,
			slot_add_clicked,
			signal_new_action,
			slot_collection_updated,
			slot_set_current_frame,
		};
	}

	fn available_buttons() -> Vec<GameButton> {
		return vec![
			GameButton::Up,
			GameButton::Down,
			GameButton::Left,
			GameButton::Right,
			GameButton::A,
			GameButton::B,
			GameButton::X,
			GameButton::Y,
			GameButton::Select,
		];
	}

	fn add_or_replace_action(
		&mut self,
		_: (),
	) {
		match self.input_frame_id.as_ref().parse::<usize>() {
			Ok(frame_id) => {
				self.signal_new_action
					.emit((self.input_button.as_ref().clone(), frame_id));
			},
			Err(_) => {},
		}
	}
}
