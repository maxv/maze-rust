use maze_core::buttons::Button;

use uniui_base::prelude::*;

#[derive(uniui_gui::Widget)]
pub struct DataWidget {
	#[base_widget]
	layout: LinearLayout,

	signal_button_updated: Signal<String>,
	signal_id_updated: Signal<String>,
}

impl DataWidget {
	pub fn new() -> DataWidget {
		let mut signal_button_updated = Signal::new();
		let mut signal_id_updated = Signal::new();

		let layout = u_linear_layout! {
			orientation: Orientation::Horizontal,
			widgets: {
				u_label!{
					text: "".to_owned(),
					slot_set_text: signal_button_updated,
				},
				u_label!{
					text: "".to_owned(),
					slot_set_text: signal_id_updated,
				},
			},
		};

		return DataWidget {
			layout,
			signal_button_updated,
			signal_id_updated,
		};
	}

	pub fn fill(
		&mut self,
		t: &(Button, usize),
	) {
		self.signal_button_updated.emit(super::button_as_string(&t.0));
		self.signal_id_updated.emit(t.1.to_string());
	}
}
