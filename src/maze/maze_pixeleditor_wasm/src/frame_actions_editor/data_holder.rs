use super::data_widget::DataWidget;

use crate::Frame;

use maze_core::buttons::Button;

use uniui_base::{
	core::Property,
	prelude::*,
};
use uniui_layout_recyclerlayout::{
	DataHolder as DataHolderBase,
	DataHolderUpdate,
};

use std::sync::{
	Arc,
	RwLock,
};


#[derive(uniui_gui::DataProcessor)]
pub struct DataHolder {
	frames: Arc<RwLock<Vec<Frame>>>,

	#[uproperty_process(full_reload)]
	#[uproperty_public_slot(slot_set_current_frame)]
	current_frame: Property<usize>,

	#[public_slot]
	#[uprocess_last(full_reload_1)]
	slot_collection_updated: SlotImpl<()>,

	#[public_slot]
	#[uprocess_each(on_new_action)]
	slot_new_action: SlotImpl<(Button, usize)>,

	signal_collection_updated: Signal<DataHolderUpdate>,
}

impl DataHolder {
	pub fn new(
		frames: Arc<RwLock<Vec<Frame>>>,
		default_frame_id: usize,
	) -> DataHolder {
		return DataHolder {
			frames,
			current_frame: Property::new(default_frame_id),
			slot_collection_updated: SlotImpl::new(),
			slot_new_action: SlotImpl::new(),
			signal_collection_updated: Signal::new(),
		};
	}

	fn full_reload(&mut self) {
		self.slot_new_action.discard_pending();
		self.signal_collection_updated.emit(DataHolderUpdate::WholeData);
	}

	fn full_reload_1(
		&mut self,
		_: (),
	) {
		self.full_reload();
	}

	fn on_new_action(
		&mut self,
		(button, id): (Button, usize),
	) {
		match self.frames.write() {
			Ok(mut frames) => {
				match frames.get_mut(*self.current_frame.as_ref()) {
					Some(frame) => {
						frame.actions.insert(button, id);
						self.signal_collection_updated.emit(DataHolderUpdate::WholeData);
					},
					None => error!("process_date:current_frame_incorrect"),
				}
			},
			Err(e) => error!("process_data:frame_lock:e:{:?}", e),
		};
	}
}

impl DataHolderBase<DataWidget> for DataHolder {
	fn count(&self) -> usize {
		return match self.frames.read() {
			Ok(frames) => {
				match frames.get(*self.current_frame.as_ref()) {
					Some(frame) => frame.actions.len(),
					None => {
						error!("count:current_frame_incorrect");
						0
					},
				}
			},
			Err(e) => {
				error!("count:frame_lock:e:{:?}", e);
				0
			},
		};
	}

	fn setup_update_slot(
		&mut self,
		slot: &dyn Slot<DataHolderUpdate>,
	) {
		self.signal_collection_updated.connect_slot(slot);
	}

	fn fill(
		&self,
		id: usize,
		dw: &mut DataWidget,
	) {
		match self.frames.read() {
			Ok(frames) => {
				let frame_id = *self.current_frame.as_ref();
				match frames.get(frame_id) {
					Some(frame) => {
						let mut iter = frame.actions.iter();
						for _ in 0..id {
							iter.next();
						}
						match iter.next() {
							Some((button, id)) => {
								dw.fill(&(*button, *id));
							},
							None => error!("fill:probably_incorrect_id:{}", id),
						}
					},
					None => error!("fill:incorrect_frame_id:{}", frame_id),
				};
			},
			Err(e) => error!("fill:frame_lock:e:{:?}", e),
		};
	}

	fn new_widget(&mut self) -> DataWidget {
		return DataWidget::new();
	}
}
