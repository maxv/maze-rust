use uniui_base::{
	prelude::*,
	Color,
};
use uniui_layout_recyclerlayout::prelude::*;

use std::sync::{
	Arc,
	RwLock,
};


mod helpers {
	mod data;
	pub use self::data::Data;

	mod dw;
	pub use self::dw::DW;
}

#[derive(uniui_gui::Widget)]
pub struct Palette {
	#[base_widget]
	layout: RecyclerLayout<helpers::DW, helpers::Data>,
	add_color_sender: SlotProxy<Color>,
	signal_color_selected: Signal<Option<Color>>,

	#[uprocess_last(signal_color_selected.emit)]
	receiver_color_selected: SlotImpl<Option<Color>>,
	receiver_colors_updated: SlotProxy<Arc<RwLock<Vec<Option<Color>>>>>,
}

impl Palette {
	pub fn new(colors: Arc<RwLock<Vec<Option<Color>>>>) -> Palette {
		let receiver_color_selected = SlotImpl::new();
		let data_holder = helpers::Data {
			colors,
			slot_add_color: SlotImpl::new(),
			signal: Signal::new(),
			slot_colors_updated: SlotImpl::new(),
			group_name: "color".to_string(),
			sender_color_selected: receiver_color_selected.proxy(),
		};
		let add_color_sender = data_holder.slot_add_color.proxy();
		let receiver_colors_updated = data_holder.slot_colors_updated.proxy();

		let layout = RecyclerLayout::new(data_holder, Orientation::Vertical);
		return Palette {
			layout,
			add_color_sender,
			receiver_color_selected,
			signal_color_selected: Signal::new(),
			receiver_colors_updated,
		};
	}

	pub fn add_color_sender(&mut self) -> SlotProxy<Color> {
		return self.add_color_sender.clone();
	}

	pub fn signal_color_selected(&mut self) -> &mut Signal<Option<Color>> {
		return &mut self.signal_color_selected;
	}

	pub fn receiver_colors_updated(&self) -> SlotProxy<Arc<RwLock<Vec<Option<Color>>>>> {
		return self.receiver_colors_updated.clone();
	}
}
