use std::sync::{
	Arc,
	RwLock,
};

use uniui_base::{
	prelude::*,
	Color,
};
use uniui_layout_recyclerlayout::{
	DataHolder,
	DataHolderUpdate,
};

use super::DW;


#[derive(uniui_gui::DataProcessor)]
pub struct Data {
	pub colors: Arc<RwLock<Vec<Option<Color>>>>,
	pub group_name: String,
	pub sender_color_selected: SlotProxy<Option<Color>>,
	pub signal: Signal<DataHolderUpdate>,

	#[uprocess_last(on_colors_updated)]
	pub slot_colors_updated: SlotImpl<Arc<RwLock<Vec<Option<Color>>>>>,

	#[uprocess_each(on_color_added)]
	pub slot_add_color: SlotImpl<Color>,
}

impl Data {
	fn on_colors_updated(
		&mut self,
		v: Arc<RwLock<Vec<Option<Color>>>>,
	) {
		self.colors = v;
		self.signal.emit(DataHolderUpdate::WholeData);
	}

	fn on_color_added(
		&mut self,
		color: Color,
	) {
		match self.colors.write() {
			Ok(mut v) => {
				v.push(Some(color));
				self.signal.emit(DataHolderUpdate::WholeData);
			},
			Err(e) => error!("couldn't write to colors e:{:?}", e),
		}
	}
}

impl DataHolder<DW> for Data {
	fn count(&self) -> usize {
		return match self.colors.read() {
			Ok(v) => v.len(),
			Err(e) => {
				error!("couldn't read colors e:{:?}", e);
				0
			},
		};
	}

	fn setup_update_slot(
		&mut self,
		slot: &dyn Slot<DataHolderUpdate>,
	) {
		self.signal.connect_slot(slot);
	}

	fn fill(
		&self,
		id: usize,
		dw: &mut DW,
	) {
		match self.colors.read() {
			Ok(v) => {
				match v.get(id) {
					Some(c) => dw.fill(c),
					None => {},
				}
			},
			Err(_) => {},
		}
	}

	fn new_widget(&mut self) -> DW {
		return DW::new(self.sender_color_selected.clone(), self.group_name.clone());
	}
}
