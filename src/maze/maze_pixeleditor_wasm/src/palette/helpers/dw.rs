use uniui_base::{
	prelude::*,
	Color,
};
use uniui_widget_radiobutton::prelude::*;

#[derive(uniui_gui::Widget)]
pub struct DW {
	#[base_widget]
	layout: LinearLayout,
	set_text_sender: SlotProxy<String>,
	sender_color_selected: SlotProxy<Option<Color>>,

	#[uprocess_last(send_color)]
	slot_checked: SlotImpl<()>,

	color: Option<Color>,
}

impl DW {
	pub fn new(
		sender_color_selected: SlotProxy<Option<Color>>,
		group_name: String,
	) -> DW {
		let slot_checked = SlotImpl::new();
		let set_text_sender;

		let layout = u_linear_layout! {
			orientation: Orientation::Horizontal,
			widgets: {
				u_radiobutton!{
					group: group_name,
					signal_checked: &slot_checked,
				},
				u_label!{
					text: "".to_owned(),
					slot_set_text_proxy: set_text_sender,
				},
			},
		};

		return DW {
			layout,
			set_text_sender,
			slot_checked,
			sender_color_selected,
			color: None,
		};
	}

	pub fn fill(
		&mut self,
		c: &Option<Color>,
	) {
		match c {
			Some(c) => {
				self.layout.set_background(c);
				let s = format!("R#{}  G#{}  B#{}", c.red, c.green, c.blue);
				match self.set_text_sender.exec_for(s) {
					true => trace!("send ok"),
					false => error!("couldn't send"),
				};
			},
			None => {
				match self.set_text_sender.exec_for("Empty".to_string()) {
					true => trace!("send ok"),
					false => error!("couldn't send"),
				};
			},
		};
		self.color = *c;
	}

	fn send_color(
		&mut self,
		_: (),
	) {
		self.sender_color_selected.exec_for(self.color);
	}
}
