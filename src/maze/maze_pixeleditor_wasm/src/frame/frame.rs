use maze_core::{
	buttons::Button,
	Direction as MoveDirection,
};

use uniui_base::Color;
use uniui_widget_canvas::Context;

use serde::{
	Deserialize,
	Serialize,
};

use std::collections::HashMap;


#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
pub struct Frame {
	pub pixels: Vec<Option<Color>>,
	pub width: usize,
	pub height: usize,

	#[serde(default)]
	pub dx: i32,

	#[serde(default)]
	pub dy: i32,

	#[serde(default)]
	pub duration: u64,

	#[serde(default)]
	pub next_direction: Option<MoveDirection>,

	#[serde(default)]
	pub next_accepted: usize,

	#[serde(default)]
	pub next_rejected: usize,

	#[serde(default)]
	pub actions: HashMap<Button, usize>,
}

impl Frame {
	pub fn new() -> Frame {
		return Frame {
			pixels: vec![None],
			width: 1,
			height: 1,
			dx: 0,
			dy: 0,
			duration: 0,
			next_direction: None,
			next_accepted: 0,
			next_rejected: 0,
			actions: HashMap::new(),
		};
	}

	pub fn clear(&mut self) {
		self.pixels.iter_mut().for_each(|c| *c = None);
	}

	fn draw(
		&self,
		frame_left: f64,
		frame_top: f64,
		cell_width: f64,
		cell_height: f64,
		use_displacement: bool,
		context: &dyn Context,
	) {
		let width = self.width;
		let frame_left = match use_displacement {
			true => frame_left + self.dx as f64,
			false => frame_left,
		};

		let frame_top = match use_displacement {
			true => frame_top + self.dy as f64,
			false => frame_top,
		};

		self.pixels.iter().enumerate().for_each(|(l, p)| {
			match p {
				Some(c) => {
					let x = (l % width) as f64 * cell_width;
					let y = (l / width) as f64 * cell_height;
					context.draw_rect(
						c,
						frame_left + x,
						frame_top + y,
						cell_width,
						cell_height,
					);
				},
				None => {},
			};
		});
	}

	pub fn draw_centered(
		&self,
		left: f64,
		top: f64,
		width: f64,
		height: f64,
		cell_width: f64,
		cell_height: f64,
		use_displacement: bool,
		context: &dyn Context,
	) -> (f64, f64, f64, f64) {
		let frame_height = self.height as f64 * cell_height;
		let frame_width = self.width as f64 * cell_width;

		let frame_left = (width - frame_width) / 2.0 + left;
		let frame_top = (height - frame_height) / 2.0 + top;
		self.draw(
			frame_left,
			frame_top,
			cell_width,
			cell_height,
			use_displacement,
			context,
		);
		return (frame_left, frame_top, frame_width, frame_height);
	}
}
