use crate::Frame;

use maze_core::{
	maneuver::Maneuver,
	DrawInfo,
};

use uniui_base::Color;

use png::{
	Decoder,
	Encoder,
};

pub fn new_maneuver_from_frame(
	frame: &Frame,
	width: usize,
	top: u32,
) -> Maneuver {
	return Maneuver {
		draw_info: DrawInfo {
			resource: String::new(),
			left: (width - frame.width) as u32,
			top,
			width: frame.width as u32,
			height: frame.height as u32,
			dx: frame.dx,
			dy: frame.dy,
		},
		duration: frame.duration,
		next_direction: frame.next_direction,
		accepted_next_maneuver: frame.next_accepted,
		rejected_next_maneuver: frame.next_rejected,
		actions: frame.actions.clone(),
	};
}

pub fn new_draw_info_from_frame(
	frame: &Frame,
	width: usize,
	top: u32,
) -> DrawInfo {
	return DrawInfo {
		resource: String::new(),
		left: (width - frame.width) as u32,
		top,
		width: frame.width as u32,
		height: frame.height as u32,
		dx: frame.dx,
		dy: frame.dy,
	};
}

pub fn collection_to_vec<T>(
	frames: &Vec<Frame>,
	f: &mut dyn FnMut(&Frame, usize, u32) -> T,
) -> Option<(Vec<u8>, Vec<T>)> {
	let mut width = 0;
	let mut height = 0;
	for frame in frames.iter() {
		width = std::cmp::max(width, frame.width);
		height += frame.height;
	}

	let mut image = Vec::new();
	let mut maneuvers = Vec::new();
	let encoding_result = {
		let mut encoder = Encoder::new(&mut image, width as u32, height as u32);
		encoder.set_color(png::ColorType::RGBA);
		encoder.set_depth(png::BitDepth::Eight);

		encoder.write_header().and_then(|mut writer| {
			let mut data: Vec<u8> = Vec::new();

			for frame in frames.iter() {
				let current_width = frame.width;

				// *4 since we have 4 elements in vector for each pixel
				let top = (data.len() / (width * 4)) as u32;
				maneuvers.push(f(frame, width, top));

				for (n, c) in frame.pixels.iter().enumerate() {
					if (n % current_width) == 0 {
						for _ in current_width..width {
							data.push(255);
							data.push(255);
							data.push(255);
							data.push(0);
						}
					};

					match c {
						Some(color) => {
							data.push(color.red);
							data.push(color.green);
							data.push(color.blue);
							data.push(255);
						},
						None => {
							data.push(255);
							data.push(255);
							data.push(255);
							data.push(0);
						},
					};
				}
			}
			writer.write_image_data(&data)
		})
	};

	return match encoding_result {
		Ok(()) => Some((image, maneuvers)),
		Err(e) => {
			error!("encoding error:{:?}", e);
			None
		},
	};
}

pub fn to_vec(
	png: &[u8],
	maneuvers: &Vec<Maneuver>,
) -> Vec<Frame> {
	let mut result: Vec<Frame> = Vec::new();

	let mut png = Decoder::new(png);
	png.set_transformations(png::Transformations::IDENTITY);
	match png.read_info() {
		Ok((info, mut reader)) => {
			let size = (info.width * info.height * 4) as usize;
			let mut png: Vec<u8> = Vec::with_capacity(size);
			png.resize(size, 0);
			match reader.next_frame(&mut png) {
				Ok(()) => {
					for maneuver in maneuvers.iter() {
						let mut frame = Frame::new();
						frame.duration = maneuver.duration;
						frame.next_direction = maneuver.next_direction;
						frame.next_accepted = maneuver.accepted_next_maneuver;
						frame.next_rejected = maneuver.rejected_next_maneuver;
						frame.actions = maneuver.actions.clone();
						frame.dx = maneuver.draw_info.dx as i32;
						frame.dy = maneuver.draw_info.dy as i32;
						frame.width = maneuver.draw_info.width as usize;
						frame.height = maneuver.draw_info.height as usize;
						frame.pixels.clear();

						let left = maneuver.draw_info.left as usize;
						let top = maneuver.draw_info.top as usize;
						let right =
							(maneuver.draw_info.left + maneuver.draw_info.width) as usize;
						let bottom =
							(maneuver.draw_info.top + maneuver.draw_info.height) as usize;

						for j in top..bottom {
							for i in left..right {
								let base = (4 * i + j * info.line_size) as usize;
								match (
									png.get(base),
									png.get(base + 1),
									png.get(base + 2),
									png.get(base + 3),
								) {
									(Some(red), Some(green), Some(blue), Some(alpha)) => {
										match *alpha == 0 {
											true => frame.pixels.push(None),
											false => {
												frame.pixels.push(Some(Color {
													red: *red,
													green: *green,
													blue: *blue,
												}))
											},
										};
									},
									_ => {
										error!(
											"couldn't get elements for base:{}, len:{}",
											base,
											png.len()
										);
									},
								};
							}
						}

						result.push(frame);
					}
				},
				Err(e) => error!("png read err:{:?}", e),
			}
		},
		Err(e) => error!("decoding png err:{:?}", e),
	};


	return result;
}
