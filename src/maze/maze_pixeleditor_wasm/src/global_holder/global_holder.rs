use crate::Frame;

use uniui_base::{
	prelude::*,
	Color,
};
use uniui_file::File;

use serde::{
	Deserialize,
	Serialize,
};

use std::sync::{
	Arc,
	RwLock,
};

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone, Debug)]
struct SavedData {
	pub frames: Vec<Frame>,
	pub colors: Vec<Option<Color>>,
	pub frame_ids: Vec<usize>,
	pub frame_length: usize,
}

#[derive(uniui_gui::DataProcessor)]
pub struct GlobalHolder {
	frames: Arc<RwLock<Vec<Frame>>>,
	colors: Arc<RwLock<Vec<Option<Color>>>>,
	frame_ids: Arc<RwLock<Vec<usize>>>,
	frame_length: Arc<RwLock<usize>>,

	#[public_slot]
	#[uprocess_last(save_to_file)]
	slot_save_to_file: SlotImpl<()>,

	#[public_slot]
	#[uprocess_last(on_load_from_file)]
	slot_load_from_file: SlotImpl<Vec<File>>,

	#[uprocess_last(on_file_data_readed)]
	slot_data_from_file_readed: SlotImpl<Result<Vec<u8>, ()>>,

	#[public_signal]
	signal_collection_reloaded: Signal<()>,

	#[public_signal]
	signal_colors_updated: Signal<Arc<RwLock<Vec<Option<Color>>>>>,

	#[public_slot]
	#[uprocess_last(set_frames)]
	slot_set_frames: SlotImpl<Vec<Frame>>,
}

impl GlobalHolder {
	pub fn new() -> GlobalHolder {
		let mut colors = Vec::new();
		colors.push(None);

		let mut frame_ids = Vec::new();
		frame_ids.push(0);

		return GlobalHolder {
			frames: Arc::new(RwLock::new(Vec::new())),
			colors: Arc::new(RwLock::new(colors)),
			frame_ids: Arc::new(RwLock::new(frame_ids)),
			frame_length: Arc::new(RwLock::new(20)),

			slot_save_to_file: SlotImpl::new(),
			slot_load_from_file: SlotImpl::new(),
			slot_data_from_file_readed: SlotImpl::new(),

			signal_collection_reloaded: Signal::new(),
			signal_colors_updated: Signal::new(),
			slot_set_frames: SlotImpl::new(),
		};
	}

	pub fn colors(&self) -> Arc<RwLock<Vec<Option<Color>>>> {
		return self.colors.clone();
	}

	pub fn frames(&self) -> Arc<RwLock<Vec<Frame>>> {
		return self.frames.clone();
	}

	pub fn frame_ids(&self) -> Arc<RwLock<Vec<usize>>> {
		return self.frame_ids.clone();
	}

	pub fn frame_length(&self) -> Arc<RwLock<usize>> {
		return self.frame_length.clone();
	}

	fn save_to_file(
		&self,
		_: (),
	) {
		let mut saved_data = SavedData {
			frames: Vec::new(),
			colors: Vec::new(),
			frame_ids: Vec::new(),
			frame_length: 20,
		};
		let mut no_errors = true;
		match self.frames.read() {
			Ok(v) => {
				saved_data.frames = v.clone();
			},
			Err(e) => {
				error!("lock failed e:{:?}", e);
				no_errors = false;
			},
		}

		match self.colors.read() {
			Ok(v) => saved_data.colors = v.clone(),
			Err(e) => {
				error!("lock failed e:{:?}", e);
				no_errors = false;
			},
		}

		match self.frame_ids.read() {
			Ok(v) => saved_data.frame_ids = v.clone(),
			Err(e) => {
				error!("lock failed e:{:?}", e);
				no_errors = false;
			},
		}

		match self.frame_length.read() {
			Ok(v) => saved_data.frame_length = v.clone(),
			Err(e) => {
				error!("lock failed e:{:?}", e);
				no_errors = false;
			},
		}

		if no_errors {
			match serde_cbor::to_vec(&saved_data) {
				Ok(v) => {
					uniui_gui::utils::save_file(&v, "file.bin");
				},
				Err(e) => error!("serde error e:{:?}", e),
			}
		}
	}

	fn set_frames(
		&mut self,
		mut frames: Vec<Frame>,
	) {
		match self.frames.write() {
			Ok(mut v) => {
				v.clear();
				v.append(&mut frames);
			},
			Err(e) => {
				error!("lock failed e:{:?}", e);
			},
		}
		self.signal_collection_reloaded.emit(());
	}

	fn load_from_vec(
		&mut self,
		v: &Vec<u8>,
	) {
		match serde_cbor::from_slice::<SavedData>(v) {
			Ok(saved_data) => {
				self.set_frames(saved_data.frames.clone());

				match self.colors.write() {
					Ok(mut colors) => *colors = saved_data.colors.clone(),
					Err(e) => {
						error!("lock failed e:{:?}", e);
					},
				}
				self.signal_colors_updated.emit(self.colors.clone());

				match self.frame_ids.write() {
					Ok(mut frame_ids) => *frame_ids = saved_data.frame_ids.clone(),
					Err(e) => {
						error!("lock failed e:{:?}", e);
					},
				}

				match self.frame_length.write() {
					Ok(mut frame_length) => {
						*frame_length = saved_data.frame_length.clone()
					},
					Err(e) => {
						error!("lock failed e:{:?}", e);
					},
				}
			},
			Err(e) => error!("sedre error e:{:?}", e),
		}
	}

	fn on_file_data_readed(
		&mut self,
		v: Result<Vec<u8>, ()>,
	) {
		match v {
			Ok(v) => self.load_from_vec(&v),
			Err(e) => error!("file reading error:{:?}", e),
		}
	}

	fn on_load_from_file(
		&mut self,
		v: Vec<File>,
	) {
		if let Some(file) = v.iter().last() {
			file.read_all(self.slot_data_from_file_readed.proxy());
		}
	}
}
