#[macro_use]
extern crate log;

use uniui_base::{
	prelude::*,
	Color,
	SizeUnit,
};
use uniui_scene::{
	PositionRequest,
	Scene as GraphicScene,
};

use uniui_widget_canvas::Canvas;
use uniui_widget_color_picker::ColorPicker;
use uniui_widget_fileselector::FileSelector;
use uniui_widget_tabwidget::TabWidget;
use uniui_wrapper_fixedsize::prelude::*;


mod palette;
mod resizer;
mod reticle;
use reticle::Reticle;

mod frame {
	pub mod frame;
}
pub use frame::frame::Frame;

mod frame_collection;
pub use frame_collection::FrameCollection;

mod frames_animation;
use frames_animation::FrameAnimation;

mod frame_ids_extractor;
use frame_ids_extractor::FrameIdsExtractor;

mod global_holder;
use global_holder::GlobalHolder;

mod frame_meta_editor;
use frame_meta_editor::FrameMetaEditor;

mod frame_actions_editor;
use frame_actions_editor::FrameActionsEditor;

mod frame_exporter;
use frame_exporter::FrameExporter;

mod png_creator;

#[uniui_gui::u_main]
fn app_main(app: &mut dyn Application) {
	uniui_base::utils::init_logger(app);
	error!("Hello");

	let mut global_holder = GlobalHolder::new();

	let width = SizeUnit::VW(15.0);
	let height = SizeUnit::Percent(100.0);
	let mut frame_collection = FrameCollection::new(width, width, global_holder.frames());
	global_holder
		.signal_collection_reloaded()
		.connect_slot(frame_collection.slot_collection_reloaded());

	let mut reticle = Reticle::new(global_holder.frames(), true);
	frame_collection.signal_frame_selected().connect_slot(reticle.slot_set_frame_id());

	let mut main_layout = LinearLayout::new(Orientation::Horizontal);

	main_layout.push_widget({
		let mut layout = LinearLayout::new(Orientation::Vertical);

		layout.push_widget({
			let mut resizer = resizer::Resizer::new();
			resizer.signal_resize().connect_slot(reticle.slot_resize());
			MinimalSize {
				widget: resizer,
			}
		});

		layout.push_widget({
			let mut frame_meta_editor =
				FrameMetaEditor::new(global_holder.frames().clone());
			frame_collection
				.signal_frame_selected()
				.connect_slot(frame_meta_editor.slot_set_frame());
			frame_meta_editor
				.signal_frames_loaded()
				.connect_slot(global_holder.slot_set_frames());

			MinimalSize {
				widget: frame_meta_editor,
			}
		});

		layout.push_widget({
			let mut tab_widget = TabWidget::new();

			tab_widget.push_widget("Palette".to_string(), {
				let mut layout = LinearLayout::new(Orientation::Vertical);

				let mut palette = palette::Palette::new(global_holder.colors());
				global_holder
					.signal_colors_updated()
					.connect_slot(&palette.receiver_colors_updated());

				let mut color_selector = ColorPicker::new(Color {
					red: 223,
					green: 220,
					blue: 25,
				});

				color_selector
					.signal_color_selected()
					.connect_slot(&palette.add_color_sender());

				palette
					.signal_color_selected()
					.connect_slot(reticle.slot_current_color());

				let mut button_clear = Button::new("Clear".to_string());
				button_clear.signal_clicked().connect_slot(reticle.slot_clear());
				layout.push_widget({
					let mut l = LinearLayout::new(Orientation::Horizontal);
					l.push_widget(color_selector);
					l.push_widget(button_clear);
					MinimalSize {
						widget: l,
					}
				});

				layout.push_widget(palette);

				layout
			});

			tab_widget.push_widget("Actions".to_string(), {
				let frame_actions_editor =
					FrameActionsEditor::new(global_holder.frames().clone());
				global_holder
					.signal_collection_reloaded()
					.connect_slot(frame_actions_editor.slot_collection_updated());

				frame_collection
					.signal_frame_selected()
					.connect_slot(frame_actions_editor.slot_set_current_frame());
				frame_actions_editor
			});

			tab_widget
		});


		layout.push_widget({
			let mut btn = Button::new("save".to_string());
			btn.signal_clicked().connect_slot(global_holder.slot_save_to_file());
			MinimalSize {
				widget: btn,
			}
		});

		layout.push_widget({
			let mut files_selector = FileSelector::new();
			files_selector
				.signal_files_selected()
				.connect_slot(global_holder.slot_load_from_file());
			MinimalSize {
				widget: files_selector,
			}
		});

		MinimalSize {
			widget: layout,
		}
	});

	main_layout.push_widget({
		let mut scene = GraphicScene::new(Color {
			red: 33,
			green: 33,
			blue: 33,
		});
		scene.add_item(reticle, PositionRequest::full());
		scene.to_canvas_widget()
	});

	main_layout.push_widget({
		let mut layout = LinearLayout::new(Orientation::Vertical);

		let mut frame_ids_extractor = FrameIdsExtractor::new();

		layout.push_widget({
			let exporter = FrameExporter::new(
				frame_collection.collection_reader(),
				global_holder.frame_ids(),
			);
			MinimalSize {
				widget: exporter,
			}
		});

		layout.push_widget({
			let mut frame_animation = FrameAnimation::new(
				frame_collection.collection_reader(),
				global_holder.frame_ids(),
				global_holder.frame_length(),
				Frame::new(),
				Color {
					red: 170,
					green: 140,
					blue: 90,
				},
			);

			frame_ids_extractor
				.signal_frame_ids()
				.connect_slot(frame_animation.slot_set_frame_ids());


			let mut canvas = Canvas::new();
			canvas.set_drawer(Some(frame_animation));
			u_fixedsize! {
				widget: canvas,
				width_strict: SizeUnit::Percent(100.0),
				height_strict: SizeUnit::VH(30.0),
			}
		});

		layout.push_widget({
			let mut frame_ids = TextEdit::new(
				"frame ids <id>*<count>{,<id>*<count>} ex: \"1*5,2,3*4\"".to_string(),
			);
			frame_ids
				.signal_text_changed()
				.connect_slot(frame_ids_extractor.slot_text_changed());

			MinimalSize {
				widget: frame_ids,
			}
		});

		Application::add_data_processor(app, frame_ids_extractor);

		let mut button = Button::new("+".to_string());

		button.signal_clicked().connect_slot(frame_collection.slot_add_new_frame());

		layout.push_widget(MinimalSize {
			widget: button,
		});
		layout.push_widget(frame_collection);

		u_fixedsize! {
			widget: layout,
			width_strict: width,
			height_strict: height,
		}
	});

	Application::add_data_processor(app, global_holder);
	app.set_main_widget(Box::new(main_layout));
}
