use crate::{
	frame_collection::CollectionReader,
	png_creator,
};

use maze_ws_proto::user_template::UserTemplate;

use uniui_base::prelude::*;


use std::sync::{
	Arc,
	RwLock,
};

#[derive(uniui_gui::Widget)]
pub struct FrameExporter {
	#[base_widget]
	button: Button,

	frames: CollectionReader,
	frame_ids: Arc<RwLock<Vec<usize>>>,

	#[uprocess_last(export_frames)]
	slot_export_requested: SlotImpl<()>,
}

impl FrameExporter {
	pub fn new(
		frames: CollectionReader,
		frame_ids: Arc<RwLock<Vec<usize>>>,
	) -> FrameExporter {
		let slot_export_requested = SlotImpl::new();
		let mut button = Button::new("Export frames".to_string());
		button.signal_clicked().connect_slot(&slot_export_requested);

		return FrameExporter {
			frames,
			frame_ids,
			button,
			slot_export_requested,
		};
	}

	fn export_frames(
		&mut self,
		_: (),
	) {
		match self.frame_ids.read() {
			Ok(frame_ids) => {
				let mut frames = Vec::new();
				for i in frame_ids.iter() {
					match self.frames.get(*i) {
						Some(f) => frames.push(f),
						None => error!("no frames with id:{}", i),
					}
				}

				match png_creator::collection_to_vec(
					&frames,
					&mut png_creator::new_draw_info_from_frame,
				) {
					Some(v) => {
						let data = UserTemplate {
							png: v.0,
							maneuvers: v.1,
						}
						.to_tar();
						uniui_gui::utils::save_file(&data, "state_template.tar");
					},
					None => error!("exraction error"),
				}
			},
			Err(e) => {
				error!("lock frame_ids err:{:?}", e);
			},
		}
	}
}
