use maze_core::{
	Game,
	LocalGame,
	TickAction,
	UserId,
};

use unitracer::{
	span,
	span_from_context,
	tag_i64,
	SpanContext,
};

use maze_ws_proto::{
	protocol::Resource,
	GameSettings,
	Protocol,
};

use uniws::{
	Error,
	UniWs,
	UniWsTrait,
};

use std::{
	collections::{
		HashMap,
		LinkedList,
	},
	sync::mpsc::{
		self,
		Receiver,
		Sender,
	},
};

pub struct ServerGame {
	users: HashMap<UserId, UniWs<Protocol>>,

	game: LocalGame,
	resources: Vec<Resource>,

	tick_length_msec: u64,
	game_id: i64,
	time_shift: u64,

	scheduled_actions: LinkedList<(UserId, TickAction)>,

	new_user_sender: Sender<UniWs<Protocol>>,
	new_user_receiver: Receiver<UniWs<Protocol>>,
}

impl ServerGame {
	pub fn new(
		game: LocalGame,
		resources: Vec<Resource>,
		tick_length_msec: u64,
		game_id: i64,
		time_shift: u64,
	) -> ServerGame {
		trace!("new");

		let (new_user_sender, new_user_receiver) = mpsc::channel();

		return ServerGame {
			users: HashMap::new(),
			game,
			resources,
			tick_length_msec,
			game_id,
			time_shift,

			scheduled_actions: LinkedList::new(),

			new_user_sender,
			new_user_receiver,
		};
	}

	pub fn get_user_adder(&self) -> Sender<UniWs<Protocol>> {
		return self.new_user_sender.clone();
	}

	fn send_to_all_except(
		&mut self,
		except_user_id: &UserId,
		tick_action: &TickAction,
	) {
		let tock = Protocol::TockAction(*except_user_id, *tick_action);
		self.users.iter_mut().for_each(|(user_id, channel)| {
			if user_id != except_user_id {
				match channel.send(&tock) {
					Ok(()) => trace!("send"),
					Err(e) => warn!("send error:{:?}", e),
				}
			}
		});
	}

	fn receive_users_messages(&mut self) -> LinkedList<UserId> {
		let mut drop_users_list = LinkedList::new();

		let mut actions: LinkedList<(UserId, TickAction)> = LinkedList::new();

		self.users.iter_mut().for_each(|(user_id, channel)| {
			match channel.try_recv() {
				Ok(Protocol::TickAction(t)) => {
					info!("message from user received");
					actions.push_back((*user_id, t));
				},
				Ok(p) => {
					warn!("unknown message:{:?} from user:{}", p, user_id);
				},
				Err(e) => {
					match e {
						Error::Empty => trace!("no message from user:{}", user_id),
						Error::NotReady => trace!("socket not ready user:{}", user_id),
						Error::Closed |
						Error::Unexpected |
						Error::NativeSocketError(_) |
						Error::SerializationJsonError(_) |
						Error::SerializationCborError(_) => {
							info!("user will be disconnected e:{:?}", e);
							drop_users_list.push_back(*user_id);
						},
					}
				},
			};
		});

		for (user_id, t) in actions.iter() {
			self.send_to_all_except(user_id, t);
		}
		self.scheduled_actions.append(&mut actions);

		return drop_users_list;
	}

	fn connect_new_users(&mut self) -> bool {
		let mut result = false;
		while let Ok(channel) = self.new_user_receiver.try_recv() {
			span!(new_user_connected);
			result = true;
			info!("new_user_connected");
			self.add_new_user(channel);
		}

		return result;
	}

	fn add_new_user(
		&mut self,
		mut channel: UniWs<Protocol>,
	) {
		span!(add_new_user);

		let user = self.game.respawn_points().get(0).unwrap().clone();
		let new_user_id = self.game.add_user(user);
		tag_i64!("user_id".to_string(), new_user_id);

		let game_settings = GameSettings {
			user_id: new_user_id,
			tick_length_msec: self.tick_length_msec,
			game_id: self.game_id,
			time_shift: self.time_shift,
		};

		let settings = Protocol::GameSettings(game_settings);
		let settings_sent = channel.send(&settings);
		let state_sent = channel.send(&Protocol::Next(self.game.clone()));
		let resources_sent = channel.send(&Protocol::Resources(self.resources.clone()));

		let connection_ok = match (settings_sent, state_sent, resources_sent) {
			(Ok(()), Ok(()), Ok(())) => true,
			(Err(e), ..) => {
				warn!("settings sent error e:{:?}", e);
				false
			},
			(_, Err(e), _) => {
				warn!("state sent error e:{:?}", e);
				false
			},
			(_, _, Err(e)) => {
				warn!("resrouce sent error e:{:?}", e);
				false
			},
		};

		if connection_ok {
			self.send_scheduled_actions(&mut channel);
			self.users.insert(new_user_id, channel);
		}
	}

	fn wait_for_first_user(&mut self) {
		match self.new_user_receiver.recv() {
			Ok(channel) => self.add_new_user(channel),
			Err(e) => error!("new user didn't come e:{:?}", e),
		}
	}

	pub fn process(
		&mut self,
		span_context: Option<SpanContext>,
	) {
		span!(game_session);
		tag_i64!("game_id".to_string(), self.game_id);

		let mut tick = 0;
		let mut next_send_tick = 0;
		let sleep_duration = std::time::Duration::from_millis(self.tick_length_msec / 2);

		{
			span_from_context!(span_context, wait_for_first_user);
			self.wait_for_first_user();
		}

		let game_epoch = std::time::Instant::now();
		loop {
			let users_to_del = self.receive_users_messages();
			for user_id in users_to_del {
				self.game.remove_user(&user_id);
				self.users.remove(&user_id);
			}
			let new_user_connected = self.connect_new_users();

			if self.users.is_empty() {
				break;
			} else {
				let msec_since_game_epoch = game_epoch.elapsed().as_millis() as u64;
				let ticks_since_game_epoch =
					msec_since_game_epoch / self.tick_length_msec;
				info!(
					"tick_since_epoch:{} msec_since_epoch:{}",
					ticks_since_game_epoch, msec_since_game_epoch
				);

				if ticks_since_game_epoch > tick {
					tick = ticks_since_game_epoch;
					self.process_ticks(tick);

					if new_user_connected || tick >= next_send_tick {
						self.send_state_all();
						next_send_tick = tick + 15;
					}
				}

				std::thread::sleep(sleep_duration);
			}
		}
	}

	fn send_state_all(&mut self) {
		let state = Protocol::Next(self.game.clone());
		self.users.iter_mut().for_each(|(user_id, channel)| {
			match channel.send(&state) {
				Ok(()) => {},
				Err(e) => warn!("coudn't send state to user:{} e:{:?}", user_id, e),
			}
		});
	}

	fn process_ticks(
		&mut self,
		last_tick: u64,
	) {
		info!("process tick:{} time:{:?}", last_tick, std::time::Instant::now());

		self.game.tick(self.scheduled_actions.iter(), last_tick);

		self.scheduled_actions = self
			.scheduled_actions
			.iter()
			.filter(|(_, t)| t.tick_num >= last_tick)
			.copied()
			.collect();
	}

	fn send_scheduled_actions(
		&self,
		out: &mut UniWs<Protocol>,
	) {
		self.scheduled_actions.iter().for_each(|(user_id, t)| {
			match out.send(&Protocol::TockAction(*user_id, *t)) {
				Ok(()) => {},
				Err(e) => warn!("couldn't send action to user:{} e:{:?}", user_id, e),
			}
		});
	}
}
