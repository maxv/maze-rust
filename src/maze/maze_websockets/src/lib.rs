#[macro_use]
extern crate log;

extern crate serde_json;
extern crate url;

extern crate maze_core;
extern crate maze_ws_proto;
extern crate unitracer;
extern crate uniws;

mod server_game;

mod utils;
pub use utils::start_server;

mod server_game_repository;
