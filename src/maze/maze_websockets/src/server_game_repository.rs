use crate::{
	server_game::ServerGame,
	utils,
};

use maze_ws_proto::{
	GameId,
	Protocol,
};

use unitracer::{
	get_span_context,
	span,
};

use uniws::{
	Handshake,
	UniWs,
};

use std::{
	collections::HashMap,
	sync::mpsc::{
		self,
		Receiver,
		Sender,
	},
};

pub type ConnectUserRequest = (Handshake, UniWs<Protocol>);

pub struct ServiceGameRepository {
	receiver: Receiver<ConnectUserRequest>,
	sender: Sender<ConnectUserRequest>,
	active_games: HashMap<i64, Sender<UniWs<Protocol>>>,
	last_game_id: GameId,
}

impl ServiceGameRepository {
	pub fn new() -> ServiceGameRepository {
		span!(ServiceGameReposistory_new);
		let (sender, receiver) = mpsc::channel();
		return ServiceGameRepository {
			active_games: HashMap::new(),
			sender,
			receiver,
			last_game_id: 7,
		};
	}

	pub fn sender(&self) -> Sender<ConnectUserRequest> {
		return self.sender.clone();
	}

	fn found_game_by_id(
		&mut self,
		game_id: &GameId,
	) -> Option<Sender<UniWs<Protocol>>> {
		span!(found_game_by_id);
		return match self.active_games.get(game_id) {
			Some(s) => Some(s.clone()),
			None => None,
		};
	}

	fn create_new_game(&mut self) -> Option<Sender<UniWs<Protocol>>> {
		span!(create_new_game);

		let tick_length = 20;
		let time_shift = 20;

		self.last_game_id += 1;
		let game_id = self.last_game_id;

		return match utils::default_game() {
			Some((game, resources)) => {
				let mut server_game =
					ServerGame::new(game, resources, tick_length, game_id, time_shift);

				let new_user_sender = server_game.get_user_adder();
				let span_context = get_span_context!();
				std::thread::spawn(move || {
					server_game.process(span_context);
				});

				self.active_games.insert(game_id, new_user_sender.clone());

				Some(new_user_sender)
			},
			None => {
				error!("couldn't load the game");
				None
			},
		};
	}

	fn connect_to_game(
		&mut self,
		game_id: Option<GameId>,
		user_channel: UniWs<Protocol>,
	) -> Result<(), ()> {
		let new_user_sender = match game_id {
			Some(game_id) => self.found_game_by_id(&game_id),
			None => self.create_new_game(),
		};

		return match new_user_sender {
			Some(new_user_sender) => {
				match new_user_sender.send(user_channel) {
					Ok(()) => {
						info!("user channel send");
						Ok(())
					},
					Err(e) => {
						error!("user channel send error:{:?}", e);
						Err(())
					},
				}
			},
			None => Err(()),
		};
	}

	pub fn process(&mut self) {
		let base_url = url::Url::parse("http://localhost");
		match base_url {
			Ok(base_url) => {
				while let Ok((handshake, user_channel)) = self.receiver.recv() {
					span!(new_client);
					let url = base_url.join(&handshake.resource);
					match url {
						Ok(url) => {
							let game_id = maze_ws_proto::game_id_from_url(&url);
							info!("handhsake path:{:?} game_id:{:?}", url, game_id);

							match self.connect_to_game(game_id, user_channel) {
								Ok(()) => info!("connected to game ok"),
								Err(e) => error!("connected to game fail:{:?}", e),
							};
						},
						Err(e) => {
							warn!(
								"incorrect url from client e:{:?}, str:{}",
								e, handshake.resource
							)
						},
					}
				}
			},
			Err(e) => {
				error!("base url parsing fatal error:{:?}", e);
			},
		}
	}
}
