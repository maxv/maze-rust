use crate::server_game_repository::ServiceGameRepository;

use maze_core::*;

use maze_ws_proto::{
	protocol::Resource,
	Protocol,
};

use uniws::{
	Format,
	UniWs,
	UniWsTrait,
};

pub fn default_game() -> Option<(LocalGame, Vec<Resource>)> {
	return maze_ws_proto::utils::load_game(include_bytes!("../levels/first_level.tar"));
}

pub fn start_server(host_port: &str) {
	let mut repository = ServiceGameRepository::new();
	let repository_channel = repository.sender();
	UniWs::<Protocol>::listen(
		host_port.to_string(),
		Format::Binary,
		move |uniws, handshake| {
			match repository_channel.send((handshake, uniws)) {
				Ok(()) => true,
				Err(_) => false,
			}
		},
	);

	repository.process();
}
