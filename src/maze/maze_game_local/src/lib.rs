#[macro_use]
extern crate log;

use uniui_base::Application;

pub mod client_game_local;
mod main_widget;

#[uniui_gui::u_main]
pub fn app_main(app: &mut dyn Application) {
	let url = maze_uniui_components::utils::get_ws_url(app, "tracer/");
	unitracer_ws_client::init(url);
	unitracer::service_global!(web_client);

	uniui_base::utils::init_logger(app);

	unitracer::start_trace!(init_application);
	main_widget::MainWidget::start_for(app);
}
