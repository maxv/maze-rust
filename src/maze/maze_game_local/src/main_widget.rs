use crate::client_game_local::ClientGameLocal;

use maze_uniui_components::{
	gamepad_controller::GamepadController,
	keyboard_controller::KeyboardController,
	renderer::{
		ImageProvider,
		Renderer,
	},
	touch_screen_controller::DirectionController,
};

use maze_ws_proto::{
	protocol::Resource,
	utils,
};

use maze_core::{
	buttons::Button as GameButton,
	Game,
	LocalGame,
};

use uniui_base::{
	prelude::*,
	Color,
};
use uniui_gamepad::Factory as GamepadFactory;
use uniui_scene::{
	FlexPosition,
	FlexSize,
	PositionRequest,
	Scene as GraphicScene,
};
use uniui_widget_canvas::Canvas;
use uniui_widget_fileloader::FileLoader;
use uniui_wrapper_keyboardlistener::KeyboardListener;
use uniui_wrapper_touchlistener::TouchListener;

use std::sync::{
	RwLock,
	Weak,
};

#[derive(uniui_gui::Widget)]
pub struct MainWidget {
	#[base_widget]
	layout: LinearLayout,

	#[uprocess_last(on_file_loaded)]
	slot_file_loaded: SlotImpl<Vec<u8>>,

	slotproxy_set_game: SlotProxy<LocalGame>,
	slotproxy_add_resource: SlotProxy<Resource>,
}

impl MainWidget {
	pub fn start_for(app: &mut dyn Application) {
		let image_provider = ImageProvider::new();
		let slotproxy_add_resource = image_provider.slot_add_image().proxy();
		let image_provider = Application::add_data_processor(app, image_provider);

		let game = ClientGameLocal::new();
		let slotproxy_button_pressed = game.slot_button_pressed().proxy();
		let slotproxy_set_game = game.slot_game_updated().proxy();
		let slotproxy_restart_game = game.slot_restart_game().proxy();
		let slotproxy_drop_user = game.slot_drop_user().proxy();
		let game = Application::add_data_processor(app, game);


		let mut layout = LinearLayout::new(Orientation::Horizontal);

		let mut keyboard_controller = KeyboardController::new();
		keyboard_controller
			.signal_button_pressed()
			.connect_slot(&slotproxy_button_pressed);
		let slotproxy_key_pressed = keyboard_controller.slot_key_pressed().proxy();
		Application::add_data_processor(app, keyboard_controller);

		let receiver = match GamepadFactory::receiver() {
			Ok(r) => r,
			Err(e) => {
				log::warn!(
					"Couldn't obtain gamepad receiver. Default to empty. Err:{:?}",
					e
				);
				let (_, receiver) = std::sync::mpsc::channel();
				receiver
			},
		};

		let mut gamepad_controller = GamepadController::new(receiver);
		gamepad_controller.game_action_signal().connect_slot(&slotproxy_button_pressed);
		Application::add_data_processor(app, gamepad_controller);

		let widget = create_scene(
			game,
			image_provider,
			&slotproxy_button_pressed,
			&slotproxy_key_pressed,
		);
		layout.push_widget(widget);

		let slot_file_loaded = SlotImpl::new();
		let sidebar = create_sidebar(
			&slotproxy_drop_user,
			&slotproxy_restart_game,
			&slot_file_loaded,
		);
		layout.push_widget(sidebar);

		app.set_main_widget(Box::new(MainWidget {
			layout,
			slot_file_loaded,
			slotproxy_set_game,
			slotproxy_add_resource,
		}));
	}

	fn on_file_loaded(
		&mut self,
		data: Vec<u8>,
	) {
		info!("on_file_loaded size:{}", data.len());
		match utils::load_game(&data) {
			Some((game, resources)) => {
				for r in resources {
					self.slotproxy_add_resource.exec_for(r);
				}

				self.slotproxy_set_game.exec_for(game);
				trace!("game loaded");
			},
			None => error!("couldn't load game from data"),
		}
	}
}

fn create_scene(
	game: Weak<RwLock<dyn Game>>,
	image_provider: Weak<RwLock<ImageProvider>>,
	slot_button_pressed: &dyn Slot<GameButton>,
	slot_keydown: &dyn Slot<u32>,
) -> KeyboardListener<TouchListener<Canvas<GraphicScene>>> {
	let renderer = Renderer::new(game, image_provider);
	match renderer.viewport().upgrade() {
		Some(viewport) => {
			match viewport.write() {
				Ok(mut viewport) => {
					viewport.cell_width = 30;
					viewport.cell_height = 30;
					viewport.left_cell = 0;
					viewport.top_cell = 0;
					viewport.width_cell_count = 15;
					viewport.height_cell_count = 15;
				},
				Err(e) => error!("viewport lock failed:{:?}", e),
			}
		},
		None => error!("viewport missed"),
	};

	let mut scene = GraphicScene::new(Color {
		red: 33,
		green: 33,
		blue: 33,
	});
	scene.add_item(renderer, PositionRequest::full());

	let mut touch_direction_controller = DirectionController::new();
	touch_direction_controller.game_action_signal().connect_slot(slot_button_pressed);

	scene.add_item(touch_direction_controller, PositionRequest {
		x: FlexPosition::Min,
		y: FlexPosition::Max,
		width: FlexSize::Exact(100.0),
		height: FlexSize::Exact(100.0),
	});

	let canvas = scene.to_canvas_widget();

	let mut key_listener = KeyboardListener::new(canvas);
	key_listener.keydown_signal().connect_slot(slot_keydown);

	return key_listener;
}

fn create_sidebar(
	slot_drop_user: &dyn Slot<()>,
	slot_restart_game: &dyn Slot<()>,
	slot_file_loaded: &dyn Slot<Vec<u8>>,
) -> LinearLayout {
	let mut layout = LinearLayout::new(Orientation::Vertical);

	layout.push_widget({
		let mut file_loader = FileLoader::new();
		file_loader.signal_file_loaded().connect_slot(slot_file_loaded);
		MinimalSize {
			widget: file_loader,
		}
	});

	layout.push_widget({
		let mut button = Button::new("Restart".to_string());
		button.signal_clicked().connect_slot(slot_restart_game);
		button
	});

	layout.push_widget({
		let mut button = Button::new("Drop user".to_string());
		button.signal_clicked().connect_slot(slot_drop_user);
		button
	});

	return layout;
}
