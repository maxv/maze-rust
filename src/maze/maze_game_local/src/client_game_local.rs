use maze_uniui_components::ticker::Ticker;

use maze_core::{
	buttons::Button,
	Game,
	LocalGame,
	Maze,
	TickAction,
	User,
	UserId,
};

use uniui_base::prelude::*;

use std::collections::HashMap;

#[derive(uniui_gui::DataProcessor)]
#[uprocess_self(data_process)]
pub struct ClientGameLocal {
	last_button: Option<Button>,

	#[public_slot]
	#[uprocess_last(last_button=Some)]
	slot_button_pressed: SlotImpl<Button>,

	#[public_slot]
	#[uprocess_last(set_game)]
	slot_game_updated: SlotImpl<LocalGame>,

	#[public_slot]
	#[uprocess_last(restart)]
	slot_restart_game: SlotImpl<()>,

	#[public_slot]
	#[uprocess_last(drop_user)]
	slot_drop_user: SlotImpl<()>,

	ticker: Ticker,
	user_id: UserId,

	local_game: LocalGame,
	restart_state: LocalGame,
}

impl ClientGameLocal {
	pub fn new() -> ClientGameLocal {
		return ClientGameLocal {
			last_button: None,
			slot_button_pressed: SlotImpl::new(),
			slot_game_updated: SlotImpl::new(),
			slot_restart_game: SlotImpl::new(),
			slot_drop_user: SlotImpl::new(),
			ticker: Ticker::new(),
			user_id: 0,
			local_game: LocalGame::empty(),
			restart_state: LocalGame::empty(),
		};
	}

	pub fn data_process<T>(
		&mut self,
		msec: i64,
		_: T,
	) {
		if let Some(tick_num) = self.ticker.next_tick(msec) {
			let actions = self.last_button.take().map(|button| {
				(self.user_id, TickAction {
					button,
					tick_num: tick_num - 1,
				})
			});

			self.local_game.tick(actions.iter(), tick_num);
		}
	}

	fn set_game(
		&mut self,
		game: LocalGame,
	) {
		self.restart_state = game;
		self.restart(());
	}

	fn restart(
		&mut self,
		_: (),
	) {
		self.last_button = None;
		self.local_game = self.restart_state.clone();
		self.ticker.set_last_tick_num(self.local_game.tick_num());
	}

	fn drop_user(
		&mut self,
		_: (),
	) {
		match self.local_game.respawn_points().get(0).cloned() {
			Some(user) => self.user_id = self.local_game.add_user(user),
			None => error!("respawn point not found"),
		};
	}
}


impl Game for ClientGameLocal {
	fn maze(&self) -> &Maze {
		return self.local_game.maze();
	}

	fn opponents(&self) -> &HashMap<UserId, User> {
		return self.local_game.opponents();
	}

	fn tick_num(&self) -> u64 {
		return self.local_game.tick_num();
	}

	fn respawn_points(&self) -> &Vec<User> {
		return self.local_game.respawn_points();
	}
}
