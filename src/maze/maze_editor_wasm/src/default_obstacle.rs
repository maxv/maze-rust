use maze_ws_proto::protocol::Resource;

use maze_core::{
	DrawInfo,
	Obstacle,
};

pub fn resource() -> Resource {
	let mut data = Vec::new();
	let width = 30;
	let height = 30;
	for _ in 0..(width * height) {
		data.push(210);
		data.push(180);
		data.push(21);
		data.push(255);
	}
	return Resource("/img/empty.png".to_string(), data, width, height);
}

pub fn empty() -> Obstacle {
	let draw_info = DrawInfo {
		resource: "/img/empty.png".to_string(),
		left: 0,
		top: 0,
		width: 30,
		height: 30,
		dx: 0,
		dy: 0,
	};
	return Obstacle::new_static(draw_info, true);
}
