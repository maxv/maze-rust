use crate::{
	decode_png::decode_png,
	default_obstacle,
};

use maze_ws_proto::{
	protocol::Resource,
	user_template::UserTemplate,
};

use maze_core::{
	DrawInfo,
	Obstacle,
	State,
};

use uniui_base::prelude::*;
use uniui_widget_checkbox::CheckBox;
use uniui_widget_combobox::prelude::*;
use uniui_widget_fileloader::FileLoader;


#[derive(uniui_gui::Widget)]
pub struct ObstacleEditor {
	#[base_widget]
	layout: LinearLayout,

	resource_name: usize,
	obstacle: Obstacle,

	populated_current_state: usize,
	x: usize,
	y: usize,

	#[public_signal]
	signal_obstacle_changed: Signal<(Obstacle, usize, usize)>,

	#[public_signal]
	signal_activated: Signal<SlotProxy<(Obstacle, usize, usize)>>,

	#[uproperty_process(selected_state_updated)]
	current_state: Property<usize>,

	#[uprocess_last(set_obstacle)]
	slot_edit_obstacle: SlotImpl<(Obstacle, usize, usize)>,

	#[uprocess_last(on_activated)]
	slot_activated: SlotImpl<()>,

	signal_states_updated: Signal<Vec<(usize, String)>>,

	#[uprocess_last(on_after_activated_changed)]
	slot_after_activated_changed: SlotImpl<usize>,
	signal_set_after_activated: Signal<usize>,

	#[uprocess_last(on_is_movable_changed)]
	slot_is_movable_changed: SlotImpl<bool>,
	signal_set_is_movable: Signal<bool>,

	#[uprocess_last(on_after_kill_all_changed)]
	slot_after_kill_all_changed: SlotImpl<usize>,
	signal_set_after_kill_all: Signal<usize>,

	#[uprocess_last(on_after_deactivated_changed)]
	slot_after_deactivated_changed: SlotImpl<usize>,
	signal_set_after_deactivated: Signal<usize>,

	#[uprocess_last(on_after_triggered_changed)]
	slot_after_triggered_changed: SlotImpl<usize>,
	signal_set_after_triggered: Signal<usize>,

	#[uprocess_each(on_state_loaded)]
	slot_file_loaded: SlotImpl<Vec<u8>>,

	#[uprocess_last(delete_current_state)]
	slot_delete_current_state: SlotImpl<()>,

	#[public_signal]
	signal_add_to_collection: Signal<Obstacle>,

	#[uprocess_last(on_add_to_collection)]
	slot_add_to_collection_clicked: SlotImpl<()>,

	#[public_signal]
	signal_new_image: Signal<Resource>,
}

impl ObstacleEditor {
	pub fn new() -> ObstacleEditor {
		let current_state = Property::new(0);
		let mut signal_states_updated = Signal::new();

		let mut layout = LinearLayout::new(Orientation::Vertical);

		let slot_activated = SlotImpl::new();
		layout.push_widget({
			let mut start_selection = Button::new("Select".to_string());
			start_selection.signal_clicked().connect_slot(&slot_activated);

			MinimalSize {
				widget: start_selection,
			}
		});

		let slot_delete_current_state = SlotImpl::new();
		layout.push_widget({
			let mut layout = LinearLayout::new(Orientation::Horizontal);
			layout.push_widget({
				let mut edit = ComboBox::new();
				signal_states_updated.connect_slot(edit.slot_set_items());
				edit.signal_selection_changed().connect_slot(current_state.slot());
				edit
			});
			layout.push_widget({
				let mut button = Button::new("X".to_string());
				button.signal_clicked().connect_slot(&slot_delete_current_state);
				MinimalSize {
					widget: button,
				}
			});

			MinimalSize {
				widget: layout,
			}
		});


		let (slot_after_activated_changed, signal_set_after_activated) =
			ObstacleEditor::add_combo_box(
				"activated".to_string(),
				&mut layout,
				&mut signal_states_updated,
			);
		let (slot_after_deactivated_changed, signal_set_after_deactivated) =
			ObstacleEditor::add_combo_box(
				"deactivated".to_string(),
				&mut layout,
				&mut signal_states_updated,
			);
		let (slot_after_triggered_changed, signal_set_after_triggered) =
			ObstacleEditor::add_combo_box(
				"triggered".to_string(),
				&mut layout,
				&mut signal_states_updated,
			);
		let (slot_after_kill_all_changed, signal_set_after_kill_all) =
			ObstacleEditor::add_combo_box(
				"kill all".to_string(),
				&mut layout,
				&mut signal_states_updated,
			);

		let mut signal_set_is_movable = Signal::new();
		let slot_is_movable_changed = SlotImpl::new();
		layout.push_widget({
			let mut check_box = CheckBox::new(format!("is movable"), false);
			check_box.signal_checkstate_changed().connect_slot(&slot_is_movable_changed);
			signal_set_is_movable.connect_slot(check_box.slot_set_checked());


			MinimalSize {
				widget: check_box,
			}
		});

		let slot_file_loaded = SlotImpl::new();
		layout.push_widget({
			let mut file_loader = FileLoader::new();
			file_loader.signal_file_loaded().connect_slot(&slot_file_loaded);


			MinimalSize {
				widget: file_loader,
			}
		});

		let slot_add_to_collection_clicked = SlotImpl::new();
		layout.push_widget({
			let mut add_to_collection_btn = Button::new("Add to collection".to_string());
			add_to_collection_btn
				.signal_clicked()
				.connect_slot(&slot_add_to_collection_clicked);

			MinimalSize {
				widget: add_to_collection_btn,
			}
		});

		return ObstacleEditor {
			resource_name: 0,
			x: 0,
			y: 0,
			obstacle: default_obstacle::empty(),
			current_state,

			signal_obstacle_changed: Signal::new(),
			slot_edit_obstacle: SlotImpl::new(),
			slot_activated,
			signal_activated: Signal::new(),

			slot_after_activated_changed,
			signal_set_after_activated,

			slot_after_deactivated_changed,
			signal_set_after_deactivated,

			slot_after_triggered_changed,
			signal_set_after_triggered,

			slot_after_kill_all_changed,
			signal_set_after_kill_all,

			signal_states_updated,
			layout,

			signal_set_is_movable,
			slot_is_movable_changed,

			slot_file_loaded,
			populated_current_state: 0,

			slot_delete_current_state,

			signal_add_to_collection: Signal::new(),
			slot_add_to_collection_clicked,
			signal_new_image: Signal::new(),
		};
	}

	fn set_obstacle(
		&mut self,
		(o, x, y): (Obstacle, usize, usize),
	) {
		let mut states = Vec::new();
		for id in 0..o.states().len() {
			states.push((id, id.to_string()));
		}

		self.obstacle = o;
		self.x = x;
		self.y = y;

		self.signal_states_updated.emit(states);
		self.signal_obstacle_changed.emit((self.obstacle.clone(), x, y));
		self.populate_state_with_id(0);
	}

	fn selected_state_updated(&mut self) {
		let state_id = *self.current_state.as_ref();
		if self.populated_current_state != state_id {
			self.populate_state_with_id(state_id);
		}
	}

	fn populate_state_with_id(
		&mut self,
		state_id: usize,
	) {
		match self.obstacle.states().get(state_id) {
			Some(s) => {
				self.signal_set_after_deactivated.emit(s.after_deactivate);
				self.signal_set_after_activated.emit(s.after_activate);
				self.signal_set_after_triggered.emit(s.after_triggered);
				self.signal_set_after_kill_all.emit(s.all_killed);
				self.signal_set_is_movable.emit(s.is_movable);
				self.populated_current_state = state_id;
			},
			None => error!("state not found for id:{}", state_id),
		}
	}

	fn update_current_state(
		&mut self,
		f: &dyn Fn(&mut State),
	) {
		let state_id = *self.current_state.as_ref();
		match self.obstacle.states_mut().get_mut(state_id) {
			Some(s) => f(s),
			None => error!("state not found for id:{}", state_id),
		}

		self.selected_state_updated();
	}

	fn delete_current_state(
		&mut self,
		_: (),
	) {
		let mut states = self.obstacle.states().clone();
		let state_id = *self.current_state.as_ref();
		states.remove(state_id);

		self.set_obstacle((Obstacle::new(states, 0, 0), self.x, self.y));
	}

	fn add_combo_box(
		label: String,
		layout: &mut LinearLayout,
		signal_states_updated: &mut Signal<Vec<(usize, String)>>,
	) -> (SlotImpl<usize>, Signal<usize>) {
		let mut signal = Signal::new();
		let slot = SlotImpl::new();
		layout.push_widget({
			let mut layout = LinearLayout::new(Orientation::Horizontal);
			layout.push_widget(Label::new(label));
			layout.push_widget({
				let mut edit = ComboBox::new();
				signal_states_updated.connect_slot(edit.slot_set_items());
				edit.signal_selection_changed().connect_slot(&slot);
				signal.connect_slot(edit.slot_set_selected_value());
				edit
			});
			MinimalSize {
				widget: layout,
			}
		});

		return (slot, signal);
	}

	fn new_state_from_tar(
		&mut self,
		v: Vec<u8>,
	) -> Option<State> {
		let oo: Option<UserTemplate<DrawInfo>> = UserTemplate::from_tar(&v);
		return match oo {
			Some(mut user_template) => {
				match decode_png(&user_template.png) {
					Some((data, height, width)) => {
						self.resource_name += 1;
						let resource_name = format!("o-{}.png", self.resource_name);
						self.signal_new_image.emit(Resource(
							resource_name.clone(),
							data,
							width,
							height,
						));

						let mut state = State::default();
						user_template.maneuvers.iter_mut().for_each(|di| {
							di.resource = resource_name.clone();
						});
						state.draw_infos = user_template.maneuvers;
						Some(state)
					},
					None => None,
				}
			},
			None => {
				error!("user template processing failed");
				None
			},
		};
	}

	fn on_state_loaded(
		&mut self,
		v: Vec<u8>,
	) {
		let state = self.new_state_from_tar(v);
		match state {
			Some(state) => {
				let mut states = self.obstacle.states().clone();
				states.push(state);
				self.set_obstacle((Obstacle::new(states, 0, 0), self.x, self.y));
			},
			None => error!("state parsing failed"),
		}
	}

	fn on_activated(
		&mut self,
		_: (),
	) {
		self.signal_activated.emit(self.slot_edit_obstacle.proxy());
	}

	fn on_after_activated_changed(
		&mut self,
		i: usize,
	) {
		self.update_current_state(&|s| s.after_activate = i);
	}

	fn on_after_deactivated_changed(
		&mut self,
		i: usize,
	) {
		self.update_current_state(&|s| s.after_deactivate = i);
	}

	fn on_after_triggered_changed(
		&mut self,
		i: usize,
	) {
		self.update_current_state(&|s| s.after_triggered = i);
	}

	fn on_after_kill_all_changed(
		&mut self,
		i: usize,
	) {
		self.update_current_state(&|s| s.all_killed = i);
	}

	fn on_is_movable_changed(
		&mut self,
		b: bool,
	) {
		self.update_current_state(&|s| s.is_movable = b);
	}

	fn on_add_to_collection(
		&mut self,
		_: (),
	) {
		self.signal_add_to_collection.emit(self.obstacle.clone());
	}
}
