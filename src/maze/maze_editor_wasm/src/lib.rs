#[macro_use]
extern crate log;

use maze_uniui_components::{
	renderer::ImageProvider,
	Renderer,
};

use maze_core::LocalGame;

use uniui_base::{
	prelude::*,
	Color,
};
use uniui_layout_recyclerlayout::RecyclerLayout;
use uniui_scene::{
	FlexPosition,
	FlexSize,
	PositionRequest,
	Scene as GraphicScene,
};
use uniui_widget_tabwidget::TabWidget;

mod editor_game;
use editor_game::{
	CurrentAction,
	EditorGame,
};

mod decode_png;
mod default_obstacle;
mod obstacle_collection;
mod obstacle_editor;
mod obstacle_view;
mod resizer;
mod respawn_points_overlay;
mod user_collection;

#[uniui_gui::u_main]
pub fn app_main(app: &mut dyn Application) {
	uniui_base::utils::init_logger(app);
	warn!("Hello");

	let image_provider = ImageProvider::new();
	image_provider.slot_add_image().exec_for(default_obstacle::resource());

	let slotproxy_add_image = image_provider.slot_add_image().proxy();
	let image_provider_ref = Application::add_data_processor(app, image_provider);
	let mut editor = EditorGame::new(LocalGame::empty(), image_provider_ref.clone());

	let mut layout = LinearLayout::new(Orientation::Horizontal);

	layout.push_widget({
		let mut layout = LinearLayout::new(Orientation::Vertical);

		layout.push_widget({
			let mut button = Button::new("Export".to_string());
			button.signal_clicked().connect_slot(editor.slot_export_game());
			MinimalSize {
				widget: button,
			}
		});

		layout.push_widget({
			let (mut resizer, layout) =
				resizer::simple_instance(5, 5, default_obstacle::empty(), app);
			resizer.signal_resize_requested().connect_slot(editor.slot_resize_game());
			Application::add_data_processor(app, resizer);
			MinimalSize {
				widget: layout,
			}
		});

		layout.push_widget({
			let mut collections = TabWidget::new();

			collections.push_widget("Users".to_string(), {
				let mut user_collection = user_collection::UserCollection::new();

				user_collection.signal_new_image().connect_slot(&slotproxy_add_image);

				let slot_proxy = editor.slot_set_current_action().proxy();
				user_collection.signal_user_selected().connect_func(move |u| {
					slot_proxy.exec_for(CurrentAction::PlaceRespawnPoint(u.clone()));
				});

				user_collection
			});

			let add_to_collection_proxy;
			collections.push_widget("Obstacles".to_string(), {
				let mut obstacle_collection =
					obstacle_collection::ObstacleCollection::new(
						default_obstacle::empty(),
						image_provider_ref.clone(),
					);

				let slot_proxy = editor.slot_set_current_action().proxy();
				obstacle_collection.signal_obstacle_selected().connect_func(move |o| {
					slot_proxy.exec_for(CurrentAction::PlaceObstacle(o.clone()));
				});

				add_to_collection_proxy = obstacle_collection.slot_add_obstacle().proxy();

				RecyclerLayout::new(obstacle_collection, Orientation::Vertical)
			});

			collections.push_widget("Editor".to_string(), {
				let mut obstacle_editor = obstacle_editor::ObstacleEditor::new();

				let slot_proxy = editor.slot_set_current_action().proxy();
				obstacle_editor.signal_activated().connect_func(move |s| {
					slot_proxy.exec_for(CurrentAction::SelectAndEdit(s.proxy()));
				});

				obstacle_editor.signal_new_image().connect_slot(&slotproxy_add_image);

				obstacle_editor
					.signal_obstacle_changed()
					.connect_slot(editor.slot_set_obstacle());

				obstacle_editor
					.signal_add_to_collection()
					.connect_slot(&add_to_collection_proxy);

				obstacle_editor
			});

			collections
		});

		MinimalSize {
			widget: layout,
		}
	});

	let obstacle_view = obstacle_view::ObstacleView::new(image_provider_ref.clone());
	editor
		.signal_current_action_updated()
		.connect_slot(obstacle_view.slot_set_current_action());

	let editor_ref = Application::add_data_processor(app, editor);

	layout.push_widget({
		let mut renderer = Renderer::new(editor_ref.clone(), image_provider_ref.clone());
		renderer.add_overlay(respawn_points_overlay::RespawnPointsOverlay::new());
		editor_ref.upgrade().iter().for_each(|v| {
			match v.read() {
				Ok(e) => {
					renderer.signal_cell_clicked().connect_slot(e.slot_cell_selected());
				},
				Err(e) => error!("editor not available e:{:?}", e),
			}
		});

		match renderer.viewport().upgrade() {
			Some(viewport) => {
				match viewport.write() {
					Ok(mut viewport) => {
						viewport.cell_width = 30;
						viewport.cell_height = 30;
						viewport.left_cell = 0;
						viewport.top_cell = 0;
						viewport.width_cell_count = 15;
						viewport.height_cell_count = 15;
					},
					Err(e) => error!("viewport lock failed:{:?}", e),
				}
			},
			None => error!("viewport missed"),
		};

		let mut scene = GraphicScene::new(Color {
			red: 33,
			green: 33,
			blue: 33,
		});
		scene.add_item(renderer, PositionRequest::full());


		scene.add_item(obstacle_view, PositionRequest {
			x: FlexPosition::Min,
			y: FlexPosition::Max,
			width: FlexSize::Exact(250.0),
			height: FlexSize::Exact(250.0),
		});

		scene.to_canvas_widget()
	});

	app.set_main_widget(Box::new(layout));
}
