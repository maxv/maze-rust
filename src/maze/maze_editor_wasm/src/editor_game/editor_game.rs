use super::{
	game_saver,
	CurrentAction,
};

use maze_uniui_components::renderer::ImageProvider;

use maze_core::{
	Game,
	LocalGame,
	Maze,
	Obstacle,
	User,
	UserId,
};

use uniui_base::prelude::*;

use std::{
	collections::HashMap,
	sync::{
		RwLock,
		Weak,
	},
};


#[derive(uniui_gui::DataProcessor)]
pub struct EditorGame {
	image_provider: Weak<RwLock<ImageProvider>>,
	local_game: LocalGame,

	#[public_slot]
	#[uprocess_last(resize_game)]
	slot_resize_game: SlotImpl<(usize, usize, Obstacle)>,

	#[public_slot]
	#[uprocess_last(on_cell_selected)]
	slot_cell_selected: SlotImpl<(usize, usize)>,

	#[public_slot]
	#[uprocess_each(on_set_obstacle)]
	slot_set_obstacle: SlotImpl<(Obstacle, usize, usize)>,

	#[uproperty_process]
	#[uproperty_public_signal(signal_current_action_updated)]
	#[uproperty_public_slot(slot_set_current_action)]
	current_action: Property<CurrentAction>,

	#[public_slot]
	#[uprocess_last(save_game)]
	slot_export_game: SlotImpl<()>,
}

impl EditorGame {
	pub fn new(
		initial_game: LocalGame,
		image_provider: Weak<RwLock<ImageProvider>>,
	) -> EditorGame {
		return EditorGame {
			image_provider,
			local_game: initial_game,
			slot_resize_game: SlotImpl::new(),
			slot_cell_selected: SlotImpl::new(),
			slot_set_obstacle: SlotImpl::new(),
			current_action: Property::new(CurrentAction::None),
			slot_export_game: SlotImpl::new(),
		};
	}

	fn save_game(
		&mut self,
		_: (),
	) {
		match self.image_provider.upgrade() {
			Some(image_provider) => {
				match image_provider.write() {
					Ok(mut image_provider) => {
						game_saver::save_game(
							&mut image_provider,
							self.local_game.clone(),
						);
					},
					Err(e) => error!("image provider write error:{:?}", e),
				};
			},
			None => error!("image provider failed"),
		}
	}

	fn resize_game(
		&mut self,
		(x, y, default_obstacle): (usize, usize, Obstacle),
	) {
		let mut maze = Maze::new(x, y, default_obstacle);
		let current_maze = self.local_game.maze();

		let min_width = std::cmp::min(maze.size().0, current_maze.size().0);
		let min_height = std::cmp::min(maze.size().1, current_maze.size().1);
		for i in 0..min_width {
			for j in 0..min_height {
				match current_maze.get(i, j) {
					Ok(o) => {
						match maze.set(i, j, o.clone()) {
							Ok(()) => trace!("updated maze for ({}, {})", i, j),
							Err(()) => error!("maze set failed for ({}, {})", i, j),
						}
					},
					Err(()) => {
						error!("current maze do not have obstacle for ({}, {})", i, j)
					},
				};
			}
		}

		self.local_game.replace_maze(maze);
	}

	fn on_cell_selected(
		&mut self,
		(x, y): (usize, usize),
	) {
		self.current_action.as_ref().process_click(x, y, &mut self.local_game);
	}

	fn on_set_obstacle(
		&mut self,
		(o, x, y): (Obstacle, usize, usize),
	) {
		let mut maze = self.local_game.maze().clone();
		match maze.set(x, y, o) {
			Err(()) => error!("maze update failed"),
			Ok(()) => {
				trace!("maze_update_ok");
				self.local_game.replace_maze(maze);
			},
		};
	}
}

impl Game for EditorGame {
	fn maze(&self) -> &Maze {
		return self.local_game.maze();
	}

	fn opponents(&self) -> &HashMap<UserId, User> {
		return self.local_game.opponents();
	}

	fn tick_num(&self) -> u64 {
		return self.local_game.tick_num();
	}

	fn respawn_points(&self) -> &Vec<User> {
		return self.local_game.respawn_points();
	}
}
