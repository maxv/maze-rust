use maze_core::{
	Game,
	LocalGame,
	Obstacle,
	User,
};

use uniui_base::prelude::*;


#[derive(Clone)]
pub enum CurrentAction {
	PlaceObstacle(Obstacle),
	SelectAndEdit(SlotProxy<(Obstacle, usize, usize)>),
	PlaceRespawnPoint(User),
	None,
}

impl CurrentAction {
	pub fn process_click(
		&self,
		x: usize,
		y: usize,
		game: &mut LocalGame,
	) {
		match self {
			CurrentAction::PlaceObstacle(o) => place_obstacle(game, x, y, o.clone()),
			CurrentAction::PlaceRespawnPoint(u) => {
				place_respawn_point(game, x, y, u.clone())
			},
			CurrentAction::SelectAndEdit(s) => send_obstacle_to_slot(game, x, y, s),
			CurrentAction::None => {},
		}
	}
}

fn place_obstacle(
	game: &mut LocalGame,
	x: usize,
	y: usize,
	obstacle: Obstacle,
) {
	let mut maze = game.maze().clone();
	match maze.set(x, y, obstacle) {
		Ok(()) => trace!("maze updated"),
		Err(()) => error!("maze update failed. incorrect coord? ({}, {})", x, y),
	}
	game.replace_maze(maze);
}

fn place_respawn_point(
	game: &mut LocalGame,
	x: usize,
	y: usize,
	mut user: User,
) {
	user.update_coordinates(x, y);
	game.add_respawn_point(user);
}

fn send_obstacle_to_slot(
	game: &LocalGame,
	x: usize,
	y: usize,
	slot: &dyn Slot<(Obstacle, usize, usize)>,
) {
	if let Ok(o) = game.maze().get(x, y) {
		slot.exec_for((o.clone(), x, y));
	}
}
