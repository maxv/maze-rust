use maze_ws_proto::saved_level::SavedLevel;

use maze_uniui_components::renderer::ImageProvider;

use maze_core::{
	DrawInfo,
	LocalGame,
	User,
};

use uniui_widget_canvas::ImageData;

use png::{
	BitDepth,
	ColorType,
	Encoder,
};

use std::collections::HashMap;


fn for_all_draw_infos_from_user(
	u: &mut User,
	f: &mut dyn FnMut(&mut DrawInfo),
) {
	for m in u.maneuvers_mut().iter_mut() {
		f(&mut m.draw_info);
	}
}

fn for_all_draw_infos_from_game(
	game: &mut LocalGame,
	f: &mut dyn FnMut(&mut DrawInfo),
) {
	for o in game.maze_mut().iter() {
		for s in o.states_mut().iter_mut() {
			for di in s.draw_infos.iter_mut() {
				f(di);
			}

			match s.next.as_mut() {
				Some((_, e)) => {
					for u in e.born.iter_mut() {
						for_all_draw_infos_from_user(u, f);
					}
				},
				None => {},
			}
		}
	}

	for (_, u) in game.opponents_mut().iter_mut() {
		for_all_draw_infos_from_user(u, f);
	}

	for u in game.respawn_points_mut().iter_mut() {
		for_all_draw_infos_from_user(u, f);
	}
}

fn as_png(bytes: &ImageData) -> Vec<u8> {
	let mut result: Vec<u8> = Vec::new();
	{
		let mut encoder = Encoder::new(&mut result, bytes.width(), bytes.height());
		encoder.set_color(ColorType::RGBA);
		encoder.set_depth(BitDepth::Eight);
		match encoder.write_header() {
			Ok(mut writer) => {
				match writer.write_image_data(bytes.data()) {
					Ok(()) => trace!("extract_bytes ok"),
					Err(e) => error!("extract_bytes err:{:?}", e),
				}
			},
			Err(e) => error!("extract_bytes header write err:{:?}", e),
		}
	}
	return result;
}

pub fn save_game(
	image_provider: &mut ImageProvider,
	mut game: LocalGame,
) {
	let mut images = HashMap::new();
	for_all_draw_infos_from_game(&mut game, &mut |di| {
		if !images.contains_key(&di.resource) {
			match image_provider.get(&di.resource) {
				Some(bytes) => {
					let data = as_png(&bytes);
					images.insert(di.resource.clone(), data.clone());
				},
				None => error!("no bytes for url:{}", di.resource),
			}
		}
	});

	let saved_level = SavedLevel {
		game,
		images,
	};
	let tar = saved_level.to_tar();
	uniui_gui::utils::save_file(&tar, "level.tar");
}
