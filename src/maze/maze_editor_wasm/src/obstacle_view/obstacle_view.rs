use crate::editor_game::CurrentAction;

use maze_uniui_components::renderer::ImageProvider;

use maze_core::{
	DrawInfo,
	Drawable,
};

use uniui_base::prelude::*;
use uniui_scene::{
	Item as GraphicItem,
	Position as GraphicPosition,
};
use uniui_widget_canvas::Context as CanvasContext;

use std::sync::{
	RwLock,
	Weak,
};


pub struct ObstacleView {
	current_action: Property<CurrentAction>,
	image_provider: Weak<RwLock<ImageProvider>>,
	draw_info: Option<DrawInfo>,
}

impl ObstacleView {
	pub fn new(image_provider: Weak<RwLock<ImageProvider>>) -> ObstacleView {
		return ObstacleView {
			current_action: Property::new(CurrentAction::None),
			image_provider,
			draw_info: None,
		};
	}

	pub fn slot_set_current_action(&self) -> &dyn Slot<CurrentAction> {
		return self.current_action.slot();
	}
}

impl GraphicItem for ObstacleView {
	fn draw(
		&mut self,
		context: &mut dyn CanvasContext,
		pos: &GraphicPosition,
	) {
		if let Some(di) = self.draw_info.as_ref() {
			match self.image_provider.upgrade() {
				Some(ip) => {
					match ip.write() {
						Ok(mut image_provider) => {
							match image_provider.get(&di.resource) {
								Some(mut image) => {
									context.draw_imagebitmap_part(
										&mut image,
										pos.x,
										pos.y,
										di.left as f64,
										di.top as f64,
										di.width as f64,
										di.height as f64,
									);
								},
								None => {
									error!("no image for {}", di.resource);
								},
							}
						},
						Err(e) => error!("couldn't lock err:{:?}", e),
					}
				},
				None => {},
			}
		}
	}

	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		let updated = self.current_action.try_update();
		if updated {
			match self.current_action.as_ref() {
				CurrentAction::PlaceObstacle(o) => {
					self.draw_info = o.current_draw_info(0).map(|di| di.clone());
				},
				CurrentAction::PlaceRespawnPoint(u) => {
					self.draw_info = u.current_draw_info(0).map(|di| di.clone());
				},
				CurrentAction::SelectAndEdit(_) | CurrentAction::None => {
					self.draw_info = None;
				},
			}
		}
	}
}
