use crate::decode_png::decode_png;

use maze_ws_proto::{
	protocol::Resource,
	user_template::UserTemplate,
};

use maze_core::{
	maneuver::Maneuver,
	User,
};

use uniui_base::prelude::*;
use uniui_file::File;
use uniui_widget_fileselector::FileSelector;

#[derive(uniui_gui::Widget)]
pub struct UserCollection {
	#[base_widget]
	layout: LinearLayout,

	users: Vec<User>,

	#[uprocess_each(on_file_selected)]
	slot_file_selected: SlotImpl<Vec<File>>,

	#[uprocess_each(on_file_readed)]
	slot_file_readed: SlotImpl<Result<Vec<u8>, ()>>,

	#[public_signal]
	signal_user_selected: Signal<User>,

	#[public_signal]
	signal_new_image: Signal<Resource>,

	next_id: usize,
}

impl UserCollection {
	pub fn new() -> UserCollection {
		let slot_file_selected = SlotImpl::new();
		let layout = {
			let mut layout = LinearLayout::new(Orientation::Vertical);

			layout.push_widget({
				let mut file_selector = FileSelector::new();
				file_selector.signal_files_selected().connect_slot(&slot_file_selected);
				file_selector
			});

			layout
		};

		return UserCollection {
			layout,
			users: Vec::new(),
			signal_user_selected: Signal::new(),
			slot_file_selected,
			slot_file_readed: SlotImpl::new(),
			signal_new_image: Signal::new(),
			next_id: 0,
		};
	}

	fn on_file_selected(
		&mut self,
		vec_of_files: Vec<File>,
	) {
		for file in vec_of_files {
			file.read_all(self.slot_file_readed.proxy());
		}
	}

	fn on_file_readed(
		&mut self,
		res: Result<Vec<u8>, ()>,
	) {
		match res {
			Ok(v) => {
				let oo: Option<UserTemplate<Maneuver>> = UserTemplate::from_tar(&v);
				match oo {
					Some(mut user_template) => {
						match decode_png(&user_template.png) {
							Some((data, width, height)) => {
								self.next_id += 1;
								let name = format!("u-{}.png", self.next_id);
								self.signal_new_image.emit(Resource(
									name.clone(),
									data,
									width,
									height,
								));
								for maneuver in user_template.maneuvers.iter_mut() {
									maneuver.draw_info.resource = name.clone();
								}
								let user =
									User::new(0, 0, 10, 0, user_template.maneuvers);

								self.signal_user_selected.emit(user.clone());
								self.users.push(user);
							},
							None => {
								error!("png decoding failed");
							},
						}
					},
					None => error!("user template processing failed"),
				};
			},
			Err(()) => error!("file not reded"),
		}
	}
}
