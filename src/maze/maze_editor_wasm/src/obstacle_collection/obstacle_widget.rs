use maze_uniui_components::{
	draw_info_drawer::DrawInfoDrawer,
	renderer::ImageProvider,
};

use maze_core::{
	DrawInfo,
	Drawable,
	Obstacle,
};

use uniui_base::{
	prelude::*,
	SizeUnit,
};
use uniui_widget_canvas::Canvas;
use uniui_wrapper_fixedsize::prelude::*;
use uniui_wrapper_mouseclicklistener::prelude::*;

use std::sync::{
	RwLock,
	Weak,
};


#[derive(uniui_gui::Widget)]
pub struct ObstacleWidget {
	obstacle: Obstacle,

	#[base_widget]
	inner: MouseClickListener<FixedSize<Canvas<DrawInfoDrawer>>>,

	#[public_signal]
	signal_obstacle_selected: Signal<Obstacle>,

	#[uprocess_last(on_clicked)]
	slot_clicked: SlotImpl<(i32, i32)>,

	slotproxy_set_draw_info: SlotProxy<DrawInfo>,
}

impl ObstacleWidget {
	pub fn new(
		obstacle: Obstacle,
		image_provider: Weak<RwLock<ImageProvider>>,
	) -> ObstacleWidget {
		let slot_clicked = SlotImpl::new();

		let drawer = DrawInfoDrawer::new(
			image_provider,
			obstacle.current_draw_info(0).cloned().unwrap_or(DrawInfo::default()),
		);
		let slotproxy_set_draw_info = drawer.slot_set_draw_info().proxy();
		let mut canvas = Canvas::new();
		canvas.set_drawer(Some(drawer));

		let mut inner = MouseClickListener::new(u_fixedsize! {
			widget: canvas,
			width_strict: SizeUnit::PX(30.0),
			height_strict: SizeUnit::PX(30.0),
		});
		inner.signal_clicked().connect_slot(&slot_clicked);

		return ObstacleWidget {
			inner,
			obstacle,
			signal_obstacle_selected: Signal::new(),
			slot_clicked,
			slotproxy_set_draw_info,
		};
	}

	pub fn set_obstacle(
		&mut self,
		o: Obstacle,
	) {
		match o.current_draw_info(0) {
			Some(draw_info) => {
				self.slotproxy_set_draw_info.exec_for(draw_info.clone());
			},
			None => {
				error!("No draw info");
			},
		}
		self.obstacle = o;
	}

	fn on_clicked(
		&mut self,
		_: (i32, i32),
	) {
		self.signal_obstacle_selected.emit(self.obstacle.clone());
	}
}
