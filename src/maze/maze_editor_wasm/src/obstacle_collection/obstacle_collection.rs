use super::ObstacleWidget;

use maze_uniui_components::renderer::ImageProvider;

use maze_core::Obstacle;

use uniui_base::prelude::*;
use uniui_layout_recyclerlayout::{
	DataHolder,
	DataHolderUpdate,
};

use std::sync::{
	RwLock,
	Weak,
};


#[derive(uniui_gui::DataProcessor)]
pub struct ObstacleCollection {
	obstacles: Vec<Obstacle>,
	slotproxy_collection_updated: SlotProxy<DataHolderUpdate>,
	default_obstacle: Obstacle,

	#[public_signal]
	signal_obstacle_selected: Signal<Obstacle>,

	#[public_slot]
	#[uprocess_each(on_obstacle_added)]
	slot_add_obstacle: SlotImpl<Obstacle>,

	#[uprocess_last(signal_obstacle_selected.emit)]
	slot_obstacle_selected: SlotImpl<Obstacle>,

	image_provider: Weak<RwLock<ImageProvider>>,
}

impl ObstacleCollection {
	pub fn new(
		default_obstacle: Obstacle,
		image_provider: Weak<RwLock<ImageProvider>>,
	) -> ObstacleCollection {
		return ObstacleCollection {
			obstacles: Vec::new(),
			default_obstacle,
			signal_obstacle_selected: Signal::new(),
			slot_obstacle_selected: SlotImpl::new(),
			slotproxy_collection_updated: SlotProxy::empty(),
			slot_add_obstacle: SlotImpl::new(),
			image_provider,
		};
	}

	fn on_obstacle_added(
		&mut self,
		o: Obstacle,
	) {
		self.obstacles.push(o);
		self.slotproxy_collection_updated.exec_for(DataHolderUpdate::WholeData);
	}
}

impl DataHolder<ObstacleWidget> for ObstacleCollection {
	fn count(&self) -> usize {
		return self.obstacles.len();
	}

	fn setup_update_slot(
		&mut self,
		slot: &dyn Slot<DataHolderUpdate>,
	) {
		self.slotproxy_collection_updated = slot.proxy();
	}

	fn fill(
		&self,
		id: usize,
		widget: &mut ObstacleWidget,
	) {
		match self.obstacles.get(id) {
			Some(o) => {
				widget.set_obstacle(o.clone());
			},
			None => {},
		}
	}

	fn new_widget(&mut self) -> ObstacleWidget {
		let mut result = ObstacleWidget::new(
			self.default_obstacle.clone(),
			self.image_provider.clone(),
		);
		result.signal_obstacle_selected().connect_slot(&self.slot_obstacle_selected);

		return result;
	}
}
