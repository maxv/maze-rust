use super::Resizer;

use maze_core::Obstacle;

use uniui_base::prelude::*;
use uniui_signalmapper::SignalMapper;


pub fn simple_instance(
	default_width: usize,
	default_height: usize,
	default_obstacle: Obstacle,
	app: &mut dyn Application,
) -> (Resizer, LinearLayout) {
	let resizer = Resizer::new(default_width, default_height, default_obstacle);

	let mut main_layout = LinearLayout::new(Orientation::Horizontal);
	main_layout.push_widget({
		let mut layout = LinearLayout::new(Orientation::Vertical);
		layout.push_widget(text_edit(
			"width",
			default_width,
			resizer.slot_set_width(),
			app,
		));

		layout.push_widget(text_edit(
			"height",
			default_height,
			resizer.slot_set_height(),
			app,
		));

		u_minimalsize!(layout)
	});

	main_layout.push_widget({
		let mut button = Button::new("R".to_string());
		button.signal_clicked().connect_slot(resizer.slot_activate());

		button
	});

	return (resizer, main_layout);
}

fn text_edit(
	comment: &str,
	default_value: usize,
	slot: &dyn Slot<usize>,
	app: &mut dyn Application,
) -> TextEdit {
	let mut text_edit = TextEdit::new(comment.to_string());

	let mut mapper = SignalMapper::new();

	let mut last_result = default_value;
	mapper.map(text_edit.signal_text_changed(), move |s| {
		match s.parse::<usize>() {
			Ok(v) => {
				last_result = v;
			},
			Err(_) => {},
		}

		last_result
	});

	mapper.signal().connect_slot(slot);
	Application::add_data_processor(app, mapper);

	text_edit.slot_set_text().exec_for(default_value.to_string());
	return text_edit;
}
