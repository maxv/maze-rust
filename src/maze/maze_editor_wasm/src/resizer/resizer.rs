use maze_core::Obstacle;

use uniui_base::prelude::*;

#[derive(uniui_gui::DataProcessor)]
pub struct Resizer {
	#[uproperty_process]
	#[uproperty_public_slot(slot_set_width)]
	width: Property<usize>,

	#[uproperty_process]
	#[uproperty_public_slot(slot_set_height)]
	height: Property<usize>,

	default_obstacle: Obstacle,

	#[public_signal]
	signal_resize_requested: Signal<(usize, usize, Obstacle)>,

	#[public_slot]
	#[uprocess_last(on_activated)]
	slot_activate: SlotImpl<()>,
}

impl Resizer {
	pub fn new(
		default_width: usize,
		default_height: usize,
		default_obstacle: Obstacle,
	) -> Resizer {
		return Resizer {
			width: Property::new(default_width),
			height: Property::new(default_height),
			default_obstacle,
			signal_resize_requested: Signal::new(),
			slot_activate: SlotImpl::new(),
		};
	}

	fn on_activated(
		&mut self,
		_: (),
	) {
		self.signal_resize_requested.emit((
			*self.width.as_ref(),
			*self.height.as_ref(),
			self.default_obstacle.clone(),
		));
	}
}
