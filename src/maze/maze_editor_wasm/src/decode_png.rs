pub fn decode_png(png: &[u8]) -> Option<(Vec<u8>, u32, u32)> {
	return match png::Decoder::new(png).read_info() {
		Ok((info, mut reader)) => {
			let width = info.width;
			let height = info.height;
			let mut data = Vec::new();
			data.resize((width * height * 4) as usize, 0);
			match reader.next_frame(&mut *data) {
				Ok(_) => {
					trace!("decode ok");
					Some((data, width, height))
				},
				Err(e) => {
					error!("decode err:{:?}", e);
					None
				},
			}
		},
		Err(e) => {
			error!("decode error2:{:?}", e);
			None
		},
	};
}
