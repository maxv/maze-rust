use maze_uniui_components::renderer::{
	helpers,
	ImageProvider,
	Overlay,
	Viewport,
};

use maze_core::{
	Drawable,
	Game,
};

use uniui_scene::Position as GraphicPosition;
use uniui_widget_canvas::Context as CanvasContext;


pub struct RespawnPointsOverlay {}

impl RespawnPointsOverlay {
	pub fn new() -> RespawnPointsOverlay {
		return RespawnPointsOverlay {};
	}
}

impl Overlay for RespawnPointsOverlay {
	fn draw(
		&mut self,
		context: &mut dyn CanvasContext,
		pos: &GraphicPosition,
		game: &dyn Game,
		viewport: &Viewport,
		image_provider: &mut ImageProvider,
	) {
		for op in game.respawn_points() {
			match op.current_draw_info(0) {
				Some(di) => {
					let (i, j) = op.coord();
					match image_provider.get(&di.resource) {
						None => {
							// FIXME(#126)
							// error!("image not found for:{}",
							// draw_info.resource);
						},
						Some(mut image) => {
							match helpers::calculate_cell_left_top_width_height(
								viewport, pos, i, j,
							) {
								Some((x, y, ..)) => {
									context.draw_imagebitmap_part(
										&mut image,
										x,
										y,
										di.left as f64,
										di.top as f64,
										di.width as f64,
										di.height as f64,
									);
								},
								None => {},
							};
						},
					};
				},
				None => {
					error!("None draw_info for respawn_point:{:?}", op.coord());
				},
			};
		}
	}
}
