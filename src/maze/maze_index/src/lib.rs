use uniui_base::{
	prelude::*,
	SizeUnit,
};

use uni_components::ui_page::UiPage;

use uniui_wrapper_border::{
	u_border,
	BorderStyle,
};

use uniui_wrapper_margin::u_margin;

struct LinkOpener<T> {
	pub page: &'static dyn UiPage<Data = ()>,
	pub slot: SlotImpl<T>,
}

impl<T> DataProcessor for LinkOpener<T> {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		if let Some(_) = self.slot.data_iter().last() {
			if let Err(e) = self.page.open() {
				log::error!("couldn't open page:{}. err:{:?}", self.page.path(), e);
			}
		}
	}
}

#[uniui_base::u_main]
fn app_main(app: &mut dyn Application) {
	uniui_base::utils::init_logger(app);

	let mut main_layout = LinearLayout::new(Orientation::Vertical);
	main_layout.push_widget({
		let mut button = Button::new("Start game".to_string());
		let mut slot = SlotImpl::new();
		button.signal_clicked().connect_slot(&mut slot);

		let link_opener = LinkOpener {
			page: maze_ws_proto::links::SERVER_GAME,
			slot,
		};
		Application::add_data_processor(app, link_opener);

		u_border! {
			widget: button,
			width: SizeUnit::PX(10.0),
		}
	});
	main_layout.push_widget({
		let mut button = Button::new("Local game".to_string());
		let mut slot = SlotImpl::new();
		button.signal_clicked().connect_slot(&mut slot);

		let link_opener = LinkOpener {
			page: maze_ws_proto::links::LOCAL_GAME,
			slot,
		};
		Application::add_data_processor(app, link_opener);

		u_border! {
			widget: button,
			radius: SizeUnit::PX(20.0),
			style: BorderStyle::Solid,
		}
	});
	main_layout.push_widget({
		let mut button = Button::new("Editor".to_string());
		let mut slot = SlotImpl::new();
		button.signal_clicked().connect_slot(&mut slot);

		let link_opener = LinkOpener {
			page: maze_ws_proto::links::EDITOR,
			slot,
		};
		Application::add_data_processor(app, link_opener);

		u_margin! {
			widget: button,
			top: SizeUnit::PX(10.0),
			right: SizeUnit::PX(20.0),
			bottom: SizeUnit::PX(30.0),
			left: SizeUnit::PX(40.0),
		}
	});
	main_layout.push_widget({
		let mut button = Button::new("PixelEditor".to_string());
		let mut slot = SlotImpl::new();
		button.signal_clicked().connect_slot(&mut slot);

		let link_opener = LinkOpener {
			page: maze_ws_proto::links::PIXELEDITOR,
			slot,
		};
		Application::add_data_processor(app, link_opener);

		u_margin! {
			widget: button,
			margin: SizeUnit::PX(15.0),
		}
	});
	app.set_main_widget(Box::new(main_layout));
}
