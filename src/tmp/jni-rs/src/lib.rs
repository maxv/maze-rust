#![warn(missing_docs)]

//! It's temporary fork for https://crates.io/crates/jni. Don't use!

/// `jni-sys` re-exports
pub mod sys;

mod wrapper {
    mod version;
    pub use self::version::*;

    #[macro_use]
    mod macros;

    /// Errors. Do you really need more explanation?
    pub mod errors;

    /// Descriptors for classes and method IDs.
    pub mod descriptors;

    /// Parser for java type signatures.
    pub mod signature;

    /// Wrappers for object pointers returned from the JVM.
    pub mod objects;

    /// String types for going to/from java strings.
    pub mod strings;

    /// Actual communication with the JVM.
    mod jnienv;
    pub use self::jnienv::*;

    /// Java VM interface.
    mod java_vm;
    pub use self::java_vm::*;

    /// Optional thread attachment manager.
    mod executor;
    pub use self::executor::*;
}

pub use wrapper::*;
