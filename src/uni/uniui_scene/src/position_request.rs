use super::Position;

#[derive(Debug, Copy, Clone)]
pub enum FlexPosition {
	Exact(f64),
	Min,
	Max,
}

#[derive(Debug, Copy, Clone)]
pub enum FlexSize {
	Exact(f64),
	Match,
}

#[derive(Debug, Copy, Clone)]
pub struct PositionRequest {
	pub x: FlexPosition,
	pub y: FlexPosition,
	pub width: FlexSize,
	pub height: FlexSize,
}

impl PositionRequest {
	pub fn fixed(
		x: f64,
		y: f64,
		width: f64,
		height: f64,
	) -> PositionRequest {
		return PositionRequest {
			x: FlexPosition::Exact(x),
			y: FlexPosition::Exact(y),
			width: FlexSize::Exact(width),
			height: FlexSize::Exact(height),
		};
	}

	pub fn full() -> PositionRequest {
		return PositionRequest {
			x: FlexPosition::Min,
			y: FlexPosition::Min,
			width: FlexSize::Match,
			height: FlexSize::Match,
		};
	}

	pub fn to_position(
		&self,
		outer_width: f64,
		outer_height: f64,
	) -> Position {
		let width = match self.width {
			FlexSize::Exact(v) => v,
			FlexSize::Match => outer_width,
		};

		let x = match self.x {
			FlexPosition::Exact(v) => v,
			FlexPosition::Min => 0.0,
			FlexPosition::Max => outer_width - width,
		};

		let height = match self.height {
			FlexSize::Exact(v) => v,
			FlexSize::Match => outer_height,
		};

		let y = match self.y {
			FlexPosition::Exact(v) => v,
			FlexPosition::Min => 0.0,
			FlexPosition::Max => outer_height - height,
		};

		return Position {
			x,
			y,
			width,
			height,
		};
	}
}
