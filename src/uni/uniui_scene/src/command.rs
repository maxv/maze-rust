use super::{
	Id,
	PositionRequest,
};

#[derive(Debug, Copy, Clone)]
pub enum Command {
	Move(Id, PositionRequest),
	Remove(Id),
}
