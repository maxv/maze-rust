#[derive(Debug, Copy, Clone)]
pub struct Position {
	pub x: f64,
	pub y: f64,
	pub width: f64,
	pub height: f64,
}

impl Position {
	pub fn contains(
		&self,
		x: f64,
		y: f64,
	) -> bool {
		return (self.x <= x) &&
			(x <= (self.x + self.width)) &&
			(self.y <= y) &&
			(y <= (self.y + self.width));
	}

	pub fn center(&self) -> (f64, f64) {
		let x = self.width / 2.0 + self.x;
		let y = self.height / 2.0 + self.y;
		return (x, y);
	}
}

impl Default for Position {
	fn default() -> Position {
		return Position {
			x: 0.0,
			y: 0.0,
			width: 0.0,
			height: 0.0,
		};
	}
}
