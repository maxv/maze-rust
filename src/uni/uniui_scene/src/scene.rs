use super::{
	item::Item,
	Command,
	Id,
	PositionRequest,
};

use uniui_widget_canvas::{
	Canvas,
	Context as CanvasContext,
	Drawer as CanvasDrawer,
};

use uniui_wrapper_touchlistener::{
	TouchEvent,
	TouchId,
	TouchListener,
};

use uniui_gui::{
	prelude::*,
	Color,
};

use std::{
	collections::HashMap,
	sync::Arc,
};

pub struct Scene {
	items: Vec<(Id, Box<dyn Item>, PositionRequest)>,
	touch_to_item_map: HashMap<TouchId, Id>,
	next_item_id: Id,
	command_slot: SlotImpl<Command>,
	background_color: Color,
	slot_touch_event: SlotImpl<Arc<TouchEvent>>,
	last_width: f64,
	last_height: f64,
}

impl Scene {
	pub fn new(background_color: Color) -> Scene {
		return Scene {
			items: Vec::new(),
			next_item_id: 0,
			command_slot: SlotImpl::new(),
			touch_to_item_map: HashMap::new(),
			background_color,
			slot_touch_event: SlotImpl::new(),
			last_height: 0.0,
			last_width: 0.0,
		};
	}

	pub fn to_canvas_widget(self) -> TouchListener<Canvas<Scene>> {
		let slotproxy = self.slot_touch_event().proxy();

		let mut canvas = Canvas::new();
		canvas.set_drawer(Some(self));

		let mut touch_listener = TouchListener::new(canvas);
		touch_listener.signal_touch_event().connect_slot(&slotproxy);
		return touch_listener;
	}

	pub fn slot_touch_event(&self) -> &dyn Slot<Arc<TouchEvent>> {
		return &self.slot_touch_event;
	}

	pub fn add_item<T: 'static + Item>(
		&mut self,
		item: T,
		position: PositionRequest,
	) -> Id {
		let id = self.next_item_id;
		self.next_item_id += 1;

		self.items.push((id, Box::new(item), position));

		return id;
	}

	pub fn command_slot(&mut self) -> &mut dyn Slot<Command> {
		return &mut self.command_slot;
	}

	pub fn process_touches(
		&mut self,
		touch_event: &TouchEvent,
	) {
		for touch in touch_event.touch_started.iter() {
			for (item_id, item, pos) in self.items.iter_mut().rev() {
				let pos = pos.to_position(self.last_width, self.last_height);
				match pos.contains(touch.x as f64, touch.y as f64) {
					true => {
						match item.accept_start_touch(touch, &pos) {
							true => {
								self.touch_to_item_map.insert(touch.id, *item_id);
								break;
							},
							false => {},
						}
					},
					false => {},
				}
			}
		}

		for touch in touch_event.touch_moved.iter() {
			match self.touch_to_item_map.get(&touch.id) {
				Some(item_id) => {
					for (id, item, pos) in self.items.iter_mut() {
						match id == item_id {
							true => {
								let pos =
									pos.to_position(self.last_width, self.last_height);
								item.move_touch(touch, &pos);
								break;
							},
							false => {},
						}
					}
				},
				None => {},
			}
		}

		for touch in touch_event.touch_ended.iter() {
			match self.touch_to_item_map.get(&touch.id) {
				Some(item_id) => {
					for (id, item, pos) in self.items.iter_mut() {
						match id == item_id {
							true => {
								let pos =
									pos.to_position(self.last_width, self.last_height);
								item.end_touch(touch, &pos);
								break;
							},
							false => {},
						}
					}
				},
				None => {},
			}
		}
	}

	fn apply(
		&mut self,
		command: Command,
	) {
		match command {
			Command::Move(id, position) => {
				self.items.iter_mut().for_each(|(item_id, _, p)| {
					match *item_id == id {
						true => *p = position,
						false => {},
					}
				})
			},
			Command::Remove(id) => {
				self.items.retain(|(item_id, ..)| id == *item_id);
			},
		};
	}
}

impl CanvasDrawer for Scene {
	fn draw(
		&mut self,
		context: &mut dyn CanvasContext,
		w: u32,
		h: u32,
	) {
		let v: Vec<_> = self.command_slot.data_iter().collect();
		for c in v {
			self.apply(c);
		}

		self.last_width = w as f64;
		self.last_height = h as f64;
		context.draw_rect(
			&self.background_color,
			0.0,
			0.0,
			self.last_width,
			self.last_height,
		);
		for (_, item, pos) in self.items.iter_mut() {
			let pos = pos.to_position(self.last_width, self.last_height);
			item.draw(context, &pos);
		}
	}
}

impl DataProcessor for Scene {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		while let Some(te) = self.slot_touch_event.next() {
			self.process_touches(&te);
		}

		self.items
			.iter_mut()
			.for_each(|(_, i, _)| i.process_data(msec_since_app_start, app));
	}
}
