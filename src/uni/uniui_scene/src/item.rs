use super::Position;

use uniui_widget_canvas::Context as CanvasContext;

use uniui_wrapper_touchlistener::Touch;

use uniui_gui::prelude::*;


pub trait Item {
	fn draw(
		&mut self,
		context: &mut dyn CanvasContext,
		position: &Position,
	);

	fn accept_start_touch(
		&mut self,
		_: &Touch,
		_: &Position,
	) -> bool {
		return false;
	}

	fn move_touch(
		&mut self,
		_: &Touch,
		_: &Position,
	) {
	}
	fn end_touch(
		&mut self,
		_: &Touch,
		_: &Position,
	) {
	}

	fn process_data(
		&mut self,
		_msec_since_app_start: i64,
		_app: &mut dyn Application,
	) {
	}
}
