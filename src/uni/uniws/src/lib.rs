#[macro_use]
extern crate log;

extern crate serde;
extern crate serde_cbor;
extern crate serde_json;

mod error;
pub use error::Error;

mod handshake;
pub use handshake::Handshake;

mod format;
pub use format::Format;

mod state;
pub use state::State;

type Result<T> = std::result::Result<T, Error>;


pub trait UniWsTrait<T>: std::marker::Sized
where
	T: serde::Serialize + serde::de::DeserializeOwned,
{
	fn connect(
		server: &str,
		format: Format,
	) -> Self;

	fn ready_state(&mut self) -> State;

	fn try_recv(&mut self) -> Result<T>;

	fn send(
		&mut self,
		t: &T,
	) -> Result<()>;

	#[cfg(not(target_arch = "wasm32"))]
	fn listen<H: 'static + std::marker::Send + FnMut(Self, Handshake) -> bool>(
		host_port: String,
		format: Format,
		handshaker: H,
	);
}


#[cfg(not(target_arch = "wasm32"))]
mod uniws {
	mod uniws_pc;
	pub use self::uniws_pc::UniWsPc as UniWs;
}

#[cfg(target_arch = "wasm32")]
mod uniws {
	mod uniws_wasm;
	pub use self::uniws_wasm::UniWsWasm as UniWs;
}

pub use uniws::UniWs;
