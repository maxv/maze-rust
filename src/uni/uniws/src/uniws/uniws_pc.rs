extern crate ws;

use crate::{
	Error,
	Format,
	Handshake,
	Result,
	State,
	UniWsTrait,
};

use self::ws::{
	Builder as WsBuilder,
	Error as WsError,
	ErrorKind as WsErrorKind,
	Handler as WsHandler,
	Handshake as WsHandshake,
	Message as WsMessage,
	Result as WsResult,
	Sender as WsSender,
	Settings as WsSettings,
};

use std::{
	marker::PhantomData,
	sync::{
		mpsc::{
			self,
			Receiver,
			Sender,
			TryRecvError,
		},
		Arc,
		Mutex,
		RwLock,
	},
};

fn from_ws_handshake(ws_handshake: &WsHandshake) -> Handshake {
	return Handshake {
		headers: ws_handshake.request.headers().clone(),
		origin: ws_handshake.request.origin().unwrap_or(None).map(|s| s.to_string()),
		resource: ws_handshake.request.resource().to_string(),
		client_addr: ws_handshake
			.request
			.client_addr()
			.unwrap_or(None)
			.map(|s| s.to_string()),
	};
}

struct Handler<T: serde::Serialize + serde::de::DeserializeOwned> {
	pub sender: Sender<WsMessage>,
	pub uniws: Option<UniWsPc<T>>,
	pub handshaker: Option<Arc<RwLock<dyn FnMut(UniWsPc<T>, Handshake) -> bool>>>,
}

impl<T: serde::Serialize + serde::de::DeserializeOwned> WsHandler for Handler<T> {
	fn on_open(
		&mut self,
		handshake: WsHandshake,
	) -> WsResult<()> {
		return match (self.uniws.take(), self.handshaker.take()) {
			(Some(uniws), Some(handshaker)) => {
				match handshaker.write() {
					Ok(mut g) => {
						let t = &mut *g;
						match t(uniws, from_ws_handshake(&handshake)) {
							true => Ok(()),
							false => Err(WsError::new(WsErrorKind::Internal, "")),
						}
					},
					Err(_) => Err(WsError::new(WsErrorKind::Internal, "")),
				}
			},
			_ => Ok(()),
		};
	}

	fn on_message(
		&mut self,
		msg: WsMessage,
	) -> WsResult<()> {
		match self.sender.send(msg) {
			Ok(()) => {},
			Err(e) => warn!("couldn't send e:{:?}", e),
		};

		return Ok(());
	}
}

pub struct UniWsPc<T: serde::Serialize + serde::de::DeserializeOwned> {
	sender: Option<WsSender>,
	receiver: Mutex<Receiver<WsMessage>>,
	socket_receiver: Option<Mutex<Receiver<WsSender>>>,
	format: Format,
	t: PhantomData<T>,
}

impl<T: serde::Serialize + serde::de::DeserializeOwned> UniWsPc<T> {
	fn get_sender(&mut self) -> Option<&WsSender> {
		if !self.sender.is_some() {
			let receiver_still_waiting = match self.socket_receiver.as_ref() {
				Some(receiver) => {
					match receiver.lock().unwrap().try_recv() {
						Ok(sender) => {
							self.sender = Some(sender);
							false
						},
						Err(e) => {
							match e {
								TryRecvError::Empty => true,
								TryRecvError::Disconnected => false,
							}
						},
					}
				},
				None => false,
			};

			if !receiver_still_waiting {
				self.socket_receiver = None;
			}
		};

		return self.sender.as_ref();
	}
}

impl<T: serde::Serialize + serde::de::DeserializeOwned> UniWsTrait<T> for UniWsPc<T> {
	fn listen<H: 'static + std::marker::Send + FnMut(Self, Handshake) -> bool>(
		host_port: String,
		format: Format,
		handshaker: H,
	) {
		std::thread::spawn(move || {
			let mut settings = WsSettings::default();
			settings.panic_on_internal = false;

			let handshaker = Arc::new(RwLock::new(handshaker));
			let socket = WsBuilder::new()
				.with_settings(settings)
				.build(move |ws| {
					trace!("new connection");
					let (sender, receiver) = mpsc::channel();
					let uniws = UniWsPc::<T> {
						sender: Some(ws),
						receiver: Mutex::new(receiver),
						socket_receiver: None,
						format,
						t: PhantomData,
					};


					return Handler::<T> {
						sender: sender.clone(),
						uniws: Some(uniws),
						handshaker: Some(handshaker.clone()),
					};
				})
				.expect("no errors during socket building");

			socket.listen(&host_port).expect("socket listening w/o errors");
		});
	}

	fn connect(
		server: &str,
		format: Format,
	) -> Self {
		let (sender, receiver) = mpsc::channel();
		let (socket_sender, socket_receiver) = mpsc::channel();

		let server = server.to_string();
		std::thread::spawn(move || {
			let result = ws::connect(server, move |ws| {
				match socket_sender.send(ws.clone()) {
					Ok(()) => {},
					Err(_) => {},
				};
				return Handler::<T> {
					sender: sender.clone(),
					uniws: None,
					handshaker: None,
				};
			});

			match result {
				Ok(()) => trace!("socket connected"),
				Err(e) => warn!("coudn't connect e:{:?}", e),
			};
		});

		return UniWsPc {
			receiver: Mutex::new(receiver),
			socket_receiver: Some(Mutex::new(socket_receiver)),
			sender: None,
			format,
			t: PhantomData,
		};
	}

	fn ready_state(&mut self) -> State {
		return match self.get_sender().is_some() {
			true => State::Open,
			false => {
				match self.socket_receiver.is_some() {
					true => State::Connecting,
					false => State::Closed,
				}
			},
		};
	}

	fn send(
		&mut self,
		t: &T,
	) -> Result<()> {
		return match (self.format, self.get_sender()) {
			(Format::Text, Some(sender)) => {
				let s = serde_json::to_string(t)?;
				sender.send(WsMessage::Text(s))?;
				Ok(())
			},
			(Format::Binary, Some(sender)) => {
				let b = serde_cbor::to_vec(t)?;
				sender.send(WsMessage::Binary(b))?;
				Ok(())
			},
			(_, None) => {
				match self.socket_receiver.is_some() {
					true => Err(Error::NotReady),
					false => Err(Error::Closed),
				}
			},
		};
	}

	fn try_recv(&mut self) -> Result<T> {
		return match self.receiver.lock().unwrap().try_recv() {
			Ok(m) => {
				match m {
					WsMessage::Text(s) => Ok(serde_json::from_str::<T>(&s)?),
					WsMessage::Binary(b) => Ok(serde_cbor::from_slice(&b[..])?),
				}
			},
			Err(e) => {
				match e {
					TryRecvError::Empty => Err(Error::Empty),
					TryRecvError::Disconnected => Err(Error::Closed),
				}
			},
		};
	}
}

impl std::convert::From<WsError> for Error {
	fn from(error: WsError) -> Self {
		return Error::NativeSocketError(Box::new(error));
	}
}
