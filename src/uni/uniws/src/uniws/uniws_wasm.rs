extern crate wasm_bindgen;
extern crate web_sys;

use crate::{
	Error,
	Format,
	Result,
	State,
	UniWsTrait,
};

use std::{
	marker::PhantomData,
	sync::mpsc::{
		self,
		Receiver,
		TryRecvError,
	},
};

use self::web_sys::{
	MessageEvent,
	WebSocket,
};

use self::wasm_bindgen::{
	closure::Closure,
	JsCast,
	JsValue,
};

pub struct UniWsWasm<T: serde::Serialize + serde::de::DeserializeOwned> {
	receiver: Receiver<MessageEvent>,
	web_ws: Option<WebSocket>,
	closure: Option<Closure<dyn FnMut(MessageEvent)>>,
	format: Format,
	t: PhantomData<T>,
}

impl<T: serde::Serialize + serde::de::DeserializeOwned> UniWsWasm<T> {
	fn close_ws(&mut self) {
		self.closure = None;
		match self.web_ws.as_ref() {
			Some(ws) => {
				ws.set_onmessage(None);
				match ws.close() {
					Ok(()) => {},
					Err(_) => {},
				};
			},
			None => {},
		};
	}
}

impl<T: serde::Serialize + serde::de::DeserializeOwned> UniWsTrait<T> for UniWsWasm<T> {
	fn connect(
		server: &str,
		format: Format,
	) -> UniWsWasm<T> {
		let (sender, receiver) = mpsc::channel();
		let (web_socket, closure) = match WebSocket::new(server) {
			Ok(ws) => {
				ws.set_binary_type(web_sys::BinaryType::Arraybuffer);
				let web_socket = ws.clone();
				let boxed = Box::new(move |message: MessageEvent| {
					match sender.send(message) {
						Ok(()) => trace!("message transfered to queue"),
						Err(e) => {
							error!("the other side of the socket is closed e:{:?}", e);
							match web_socket.close() {
								Ok(()) => trace!("socket closed"),
								Err(e) => trace!("socket closing error e:{:?}", e),
							}
						},
					};
				}) as Box<dyn FnMut(MessageEvent)>;
				let closure = Closure::wrap(boxed);
				ws.set_onmessage(Some(closure.as_ref().unchecked_ref()));
				(Some(ws), Some(closure))
			},
			Err(_) => (None, None),
		};

		return UniWsWasm {
			receiver,
			closure,
			web_ws: web_socket,
			format,
			t: PhantomData,
		};
	}

	fn ready_state(&mut self) -> State {
		return match self.web_ws.as_ref() {
			Some(ws) => {
				match ws.ready_state() {
					WebSocket::CLOSING => State::Closing,
					WebSocket::CLOSED => State::Closed,
					WebSocket::OPEN => State::Open,
					WebSocket::CONNECTING => State::Connecting,
					_ => State::Unknown,
				}
			},
			None => State::Closed,
		};
	}

	fn send(
		&mut self,
		t: &T,
	) -> Result<()> {
		return match (self.format, self.web_ws.as_ref()) {
			(Format::Text, Some(ws)) => {
				let s = serde_json::to_string(t)?;
				ws.send_with_str(&s)?;
				Ok(())
			},
			(Format::Binary, Some(ws)) => {
				let mut b = serde_cbor::to_vec(t)?;
				ws.send_with_u8_array(&mut b[..])?;
				Ok(())
			},
			(_, None) => Err(Error::Closed),
		};
	}

	fn try_recv(&mut self) -> Result<T> {
		return match self.receiver.try_recv() {
			Ok(message) => {
				let data = message.data();
				match data.is_string() {
					true => {
						match data.as_string() {
							Some(s) => Ok(serde_json::from_str::<T>(&s)?),
							None => {
								warn!(
									"data is string but can't be used as_string d:{:?}",
									data
								);
								Err(Error::Unexpected)
							},
						}
					},
					false => {
						let ara = js_sys::Uint8Array::new(&data);
						let mut b = Vec::<u8>::new();
						ara.for_each(&mut |d, _, _| {
							b.push(d);
						});
						Ok(serde_cbor::from_slice(&b[..])?)
					},
				}
			},
			Err(e) => {
				match e {
					TryRecvError::Empty => Err(Error::Empty),
					TryRecvError::Disconnected => {
						self.close_ws();
						Err(Error::Closed)
					},
				}
			},
		};
	}
}

impl<T: serde::Serialize + serde::de::DeserializeOwned> Drop for UniWsWasm<T> {
	fn drop(&mut self) {
		self.close_ws();
	}
}

impl std::convert::From<JsValue> for Error {
	fn from(error: JsValue) -> Self {
		return Error::NativeSocketError(Box::new(error));
	}
}
