#[derive(Clone, Debug)]
pub struct Handshake {
	pub headers: Vec<(String, Vec<u8>)>,
	pub origin: Option<String>,
	pub resource: String,
	pub client_addr: Option<String>,
}
