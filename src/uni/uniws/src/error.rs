use std::any::Any;

#[derive(Debug)]
pub enum Error {
	SerializationJsonError(serde_json::error::Error),
	SerializationCborError(serde_cbor::Error),
	Empty,
	Closed,
	NotReady,
	NativeSocketError(Box<dyn Any>),
	Unexpected,
}

impl std::convert::From<serde_json::Error> for Error {
	fn from(error: serde_json::Error) -> Self {
		return Error::SerializationJsonError(error);
	}
}

impl std::convert::From<serde_cbor::Error> for Error {
	fn from(error: serde_cbor::Error) -> Self {
		return Error::SerializationCborError(error);
	}
}
