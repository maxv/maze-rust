#[derive(Clone, Debug, Copy)]
pub enum State {
	Open,
	Connecting,
	Closing,
	Closed,
	Unknown,
}
