#[derive(Clone, Debug, Copy)]
pub enum Format {
	Text,
	Binary,
}
