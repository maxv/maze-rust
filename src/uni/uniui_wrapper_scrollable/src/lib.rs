use uniui_gui::prelude::*;

mod macros;

pub enum ScrollDirection {
	ScrollX,
	ScrollY,
	Both,
}

pub struct Scrollable<T: 'static + Widget> {
	pub widget: T,
	pub direction: Option<ScrollDirection>,
}

impl<T: 'static + Widget> Widget for Scrollable<T> {
	#[cfg(target_os = "linux")]
	fn to_native(
		&mut self,
		wg: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		log::error!("Scrollable is not implemented for Qt5. Yet.");
		return self.widget.to_native(wg);
	}

	#[cfg(target_arch = "wasm32")]
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;

		let s = e.style();

		if let Some(val) = self.direction.as_ref() {
			let val_str = match val {
				ScrollDirection::ScrollX => "overflow-x",
				ScrollDirection::ScrollY => "overflow-y",
				ScrollDirection::Both => "overflow",
			};
			s.set_property_silent(val_str, "scroll");
		}

		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for Scrollable<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
