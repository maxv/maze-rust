#[macro_export]
macro_rules! u_scrollable_int {
	($b:ident,direction: $va:expr) => {
		$b.direction = Some($va);
	};
}

#[macro_export]
macro_rules! u_scrollable {
	(widget: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut scrollable = $crate::Scrollable{
			widget: $w,
			direction: None,
		};
		$($crate::u_scrollable_int!(scrollable, $param_name: $val);)*;
		scrollable
	}};
}
