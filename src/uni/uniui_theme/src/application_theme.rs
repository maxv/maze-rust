use std::{
	collections::HashMap,
	fmt::Write,
};


#[derive(Clone, Debug, PartialEq, Eq, Hash, serde::Deserialize, serde::Serialize)]
pub enum AttributeValue {
	Custom(String),
	Default(String),
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, serde::Deserialize, serde::Serialize)]
pub struct Attribute {
	pub name: String,
	pub value: AttributeValue,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, serde::Deserialize, serde::Serialize)]
pub struct ClassTheme {
	pub name: String,
	pub attributes: Vec<Attribute>,
}

#[derive(Clone, Debug, PartialEq, Eq, serde::Deserialize, serde::Serialize)]
pub struct ApplicationTheme {
	classes: Vec<ClassTheme>,
	defaults: HashMap<String, String>,
}

const EMPTY_STRING: &'static str = "\"\"";

impl ApplicationTheme {
	pub fn new() -> ApplicationTheme {
		return ApplicationTheme {
			classes: Vec::new(),
			defaults: HashMap::new(),
		};
	}

	pub fn add_class(
		&mut self,
		class: ClassTheme,
	) {
		self.classes.push(class);
	}

	pub fn add_class_with_inherit(
		&mut self,
		mut class: ClassTheme,
		inherit_from: String,
	) -> Result<(), ()> {
		return match self.classes.iter().filter(|c| c.name == inherit_from).next() {
			Some(c) => {
				let mut attributes = c.attributes.clone();
				attributes.append(&mut class.attributes);
				class.attributes = attributes;
				self.classes.push(class);
				Ok(())
			},
			None => Err(()),
		};
	}

	pub fn set_default(
		&mut self,
		kind: String,
		value: String,
	) {
		self.defaults.insert(kind, value);
	}

	pub fn css_string(&self) -> Result<String, std::fmt::Error> {
		let mut result = String::new();

		for class in self.classes.iter() {
			write!(result, ".{} {{", class.name)?;

			for attribute in class.attributes.iter() {
				let value = match &attribute.value {
					AttributeValue::Custom(s) => s,
					AttributeValue::Default(kind) => {
						match self.defaults.get(kind) {
							Some(s) => s,
							None => EMPTY_STRING,
						}
					},
				};
				write!(result, "{}: {};\n", attribute.name, value)?;
			}
			write!(result, "}}")?;
		}

		return Ok(result);
	}
}
