extern crate unitracer;

extern crate serde;

use serde::{
	Deserialize,
	Serialize,
};


#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub enum ProtocolV1 {
	Batch(unitracer::Batch),
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub enum Protocol {
	V1(ProtocolV1),
}
