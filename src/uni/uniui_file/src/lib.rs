#[macro_use]
extern crate log;

extern crate uniui_gui;

#[cfg(target_arch = "wasm32")]
extern crate web_sys;

#[cfg(target_arch = "wasm32")]
extern crate wasm_bindgen;

#[cfg(target_arch = "wasm32")]
extern crate js_sys;


#[cfg(target_arch = "wasm32")]
pub type NativeFile = web_sys::File;

#[cfg(target_os = "linux")]
pub type NativeFile = i32;

use uniui_gui::core::SlotProxy;


#[derive(Clone)]
pub struct File {
	web_file: NativeFile,
}

// FIXME(#149)
impl File {
	pub fn new(web_file: NativeFile) -> File {
		return File {
			web_file,
		};
	}

	/// The method is unsafe because NativeFile is different for different platofmrs
	pub unsafe fn native_file(&self) -> NativeFile {
		return self.web_file.clone();
	}

	#[cfg(target_os = "linux")]
	pub fn read_all(
		&self,
		resulter: SlotProxy<Result<Vec<u8>, ()>>,
	) {
		error!("not implemented");
		resulter.exec_for(Err(()));
	}

	#[cfg(target_arch = "wasm32")]
	pub fn read_all(
		&self,
		resulter: SlotProxy<Result<Vec<u8>, ()>>,
	) {
		use web_sys::FileReader;

		use js_sys::Uint8Array;

		use wasm_bindgen::{
			closure::Closure,
			JsCast,
		};

		match FileReader::new() {
			Ok(file_reader) => {
				let fr = file_reader.clone();
				let closure_sender = resulter.clone();
				let closure = Closure::wrap(Box::new(move || {
					let typed_array = Uint8Array::new(&fr.result().unwrap());
					let mut result: Vec<u8> =
						Vec::with_capacity(typed_array.length() as usize);
					typed_array.for_each(&mut |v, _, _| {
						result.push(v);
					});
					closure_sender.exec_for(Ok(result));
					fr.set_onload(None);
				}) as Box<dyn FnMut()>);
				file_reader.set_onload(Some(closure.as_ref().unchecked_ref()));

				// FIXME(#113)
				closure.forget();

				match file_reader.read_as_array_buffer(&self.web_file) {
					Ok(()) => trace!("read ok"),
					Err(e) => {
						error!("file reader error:{:?}", e);
						resulter.exec_for(Err(()));
					},
				}
			},
			Err(e) => {
				error!("FileReader creation error:{:?}", e);
				resulter.exec_for(Err(()));
			},
		};
	}
}
