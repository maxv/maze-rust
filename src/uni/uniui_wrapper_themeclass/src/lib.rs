#[cfg(target_arch = "wasm32")]
mod implz {
	pub extern crate wasm_bindgen;
	pub extern crate web_sys;
}

#[cfg(target_os = "linux")]
mod implz {
	extern "C" {
		pub fn uniui_wrapper_themeclass_set_for(widget: *mut std::ffi::c_void);
	}
}

use uniui_gui::prelude::*;

pub mod prelude {
	pub use crate::{
		u_themeclass_replace,
		u_themeclass_add,
		ThemeClass,
	};
}

/// Simplifies [ThemeClass] creation.
///
/// All existing classes will be removed
///
/// ```
/// let minimal_widget = u_themeclass_replace!(
///     classes: {"class1",  "class2", },
///     widget: the_widget
/// );
/// ```
#[macro_export]
macro_rules! u_themeclass_replace {
	(classes: {$($class:expr),*$(,)*}, widget: $w:expr $(,)?) => {{
		let mut r = $crate::ThemeClass::new($w, true);
		$(r.add_class($class);)*;
		r
	}};
}

/// Simplifies [ThemeClass] creation.
///
/// All existing classes will be keeped in place
///
/// ```
/// let minimal_widget = u_themeclass_add!(
///     classes: {"class1",  "class2", },
///     widget: the_widget
/// );
/// ```
#[macro_export]
macro_rules! u_themeclass_add {
	(classes: {$($class:expr),*$(,)*}, widget: $w:expr $(,)?) => {{
		let mut r = $crate::ThemeClass::new($w, false);
		$(r.add_class($class);)*;
		r
	}};
}


/// Add/Replaces class names for provided widget
///
/// ```
/// TBD
/// ```
pub struct ThemeClass<T: Widget> {
	widget: T,
	classes: Vec<String>,
	replace_existing: bool,
}

impl<T: Widget> ThemeClass<T> {
	pub fn new(widget: T, replace_existing: bool) -> ThemeClass<T> {
		return ThemeClass{
			widget,
			classes: Vec::new(),
			replace_existing,
		};
	}
	
	pub fn add_class(&mut self, class_name: String) {
		self.classes.push(class_name);
	}
	
	pub fn widget(&mut self) -> &mut T {
		return &mut self.widget;
	}
}


impl<T: Widget> Widget for ThemeClass<T> {
	#[cfg(target_os = "linux")]
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		log::error!("ThemeClass is not implemented for Qt5. Yet.");
		
		let result = self.widget.to_native(widget_generator)?;
		
		uniui_gui::desktop_qt! {
			unsafe {
				implz::uniui_wrapper_themeclass_set_for(result);
				self.replace_existing = self.replace_existing;
			};
		}
		
		return Ok(result);
	}

	#[cfg(target_arch = "wasm32")]
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;
		let existing_classes = match self.replace_existing {
			true => String::new(),
			false => e.class_name(),
		};
		let new_classes = format!("{} {}", existing_classes, self.classes.join(" "));
		e.set_class_name(&new_classes);
		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: Widget> DataProcessor for ThemeClass<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
