// Buildsystem will put all our files into the package with the 
// same name as our crate's name is.
package uniui_wrapper_minimalsize;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

class UniuiWrapperMinimalSize {
	public static void exec(Object object) {
		if (object instanceof View) {
			View view = (View) object;
			ViewGroup.LayoutParams p = view.getLayoutParams();

			final LinearLayout.LayoutParams params;
			if (p != null) {
				params = new LinearLayout.LayoutParams(p);
			} else {
				params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT
				);
				params.setMargins(0,0,0,0);
			}

			params.weight = 0.0f;
			view.setLayoutParams(params);
		} else {
			//TODO: log error
		}
	}
}

