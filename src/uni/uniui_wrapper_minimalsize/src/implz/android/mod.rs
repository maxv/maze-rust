use uniui_gui::{
	implz::android::{
		jni_error::JniError,
		reimport::uni_jnihelpers::{
			reimport::jni::{
				objects::JClass,
				JNIEnv,
			},
			UniGlobalRef,
		},
	},
	prelude::*,
};

pub fn set_minimal(
	w: NativeWidget,
	widget_generator: &mut dyn WidgetGenerator,
) -> Result<NativeWidget, ()> {
	let jni_env = widget_generator.context().jni_env().map_err(|_| ())?;
	set_minimal_inner(&w, &jni_env).map_err(|_| ())?;
	return Ok(w);
}

fn set_minimal_inner(
	w: &NativeWidget,
	env: &jni::JNIEnv,
) -> Result<(), JniError> {
	use jni::{
		objects::JValue,
		signature::{
			JavaType::Primitive,
			Primitive::Void,
		},
	};

	use uniui_gui::implz::android::reimport::uni_jnihelpers::*;

	let class = get_class(env)?;
	let method =
		get_java_static_method!(env, class.as_obj(), "exec", "(Ljava/lang/Object;)V");

	env.call_static_method_unchecked(class.as_obj(), method, Primitive(Void), &[
		JValue::Object(w.as_obj().into()),
	])?;

	return Ok(());
}

fn get_class(env: &JNIEnv) -> Result<UniGlobalRef<JClass<'static>>, JniError> {
	let class = uniui_gui::get_java_class!(
		env,
		"uniui_wrapper_minimalsize/UniuiWrapperMinimalSize"
	)?;

	return Ok(class);
}
