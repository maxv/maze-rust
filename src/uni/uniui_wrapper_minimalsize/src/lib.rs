#[doc(hidden)]
#[cfg(target_os = "linux")]
mod implz {
	use uniui_gui::prelude::*;
	uniui_gui::desktop_qt! {
		extern "C" {
			pub fn uniui_wrapper_minimalsize_set_for(widget: *mut std::ffi::c_void);
		}
	}

	pub fn set_minimal(
		w: NativeWidget,
		_: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		uniui_gui::desktop_qt! {
			unsafe {
				uniui_wrapper_minimalsize_set_for(w);
			};
		}

		uniui_gui::desktop_noop! {
			// nothing to do
		}

		return Ok(w);
	}
}

#[doc(hidden)]
#[cfg(target_os = "android")]
extern crate uni_tmp_jni as jni;

use uniui_gui::prelude::*;

/// Contains [MinimalSize] and [u_minimalsize!]
pub mod prelude {
	pub use crate::{
		u_minimalsize,
		MinimalSize,
	};
}

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
mod implz {
	use uniui_gui::prelude::*;

	pub fn set_minimal(
		w: NativeWidget,
		_: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		w.style().set_property_silent("flex-grow", "0");
		return Ok(w);
	}
}

#[doc(hidden)]
#[cfg(target_os = "android")]
mod implz {
	mod android;
	pub use android::set_minimal;
}


/// Simplifies [MinimalSize] creation.
///
/// ```
/// let minimal_widget = u_minimalsize!(widget);
/// ```
#[macro_export]
macro_rules! u_minimalsize {
	($w:expr) => {{
		$crate::MinimalSize {
			widget: $w,
			}
		}};
}


/// Force widget to be as small as possible.
///
/// Consider using [u_minimalsize!](u_minimalsize!) for simplification.
///
/// Example:
/// ```
/// let nonexpandable_widget = MinimalSize {
///     widget: expandable_widget,
/// };
/// ```
pub struct MinimalSize<T: Widget> {
	pub widget: T,
}


impl<T: Widget> Widget for MinimalSize<T> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let result = self.widget.to_native(widget_generator)?;
		return implz::set_minimal(result, widget_generator);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: Widget> DataProcessor for MinimalSize<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
