#include <QWidget>
#include <QLayout>

extern "C" void uniui_wrapper_minimalsize_set_for(void* w) {
	QWidget* widget = (QWidget*)w;
	QSizePolicy policy{QSizePolicy::Minimum, QSizePolicy::Minimum};
	widget->setSizePolicy(policy);
}

