use crate::{
	implz::Platform,
	TouchEvent,
};

use uniui_gui::prelude::*;

use std::sync::Arc;

pub struct TouchListener<T: 'static + Widget> {
	inner_element: T,
	touch_signal: Signal<Arc<TouchEvent>>,
	platform: Platform,
}


impl<T: 'static + Widget> TouchListener<T> {
	pub fn new(inner_element: T) -> TouchListener<T> {
		return TouchListener {
			touch_signal: Signal::new(),
			platform: Platform::new(),
			inner_element,
		};
	}

	pub fn signal_touch_event(&mut self) -> &mut Signal<Arc<TouchEvent>> {
		return &mut self.touch_signal;
	}
}

impl<T: 'static + Widget> Widget for TouchListener<T> {
	fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let mut inner_native = self.inner_element.to_native(document)?;
		self.platform.wrap(&mut inner_native);
		return Ok(inner_native);
	}

	fn draw(&mut self) {
		self.inner_element.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for TouchListener<T> {
	fn process_data(
		&mut self,
		msec_since_app_epoch: i64,
		app: &mut dyn Application,
	) {
		self.inner_element.process_data(msec_since_app_epoch, app);

		let event = self.platform.generate_touch_event();
		self.touch_signal.emit(Arc::new(event));
	}
}
