pub type TouchId = i32;
pub type TouchCoord = f64;

#[derive(Debug, Copy, Clone)]
pub struct Touch {
	pub x: TouchCoord,
	pub y: TouchCoord,
	pub id: TouchId,
}

impl Touch {
	pub fn offset(
		self,
		x: f64,
		y: f64,
	) -> Touch {
		return Touch {
			id: self.id,
			x: self.x - x,
			y: self.y - y,
		};
	}
}

#[derive(Debug, Clone)]
pub struct TouchEvent {
	pub touch_started: Vec<Touch>,
	pub touch_moved: Vec<Touch>,
	pub touch_ended: Vec<Touch>,
}


mod touch_listener;
pub use touch_listener::TouchListener;

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_touch_listener;
	pub use self::wasm_touch_listener::WasmTouchListener as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_touch_listener;
	pub use self::qt5_touch_listener::Qt5TouchListener as Platform;
}
