use crate::{
	Touch,
	TouchEvent,
};

use uniui_gui::prelude::*;

use crate::implz::web_sys::{
	MouseEvent as WebMouseEvent,
	Touch as WebTouch,
	TouchEvent as WebTouchEvent,
};

use crate::implz::wasm_bindgen::{
	closure::Closure,
	JsCast,
};

use std::sync::{
	mpsc,
	mpsc::Receiver,
};


type ClosureTouch = Closure<dyn FnMut(WebTouchEvent)>;
type ClosureMouse = Closure<dyn FnMut(WebMouseEvent)>;

pub struct WasmTouchListener {
	start_closure: ClosureTouch,
	mouse_start_closure: ClosureMouse,
	start_receiver: Receiver<Touch>,

	move_closure: ClosureTouch,
	mouse_move_closure: ClosureMouse,
	move_receiver: Receiver<Touch>,

	end_closure: ClosureTouch,
	mouse_end_closure: ClosureMouse,
	end_receiver: Receiver<Touch>,

	native_element: Option<NativeWidget>,
}


impl WasmTouchListener {
	pub fn new() -> WasmTouchListener {
		let (start_closure, mouse_start_closure, start_receiver) =
			generate_closures_and_receiver(false);
		let (move_closure, mouse_move_closure, move_receiver) =
			generate_closures_and_receiver(false);
		let (end_closure, mouse_end_closure, end_receiver) =
			generate_closures_and_receiver(true);

		return WasmTouchListener {
			start_closure,
			mouse_start_closure,
			start_receiver,

			move_closure,
			mouse_move_closure,
			move_receiver,

			end_closure,
			mouse_end_closure,
			end_receiver,

			native_element: None,
		};
	}

	pub fn wrap(
		&mut self,
		e: &mut NativeWidget,
	) {
		self.native_element = Some(e.clone());
		let closure_ref = self.start_closure.as_ref().unchecked_ref();
		e.add_event_listener_silent("touchstart", closure_ref);

		let closure_ref = self.end_closure.as_ref().unchecked_ref();
		e.add_event_listener_silent("touchend", closure_ref);
		e.add_event_listener_silent("touchcancel", closure_ref);

		let closure_ref = self.move_closure.as_ref().unchecked_ref();
		e.add_event_listener_silent("touchmove", closure_ref);

		let closure_ref = self.mouse_start_closure.as_ref().unchecked_ref();
		e.add_event_listener_silent("mousedown", closure_ref);

		let closure_ref = self.mouse_move_closure.as_ref().unchecked_ref();
		e.add_event_listener_silent("mousemove", closure_ref);

		let closure_ref = self.mouse_end_closure.as_ref().unchecked_ref();
		e.add_event_listener_silent("mouseup", closure_ref);
	}

	pub fn generate_touch_event(&self) -> TouchEvent {
		let (x, y) = match self.native_element.as_ref() {
			Some(native_element) => {
				let rect = native_element.get_bounding_client_rect();
				(rect.x(), rect.y())
			},
			None => (0.0, 0.0),
		};

		let mut touch_started = Vec::new();
		while let Ok(touch) = self.start_receiver.try_recv() {
			touch_started.push(touch.offset(x, y));
		}

		let mut touch_ended = Vec::new();
		while let Ok(touch) = self.end_receiver.try_recv() {
			touch_ended.push(touch.offset(x, y));
		}

		let mut touch_moved = Vec::new();
		while let Ok(touch) = self.move_receiver.try_recv() {
			touch_moved.push(touch.offset(x, y));
		}

		return TouchEvent {
			touch_started,
			touch_moved,
			touch_ended,
		};
	}
}

fn generate_closures_and_receiver(
	allow_no_buttons: bool
) -> (ClosureTouch, ClosureMouse, Receiver<Touch>) {
	fn from_web_touch(touch: WebTouch) -> Touch {
		return Touch {
			id: touch.identifier(),
			x: touch.client_x() as f64,
			y: touch.client_y() as f64,
		};
	}

	let (sender, receiver) = mpsc::channel();
	let sender_clone = sender.clone();
	let closure_touch = Closure::wrap(Box::new(move |event: WebTouchEvent| {
		event.prevent_default();
		let touches = event.changed_touches();
		for i in 0..touches.length() {
			match touches.get(i) {
				Some(touch) => sender.send_silent(from_web_touch(touch)),
				None => log::warn!("touch not found"),
			}
		}
	}) as Box<dyn FnMut(WebTouchEvent)>);

	fn from_web_mouse(event: WebMouseEvent) -> Touch {
		return Touch {
			id: std::i32::MIN,
			x: event.client_x() as f64,
			y: event.client_y() as f64,
		};
	}

	let closure_mouse = Closure::wrap(Box::new(move |event: WebMouseEvent| {
		if (event.buttons() > 0) || (allow_no_buttons) {
			sender_clone.send_silent(from_web_mouse(event));
		}
	}) as Box<dyn FnMut(WebMouseEvent)>);


	return (closure_touch, closure_mouse, receiver);
}
