use crate::TouchEvent;

use uniui_gui::NativeWidget;

pub struct Qt5TouchListener {}

impl Qt5TouchListener {
	pub fn new() -> Qt5TouchListener {
		return Qt5TouchListener {};
	}

	pub fn wrap(
		&mut self,
		_e: &mut NativeWidget,
	) {
		log::error!("not implemented");
	}

	pub fn generate_touch_event(&self) -> TouchEvent {
		return TouchEvent {
			touch_started: Vec::new(),
			touch_moved: Vec::new(),
			touch_ended: Vec::new(),
		};
	}
}
