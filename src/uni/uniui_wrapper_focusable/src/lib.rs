extern crate uniui_core;
extern crate uniui_gui;

#[cfg(target_arch = "wasm32")]
extern crate web_sys;

#[cfg(target_arch = "wasm32")]
extern crate wasm_bindgen;


use uniui_gui::prelude::*;


pub struct Focusable<T: Widget> {
	pub widget: T,
}

impl<T: Widget> Widget for Focusable<T> {
	#[cfg(target_os = "linux")]
	fn to_native(
		&mut self,
		_native_parent: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		Err(())
	}

	#[cfg(target_arch = "wasm32")]
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;
		e.set_tab_index(0);
		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: Widget> DataProcessor for Focusable<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
