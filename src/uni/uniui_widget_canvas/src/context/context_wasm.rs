use super::Context;

use crate::{
	canvas::ImageData,
	Color,
	Image,
};

use web_sys::{
	CanvasRenderingContext2d as Context2d,
	ImageData as WebImageData,
};

pub struct WebCanvasContext {
	context: Context2d,
}

impl WebCanvasContext {
	pub fn new(context: Context2d) -> WebCanvasContext {
		return WebCanvasContext {
			context,
		};
	}

	fn draw_imagebitmap_part_1(
		&self,
		image: &WebImageData,
		dx: f64,
		dy: f64,
		sx: f64,
		sy: f64,
		sw: f64,
		sh: f64,
	) -> Result<(), wasm_bindgen::JsValue> {
		return self
			.context
			.put_image_data_with_dirty_x_and_dirty_y_and_dirty_width_and_dirty_height(
				image,
				dx - sx,
				dy - sy,
				sx,
				sy,
				sw,
				sh,
			);
	}
}

impl Context for WebCanvasContext {
	fn draw_image(
		&self,
		image: &Image,
		x: f64,
		y: f64,
	) {
		let result =
			self.context.draw_image_with_html_image_element(image.as_native(), x, y);
		match result {
			Ok(()) => {},
			Err(err) => error!("draw error:{:?}", err),
		}
	}

	fn draw_image_stretch(
		&self,
		image: &Image,
		x: f64,
		y: f64,
		w: f64,
		h: f64,
	) {
		let result = self.context.draw_image_with_html_image_element_and_dw_and_dh(
			image.as_native(),
			x,
			y,
			w,
			h,
		);
		match result {
			Ok(()) => {},
			Err(err) => error!("draw error:{:?}", err),
		}
	}

	fn draw_image_part(
		&self,
		image: &Image,
		sx: f64,
		sy: f64,
		sw: f64,
		sh: f64,
		dx: f64,
		dy: f64,
		dw: f64,
		dh: f64,
	) {
		let result = self
			.context
			.draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
				image.as_native(),
				sx,
				sy,
				sw,
				sh,
				dx,
				dy,
				dw,
				dh,
			);

		match result {
			Ok(()) => {},
			Err(err) => error!("draw error:{:?}", err),
		}
	}

	fn draw_imagebitmap_part(
		&self,
		image: &mut ImageData,
		dx: f64,
		dy: f64,
		sx: f64,
		sy: f64,
		sw: f64,
		sh: f64,
	) {
		match image.as_native() {
			Some(native) => {
				let result =
					self.draw_imagebitmap_part_1(&native, dx, dy, sx, sy, sw, sh);

				match result {
					Ok(()) => {},
					Err(err) => error!("draw error:{:?}", err),
				}
			},
			None => error!("native image data conversion failed"),
		}
	}

	fn draw_ellipse(
		&self,
		x: f64,
		y: f64,
		radius_x: f64,
		radius_y: f64,
		rotation: f64,
		start_angle: f64,
		end_angle: f64,
	) {
		self.context.begin_path();

		match self.context.ellipse(
			x,
			y,
			radius_x,
			radius_y,
			rotation,
			start_angle,
			end_angle,
		) {
			Ok(()) => {},
			Err(err) => error!("draw ellipse error:{:?}", err),
		}

		self.context.stroke();
	}

	fn draw_rect(
		&self,
		color: &Color,
		x: f64,
		y: f64,
		w: f64,
		h: f64,
	) {
		self.context.set_fill_style(&color.html_string().into());
		self.context.fill_rect(x, y, w, h);
	}
}
