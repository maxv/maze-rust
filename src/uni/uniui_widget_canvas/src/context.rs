use crate::{
	Image,
	ImageData,
};

use uniui_gui::Color;

/// Drawing context
///
/// The instance of the context will be passed to (Drawer)[crate::Drawer].
/// Then `Drawer` can use the context to change the canvas's content.
pub trait Context {
	fn draw_image(
		&self,
		image: &Image,
		x: f64,
		y: f64,
	);

	fn draw_image_stretch(
		&self,
		image: &Image,
		x: f64,
		y: f64,
		width: f64,
		height: f64,
	);

	fn draw_image_part(
		&self,
		image: &Image,
		sx: f64,
		sy: f64,
		sw: f64,
		sh: f64,
		dx: f64,
		dy: f64,
		dw: f64,
		dh: f64,
	);

	fn draw_imagebitmap_part(
		&self,
		image: &mut ImageData,
		dx: f64,
		dy: f64,
		sx: f64,
		sy: f64,
		sw: f64,
		sh: f64,
	);

	fn draw_ellipse(
		&self,
		x: f64,
		y: f64,
		radius_x: f64,
		radius_y: f64,
		rotation: f64,
		start_angle: f64,
		end_angle: f64,
	);

	fn draw_rect(
		&self,
		color: &Color,
		x: f64,
		y: f64,
		w: f64,
		h: f64,
	);
}
