#[cfg(target_arch = "wasm32")]
use web_sys::ImageData as WebImageData;

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::Clamped;


pub struct ImageData {
	data: Vec<u8>,
	width: u32,
	height: u32,
}

impl ImageData {
	pub fn new(
		data: Vec<u8>,
		width: u32,
		height: u32,
	) -> ImageData {
		return ImageData {
			data,
			width,
			height,
		};
	}

	#[cfg(target_arch = "wasm32")]
	pub fn as_native(&mut self) -> Option<WebImageData> {
		return WebImageData::new_with_u8_clamped_array_and_sh(
			Clamped(&mut self.data),
			self.width,
			self.height,
		)
		.ok();
	}

	pub fn width(&self) -> u32 {
		return self.width;
	}

	pub fn height(&self) -> u32 {
		return self.height;
	}

	pub fn data(&self) -> &Vec<u8> {
		return &self.data;
	}
}
