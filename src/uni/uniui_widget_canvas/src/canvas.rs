use crate::implz::Platform;

use crate::{
	Context as CanvasContext,
	Drawer as CanvasDrawer,
};

use uniui_gui::prelude::*;

pub struct Canvas<D: CanvasDrawer> {
	width: u32,
	height: u32,
	context: Option<Box<dyn CanvasContext>>,
	platform: Platform,
	drawer: Option<D>,
}

impl<D: CanvasDrawer> Canvas<D> {
	pub fn new() -> Canvas<D> {
		return Canvas {
			width: 0,
			height: 0,
			context: None,
			platform: Platform::new(),
			drawer: None,
		};
	}

	pub fn get_width(&self) -> u32 {
		return self.width;
	}

	pub fn get_height(&self) -> u32 {
		return self.height;
	}

	pub fn set_drawer(
		&mut self,
		drawer: Option<D>,
	) {
		self.drawer = drawer;
	}

	fn resize_if_needed(&mut self) {
		match self.platform.actual_size() {
			Some((width, height)) => {
				if self.width != width || self.height != height {
					log::warn!("resize:(w,h):({},{})", width, height);
					self.width = width;
					self.height = height;
					self.platform.set_width(self.width);
					self.platform.set_height(self.height);
				}
			},
			None => {},
		}
	}
}

impl<D: CanvasDrawer> Widget for Canvas<D> {
	fn draw(&mut self) {
		if let (Some(c), Some(drawer)) = (self.context.as_mut(), self.drawer.as_mut()) {
			drawer.draw(c.as_mut(), self.width, self.height);
		};
	}

	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let result = self.platform.to_native(widget_generator)?;
		self.context = self.platform.get_context();
		return Ok(result);
	}
}

impl<D: CanvasDrawer> DataProcessor for Canvas<D> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.resize_if_needed();
		if let Some(d) = self.drawer.as_mut() {
			d.process_data(msec_since_app_start, app);
		}
	}
}
