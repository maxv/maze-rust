mod canvas;
pub use self::{
	canvas::Canvas,
	implz::Image,
};

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
mod implz {
	mod wasm_canvas;
	pub use self::wasm_canvas::WasmCanvas as Platform;
	pub use web_sys::HtmlImageElement as Image;
}

#[doc(hidden)]
#[cfg(target_os = "linux")]
mod implz {
	mod qt5_canvas;
	pub use self::qt5_canvas::Qt5Canvas as Platform;
	pub type Image = u32;
}


use uniui_gui::DataProcessor;

/// The trait defines `draw` operation.
///
/// For more details see [Canvas](crate::Canvas)
pub trait Drawer: DataProcessor {
	fn draw(
		&mut self,
		context: &mut dyn Context,
		width: u32,
		height: u32,
	);
}

mod context;
pub use self::context::Context;

mod image_data;
pub use self::image_data::ImageData;

pub mod prelude {
	pub use super::{
		Canvas,
		Context,
		Drawer,
	};
}
