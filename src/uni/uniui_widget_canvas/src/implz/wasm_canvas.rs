use crate::Context;

use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use web_sys::HtmlCanvasElement;

use wasm_bindgen::JsCast;

mod wasm_canvas_context;
use self::wasm_canvas_context::WasmCanvasContext;

pub struct WasmCanvas {
	native: OptionalNative<HtmlCanvasElement>,
}

impl WasmCanvas {
	pub fn new() -> WasmCanvas {
		return WasmCanvas {
			native: OptionalNative::new(),
		};
	}

	pub fn actual_size(&self) -> Option<(u32, u32)> {
		return self
			.native
			.as_ref()
			.map(|c| c.get_bounding_client_rect())
			.map(|rect| (rect.width() as u32, rect.height() as u32));
	}

	pub fn set_width(
		&mut self,
		width: u32,
	) {
		self.native.as_mut().iter_mut().for_each(|c| c.set_width(width));
	}

	pub fn set_height(
		&mut self,
		height: u32,
	) {
		self.native.as_mut().iter_mut().for_each(|c| c.set_height(height));
	}

	pub fn get_context(&mut self) -> Option<Box<dyn Context>> {
		return self.native.as_mut().map(|canvas| {
			let context = canvas
				.get_context("2d")
				.unwrap()
				.unwrap()
				.dyn_into::<web_sys::CanvasRenderingContext2d>()
				.unwrap();
			let wasm_canvas_context = WasmCanvasContext::new(context);
			let result: Box<dyn Context> = Box::new(wasm_canvas_context);
			result
		});
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("canvas").unwrap();
		let canvas = node.dyn_into::<HtmlCanvasElement>().unwrap();
		self.native.replace(canvas.clone());
		return Ok(From::from(canvas));
	}
}
