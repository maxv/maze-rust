use crate::{
	Context,
	Image,
	ImageData,
};

use uniui_gui::Color;

pub struct Qt5CanvasContext {}

impl Qt5CanvasContext {
	pub fn new() -> Qt5CanvasContext {
		return Qt5CanvasContext {};
	}
}

impl Context for Qt5CanvasContext {
	fn draw_image(
		&self,
		_image: &Image,
		_x: f64,
		_y: f64,
	) {
	}

	fn draw_image_stretch(
		&self,
		_image: &Image,
		_x: f64,
		_y: f64,
		_w: f64,
		_h: f64,
	) {
	}

	fn draw_image_part(
		&self,
		_image: &Image,
		_sx: f64,
		_sy: f64,
		_sw: f64,
		_sh: f64,
		_dx: f64,
		_dy: f64,
		_dw: f64,
		_dh: f64,
	) {
	}

	fn draw_imagebitmap_part(
		&self,
		_image: &mut ImageData,
		_dx: f64,
		_dy: f64,
		_sx: f64,
		_sy: f64,
		_sw: f64,
		_sh: f64,
	) {
	}

	fn draw_ellipse(
		&self,
		_x: f64,
		_y: f64,
		_radius_x: f64,
		_radius_y: f64,
		_rotation: f64,
		_start_angle: f64,
		_end_angle: f64,
	) {
	}

	fn draw_rect(
		&self,
		_color: &Color,
		_x: f64,
		_y: f64,
		_w: f64,
		_h: f64,
	) {
	}
}
