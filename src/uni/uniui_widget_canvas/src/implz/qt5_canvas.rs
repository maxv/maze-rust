use crate::Context;

use uniui_gui::prelude::*;

mod qt5_canvas_context;
use self::qt5_canvas_context::Qt5CanvasContext;

pub struct Qt5Canvas {}

impl Qt5Canvas {
	pub fn new() -> Qt5Canvas {
		return Qt5Canvas {};
	}

	pub fn actual_size(&self) -> Option<(u32, u32)> {
		return None;
	}

	pub fn set_width(
		&mut self,
		_width: u32,
	) {
	}

	pub fn set_height(
		&mut self,
		_height: u32,
	) {
	}

	pub fn get_context(&mut self) -> Option<Box<dyn Context>> {
		return Some(Box::new(Qt5CanvasContext::new()));
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}
}
