extern crate uniui_core;
extern crate uniui_gui;

mod datepicker;
pub use self::datepicker::Datepicker;

mod macros;

pub mod prelude {
	pub use crate::{
		u_datepicker,
		Datepicker,
	};
	pub use chrono::{
		naive::NaiveDate,
		Datelike,
	};
}

pub const THEME_CLASS_NAME: &str = "u_datepicker_theme_class";

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_datepicker;
	pub use self::wasm_datepicker::WasmDatepicker as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_datepicker;
	pub use self::qt5_datepicker::Qt5Datepicker as Platform;
}
