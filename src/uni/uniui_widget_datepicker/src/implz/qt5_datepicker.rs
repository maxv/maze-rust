use std::sync::mpsc::Sender;

use chrono::naive::NaiveDate;

use uniui_gui::prelude::*;


pub struct Qt5Datepicker {}

impl Qt5Datepicker {
	pub fn new() -> Qt5Datepicker {
		return Qt5Datepicker {};
	}

	pub fn set_value(
		&mut self,
		_date: NaiveDate,
	) {
		log::error!("Qt5Datepicker is not implemented. Yet.");
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_sender: Sender<NaiveDate>,
	) -> Result<NativeWidget, ()> {
		log::error!("Qt5Datepicker is not implemented. Yet.");
		return Err(());
	}
}
