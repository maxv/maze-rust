use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::HtmlInputElement;

use crate::implz::wasm_bindgen::{
	closure::Closure,
	JsCast,
};

use chrono::{
	Datelike,
	NaiveDate,
};

use std::sync::mpsc::Sender;


pub struct WasmDatepicker {
	closure: Option<Closure<dyn FnMut()>>,
	native: OptionalNative<HtmlInputElement>,
}

impl WasmDatepicker {
	pub fn new() -> WasmDatepicker {
		return WasmDatepicker {
			closure: None,
			native: OptionalNative::new(),
		};
	}

	pub fn set_value(
		&mut self,
		date: NaiveDate,
	) {
		match self.native.as_mut() {
			Some(native) => {
				let date_string = format!(
					"{:0>4}-{:0>2}-{:0>2}",
					date.year(),
					date.month(),
					date.day()
				);
				native.set_value(&date_string);
			},
			None => {},
		};
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		sender: Sender<NaiveDate>,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("input").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let picker = node.dyn_into::<HtmlInputElement>().unwrap();
		picker.set_type("date");

		let closure_picker = picker.clone();
		let closure = Closure::wrap(Box::new(move || {
			let date_string = closure_picker.value();
			match NaiveDate::parse_from_str(&date_string, "%Y-%m-%d") {
				Ok(date) => sender.send_silent(date),
				Err(_) => log::warn!("incorrect date format:{}", date_string),
			}
		}) as Box<dyn FnMut()>);

		picker.set_onchange(Some(closure.as_ref().unchecked_ref()));
		self.closure = Some(closure);

		self.native.replace(picker.clone());
		Ok(From::from(picker))
	}
}
