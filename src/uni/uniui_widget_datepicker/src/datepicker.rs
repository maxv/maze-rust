use crate::implz::Platform;

use uniui_gui::prelude::*;

use chrono::naive::NaiveDate;

use std::sync::mpsc::{
	self,
	Receiver,
	Sender,
};

/// Widget for Date selection
pub struct Datepicker {
	platform: Platform,
	sender: Sender<NaiveDate>,
	receiver: Receiver<NaiveDate>,
	signal_date_selected: Signal<NaiveDate>,
	slot_set_date: SlotImpl<NaiveDate>,
}

impl Datepicker {
	pub fn new() -> Datepicker {
		let (sender, receiver) = mpsc::channel();
		return Datepicker {
			platform: Platform::new(),
			sender,
			receiver,
			signal_date_selected: Signal::new(),
			slot_set_date: SlotImpl::new(),
		};
	}

	pub fn signal_date_selected(&mut self) -> &mut Signal<NaiveDate> {
		return &mut self.signal_date_selected;
	}

	pub fn slot_set_date(&self) -> &dyn Slot<NaiveDate> {
		return &self.slot_set_date;
	}

	fn update_selected_value(&mut self) {
		if let Some(v) = self.slot_set_date.last() {
			self.platform.set_value(v);
		}
	}
}

impl Widget for Datepicker {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let result = self.platform.to_native(widget_generator, self.sender.clone());
		self.update_selected_value();
		return result;
	}
}

impl DataProcessor for Datepicker {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		self.update_selected_value();

		for date in self.receiver.try_recv() {
			self.signal_date_selected.emit(date);
		}

		if let Some(date) = self.slot_set_date.last() {
			self.platform.set_value(date);
		}
	}
}
