#[doc(hidden)]
#[macro_export]
macro_rules! u_datepicker_int {
	($b:ident,signal_date_selected: $va:expr) => {
		$b.signal_date_selected().connect_slot($va);
	};
	($b:ident,slot_set_date: $va:expr) => {
		$va.connect_slot($b.slot_set_date());
	};
	($b:ident,slot_set_date_proxy: $va:expr) => {
		$va = $b.slot_set_date().proxy();
	};
}

/// Simplifies [Datepicker](crate::Datepicker) creation
///
/// Parameters:
/// * signal_date_selected (optional): [&Slot\<NaiveDate\>](uniui_core::Slot), will be
///   connected to [Datepicker::signal_date_selected] to receive
///   [NaiveDate](chrono::NaiveDate),
/// * slot_set_date (optional): [&Signal\<NaiveDate\>](uniui_core::Signal) -
///   [NaiveDate](chrono::NaiveDate),
/// * slot_set_date_proxy (optional): identifier,
///
/// Example:
/// ```
/// TBD
/// ```
#[macro_export]
macro_rules! u_datepicker {
	($($param_name:ident: $val:expr),*$(,)*) => {{
		let mut dp = $crate::Datepicker::new();
		$($crate::u_datepicker_int!(dp, $param_name: $val);)*;
		dp
	}};
}
