// Buildsystem will put all our files into the package with the 
// same name as our crate's name is.
package uniui_layout_linear_layout;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class UniuiLinearLayout extends LinearLayout{
	public UniuiLinearLayout(Context context) {
		super(context);
	}

	public void uniAddView(View view) {
		if (view.getLayoutParams() == null) {
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT,
				1.0f
			);
			lp.setMargins(0,0,0,0);
			addView(view, lp);
		} else {
			addView(view);
		}
	}

	public void uniSetOrientation(int orientation) {
		final int width;
		final int height;
		switch (orientation) {
			case VERTICAL:
					width = ViewGroup.LayoutParams.MATCH_PARENT;
					height = ViewGroup.LayoutParams.WRAP_CONTENT;
				break;
			case HORIZONTAL:
			default:
				width = ViewGroup.LayoutParams.WRAP_CONTENT;
				height = ViewGroup.LayoutParams.MATCH_PARENT;
				break;
		}

		int childCount = getChildCount();
		for (int i = 0; i < childCount; ++i) {
			View child = getChildAt(i);
			LinearLayout.LayoutParams lp = (LayoutParams) child.getLayoutParams();
			lp.width = width;
			lp.height = height;
			child.setLayoutParams(lp);
		}

		setOrientation(orientation);
	}
}

