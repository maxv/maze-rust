#include <QApplication>
#include <QLabel>
#include <QHBoxLayout>
#include <QWidget>

#include <QPalette>
#include <QColor>

namespace uniui_layout_linear_layout {
	enum Orientation {
		Horizontal,
		Vertical,
	};
}

extern "C" {
	extern const int UniUiLayoutLinearLayoutOrientationHorizontal = uniui_layout_linear_layout::Horizontal;
	extern const int UniUiLayoutLinearLayoutOrientationVertical = uniui_layout_linear_layout::Vertical;
};


extern "C" void* uniui_layout_linear_layout_create(int orientation) {
	QBoxLayout* result;
	
	switch (orientation) {
		case UniUiLayoutLinearLayoutOrientationVertical:
			result = new QVBoxLayout();
		break;
		
		default:
		case UniUiLayoutLinearLayoutOrientationHorizontal:
			result = new QHBoxLayout();
		break;
	}
	
	return result;
}

extern "C" void uniui_layout_linear_layout_add_widget(void* layout, void* widget) {
	QWidget* w = (QWidget*) widget;
	QBoxLayout* l = (QBoxLayout*) layout;
	l->addWidget(w);
}

extern "C" void* uniui_layout_linear_layout_create_widget_with_layout(void* layout) {
	QWidget* widget = new QWidget();
	widget->setLayout((QBoxLayout*)layout);
	return widget;
}

extern "C" void uniui_layout_linear_layout_set_widget_background(void* qwidget, unsigned char red, unsigned char green, unsigned char blue) {
	QWidget* w = (QWidget*) qwidget;
	
	QPalette pal = QPalette();
	pal.setColor(QPalette::Background, QColor(red, green, blue));
	w->setAutoFillBackground(true);
	w->setPalette(pal);
}

