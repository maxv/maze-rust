//! [LinearLayout](crate::LinearLayout) can help you to orginize set of widgets into
//! Horizontal or Vertical line. Please check [LinearLayout](crate::LinearLayout)
//! documentation for more info.

mod linear_layout;
pub use self::linear_layout::LinearLayout;

mod macros;

/// Contains [LinearLayout], [u_linear_layout!], [ItemsAlignment] and
/// [Orientation](uniui_gui::Orientation),
/// [AlignmentHorizontal](uniui_gui::AlignmentHorizontal),
/// [AlignmentVertical](uniui_gui::AlignmentVertical).
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_linear_layout,
		ItemsAlignment,
		LinearLayout,
	};
	#[doc(hidden)]
	pub use uniui_gui::{
		AlignmentHorizontal,
		AlignmentVertical,
		Orientation,
	};
}

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
mod implz {
	mod wasm_linear_layout;
	pub use self::wasm_linear_layout::WasmLinearLayout as Platform;
}

#[doc(hidden)]
#[cfg(target_os = "linux")]
mod implz {
	uniui_gui::desktop_qt! {
		mod qt5_linear_layout;
		pub use self::qt5_linear_layout::Qt5LinearLayout as Platform;
	}

	uniui_gui::desktop_noop! {
		mod noop_linear_layout;
		pub use self::noop_linear_layout::NoopLinearLayout as Platform;
	}
}

#[doc(hidden)]
#[cfg(target_os = "android")]
extern crate uni_tmp_jni as jni;

#[doc(hidden)]
#[cfg(target_os = "android")]
mod implz {
	mod android_linear_layout;
	pub use self::android_linear_layout::AndroidLinearLayout as Platform;
}

/// Possible alignment of the items in [LinearLayout](crate::LinearLayout).
#[derive(Debug, Copy, Clone)]
pub enum ItemsAlignment {
	/// Align widgets to the center
	Center,

	/// Align widgets to the start
	Start,

	/// Align widgets to the end
	End,

	/// Stretches widget to fill up all available space
	Stretch,
}
