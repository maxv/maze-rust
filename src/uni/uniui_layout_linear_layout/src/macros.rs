#[doc(hidden)]
#[macro_export]
macro_rules! u_linear_layout_int {
	($layout:ident, widgets, {$($w:expr),*$(,)*}) => {
		$($layout.push_widget($w);)*;
	};
	($layout:ident, alignment_horizontal, $w:expr) => {
		$layout.slot_alignment_horizontal().exec_for($w);
	};
	($layout:ident, alignment_vertical, $w:expr) => {
		$layout.slot_alignment_vertical().exec_for($w);
	};
	($layout:ident, items_alignment, $w:expr) => {
		$layout.slot_set_items_alignment().exec_for($w);
	};
}


/// Simplifies [LinearLayout](crate::LinearLayout) creation
///
/// Parameters:
/// * orientation (required, always first parameter, parameter name may be omited):
///   layout's [Orientation](uniui_gui::Orientation),
/// * alignment_horizontal (optional): layout's
///   [AlignmentHorizontal](uniui_gui::AlignmentHorizontal),
/// * alignemnt_verical (optional): layout's
///   [AlignmentVertical](uniui_gui::AlignmentVertical),
/// * items_alignment (optional): [ItemsAlignment]
/// * widgets (optional): ordered list of widgets to be moved to linear layout.
///
/// Example:
/// ```
/// use uniui_gui::Orientation;
/// use uniui_widget_button::Button;
///
/// let linear_layout = u_linear_layout! {
///     orientation: Orientation::Vertical,
///     widgets: {
///         Button::new("Button1".to_owned()),
///         Button::new("Button2".to_owned()),
///     }
/// };
/// ```
#[macro_export]
macro_rules! u_linear_layout {
	(orientation: $orientation:expr, $($kind:ident : $val:tt),*$(,)?) => {{
		let mut layout = $crate::LinearLayout::new($orientation);
		$($crate::u_linear_layout_int!(layout, $kind, $val);)*;
		layout
	}};
	($orientation:expr, $($kind:ident : $val:tt),*$(,)?) => {{
		let mut layout = $crate::LinearLayout::new($orientation);
		$($crate::u_linear_layout_int!(layout, $kind, $val);)*;
		layout
	}};
}
