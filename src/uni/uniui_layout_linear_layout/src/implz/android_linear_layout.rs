use crate::ItemsAlignment;

use uniui_gui::{
	implz::android::{
		jni_error::JniError,
		AndroidOptionalNative,
	},
	AlignmentHorizontal,
	AlignmentVertical,
	Color,
	NativeWidget,
	Orientation,
	Widget,
	WidgetGenerator,
	Wrap,
};

mod native_interface;
use native_interface::{
	LinearLayout,
	NativeLinearLayout,
};

pub struct AndroidLinearLayout {
	native: AndroidOptionalNative<NativeLinearLayout>,
}

impl AndroidLinearLayout {
	pub fn new() -> Self {
		return Self {
			native: AndroidOptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		wg: &mut dyn WidgetGenerator,
		childs: &mut Vec<Box<dyn Widget>>,
		orientation: &Orientation,
		_wrap: &Wrap,
		_alignment_horizontal: &AlignmentHorizontal,
		_alignment_vertical: &AlignmentVertical,
		_alignment: &ItemsAlignment,
	) -> Result<NativeWidget, ()> {
		let native_widgets: Vec<_> = childs.iter_mut().map(|c| c.to_native(wg)).collect();

		let toe = || -> Result<NativeLinearLayout, JniError> {
			let layout = LinearLayout::new_for_context(wg.context())?;

			for nw in native_widgets {
				match nw {
					Ok(native_widget) => {
						layout.add_widget(native_widget)?;
					},
					Err(e) => log::error!("child's to_native err:{:?}", e),
				}
			}

			layout.set_orientation(orientation)?;
			self.native.replace(&layout)?;

			Ok(layout)
		};

		return match toe() {
			Ok(layout) => Ok(layout.into_global_ref()),
			Err(e) => {
				log::error!("to_native failed err:{:?}", e);
				Err(())
			},
		};
	}

	pub fn set_background(
		&mut self,
		_color: &Color,
	) {
		log::error!("set_background is not implemented for Android. Yet.");
	}

	pub fn update_alignment(
		&mut self,
		_alignment: &ItemsAlignment,
	) {
		log::error!("ItemsAlignment not implemented for Android. Yet.");
	}

	pub fn update_wrap(
		&mut self,
		_wrap: &Wrap,
	) {
		log::error!("Wrap not implemented for Android. Yet.");
	}

	pub fn update_alignment_horizontal(
		&mut self,
		_alignment_horizontal: &AlignmentHorizontal,
	) {
		log::error!("Alignment_horizontal not implemented for Android. Yet.");
	}

	pub fn update_alignment_vertical(
		&mut self,
		_alignment_vertical: &AlignmentVertical,
	) {
		log::error!("Alignment_vertical not implemented for Android. Yet.");
	}
}
