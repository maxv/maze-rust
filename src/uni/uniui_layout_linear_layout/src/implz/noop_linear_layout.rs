use crate::ItemsAlignment;

use uniui_gui::{
	AlignmentHorizontal,
	AlignmentVertical,
	Color,
	NativeWidget,
	Orientation,
	Widget,
	WidgetGenerator,
	Wrap,
};

pub struct NoopLinearLayout {}
		
impl NoopLinearLayout {
	pub fn new() -> NoopLinearLayout {
		return NoopLinearLayout {
		};
	}

	pub fn set_background(
		&mut self,
		_color: &Color,
	) {
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_childs: &mut Vec<Box<dyn Widget>>,
		_orientation: &Orientation,
		_wrap: &Wrap,
		_alignment_horizontal: &AlignmentHorizontal,
		_alignment_vertical: &AlignmentVertical,
		_alignment: &ItemsAlignment,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}

	pub fn update_alignment(
		&mut self,
		_alignment: &ItemsAlignment,
	) {
	}

	pub fn update_wrap(
		&mut self,
		_wrap: &Wrap,
	) {
	}

	pub fn update_alignment_horizontal(
		&mut self,
		_alignment_horizontal: &AlignmentHorizontal,
	) {
	}

	pub fn update_alignment_vertical(
		&mut self,
		_alignment_vertical: &AlignmentVertical,
	) {
	}
}
