use jni::{
	objects::{
		JClass,
		JObject,
		JValue,
	},
	JNIEnv,
	JavaVM,
};


use uniui_gui::implz::android::{
	android_context::Context,
	java_object::{
		JavaObject,
		JavaObjectConstructorable,
	},
	jni_error::JniError,
	uni_global_ref::UniGlobalRef,
};


pub struct NativeLinearLayout {
	global_ref: UniGlobalRef<JObject<'static>>,
	vm: JavaVM,
}

impl NativeLinearLayout {
	pub fn into_global_ref(self) -> UniGlobalRef<JObject<'static>> {
		return self.global_ref;
	}
}

impl JavaObject for NativeLinearLayout {
	fn jobject(&self) -> JObject {
		return self.global_ref.as_obj();
	}

	fn jni_env(&self) -> Result<JNIEnv, JniError> {
		return Ok(self.vm.get_env()?);
	}
}

impl JavaObjectConstructorable for NativeLinearLayout {
	fn construct(
		global_ref: UniGlobalRef<JObject<'static>>,
		vm: jni::JavaVM,
	) -> Self {
		return Self {
			global_ref,
			vm,
		};
	}
}

impl LinearLayout for NativeLinearLayout {
}

pub trait LinearLayout: JavaObject {
	fn add_widget(
		&self,
		widget: uniui_gui::NativeWidget,
	) -> Result<(), JniError> {
		use jni::signature::{
			JavaType::Primitive,
			Primitive::Void,
		};

		let env = self.jni_env()?;

		let add_view = uniui_gui::get_java_method!(
			env,
			LinearLayout::get_class(&env)?.as_obj(),
			"uniAddView",
			"(Landroid/view/View;)V"
		);

		env.call_method_unchecked(self.jobject(), add_view, Primitive(Void), &[
			JValue::Object(widget.as_obj()),
		])?;

		return Ok(());
	}

	fn set_orientation(
		&self,
		orientation: &uniui_gui::Orientation,
	) -> Result<(), JniError> {
		use jni::{
			signature::{
				JavaType::Primitive,
				Primitive::Void,
			},
			sys::jint,
		};

		const HORIZONTAL: jint = 0;
		const VERTICAL: jint = 1;

		let orientation = match orientation {
			uniui_gui::Orientation::Horizontal => HORIZONTAL,
			uniui_gui::Orientation::Vertical => VERTICAL,
		};

		let env = self.jni_env()?;

		let method = uniui_gui::get_java_method!(
			env,
			LinearLayout::get_class(&env)?.as_obj(),
			"uniSetOrientation",
			"(I)V"
		);

		env.call_method_unchecked(self.jobject(), method, Primitive(Void), &[
			JValue::Int(orientation),
		])?;

		return Ok(());
	}
}

impl dyn LinearLayout {
	pub fn new_for_context(
		context: &dyn Context
	) -> Result<NativeLinearLayout, JniError> {
		let env = context.jni_env()?;
		let button_class = Self::get_class(&env)?;

		let ctor = uniui_gui::get_java_method!(
			env,
			button_class.as_obj(),
			"<init>",
			"(Landroid/content/Context;)V"
		);

		let local_object =
			env.new_object_unchecked(button_class.as_obj(), ctor, &[JValue::Object(
				context.jobject(),
			)])?;

		// Yes, it looks non-elegant. But I don't know any other way to
		// convert JObject<'a> into JObject<'static>.
		// And UniGlobalRef will not work for JObject<'a>
		let local_object: JObject<'static> = local_object.into_inner().into();

		let global_ref = UniGlobalRef::try_new(&env, local_object)?;

		return Ok(NativeLinearLayout {
			global_ref,
			vm: env.get_java_vm()?,
		});
	}

	fn get_class(env: &JNIEnv) -> Result<UniGlobalRef<JClass<'static>>, JniError> {
		let class = uniui_gui::get_java_class!(
			env,
			"uniui_layout_linear_layout/UniuiLinearLayout"
		)?;
		return Ok(class);
	}
}
