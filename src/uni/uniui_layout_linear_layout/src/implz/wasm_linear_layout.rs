use crate::ItemsAlignment;

use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	utils::*,
	AlignmentHorizontal,
	AlignmentVertical,
	Color,
	NativeWidget,
	Orientation,
	Widget,
	WidgetGenerator,
	Wrap,
};

use web_sys::HtmlDivElement;

use wasm_bindgen::JsCast;

pub struct WasmLinearLayout {
	native: OptionalNative<HtmlDivElement>,
}

impl WasmLinearLayout {
	pub fn new() -> WasmLinearLayout {
		return WasmLinearLayout {
			native: OptionalNative::new(),
		};
	}

	pub fn set_background(
		&mut self,
		color: &Color,
	) {
		match self.native.as_mut() {
			Some(n) => {
				n.style().set_property_silent("background", &color.html_string());
			},
			None => {},
		}
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		childs: &mut Vec<Box<dyn Widget>>,
		orientation: &Orientation,
		wrap: &Wrap,
		alignment_horizontal: &AlignmentHorizontal,
		alignment_vertical: &AlignmentVertical,
		alignment: &ItemsAlignment,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("div").unwrap();
		let div = node.dyn_into::<HtmlDivElement>().unwrap();
		let style = div.style();
		style.set_property_silent("display", "flex");

		match orientation {
			Orientation::Horizontal => style.set_property_silent("flex-direction", "row"),
			Orientation::Vertical => {
				style.set_property_silent("flex-direction", "column")
			},
		};

		childs.iter_mut().for_each(|child| {
			match child.as_mut().to_native(widget_generator) {
				Ok(e) => {
					let style = e.style();
					match style.get_property_value("flex-grow") {
						Ok(v) => {
							match v.is_empty() {
								true => style.set_property_silent("flex-grow", "1"),
								false => log::trace!("flex already setted up"),
							}
						},
						Err(_) => log::warn!("flex Err"),
					};

					match style.get_property_value("max-height") {
						Ok(v) => {
							match v.is_empty() {
								true => style.set_property_silent("max-height", "100%"),
								false => {},
							};
						},
						Err(_) => {},
					};

					match style.get_property_value("max-width") {
						Ok(v) => {
							match v.is_empty() {
								true => style.set_property_silent("max-width", "100%"),
								false => {},
							};
						},
						Err(_) => {},
					};

					match div.append_child(&e) {
						Ok(_) => log::trace!("layout ok"),
						Err(e) => log::error!("layout inssert err:{:?}", e),
					}
				},
				Err(()) => log::error!("layout failed"),
			}
		});


		self.native.replace(div.clone());

		self.update_wrap(wrap);
		self.update_alignment_horizontal(alignment_horizontal);
		self.update_alignment_vertical(alignment_vertical);
		self.update_alignment(alignment);

		Ok(From::from(div))
	}

	pub fn update_wrap(
		&mut self,
		wrap: &Wrap,
	) {
		match self.native.as_mut() {
			Some(n) => {
				let wrap_str = match wrap {
					Wrap::Wrap => "wrap",
					Wrap::NoWrap => "nowrap",
				};

				n.style().set_property_silent("flex-wrap", wrap_str);
			},
			None => log::warn!("native not found. failed"),
		}
	}

	pub fn update_alignment_horizontal(
		&mut self,
		alignment_horizontal: &AlignmentHorizontal,
	) {
		match self.native.as_mut() {
			Some(n) => {
				let value = match alignment_horizontal {
					AlignmentHorizontal::Center => "center",
					AlignmentHorizontal::Start => "flex-start",
					AlignmentHorizontal::End => "flex-end",
					AlignmentHorizontal::SpaceAround => "space-around",
					AlignmentHorizontal::SpaceBetween => "space-between",
					AlignmentHorizontal::Stretch => "stretch",
				};

				n.style().set_property_silent("justify-content", value);
			},
			None => log::warn!("native not found. failed"),
		}
	}

	pub fn update_alignment_vertical(
		&mut self,
		alignment_vertical: &AlignmentVertical,
	) {
		match self.native.as_mut() {
			Some(n) => {
				let value = match alignment_vertical {
					AlignmentVertical::Center => "center",
					AlignmentVertical::Start => "flex-start",
					AlignmentVertical::End => "flex-end",
					AlignmentVertical::SpaceAround => "space-around",
					AlignmentVertical::SpaceBetween => "space-between",
					AlignmentVertical::Stretch => "stretch",
				};

				n.style().set_property_silent("align-content", value);
			},
			None => log::warn!("native not found. failed"),
		}
	}

	pub fn update_alignment(
		&mut self,
		alignment: &ItemsAlignment,
	) {
		match self.native.as_mut() {
			Some(n) => {
				let alignment_val = match alignment {
					ItemsAlignment::Center => "center",
					ItemsAlignment::Start => "start",
					ItemsAlignment::End => "end",
					ItemsAlignment::Stretch => "stretch",
				};
				n.style().set_property_silent("align-items", alignment_val);
			},
			None => log::warn!("native not found. failed"),
		}
	}
}
