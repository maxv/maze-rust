use crate::ItemsAlignment;

use uniui_gui::{
	implz::qt5::Qt5OptionalNative,
	AlignmentHorizontal,
	AlignmentVertical,
	Color,
	NativeWidget,
	Orientation,
	Widget,
	WidgetGenerator,
	Wrap,
};

use std::ffi::c_void;

extern "C" {
	static UniUiLayoutLinearLayoutOrientationHorizontal: i32;
	static UniUiLayoutLinearLayoutOrientationVertical: i32;


	fn uniui_layout_linear_layout_create(orientation: i32) -> *mut c_void;
	fn uniui_layout_linear_layout_add_widget(
		layout: *mut c_void,
		widget: *mut c_void,
	);
	fn uniui_layout_linear_layout_create_widget_with_layout(
		layout: *mut c_void
	) -> *mut c_void;
	fn uniui_layout_linear_layout_set_widget_background(
		widget: *mut c_void,
		red: u8,
		green: u8,
		blue: u8,
	);
}

pub struct Qt5LinearLayout {
	native: Qt5OptionalNative,
}

impl Qt5LinearLayout {
	pub fn new() -> Qt5LinearLayout {
		return Qt5LinearLayout {
			native: Qt5OptionalNative::new(),
		};
	}

	pub fn set_background(
		&mut self,
		color: &Color,
	) {
		if let Some(p) = self.native.as_mut() {
			unsafe {
				uniui_layout_linear_layout_set_widget_background(
					p,
					color.red,
					color.green,
					color.blue,
				);
			};
		}
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		childs: &mut Vec<Box<dyn Widget>>,
		orientation: &Orientation,
		wrap: &Wrap,
		alignment_horizontal: &AlignmentHorizontal,
		alignment_vertical: &AlignmentVertical,
		alignment: &ItemsAlignment,
	) -> Result<NativeWidget, ()> {
		let native_layout = unsafe {
			let orientation = Qt5LinearLayout::convert_orientation(orientation);
			uniui_layout_linear_layout_create(orientation)
		};

		for c in childs.iter_mut() {
			match c.to_native(widget_generator) {
				Ok(native_widget) => unsafe {
					uniui_layout_linear_layout_add_widget(native_layout, native_widget);
				},
				Err(e) => log::error!("child's to_native err:{:?}", e),
			}
		}

		let native_widget = unsafe {
			uniui_layout_linear_layout_create_widget_with_layout(native_layout)
		};

		unsafe {
			self.native.replace(native_widget);
		};

		self.update_wrap(wrap);
		self.update_alignment_horizontal(alignment_horizontal);
		self.update_alignment_vertical(alignment_vertical);
		self.update_alignment(alignment);

		return Ok(native_widget);
	}

	pub fn update_alignment(
		&mut self,
		_alignment: &ItemsAlignment,
	) {
		log::error!("ItemsAlignment not implemented for Qt5. Yet.");
	}

	pub fn update_wrap(
		&mut self,
		_wrap: &Wrap,
	) {
		log::error!("Wrap not implemented for Qt5. Yet.");
	}

	pub fn update_alignment_horizontal(
		&mut self,
		_alignment_horizontal: &AlignmentHorizontal,
	) {
		log::error!("Alignment_horizontal not implemented for Qt5. Yet.");
	}

	pub fn update_alignment_vertical(
		&mut self,
		_alignment_vertical: &AlignmentVertical,
	) {
		log::error!("Alignment_vertical not implemented for Qt5. Yet.");
	}

	unsafe fn convert_orientation(o: &Orientation) -> i32 {
		return match o {
			Orientation::Horizontal => UniUiLayoutLinearLayoutOrientationHorizontal,
			Orientation::Vertical => UniUiLayoutLinearLayoutOrientationVertical,
		};
	}
}
