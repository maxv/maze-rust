extern crate uni_tmp_jni as jni;

mod uni_global_ref;
pub use uni_global_ref::UniGlobalRef;

pub mod macroses;

#[doc(hidden)]
pub mod reimport {
	pub mod jni {
		pub use jni::*;
	}

	pub mod parking_lot {
		pub use parking_lot::*;
	}

	pub mod log {
		pub use log::*;
	}
}

/// Represents JNI error
#[non_exhaustive]
#[derive(Debug, thiserror::Error)]
pub enum JniError {
	/// Error from jni-rs crate
	#[error("jni went wrong")]
	JniError(#[from] jni::errors::Error),

	/// Another type of error
	#[error("something interesing happend what we don't know about")]
	Custom(Box<dyn std::error::Error>),
}

/// Clears pending exceptions
pub fn clear_exception(
	env: &jni::JNIEnv,
	e: jni::errors::Error,
) {
	if let jni::errors::Error::JavaException = e {
		if let Err(_) = env.exception_clear() {
			// what else can we do?
			log::error!("exception clear failed");
		}
	}
}
