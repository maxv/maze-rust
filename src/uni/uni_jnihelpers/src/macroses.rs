#[derive(Copy, Clone)]
#[doc(hidden)]
pub struct JMethodIDHack {
	pub jmethodid: jni::sys::jmethodID,
}

unsafe impl Send for JMethodIDHack {
}
unsafe impl Sync for JMethodIDHack {
}

/// At the point of instantiaction the macros creates global cache to
/// access java class by name.
///
/// ```
/// let class: Result<jni::objects::JClass<'static>, uni_jnihelpers::JniError>;
/// class = get_java_class!(jni_env, "my/class/full/Name");
/// ```
#[macro_export]
macro_rules! get_java_class {
	($env:expr, $class_name:expr) => {{
		use $crate::{
			reimport::{
				jni::objects::JClass,
				parking_lot::{
					const_rwlock,
					RwLock,
				},
			},
			JniError,
			UniGlobalRef as UGR,
			};

		static CACHE: RwLock<Option<UGR<JClass<'static>>>> = const_rwlock(None);

		let read_lock = CACHE.read();
		let result = match read_lock.as_ref() {
			Some(bc) => Ok(bc.clone()),
			None => {
				std::mem::drop(read_lock);
				match $env.find_class($class_name) {
					Err(e) => Err(JniError::JniError(e)),
					Ok(class) => {
						let class: JClass<'static> = class.into_inner().into();
						match UGR::try_new($env, class) {
							Err(e) => Err(e),
							Ok(class) => {
								*CACHE.write() = Some(class.clone());
								Ok(class)
							},
						}
					},
				}
			},
			};

		result
		}};
}


/// At the point of instantiaction the macros creates global cache to
/// access java method by name.
///
/// ```
/// let method: jni::sys::jmethodID = get_java_method!(
///     jni_env,
///     class_ref,  // jni::objects::JClass
///     "methodName",
///     "(Lmethod;Lsignature;)V"
/// )?;
/// ```
#[macro_export]
macro_rules! get_java_method {
	($env:expr, $class:expr, $method_name:expr, $method_signature:expr) => {{
		use $crate::{
			macroses::JMethodIDHack,
			reimport::{
				jni::objects::JMethodID,
				parking_lot::{
					const_rwlock,
					RwLock,
				},
			},
			};

		static CACHE: RwLock<Option<JMethodIDHack>> = const_rwlock(None);

		let method = CACHE.read();
		let jmethodid = match method.as_ref() {
			Some(method) => method.jmethodid,
			None => {
				std::mem::drop(method);
				let maybe_method =
					$env.get_method_id($class, $method_name, $method_signature);
				let method = match maybe_method {
					Ok(method) => method,
					Err(e) => {
						$crate::reimport::log::error!("get_method err:{:?}", e);
						// We call it one more time to allow Java to print stack trace for
						// potential exception
						let _ =
							$env.get_method_id($class, $method_name, $method_signature);
						panic!("get_method failed");
					},
				};

				let method = JMethodIDHack {
					jmethodid: method.into_inner(),
				};

				*CACHE.write() = Some(method);
				method.jmethodid
			},
			};

		JMethodID::from(jmethodid)
		}};
}

/// At the point of instantiaction the macros creates global cache to
/// access java static method by name.
///
/// ```
/// let method: jni::sys::jmethodID = get_java_static_method!(
///     jni_env,
///     class_ref,  // jni::objects::JClass
///     "methodName",
///     "(Lmethod;Lsignature;)V"
/// )?;
/// ```
#[macro_export]
macro_rules! get_java_static_method {
	($env:expr, $class:expr, $method_name:expr, $method_signature:expr) => {{
		use $crate::{
			macroses::JMethodIDHack,
			reimport::{
				jni::objects::JStaticMethodID,
				parking_lot::{
					const_rwlock,
					RwLock,
				},
			},
			};

		static CACHE: RwLock<Option<JMethodIDHack>> = const_rwlock(None);

		let method = CACHE.read();
		let jmethodid = match method.as_ref() {
			Some(method) => method.jmethodid,
			None => {
				std::mem::drop(method);
				let maybe_method =
					$env.get_static_method_id($class, $method_name, $method_signature);
				let method = match maybe_method {
					Ok(method) => method,
					Err(e) => {
						$crate::reimport::log::error!("get_method err:{:?}", e);
						// We call it one more time to allow Java to print stack trace for
						// potential exception
						let _ = $env.get_static_method_id(
							$class,
							$method_name,
							$method_signature,
						);
						panic!("get_method failed");
					},
				};

				let method = JMethodIDHack {
					jmethodid: method.into_inner(),
				};

				*CACHE.write() = Some(method);
				method.jmethodid
			},
			};

		JStaticMethodID::from(jmethodid)
		}};
}
