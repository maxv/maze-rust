// Buildsystem will put all our files into the package with the 
// same name as our crate's name is.
package uniui_widget_textedit

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

@SuppressLint("AppCompatCustomView")
class UniuiWidgetTextEdit(context: Context?) : EditText(context) {
    init {
        addTextChangedListener(
            MyWatcher()
        )
    }

    inner class MyWatcher: TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // TODO("Not yet implemented")
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (native != 0L) {
                native = textChanged(native, s.toString());
            }
        }

        override fun afterTextChanged(s: Editable?) {
            // TODO("Not yet implemented")
        }
    }

    protected fun finalize() {
        val old_native = native
        native = 0
        free(old_native)
    }

    private external fun textChanged(native: Long, text: String): Long
    private external fun free(native: Long)

    // public to simplify JNI access
    public var native: Long = 0
}

