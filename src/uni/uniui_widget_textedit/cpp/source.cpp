#include <QApplication>
#include <QLineEdit>
#include <QWidget>


extern "C" void* uniui_widget_textedit_create(char* text) {
	QLineEdit* result = new QLineEdit();
	result->setPlaceholderText(text);
	return result;
}

namespace uniui_widget_textedit{
typedef void (*sender_callback)(const char*, void*); 
sender_callback SENDER_CALLBACK = nullptr;
}

extern "C" void uniui_widget_textedit_set_global_text_changed_callback(
	uniui_widget_textedit::sender_callback callback
) {
	uniui_widget_textedit::SENDER_CALLBACK = callback;
}

extern "C" void uniui_widget_textedit_set_text_chnage_listener_listener(void* qlineedit, void* data) {
	QObject::connect(
		((QLineEdit*)qlineedit),
		&QLineEdit::textChanged,
		[=](const QString& text){ 
			uniui_widget_textedit::SENDER_CALLBACK(
				text.toUtf8().constData(), 
				data
			); 
		}
	);
}

extern "C" void uniui_widget_textedit_set_text(void* qlineedit, const char* text) {
	((QLineEdit*)qlineedit)->setText(text);
}






