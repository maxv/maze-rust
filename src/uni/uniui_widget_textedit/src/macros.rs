#[doc(hidden)]
#[macro_export]
macro_rules! u_textedit_int {
	($b:ident,slot_set_text: $va:expr) => {
		$va.connect_slot($b.slot_set_text());
	};
	($b:ident,slot_set_text_proxy: $va:expr) => {
		$va = $b.slot_set_text().proxy();
	};

	($b:ident,signal_text_changed: $va:expr) => {
		$b.signal_text_changed().connect_slot($va);
	};
}

/// Simplifies [TextEdit](crate::TextEdit) creation
///
/// Parameters:
/// * placeholder (required): String,
/// * slot_set_text (optional): provided [Signal\<String\>](uniui_core::Signal) will be
///   connected to [TextEdit::slot_set_text],
/// * slot_set_text_proxy (optional): [TextEdit::slot_set_text]'s proxy will be assigned
///   to provided identifier,
/// * signal_text_changed (optional): [TextEdit::signal_text_changed] will be connected to
///   provided [Slot\<String\>](uniui_core::Slot).
///
///
/// Example:
/// ```
/// let proxyslot_set_text;
///
/// let label = u_textedit! {
///     placeholder: "First Name".to_owned(),
///     slot_set_text_proxy: proxyslot_set_text,
/// };
///
/// // ...
///
/// // Changes TextEdit's text to "John"
/// proxyslot_set_text.exec_for("John".to_owned())
/// ```
#[macro_export]
macro_rules! u_textedit {
	(placeholder: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut textedit = $crate::TextEdit::new($w);
		$($crate::u_textedit_int!(textedit, $param_name: $val);)*;
		textedit
	}};
}
