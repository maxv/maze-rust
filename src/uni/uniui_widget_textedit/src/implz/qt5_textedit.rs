use uniui_gui::{
	implz::qt5::Qt5OptionalNative,
	NativeWidget,
	WidgetGenerator,
};

use std::{
	ffi::{
		c_void,
		CStr,
		CString,
	},
	os::raw::c_char,
	sync::mpsc::Sender,
};

extern "C" {
	fn uniui_widget_textedit_create(placehoder: *const c_char) -> *mut c_void;

	fn uniui_widget_textedit_set_global_text_changed_callback(
		cb: extern "C" fn(*const c_char, *mut c_void)
	);

	fn uniui_widget_textedit_set_text_chnage_listener_listener(
		qlineedit: *mut c_void,
		sender: *mut c_void,
	);

	fn uniui_widget_textedit_set_text(
		qlineedit: *mut c_void,
		text: *const c_char,
	);
}

extern "C" fn callback(
	string: *const c_char,
	sender: *mut c_void,
) {
	let maybe_str = unsafe { CStr::from_ptr(string).to_str() };
	match maybe_str {
		Ok(s) => {
			let sender: &mut Sender<String> =
				unsafe { &mut *(sender as *mut Sender<String>) };
			match sender.send(s.to_string()) {
				Ok(()) => log::trace!("send ok"),
				Err(e) => log::error!("send err:{:?}", e),
			}
		},
		Err(e) => log::warn!("string conversation err:{:?}", e),
	}
}

pub struct Qt5TextEdit {
	text_changed_sender: Sender<String>,
	native: Qt5OptionalNative,
}

impl Qt5TextEdit {
	pub fn new(text_changed_sender: Sender<String>) -> Qt5TextEdit {
		return Qt5TextEdit {
			native: Qt5OptionalNative::new(),
			text_changed_sender,
		};
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		placeholder: &String,
	) -> Result<NativeWidget, ()> {
		let ptr = unsafe {
			let cstring_text =
				CString::new(placeholder.clone()).unwrap_or(CString::default());
			let raw_char = cstring_text.into_raw();
			let native = uniui_widget_textedit_create(raw_char);
			CString::from_raw(raw_char);

			uniui_widget_textedit_set_global_text_changed_callback(callback);
			uniui_widget_textedit_set_text_chnage_listener_listener(
				native,
				&mut self.text_changed_sender as *mut _ as *mut std::ffi::c_void,
			);

			self.native.replace(native);

			native
		};

		return Ok(ptr);
	}

	pub fn set_text(
		&mut self,
		text: &str,
	) {
		if let Some(native) = self.native.as_mut() {
			unsafe {
				let cstring_text =
					CString::new(text.clone()).unwrap_or(CString::default());
				let raw_char = cstring_text.into_raw();
				uniui_widget_textedit_set_text(native, raw_char);
				CString::from_raw(raw_char);
			}
		}
	}
}
