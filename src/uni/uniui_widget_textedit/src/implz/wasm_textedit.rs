use std::sync::mpsc::Sender;

use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	NativeWidget,
	WidgetGenerator,
};

use crate::implz::web_sys::{
	Event as WebEvent,
	HtmlInputElement,
};

use crate::implz::wasm_bindgen::{
	closure::Closure,
	JsCast,
};


pub struct WasmTextEdit {
	native: OptionalNative<HtmlInputElement>,
	closure: Closure<dyn FnMut(WebEvent)>,
}

impl WasmTextEdit {
	pub fn new(text_changed_sender: Sender<String>) -> WasmTextEdit {
		let closure = Closure::wrap(Box::new(move |event: WebEvent| {
			let el = event.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
			let text = el.value();
			match text_changed_sender.send(text) {
				Ok(()) => log::trace!("text_change sent:ok"),
				Err(e) => log::error!("text_change sent:err:{:?}", e),
			}
		}) as Box<dyn FnMut(WebEvent)>);

		return WasmTextEdit {
			native: OptionalNative::new(),
			closure,
		};
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		placeholder: &String,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("input").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let input = node.dyn_into::<HtmlInputElement>().unwrap();
		input.set_type("text");
		input.set_placeholder(placeholder);

		input.set_oninput(Some(self.closure.as_ref().unchecked_ref()));

		self.native.replace(input.clone());
		Ok(From::from(input))
	}

	pub fn set_text(
		&mut self,
		text: &str,
	) {
		match self.native.as_mut() {
			Some(n) => {
				n.set_value(text);
			},
			None => {},
		}
	}
}
