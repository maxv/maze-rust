use std::sync::mpsc::Sender;

use uniui_gui::{
	NativeWidget,
	WidgetGenerator,
};

pub struct NoopTextEdit {}

impl NoopTextEdit {
	pub fn new(_text_changed_sender: Sender<String>) -> NoopTextEdit {
		return NoopTextEdit {};
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_placeholder: &String,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}

	pub fn set_text(
		&mut self,
		_text: &str,
	) {
	}
}
