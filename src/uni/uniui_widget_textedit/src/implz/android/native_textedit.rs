use std::sync::mpsc::Sender;

use jni::{
	objects::{
		JClass,
		JObject,
		JValue,
	},
	JNIEnv,
	JavaVM,
};


use uniui_gui::implz::android::{
	android_context::Context,
	java_object::{
		JavaObject,
		JavaObjectConstructorable,
	},
	jni_error::JniError,
	uni_global_ref::UniGlobalRef,
};


pub struct NativeTextEdit {
	global_ref: UniGlobalRef<JObject<'static>>,
	vm: JavaVM,
}

impl NativeTextEdit {
	pub fn into_global_ref(self) -> UniGlobalRef<JObject<'static>> {
		return self.global_ref;
	}
}

impl JavaObject for NativeTextEdit {
	fn jobject(&self) -> JObject {
		return self.global_ref.as_obj();
	}

	fn jni_env(&self) -> Result<JNIEnv, JniError> {
		return Ok(self.vm.get_env()?);
	}
}

impl JavaObjectConstructorable for NativeTextEdit {
	fn construct(
		global_ref: UniGlobalRef<JObject<'static>>,
		vm: jni::JavaVM,
	) -> Self {
		return Self {
			global_ref,
			vm,
		};
	}
}

impl TextEdit for NativeTextEdit {
}

pub trait TextEdit: JavaObject {
	fn set_text(
		&self,
		text: &str,
	) -> Result<(), JniError> {
		use jni::signature::{
			JavaType::Primitive,
			Primitive::Void,
		};

		let env = self.jni_env()?;
		let class = TextEdit::get_class(&env)?;
		let set_text = uniui_gui::get_java_method!(
			env,
			class.as_obj(),
			"setText",
			"(Ljava/lang/CharSequence;)V"
		);
		let text = env.new_string(text)?;

		let _ =
			env.call_method_unchecked(self.jobject(), set_text, Primitive(Void), &[
				JValue::Object(text.into()),
			])?;

		return Ok(());
	}

	fn set_hint(
		&self,
		text: &str,
	) -> Result<(), JniError> {
		use jni::signature::{
			JavaType::Primitive,
			Primitive::Void,
		};

		let env = self.jni_env()?;
		let class = TextEdit::get_class(&env)?;
		let set_text = uniui_gui::get_java_method!(
			env,
			class.as_obj(),
			"setHint",
			"(Ljava/lang/CharSequence;)V"
		);
		let text = env.new_string(text)?;

		let _ =
			env.call_method_unchecked(self.jobject(), set_text, Primitive(Void), &[
				JValue::Object(text.into()),
			])?;

		return Ok(());
	}

	fn set_native(
		&self,
		sender: Sender<String>,
	) -> Result<(), JniError> {
		use jni::signature::{
			JavaType::Primitive,
			Primitive::Void,
		};

		let env = self.jni_env()?;
		let class = TextEdit::get_class(&env)?;
		let set_native =
			uniui_gui::get_java_method!(env, class.as_obj(), "setNative", "(J)V");

		let sender_ptr = Box::into_raw(Box::new(sender)) as i64;


		let _ =
			env.call_method_unchecked(self.jobject(), set_native, Primitive(Void), &[
				JValue::Long(sender_ptr),
			])?;
		return Ok(());
	}
}

impl dyn TextEdit {
	pub fn new_for_context(context: &dyn Context) -> Result<NativeTextEdit, JniError> {
		let env = context.jni_env()?;
		let class = Self::get_class(&env)?;

		let ctor = uniui_gui::get_java_method!(
			env,
			class.as_obj(),
			"<init>",
			"(Landroid/content/Context;)V"
		);

		let local_object =
			env.new_object_unchecked(class.as_obj(), ctor, &[JValue::Object(
				context.jobject(),
			)])?;

		// Yes, it looks non-elegant. But I don't know any other way to
		// convert JObject<'a> into JObject<'static>.
		// And UniGlobalRef will NOT work for JObject<'a>
		let local_object: JObject<'static> = local_object.into_inner().into();

		let global_ref = UniGlobalRef::try_new(&env, local_object)?;

		return Ok(NativeTextEdit {
			global_ref,
			vm: env.get_java_vm()?,
		});
	}

	fn get_class(env: &JNIEnv) -> Result<UniGlobalRef<JClass<'static>>, JniError> {
		let class =
			uniui_gui::get_java_class!(env, "uniui_widget_textedit/UniuiWidgetTextEdit")?;
		return Ok(class);
	}
}
