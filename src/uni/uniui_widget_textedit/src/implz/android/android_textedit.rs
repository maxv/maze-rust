use std::sync::mpsc::Sender;

use uniui_gui::{
	implz::android::{
		jni_error::JniError,
		AndroidOptionalNative,
	},
	NativeWidget,
	WidgetGenerator,
};

use super::native_textedit::{
	NativeTextEdit,
	TextEdit,
};

pub struct AndroidTextEdit {
	text_changed_sender: Sender<String>,
	native: AndroidOptionalNative<NativeTextEdit>,
}

impl AndroidTextEdit {
	pub fn new(text_changed_sender: Sender<String>) -> AndroidTextEdit {
		return AndroidTextEdit {
			native: AndroidOptionalNative::new(),
			text_changed_sender,
		};
	}

	pub fn to_native(
		&mut self,
		wg: &mut dyn WidgetGenerator,
		placeholder: &String,
	) -> Result<NativeWidget, ()> {
		let mut ton = || -> Result<NativeTextEdit, JniError> {
			let text_edit = TextEdit::new_for_context(wg.context())?;
			text_edit.set_native(self.text_changed_sender.clone())?;
			text_edit.set_hint(placeholder)?;
			self.native.replace(&text_edit)?;
			Ok(text_edit)
		};

		return match ton() {
			Ok(b) => Ok(b.into_global_ref()),
			Err(e) => {
				// clear exception?
				log::error!("to_native err:{:?}", e);
				Err(())
			},
		};
	}

	pub fn set_text(
		&mut self,
		text: &str,
	) {
		if let Some(n) = self.native.get() {
			if let Err(e) = n.set_text(text) {
				log::error!("set_text failed err:{:?}", e);
			}
		}
	}
}
