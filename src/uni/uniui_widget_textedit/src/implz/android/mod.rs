mod android_textedit;
pub use android_textedit::AndroidTextEdit;

mod native_textedit;

pub fn text_changed(
	env: jni::JNIEnv,
	_: jni::objects::JClass,
	sender_ptr: jni::sys::jlong,
	string: jni::sys::jobject,
) -> jni::sys::jlong {
	use std::sync::mpsc::Sender;

	let sender = unsafe { Box::from_raw(sender_ptr as *mut Sender<String>) };
	match env.get_string(string.into()) {
		Err(e) => log::error!("Couldn't parse string, err:{:?}", e),
		Ok(string) => {
			let _ = sender.send(string.into());
		},
	};
	return Box::into_raw(sender) as i64;
}

pub fn free(
	_: jni::JNIEnv,
	_: jni::objects::JClass,
	sender_ptr: jni::sys::jlong,
) {
	use std::sync::mpsc::Sender;
	let _to_be_killed = unsafe { Box::from_raw(sender_ptr as *mut Sender<String>) };
}
