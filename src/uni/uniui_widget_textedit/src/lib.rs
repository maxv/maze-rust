mod textedit;
pub use self::textedit::TextEdit;

mod macros;

/// Contains [TextEdit] and [u_textedit!].
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_textedit,
		TextEdit,
	};
}

/// The class-name for theme configuration. Please check [uniui_theme] for
/// more info.
pub const THEME_CLASS_NAME: &str = "u_textedit_theme_class";

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_textedit;
	pub use self::wasm_textedit::WasmTextEdit as Platform;
}

#[doc(hidden)]
#[cfg(target_os = "linux")]
mod implz {
	uniui_gui::desktop_qt! {
		mod qt5_textedit;
		pub use self::qt5_textedit::Qt5TextEdit as Platform;
	}

	uniui_gui::desktop_noop! {
		mod noop_textedit;
		pub use self::noop_textedit::NoopTextEdit as Platform;
	}
}

#[doc(hidden)]
#[cfg(target_os = "android")]
extern crate uni_tmp_jni as jni;

#[doc(hidden)]
#[cfg(target_os = "android")]
pub mod implz {
	// it's pub to provide access to inner functions
	// it's necessary to reexport them from the main library until
	// https://github.com/rust-lang/rfcs/issues/2771
	pub mod android;
	pub use self::android::AndroidTextEdit as Platform;
}
