use crate::implz::Platform;

use uniui_core::{
	Signal,
	Slot,
	SlotImpl,
};

use uniui_gui::{
	Application,
	DataProcessor,
	NativeWidget,
	Widget,
	WidgetGenerator,
};

use std::sync::mpsc::{
	self,
	Receiver,
};

/// Represents field where user can input their own text
///
/// ```
/// let mut text_edit = TextEdit::new("First Name".to_owned());
/// let slot = SlotImpl::new();
/// text_edit.signal_text_changed().connect_slot(&slot);
///
/// // Whenewer TextEdit's text will be changed then `slot` will be triggered
/// ```
///
/// # Macros
///
/// [u_textedit!] simplifies [TextEdit] creation:
/// ```
/// let slot = SlotImpl::new();
///
/// let text_edit = u_textedit!{
///     placeholder: "First Name",
///     signal_text_changed: slot,
/// };
/// ```
pub struct TextEdit {
	signal_text_changed: Signal<String>,
	slot_set_text: SlotImpl<String>,
	text_changed_receiver: Receiver<String>,
	placeholder: String,

	platform: Platform,
}

impl TextEdit {
	/// Creates new TextEdit with provided placeholder. You may be interested in
	/// [u_textedit!] to simplify TextEdit creation/setup.
	///
	/// ```
	/// let label = TextEdit::new("First Name".to_owned());
	/// ```
	pub fn new(placeholder: String) -> TextEdit {
		let (text_changed_sender, text_changed_receiver) = mpsc::channel();

		return TextEdit {
			signal_text_changed: Signal::new(),
			slot_set_text: SlotImpl::new(),
			text_changed_receiver,
			placeholder,
			platform: Platform::new(text_changed_sender),
		};
	}

	/// The signal will trigger all connected slots whenever TextEdit's text will be
	/// changed
	///
	/// ```
	/// let mut text_edit = u_textedit!{
	///     placeholder: "First Name".to_owned(),
	/// };
	///
	/// let slot = SlotImpl::new();
	///
	/// // Or concider to use u_textedit!'s signal_text_changed parameter
	/// text_edit.signal_text_changed().connect_slot(&slot);
	///
	/// // Whenever text will be changed, the `slot` will be triggered
	/// ```
	pub fn signal_text_changed(&mut self) -> &mut Signal<String> {
		return &mut self.signal_text_changed;
	}

	/// Slot to change TextEdit's text
	///
	/// ```
	/// let text_edit = u_textedit!{
	///     placeholder: "First Name".to_owned(),
	/// };
	///
	/// let mut signal = Signal::new();
	/// signal.connect_slot(text_edit.slot_set_text());
	///
	/// // <...>
	///
	/// signal.emit("John".to_owned());
	/// // text_edit's text will be set to "John"
	/// ```
	pub fn slot_set_text(&mut self) -> &mut dyn Slot<String> {
		return &mut self.slot_set_text;
	}
}

impl Widget for TextEdit {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(widget_generator, &self.placeholder);
	}
}

impl DataProcessor for TextEdit {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		for s in self.text_changed_receiver.try_iter() {
			self.signal_text_changed.emit(s);
		}

		for s in self.slot_set_text.data_iter() {
			self.platform.set_text(&s);
		}
	}
}
