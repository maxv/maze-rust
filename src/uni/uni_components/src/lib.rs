//! The crate defines main Application components for uniui_* crates family.
//!
//! * [Service](service::Service) - server handler
//! * [UiPage](ui_page::UiPage) - main UI component

/// server related stuff
pub mod service;

/// UI related stuff
pub mod ui_page;

#[doc(hidden)]
pub const APPLICATION_PACKAGE: &'static str = "com.uniuirust";

#[doc(hidden)]
pub const ANDROID_INTENT_KEY: &'static str = "com.uniuirust.data";

#[doc(hidden)]
pub fn android_activity_name_for(path: &str) -> String {
	return format!(
		"activity_{}",
		path.replace("_", "__").replace("/", "_").replace(".", "_")
	);
}

#[doc(hidden)]
pub mod net {
	pub use uni_net::*;
}

#[doc(hidden)]
pub mod log {
	pub use log::*;
}

#[doc(hidden)]
pub mod serde_json {
	pub use serde_json::*;
}

#[doc(hidden)]
pub mod serde {
	pub use serde::*;
}

#[doc(hidden)]
pub mod uniui_core {
	pub use uniui_core::*;
}

pub use uni_components_macro::*;

/// Simplified representation of HTTP response
///
/// The struct is still in progress. Cookies and Headers are coming soon
#[derive(Clone, Debug, PartialEq, Eq, Hash, serde::Deserialize, serde::Serialize)]
pub struct Response {
	pub code: u16,
	pub body: Option<Vec<u8>>,
	/* 	cookies: ???,
	 * 	headers: ???, */
}

/// The trait to convert to [Response]
pub trait AsResponse:
	'static + serde::de::DeserializeOwned + serde::Serialize + Send + Sync
{
	fn to_response(self) -> Response;
}

/// Supported kinds of HTTP requests
#[derive(Clone, Debug, PartialEq, Eq, Hash, serde::Deserialize, serde::Serialize)]
pub enum Kind {
	/// HTTP-POST request
	Post,

	/// HTTP-GET request
	Get,
}

/// Generic error enum. Will be redesigned soon.
#[non_exhaustive]
#[derive(thiserror::Error, Debug)]
pub enum CallError {
	#[error("Couldn't convert parameter to string")]
	SerdeJsonError(#[from] serde_json::Error),

	#[error("Couldn't convert parameter to string")]
	SerdeQsSerError(#[from] serde_urlencoded::ser::Error),

	#[error("Couldn't convert string to parameter")]
	SerdeQsDeError(#[from] serde_urlencoded::de::Error),

	#[error("Complicated net error")]
	UniNetError(#[from] uni_net::UniNetError),

	#[cfg(target_os = "android")]
	#[error("Complicated jni error")]
	UniJniError(#[from] uni_jnihelpers::JniError),

	#[error("Not available for current platform. Yet?")]
	NotAvailableForPlatform,

	#[error("Incorrect input")]
	IncorrectInput,
}

fn to_path<T>(
	path: &str,
	t: &T,
) -> Result<String, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	let query_params = serde_urlencoded::to_string(t)?;
	let result = format!("{}?{}", path, query_params);
	return Ok(result);
}
