use crate::CallError;

#[doc(hidden)]
pub trait UiPageData {
	fn path(&self) -> &str;
}

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
mod implz {
	mod ui_page_wasm;
	pub use self::ui_page_wasm::*;
}

#[doc(hidden)]
#[cfg(target_os = "android")]
mod implz {
	mod ui_page_android;
	pub use self::ui_page_android::*;
}

#[doc(hidden)]
#[cfg(target_os = "linux")]
mod implz {
	mod ui_page_linux;
	pub use self::ui_page_linux::*;
}

#[doc(hidden)]
pub use implz::system_depend_path;

/// Represents independent UI element (like Window or Activity or WebPage).
///
/// The best way you can use it is [define_ui_page!](crate::define_ui_page) macros.
pub trait UiPage: UiPageData {
	type Data: 'static + serde::de::DeserializeOwned + serde::Serialize;

	fn encode_data(
		&self,
		data: &Self::Data,
	) -> Result<String, CallError> {
		return implz::encode_data(data);
	}

	fn extract_launch_parameter(
		&self,
		s: &str,
	) -> Result<Self::Data, CallError> {
		return implz::extract_launch_parameter(s);
	}
}

impl<T> dyn UiPage<Data = T>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	pub fn get_launch_parameter(&self) -> Result<T, CallError> {
		return implz::get_launch_parameter(self);
	}

	pub fn open(&self) -> Result<(), CallError> {
		return implz::open(self);
	}

	pub fn open_with_data(
		&self,
		data: &T,
	) -> Result<(), CallError> {
		return implz::open_with_data(self, data);
	}
}
