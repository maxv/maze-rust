use uniui_core::Slot;

use crate::{
	to_path,
	CallError,
	Kind,
	Response,
};

/// Represents server side handler.
///
/// It's a good idea to use [define_service!](crate::define_service!) instead of trying to
/// implement the trait by yourself.
pub trait Service {
	/// Input parameter type (There are special limitations for [Kind::Get] requests
	type In: 'static + serde::de::DeserializeOwned + serde::Serialize;

	/// Output parameter type
	type Out: 'static + serde::de::DeserializeOwned + serde::Serialize;

	/// Path to the handler at service
	fn path(&self) -> &str;

	/// The kind of requests service will process
	fn kind(&self) -> Kind;

	/// Converter from HTTP response to Service's output type
	fn from_response(&self) -> fn(response: Response) -> Result<Self::Out, Response>;

	/// Executes the service. Default implementation call related to the `path` at
	/// server side.
	///
	/// parameter - input parameter
	///
	/// slot - will receive result of the server call
	fn exec(
		&self,
		parameter: &Self::In,
		slot: &dyn Slot<Result<Self::Out, Response>>,
	) -> Result<(), CallError> {
		let p = parameter;
		let s = slot.proxy();

		let converter = self.from_response();
		let cpa = move |code, body: String| {
			let result = Response {
				code,
				body: Some(body.into()),
			};
			let data = converter(result);
			s.exec_for(data);
		};

		return match self.kind() {
			Kind::Get => {
				let path = to_path(self.path(), p)?;
				uni_net::get(&path, cpa)?;
				Ok(())
			},
			Kind::Post => {
				let data = serde_json::to_string(p)?;
				uni_net::post_json(self.path(), &data, cpa)?;
				Ok(())
			},
		};
	}
}
