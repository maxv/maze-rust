use crate::CallError;

use super::super::UiPage;

pub fn open<T>(page: &dyn UiPage<Data = T>) -> Result<(), CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	crate::net::open(page.path())?;
	return Ok(());
}

pub fn open_with_data<T>(
	page: &dyn UiPage<Data = T>,
	data: &T,
) -> Result<(), CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	let path = crate::to_path(page.path(), data)?;
	crate::net::open(&path)?;
	return Ok(());
}

pub fn get_launch_parameter<T>(_page: &dyn UiPage<Data = T>) -> Result<T, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	let s = web_sys::window().unwrap().location().search().unwrap();
	return match s.len() {
		0 | 1 => Err(CallError::IncorrectInput),
		_ => {
			let data = serde_urlencoded::from_str(&s[1..])?;
			Ok(data)
		},
	};
}

pub fn system_depend_path<T>(page: &dyn UiPage<Data = T>) -> String
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return page.path().to_owned();
}

pub fn encode_data<T>(data: &T) -> Result<String, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	let query_params = serde_urlencoded::to_string(data)?;
	return Ok(query_params);
}

pub fn extract_launch_parameter<T>(s: &str) -> Result<T, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	let data = serde_urlencoded::from_str(s)?;
	return Ok(data);
}
