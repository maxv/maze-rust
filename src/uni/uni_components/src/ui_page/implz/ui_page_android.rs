use crate::CallError;

use super::super::UiPage;

pub fn open<T>(_page: &dyn UiPage<Data = T>) -> Result<(), CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return Err(CallError::NotAvailableForPlatform);
}

pub fn open_with_data<T>(
	_page: &dyn UiPage<Data = T>,
	_data: &T,
) -> Result<(), CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return Err(CallError::NotAvailableForPlatform);
}

pub fn get_launch_parameter<T>(_page: &dyn UiPage<Data = T>) -> Result<T, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return Err(CallError::NotAvailableForPlatform);
}

pub fn system_depend_path<T>(page: &dyn UiPage<Data = T>) -> String
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return format!(
		"{}.{}",
		crate::APPLICATION_PACKAGE,
		crate::android_activity_name_for(page.path())
	);
}

pub fn encode_data<T>(data: &T) -> Result<String, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	let json = serde_json::to_string(data)?;
	return Ok(json);
}

pub fn extract_launch_parameter<T>(s: &str) -> Result<T, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	let data = serde_json::from_str(s)?;
	return Ok(data);
}
