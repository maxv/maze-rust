use crate::CallError;

use super::super::UiPage;

pub fn open<T>(_page: &dyn UiPage<Data = T>) -> Result<(), CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return Err(CallError::NotAvailableForPlatform);
}

pub fn open_with_data<T>(
	_page: &dyn UiPage<Data = T>,
	_data: &T,
) -> Result<(), CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return Err(CallError::NotAvailableForPlatform);
}

pub fn get_launch_parameter<T>(_page: &dyn UiPage<Data = T>) -> Result<T, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return Err(CallError::NotAvailableForPlatform);
}

pub fn system_depend_path<T>(page: &dyn UiPage<Data = T>) -> String
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return page.path().to_owned();
}

pub fn encode_data<T>(_data: &T) -> Result<String, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return Err(CallError::NotAvailableForPlatform);
}

pub fn extract_launch_parameter<T>(_s: &str) -> Result<T, CallError>
where
	T: 'static + serde::de::DeserializeOwned + serde::Serialize,
{
	return Err(CallError::NotAvailableForPlatform);
}
