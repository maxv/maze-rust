use uniui_gui::{
	prelude::*,
	Color,
};

pub struct BackgroundGradient<T: 'static + Widget> {
	widget: T,
	gradient: Property<Vec<(f32, Color)>>,
	native: Option<NativeWidget>,
}

impl<T: 'static + Widget> BackgroundGradient<T> {
	pub fn new(
		widget: T,
		gradient: Vec<(f32, Color)>,
	) -> BackgroundGradient<T> {
		return BackgroundGradient {
			widget,
			gradient: Property::new(gradient),
			native: None,
		};
	}

	#[cfg(target_os = "linux")]
	fn update_color(&mut self) {
		log::error!("BackgroundGradient not implemented for Qt5. Yet.");
	}

	#[cfg(target_arch = "wasm32")]
	fn update_color(&mut self) {
		if let Some(n) = self.native.as_mut() {
			let mut s = format!("linear-gradient(");
			for (n, (percentage, color)) in self.gradient.as_ref().iter().enumerate() {
				let separator = match n {
					0 => "",
					_ => ",",
				};

				s += &format!(
					"{separator} {color} {percentage}%",
					separator = separator,
					color = color.html_string(),
					percentage = percentage
				);
			}
			s += ")";

			n.style().set_property_silent("background", &s);
		}
	}
}

impl<T: 'static + Widget> Widget for BackgroundGradient<T> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;
		self.native = Some(e.clone());
		self.update_color();
		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for BackgroundGradient<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
		if self.gradient.try_update() {
			self.update_color();
		}
	}
}
