mod label;
pub use self::label::Label;

mod macros;

/// Contains [Label], [TextAlignment] and [u_label!].
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_label,
		Label,
		TextAlignment,
	};
}

/// The class-name for theme configuration. Please check [uniui_theme] for
/// more info.
pub const THEME_CLASS_NAME: &str = "u_label_theme_class";

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_label;
	pub use self::wasm_label::WasmLabel as Platform;
}

#[doc(hidden)]
#[cfg(target_os = "linux")]
mod implz {
	uniui_gui::desktop_qt! {
		mod qt5_label;
		pub use self::qt5_label::Qt5Label as Platform;
	}

	uniui_gui::desktop_noop! {
		mod noop_label;
		pub use self::noop_label::NoopLabel as Platform;
	}
}

#[doc(hidden)]
#[cfg(target_os = "android")]
extern crate uni_tmp_jni as jni;

#[doc(hidden)]
#[cfg(target_os = "android")]
mod implz {
	mod android_label;
	pub use self::android_label::AndroidLabel as Platform;
}

/// Variants of [Label]'s text alignment options
#[derive(Debug, Copy, Clone)]
pub enum TextAlignment {
	Center,
	Start,
	End,
}
