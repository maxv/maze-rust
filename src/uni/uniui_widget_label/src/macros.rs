#[doc(hidden)]
#[macro_export]
macro_rules! u_label_int {
	($b:ident,slot_set_text: $va:expr) => {
		$va.connect_slot($b.slot_set_text());
	};
	($b:ident,slot_set_text_proxy: $va:expr) => {
		$va = $b.slot_set_text().proxy();
	};

	($b:ident,text_alignment: $va:expr) => {
		$b.slot_set_text_alignment().exec_for($va);
	};
	($b:ident,slot_set_text_alignment: $va:expr) => {
		$va.connect_slot($b.slot_set_text_alignment());
	};
}

/// Simplifies [Label](crate::Label) creation
///
/// Parameters:
/// * text (required): String,
/// * slot_set_text (optional): provided [Signal\<String\>](uniui_core::Signal) will be
///   connected to [Label::slot_set_text],
/// * slot_set_text_proxy (optional): [Label::slot_set_text]'s proxy will be assigned to
///   provided identifier,
/// * text_alignment (optional): [TextAlignment](crate::TextAlignment),
/// * slot_set_text_alignment (optional): provided
///   [Signal\<TextAlignment\>](uniui_core::Signal) will be connected to
///   [Label::slot_set_text_alignment].
///
/// Example:
/// ```
/// let proxyslot_set_text;
///
/// let label = u_label! {
///     text: "Hello world!".to_owned(),
///     text_alignment: TextAlignment::Center,
///     slot_set_text_proxy: proxyslot_set_text,
/// };
///
/// // ...
///
/// // Changes label's text to "Goodbye"
/// proxyslot_set_text.exec_for("Goodbye".to_owned())
/// ```
#[macro_export]
macro_rules! u_label {
	(text: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut label = $crate::Label::new($w);
		$($crate::u_label_int!(label, $param_name: $val);)*;
		label
	}};
}
