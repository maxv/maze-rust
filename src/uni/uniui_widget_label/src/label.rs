use crate::TextAlignment;

use super::implz::Platform;

use uniui_gui::prelude::*;

/// Shows text on the screen.
///
/// ```
/// let label = Label::new("Hello world".to_owned());
/// lable.slot_set_text_alignment().exec_for(TextAlignment::Center);
/// ```
///
/// # Macros
///
/// You can use [u_label!] to simplify Label creation/setup:
/// ```
/// let label = u_label! {
///     text: "Hello world!".to_owned(),
///     text_alignment: TextAlignment::Center,
/// };
/// ```
pub struct Label {
	text: String,
	platform: Platform,
	slot_set_text: SlotImpl<String>,
	text_alignment: Property<TextAlignment>,
}

impl Label {
	/// Creates new Label with provided text. You may be interested in [u_label!] to
	/// simplify Label creation/setup.
	///
	/// ```
	/// let label = Label::new("Hello".to_owned());
	/// ```
	pub fn new(text: String) -> Label {
		return Label {
			text,
			platform: Platform::new(),
			slot_set_text: SlotImpl::new(),
			text_alignment: Property::new(TextAlignment::Start),
		};
	}

	/// Label's text can be changed via provided slot
	///
	/// ```
	/// let label = u_label!{
	///     text: "Hello".to_owned(),
	/// };
	///
	/// let mut signal = Signal::new();
	/// signal.connect_slot(label.slot_set_text);
	///
	/// // <...>
	///
	/// // Changes label's text to "Goodbye"
	/// signal.emit("Goodbye".to_owned());
	/// ```
	pub fn slot_set_text(&self) -> &dyn Slot<String> {
		return &self.slot_set_text;
	}

	/// Label's text alignment can be changed via provided slot
	///
	/// ```
	/// let label = u_lable!{
	/// 	text: "Hello".to_owned(),
	/// };
	///
	/// let mut signal = Signal::new();
	/// signal.connect_slot(label.slot_set_text_alignment());
	///
	/// // <...>
	///
	/// // Changes text alignemnt
	/// signal.emit(TextAligment::Center);
	/// ```
	pub fn slot_set_text_alignment(&self) -> &dyn Slot<TextAlignment> {
		return self.text_alignment.slot();
	}

	fn set_text(
		&mut self,
		s: String,
	) {
		self.text = s;
		self.platform.set_text(&self.text);
	}
}

impl Widget for Label {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(widget_generator, &self.text);
	}
}

impl DataProcessor for Label {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		if let Some(text) = self.slot_set_text.data_iter().last() {
			self.set_text(text);
		}

		if self.text_alignment.try_update() {
			self.platform.update_alignment(self.text_alignment.as_ref());
		}
	}
}
