use crate::TextAlignment;

use uniui_gui::{
	prelude::*,
};

pub struct NoopLabel {}

impl NoopLabel {
	pub fn new() -> NoopLabel {
		return NoopLabel {};
	}

	pub fn set_text(
		&mut self,
		_text: &str,
	) {
	}

	pub fn to_native(
		&mut self,
		_: &mut dyn WidgetGenerator,
		_text: &str,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}

	pub fn update_alignment(
		&mut self,
		_alignment: &TextAlignment,
	) {
	}
}
