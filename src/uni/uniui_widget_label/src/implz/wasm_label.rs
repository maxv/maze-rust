use crate::TextAlignment;

use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::{
	wasm_bindgen::JsCast,
	web_sys::HtmlLabelElement,
};


pub struct WasmLabel {
	native: OptionalNative<HtmlLabelElement>,
}

impl WasmLabel {
	pub fn new() -> WasmLabel {
		return WasmLabel {
			native: OptionalNative::new(),
		};
	}

	pub fn set_text(
		&mut self,
		s: &str,
	) {
		if let Some(n) = self.native.as_mut() {
			n.set_inner_text(s);
		}
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		text: &str,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("label").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let label = node.dyn_into::<HtmlLabelElement>().unwrap();
		label.set_inner_text(text);

		self.native.replace(From::from(label.clone()));
		Ok(From::from(label))
	}

	pub fn update_alignment(
		&mut self,
		alignment: &TextAlignment,
	) {
		if let Some(n) = self.native.as_mut() {
			let property_value = match alignment {
				TextAlignment::Center => "center",
				TextAlignment::Start => "start",
				TextAlignment::End => "end",
			};
			n.style().set_property_silent("text-align", property_value);
		}
	}
}
