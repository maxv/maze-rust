use crate::TextAlignment;

use uniui_gui::{
	implz::android::{
		jni_error::JniError,
		AndroidOptionalNative as OptionalNative,
	},
	NativeWidget,
	WidgetGenerator,
};

mod native_label;
use native_label::{
	NativeLabel,
	TextView,
};


pub struct AndroidLabel {
	native: OptionalNative<NativeLabel>,
}

impl AndroidLabel {
	pub fn new() -> Self {
		return Self {
			native: OptionalNative::new(),
		};
	}

	pub fn set_text(
		&mut self,
		text: &str,
	) {
		if let Some(n) = self.native.get() {
			if let Err(e) = n.set_text(text) {
				log::error!("set_text failed err:{:?}", e);
			}
		}
	}

	pub fn to_native(
		&mut self,
		wg: &mut dyn WidgetGenerator,
		text: &str,
	) -> Result<NativeWidget, ()> {
		let mut ton = || -> Result<NativeLabel, JniError> {
			let native_label = TextView::new_for_context(wg.context())?;
			native_label.set_text(&text)?;
			self.native.replace(&native_label)?;
			Ok(native_label)
		};

		return match ton() {
			Ok(b) => Ok(b.into_global_ref()),
			Err(e) => {
				// clear exception?
				log::error!("to_native err:{:?}", e);
				Err(())
			},
		};
	}

	pub fn update_alignment(
		&mut self,
		alignment: &TextAlignment,
	) {
		use TextAlignment::*;

		let gravity = match alignment {
			Center => 1,
			Start => 800003,
			End => 800005,
		};

		if let Some(n) = self.native.get() {
			if let Err(e) = n.set_gravity(gravity) {
				log::error!("set_gravity failed err:{:?}", e);
			}
		}
	}
}
