use crate::TextAlignment;

use uniui_gui::{
	implz::qt5::Qt5OptionalNative as OptionalNative,
	prelude::*,
};

use std::{
	ffi::{
		c_void,
		CString,
	},
	os::raw::c_char,
};

extern "C" {
	fn uniui_widget_label_create(text: *const c_char) -> *mut c_void;
	fn uniui_widget_label_set_text(
		qcheckbox: *mut c_void,
		text: *const c_char,
	);
}

pub struct Qt5Label {
	native: OptionalNative,
}

impl Qt5Label {
	pub fn new() -> Qt5Label {
		return Qt5Label {
			native: OptionalNative::new(),
		};
	}

	pub fn set_text(
		&mut self,
		text: &str,
	) {
		if let Some(native) = self.native.as_mut() {
			unsafe {
				let cstring_text =
					CString::new(text.to_string()).unwrap_or(CString::default());
				let raw_char = cstring_text.into_raw();
				uniui_widget_label_set_text(native, raw_char);
				CString::from_raw(raw_char);
			}
		}
	}

	pub fn to_native(
		&mut self,
		_: &mut dyn WidgetGenerator,
		text: &str,
	) -> Result<NativeWidget, ()> {
		let ptr = unsafe {
			let cstring_text =
				CString::new(text.to_string()).unwrap_or(CString::default());
			let raw_char = cstring_text.into_raw();
			let native = uniui_widget_label_create(raw_char);
			CString::from_raw(raw_char);

			self.native.replace(native);
			native
		};

		return Ok(ptr);
	}

	pub fn update_alignment(
		&mut self,
		_alignment: &TextAlignment,
	) {
		log::error!("TextAlignment not implemented for Qt5. Yet.");
	}
}
