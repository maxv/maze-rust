#include<QLabel>

extern "C" void* uniui_widget_label_create(char* text) {
	QLabel* result = new QLabel(text);
	result->setTextFormat(Qt::PlainText);
	return result;
}


extern "C" void uniui_widget_label_set_text(void* label, char* text) {
	((QLabel*)label)->setText(text);
}
