#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_gui;

mod slider;
pub use self::slider::Slider;

pub const THEME_CLASS_NAME: &str = "u_slider_theme_class";

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_slider;
	pub use self::wasm_slider::WasmSlider as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_slider;
	pub use self::qt5_slider::Qt5Slider as Platform;
}
