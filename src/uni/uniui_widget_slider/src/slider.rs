use crate::implz::Platform;

use uniui_gui::{
	prelude::*,
	Orientation as Direction,
};


use std::sync::mpsc::{
	self,
	Receiver,
};

pub struct Slider {
	min: f64,
	max: f64,
	step: f64,
	receiver: Receiver<f64>,
	signal_value: Signal<f64>,
	direction: Direction,

	platform: Platform,
}

impl Slider {
	pub fn new(
		min: f64,
		max: f64,
		step: f64,
	) -> Slider {
		let (sender, receiver) = mpsc::channel();

		return Slider {
			signal_value: Signal::new(),
			min,
			max,
			step,
			receiver,
			direction: Direction::Horizontal,
			platform: Platform::new(sender),
		};
	}

	pub fn set_direction(
		&mut self,
		direction: Direction,
	) {
		self.direction = direction;
		self.platform.set_direction(&direction);
	}

	pub fn signal_value(&mut self) -> &mut Signal<f64> {
		return &mut self.signal_value;
	}
}

impl Widget for Slider {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(
			widget_generator,
			self.min,
			self.step,
			self.max,
			&self.direction,
		);
	}
}

impl DataProcessor for Slider {
	fn process_data(
		&mut self,
		_msec_since_app_start: i64,
		_app: &mut dyn Application,
	) {
		if let Some(v) = self.receiver.try_iter().last() {
			self.signal_value.emit(v);
		}
	}
}
