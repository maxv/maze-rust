use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
	Orientation as Direction,
};

use crate::implz::web_sys::HtmlInputElement;

use crate::implz::wasm_bindgen::closure::Closure;

use crate::implz::wasm_bindgen::JsCast;


use std::sync::mpsc::Sender;

pub struct WasmSlider {
	native: OptionalNative<HtmlInputElement>,
	closure: Option<Closure<dyn FnMut()>>,
	sender: Sender<f64>,
}

impl WasmSlider {
	pub fn new(sender: Sender<f64>) -> WasmSlider {
		return WasmSlider {
			native: OptionalNative::new(),
			closure: None,
			sender,
		};
	}

	pub fn set_direction(
		&mut self,
		_direction: &Direction,
	) {
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		min: f64,
		step: f64,
		max: f64,
		_direction: &Direction,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("input").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let input = node.dyn_into::<HtmlInputElement>().unwrap();
		input.set_type("range");

		input.set_min(&min.to_string());
		input.set_step(&step.to_string());
		input.set_max(&max.to_string());

		let input_clone = input.clone();
		let sender = self.sender.clone();
		let closure = Closure::wrap(Box::new(move || {
			match input_clone.value().parse::<f64>() {
				Ok(v) => sender.send_silent(v),
				Err(_) => error!("couldn't parse value"),
			}
		}) as Box<dyn FnMut()>);

		let closure_ref = closure.as_ref().unchecked_ref();
		input.add_event_listener_silent("input", closure_ref);

		self.native.replace(input.clone());
		self.closure = Some(closure);
		Ok(From::from(input))
	}
}
