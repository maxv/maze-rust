use uniui_gui::{
	prelude::*,
	Orientation as Direction,
};

use std::sync::mpsc::Sender;

pub struct Qt5Slider {}

impl Qt5Slider {
	pub fn new(_sender: Sender<f64>) -> Qt5Slider {
		return Qt5Slider {};
	}

	pub fn set_direction(
		&mut self,
		_direction: &Direction,
	) {
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_min: f64,
		_step: f64,
		_max: f64,
		_direction: &Direction,
	) -> Result<NativeWidget, ()> {
		error!("not implemented");
		return Err(());
	}
}
