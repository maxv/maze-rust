#include <QWidget>
#include <QPalette>
#include <QColor>

extern "C" void uniui_wrapper_backgroundcolor_set_widget_background(void* qwidget, unsigned char red, unsigned char green, unsigned char blue) {
	QWidget* w = (QWidget*) qwidget;
	
	QPalette pal = QPalette();
	pal.setColor(QPalette::Background, QColor(red, green, blue));
	w->setAutoFillBackground(true);
	w->setPalette(pal);
}

