use uniui_gui::{
	prelude::*,
	Color,
};

mod macros;

pub mod prelude {
	pub use crate::{
		u_backgroundcolor,
		BackgroundColor,
	};
	pub use uniui_gui::Color;
}

#[cfg(target_os = "linux")]
uniui_gui::desktop_qt! {
	extern "C" {
		fn uniui_wrapper_backgroundcolor_set_widget_background(
			widget: *mut std::ffi::c_void,
			red: u8,
			green: u8,
			blue: u8,
		);
	}
}


pub struct BackgroundColor<T: 'static + Widget> {
	widget: T,
	slot_set_color: SlotImpl<Option<Color>>,
	native: Option<NativeWidget>,
}

impl<T: 'static + Widget> BackgroundColor<T> {
	#[cfg(target_os = "linux")]
	uniui_gui::desktop_qt! {
		fn update_color(&mut self) {
			log::error!("BackgroundColor don't have safe implemetation for Qt5. Yet.");
			// The problem is we don't know if QWidget* still valid
			if let Some(e) = self.native.as_mut() {
				if let Some(Some(color)) = self.slot_set_color.last() {
					unsafe {
						uniui_wrapper_backgroundcolor_set_widget_background(
							*e,
							color.red,
							color.green,
							color.blue,
						);
					}
				}
			}
		}
	}

	#[cfg(target_os = "linux")]
	uniui_gui::desktop_noop! {
		fn update_color(&mut self) {
		}
	}

	pub fn new(widget: T) -> BackgroundColor<T> {
		return BackgroundColor {
			widget,
			slot_set_color: SlotImpl::new(),
			native: None,
		};
	}

	pub fn slot_set_color(&self) -> &dyn Slot<Option<Color>> {
		return &self.slot_set_color;
	}

	#[cfg(target_arch = "wasm32")]
	fn update_color(&mut self) {
		if let Some(color_update) = self.slot_set_color.last() {
			if let Some(n) = self.native.as_mut() {
				match color_update {
					Some(color) => {
						n.style().set_property_silent("background", &color.html_string())
					},
					None => n.style().remove_property_silent("background"),
				}
			}
		}
	}
}

impl<T: 'static + Widget> Widget for BackgroundColor<T> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;
		self.native = Some(e.clone());
		self.update_color();
		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for BackgroundColor<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
		self.update_color();
	}
}
