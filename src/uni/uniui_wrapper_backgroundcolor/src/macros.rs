#[doc(hidden)]
#[macro_export]
macro_rules! u_backgroundcolor_int {
	($b:ident,slot_set_color: $va:expr) => {
		$va.connect_slot($b.slot_set_color());
	};
	($b:ident,slot_set_color_proxy: $va:expr) => {
		$va = $b.slot_set_color().proxy();
	};
}

/// Simplifies [BackgroundColor](crate::BackgroundColor) creation
///
/// Parameters:
/// * widget (required): [Widget](uniui_gui::Widget) to wrap,
/// * slot_set_color (optional): provided [Signal\<Option\<Color\>\>](uniui_core::Signal)
///   will be connected to [BackgroundColor::slot_set_color],
/// * slot_set_color_proxy (optional): [BackgroundColor::slot_set_color]'s proxy will be
///   assigned to provided identifier,
///
/// Example:
/// ```
/// let proxyslot_set_color;
///
/// let label = u_backgroundcolor! {
///     widget: "First Name".to_owned(),
///     slot_set_color_proxy: proxyslot_set_colors,
/// };
///
/// // ...
///
/// // Changes my_widget's backgorund to yellow
/// proxyslot_set_color.exec_for(uniui_gui::Color::yellow())
/// ```
#[macro_export]
macro_rules! u_backgroundcolor {
	(widget: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut bg = $crate::BackgroundColor::new($w);
		$($crate::u_backgroundcolor_int!(bg, $param_name: $val);)*;
		bg
	}};
}
