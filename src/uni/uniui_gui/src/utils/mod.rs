use crate::Application;

#[cfg(target_arch = "wasm32")]
mod wasm_utils;

#[cfg(target_arch = "wasm32")]
pub use self::wasm_utils::*;

#[cfg(target_os = "linux")]
mod qt5_utils;

#[cfg(target_os = "linux")]
pub use self::qt5_utils::*;

#[cfg(target_os = "android")]
pub mod android_utils;


use std::sync::mpsc::Sender;

#[cfg(target_os = "android")]
mod logger_tag {
	use std::sync::{
		Arc,
		Mutex,
	};

	lazy_static::lazy_static! {
		static ref LOGGER_TAG: Arc<Mutex<Option<String>>> = Arc::new(Mutex::new(None));
	}

	pub fn set(tag: String) {
		match LOGGER_TAG.lock() {
			Ok(mut m) => *m = Some(tag),
			Err(e) => {
				log::error!("couldn't set tag err:{:?}", e);
			},
		}
	}

	pub fn get() -> String {
		return match LOGGER_TAG.lock() {
			Ok(m) => (*m).clone().unwrap_or("".to_owned()),
			Err(e) => {
				log::error!("couldn't set tag err:{:?}", e);
				"".to_owned()
			},
		};
	}
}

#[cfg(not(target_os = "android"))]
mod logger_tag {
	pub fn set(_: String) {
		// no-op
	}
}


pub fn set_logger_tag(tag: String) {
	logger_tag::set(tag);
}

pub fn init_logger(app: &dyn Application) {
	let level = app.get_env_var("loglevel");
	let level = if level == Some("error".to_string()) {
		log::Level::Error
	} else if level == Some("warn".to_string()) {
		log::Level::Warn
	} else if level == Some("info".to_string()) {
		log::Level::Info
	} else if level == Some("debug".to_string()) {
		log::Level::Debug
	} else if level == Some("trace".to_string()) {
		log::Level::Trace
	} else {
		log::Level::Warn
	};

	let logs = app.get_env_var("logs");
	if logs == Some("console".to_string()) {
		activate_console_logger(level);
	}
}

#[cfg(target_arch = "wasm32")]
fn activate_console_logger(level: log::Level) {
	let logger_config = wasm_logger::Config::new(level);
	let logger_config = logger_config.message_on_new_line();
	wasm_logger::init(logger_config);
}

#[cfg(target_os = "linux")]
fn activate_console_logger(_level: log::Level) {
}

#[cfg(target_os = "android")]
fn activate_console_logger(_level: log::Level) {
	let tag = logger_tag::get();

	// get env properties from "log.uniui_android.{}" property somehow

	android_logger::init_once(
		android_logger::Config::default().with_min_level(log::Level::Debug).with_tag(tag),
	);
	log::error!("Logs activated");
}

pub trait SilentSender<T> {
	fn send_silent(
		&self,
		t: T,
	);
}

impl<T> SilentSender<T> for Sender<T> {
	fn send_silent(
		&self,
		t: T,
	) {
		let send_result = self.send(t);
		match send_result {
			Ok(()) => { /*nop*/ },
			Err(e) => log::error!("couldn't send e:{:?}", e),
		};
	}
}
