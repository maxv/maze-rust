pub fn clear_exception(
	env: &jni::JNIEnv,
	e: jni::errors::Error,
) {
	if let jni::errors::Error::JavaException = e {
		if let Err(_) = env.exception_clear() {
			// what else can we do?
			log::error!("exception clear failed");
		}
	}
}
