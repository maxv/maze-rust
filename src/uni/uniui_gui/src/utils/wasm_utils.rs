use wasm_bindgen::JsCast;

use web_sys::{
	CssStyleDeclaration,
	Node,
};

use js_sys::Function;

pub fn slice_to_url(data: &[u8]) -> Result<String, ()> {
	let uint8array = js_sys::Uint8Array::from(data);
	let array_to_save = js_sys::Array::new();
	array_to_save.set(0, uint8array.into());
	return match web_sys::Blob::new_with_u8_array_sequence(&array_to_save) {
		Ok(blob) => {
			match web_sys::Url::create_object_url_with_blob(&blob) {
				Ok(url) => {
					let mut v = Vec::new();
					v.extend_from_slice(data);
					Ok(url)
				},
				Err(e) => {
					log::error!("url failed e:{:?}", e);
					Err(())
				},
			}
		},
		Err(e) => {
			log::error!("blob failed e:{:?}", e);
			Err(())
		},
	};
}

pub fn save_file(
	data: &[u8],
	file_name: &str,
) {
	match slice_to_url(data) {
		Ok(url) => {
			let document = web_sys::window().unwrap().document().unwrap();
			let a = document.create_element("a").unwrap();
			let a = a.dyn_into::<web_sys::HtmlAnchorElement>().unwrap();
			a.set_download(file_name);
			a.set_href(&url);

			match document.body() {
				Some(body) => {
					match body.append_child(&a) {
						Ok(_) => log::trace!("anchor added"),
						Err(e) => log::error!("couldn't add anchor e:{:?}", e),
					}
				},
				None => {
					log::error!("document w/o body");
				},
			}

			a.click();
		},
		Err(()) => log::error!("slice_to_url failed"),
	};
}

pub trait SilentAddEventListener {
	fn add_event_listener_silent(
		&self,
		event_name: &str,
		listener: &Function,
	);
}

impl SilentAddEventListener for Node {
	fn add_event_listener_silent(
		&self,
		event_name: &str,
		listener: &Function,
	) {
		let result = self.add_event_listener_with_callback(event_name, listener);
		match result {
			Ok(()) => log::trace!("listener for {} added", event_name),
			Err(e) => log::error!("couldn't add listener for {} err:{:?}", event_name, e),
		}
	}
}

pub trait SilentCssPropertySetter {
	fn set_property_silent(
		&self,
		property_name: &str,
		value: &str,
	);

	fn remove_property_silent(
		&self,
		property_name: &str,
	);
}

impl SilentCssPropertySetter for CssStyleDeclaration {
	fn set_property_silent(
		&self,
		property_name: &str,
		value: &str,
	) {
		match self.set_property(property_name, value) {
			Ok(()) => log::trace!("property:{} set to value:{}", property_name, value),
			Err(e) => {
				log::error!(
					"can't set property:{} to value:{} e:{:?}",
					property_name,
					value,
					e
				)
			},
		}
	}

	fn remove_property_silent(
		&self,
		property_name: &str,
	) {
		match self.remove_property(property_name) {
			Ok(_) => log::trace!("property:{} removed", property_name),
			Err(e) => log::error!("can't remove property:{}, e:{:?}", property_name, e),
		}
	}
}
