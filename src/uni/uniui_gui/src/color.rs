use serde::{
	Deserialize,
	Serialize,
};


#[derive(Serialize, Deserialize, Hash, PartialEq, Eq, Clone, Debug, Copy)]
/// Simple 8-bit RGB color
pub struct Color {
	pub red: u8,
	pub green: u8,
	pub blue: u8,
}

impl Color {
	/// Generate html/css-like color as a string like "#FF0000" for red color.
	pub fn html_string(&self) -> String {
		return format!("#{:02x}{:02x}{:02x}", self.red, self.green, self.blue);
	}
}
