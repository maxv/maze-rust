#[cfg(target_arch = "wasm32")]
pub mod wasm_bindgen {
	pub use wasm_bindgen::*;
}

pub use uniui_gui_macro::*;

pub mod prelude {
	pub use uniui_core::{
		Property,
		Signal,
		Slot,
		SlotImpl,
		SlotProxy,
	};

	pub use crate::{
		utils::*,
		Application,
		DataProcessor,
		NativeWidget,
		UiPageContext,
		Widget,
		WidgetGenerator,
	};
}

#[doc(hidden)]
pub mod core {
	pub use uniui_core::*;
}

#[doc(hidden)]
pub mod log {
	pub use log::*;
}

mod widget;
pub use crate::widget::Widget;

mod data_processor;
pub use self::data_processor::DataProcessor;

mod widget_generator;
pub use crate::widget_generator::WidgetGenerator;

pub mod utils;

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
pub mod implz {
	pub mod wasm;
	pub use self::wasm::{
		WasmApplication as Application,
		WasmNativeWidget as NativeWidget,
		WasmWidgetGenerator as WigetGenerator,
	};
}

#[doc(hidden)]
#[cfg(target_os = "linux")]
pub mod implz {
	crate::desktop_qt! {
		pub mod qt5;
		pub use self::qt5::{
			Qt5Application as Application,
			Qt5NativeWidget as NativeWidget,
			Qt5WidgetGenerator as WigetGenerator,
		};
	}

	crate::desktop_noop! {
		#[derive(Clone)]
		pub struct NativeWidget;
	}
}

#[cfg(target_os = "android")]
extern crate uni_tmp_jni as jni;

#[doc(hidden)]
#[cfg(target_os = "android")]
pub mod implz {
	pub mod jni {
		pub use jni::*;
	}

	pub mod android;
	pub use self::android::{
		AndroidApplication as Application,
		AndroidNativeWidget as NativeWidget,
	};
}

pub use implz::NativeWidget;

pub use ui_page_context::{
	UiPageContext,
	UiPageContext as Application,
};

mod color;
pub use color::Color;

mod size_unit;
pub use size_unit::SizeUnit;

#[derive(Debug, Copy, Clone)]
pub enum Orientation {
	Horizontal,
	Vertical,
}

#[derive(Debug, Copy, Clone)]
pub enum Wrap {
	Wrap,
	NoWrap,
}

#[derive(Debug, Copy, Clone)]
pub enum AlignmentVertical {
	Center,
	Start,
	End,
	SpaceAround,
	SpaceBetween,
	Stretch,
}

#[derive(Debug, Copy, Clone)]
pub enum AlignmentHorizontal {
	Center,
	Start,
	End,
	SpaceAround,
	SpaceBetween,
	Stretch,
}

pub mod compilation_macroses;
pub mod ui_page_context;
