#[cfg(feature = "desktop_qt")]
#[macro_export]
macro_rules! desktop_qt {
	( $( $tok:tt )* ) => { $( $tok )* }
}

#[cfg(not(feature = "desktop_qt"))]
#[macro_export]
macro_rules! desktop_qt {
	($($tok:tt)*) => {};
}

#[cfg(not(any(feature = "desktop_qt")))]
#[macro_export]
macro_rules! desktop_noop {
	( $( $tok:tt )* ) => {
		$( $tok )*
	}
}

#[cfg(any(feature = "desktop_qt"))]
#[macro_export]
macro_rules! desktop_noop {
	($($tok:tt)*) => {};
}
