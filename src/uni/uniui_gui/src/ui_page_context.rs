use std::sync::{
	Arc,
	RwLock,
	Weak,
};

use uni_components::{
	ui_page::UiPage,
	CallError,
};

use crate::{
	DataProcessor,
	WidgetGenerator,
};

/// The trait represents cross platform abstraction of a UiPage
///
/// For the most cases it'll represent a highest level UI element such as
/// window for desktop, activity for Android, tab for browser, etc.
pub trait UiPageContext {
	/// Get value for environment vairable by variable name
	fn get_env_var(
		&self,
		var_name: &str,
	) -> Option<String>;

	/// Set title for the related window/activity/tab/etc.
	fn set_title(
		&self,
		title: &str,
	);

	/// Set main widget. The widget will take the whole space of the UiPage.
	fn set_main_widget(
		&mut self,
		widget: Box<dyn crate::Widget + 'static>,
	);

	/// Get path to the resource. Will be changed soon, please don't use.
	fn get_resource_path(
		&self,
		link: &str,
	) -> String;

	/// Add data processor to the UiPageContext.
	///
	/// It's better to use [UiPageContext::add_data_processor] instead.
	fn add_data_processor_shared(
		&mut self,
		dp: Arc<RwLock<dyn DataProcessor>>,
	);

	/// Returns reference to WidgetGenerator
	fn widget_generator(&mut self) -> &mut dyn WidgetGenerator;

	fn open(
		&self,
		path: &str,
		data: &str,
	) -> Result<(), CallError>;

	fn get_launch_string(&self) -> Result<String, CallError>;

	/// Temporary. Will be removed soon.
	fn get_host(&self) -> String {
		return "localhost".to_owned();
	}
}

impl dyn UiPageContext {
	/// Add data processor to the UiPaqeContext.
	///
	/// The processor will be called to process pending data each tick.
	pub fn add_data_processor<T>(
		context: &mut dyn UiPageContext,
		processor: T,
	) -> Weak<RwLock<T>>
	where
		T: 'static + DataProcessor,
	{
		let holder = Arc::new(RwLock::new(processor));
		let result = Arc::downgrade(&holder);
		context.add_data_processor_shared(holder);
		return result;
	}

	pub fn open_page<T>(
		context: &dyn UiPageContext,
		target: &dyn UiPage<Data = T>,
	) -> Result<(), CallError>
	where
		T: 'static + serde::de::DeserializeOwned + serde::Serialize,
	{
		let path = uni_components::ui_page::system_depend_path(target);
		return context.open(&path, "");
	}

	pub fn open_page_with_data<T>(
		context: &dyn UiPageContext,
		target: &dyn UiPage<Data = T>,
		data: &T,
	) -> Result<(), CallError>
	where
		T: 'static + serde::de::DeserializeOwned + serde::Serialize,
	{
		let path = uni_components::ui_page::system_depend_path(target);
		let data = target.encode_data(data)?;
		return context.open(&path, &data);
	}

	pub fn get_launch_parameter<T>(
		context: &dyn UiPageContext,
		page: &dyn UiPage<Data = T>,
	) -> Result<T, CallError>
	where
		T: 'static + serde::de::DeserializeOwned + serde::Serialize,
	{
		let parameter_string = context.get_launch_string()?;
		return page.extract_launch_parameter(&parameter_string);
	}
}
