#[cfg(target_arch = "wasm32")]
use web_sys::CssStyleDeclaration;

#[cfg(target_arch = "wasm32")]
use crate::prelude::*;

/// Represents value with unit type
#[derive(Debug, Copy, Clone)]
pub enum SizeUnit {
	/// Pixel
	PX(f32),

	/// Point
	PT(f32),

	/// View height
	VH(f32),

	/// View width
	VW(f32),

	/// View max (width or height)
	VMax(f32),

	/// View min (width or height)
	VMin(f32),

	/// %
	Percent(f32),

	/// em
	EM(f32),
}

impl SizeUnit {
	/// ???
	pub fn to_native(&self) -> &'static str {
		return match self {
			SizeUnit::PX(_) => "px",
			SizeUnit::PT(_) => "pt",
			SizeUnit::VH(_) => "vh",
			SizeUnit::VW(_) => "vw",
			SizeUnit::VMax(_) => "vmax",
			SizeUnit::VMin(_) => "vmin",
			SizeUnit::Percent(_) => "%",
			SizeUnit::EM(_) => "em",
		};
	}

	/// Generate string which may be used as a part of html/css
	pub fn to_htmlstring(&self) -> String {
		return match self {
			SizeUnit::PX(v) => format!("{}px", v),
			SizeUnit::PT(v) => format!("{}pt", v),
			SizeUnit::VH(v) => format!("{}vh", v),
			SizeUnit::VW(v) => format!("{}vw", v),
			SizeUnit::VMax(v) => format!("{}vmax", v),
			SizeUnit::VMin(v) => format!("{}vmin", v),
			SizeUnit::Percent(v) => format!("{}%", v),
			SizeUnit::EM(v) => format!("{}em", v),
		};
	}

	/// Add property with name `property_name` and Self's value into `style`
	#[cfg(target_arch = "wasm32")]
	pub fn apply_as_property_value(
		&self,
		style: &CssStyleDeclaration,
		property_name: &str,
	) {
		let value_str = self.to_htmlstring();
		style.set_property_silent(property_name, &value_str);
	}
}
