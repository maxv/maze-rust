use crate::{
	DataProcessor,
	NativeWidget,
	WidgetGenerator,
};

/// Minimal UI element.
///
/// Widget represents minimal UI element. Most common widgets is Button and TextEdit.
pub trait Widget: DataProcessor {
	/// Generate native widget
	fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()>;

	/// Allows drawing on top of generated NativeWidget
	fn draw(&mut self) {
	}
}
