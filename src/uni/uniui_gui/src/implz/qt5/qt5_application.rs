use std::{
	ffi::c_void,
	sync::{
		Arc,
		RwLock,
	},
};

use uni_components::CallError;

use crate::{
	DataProcessor,
	Widget,
	UiPageContext,
	WidgetGenerator,
};

use super::Qt5WidgetGenerator;


extern "C" {
	fn uniui_gui_application_create() -> *mut c_void;

	fn uniui_gui_application_start(
		native_app: *mut c_void,
		cb: extern "C" fn(*mut std::ffi::c_void),
		app: *mut c_void,
	);

	fn uniui_gui_widget_show(native_app: *mut c_void);
}


extern "C" fn callback(app: *mut std::ffi::c_void) {
	let app: &mut Qt5Application = unsafe { &mut *(app as *mut Qt5Application) };
	app.process_data();
	app.on_draw();
}

pub struct Qt5Application {
	native: *mut c_void,
	main_widget: Option<Box<dyn Widget>>,
	widget_generator: Qt5WidgetGenerator,
	data_processor_list: Vec<Arc<RwLock<dyn DataProcessor>>>,
}

impl crate::UiPageContext for Qt5Application {
	fn get_env_var(
		&self,
		var_name: &str,
	) -> Option<String> {
		return self.get_env_var(var_name);
	}
	
	fn set_title(
		&self,
		title: &str,
	) {
		self.set_title(title);
	}

	fn set_main_widget(
		&mut self,
		widget: Box<dyn crate::Widget + 'static>,
	) {
		self.set_main_widget(widget);
	}
	
	fn get_resource_path(
		&self,
		_link: &str,
	) -> String {
		log::error!("get_resource_path is not implemented for Qt5. Yet?");
		return "".to_owned();
	}
	
	fn add_data_processor_shared(
		&mut self, 
		dp: Arc<RwLock<dyn DataProcessor>>
	) {
		self.data_processor_list.push(dp);
	}
	
	fn open(&self, _path: &str, _data: &str) -> Result<(), CallError> {
		return Ok(());
	}

	fn get_launch_string(&self) -> Result<String, CallError> {
		return Err(CallError::NotAvailableForPlatform);
	}

	fn widget_generator(&mut self) -> &mut dyn WidgetGenerator {
		return &mut self.widget_generator;
	}
}

impl Qt5Application {
	pub fn start(f: &dyn Fn(&mut dyn UiPageContext)) -> Result<(), ()> {
		let mut app = Qt5Application::new();
		f(&mut app);

		app.layout();

		log::trace!("app started");
		unsafe {
			uniui_gui_application_start(
				app.native,
				callback,
				&mut app as *mut _ as *mut std::ffi::c_void,
			);
		};
		Ok(())
	}

	pub fn set_title(
		&self,
		_title: &str,
	) {
		log::error!("set_title is not implemented for Qt5. Yet.");
	}

	pub fn set_local_path(
		&mut self,
		_module_address: &str,
	) -> Result<(), ()> {
		log::error!("set_local_path is not implemented for Qt5. Yet.");
		return Ok(());
	}

	pub fn link_from_base(
		&self,
		link: &str,
	) -> String {
		log::error!("link_from_base is not implemented for Qt5. Yet.");
		return link.to_string();
	}

	pub fn set_main_widget(
		&mut self,
		widget: Box<dyn Widget + 'static>,
	) {
		self.main_widget = Some(widget);
	}

	pub fn get_env_var(
		&self,
		_var_name: &str,
	) -> Option<String> {
		None
	}

	pub fn replace_local_ref(
		&self,
		_: &str,
	) {
	}

	pub fn get_host(&self) -> String {
		return String::new();
	}

	pub fn new() -> Qt5Application {
		let native = unsafe { uniui_gui_application_create() };
		return Qt5Application {
			native,
			main_widget: None,
			widget_generator: Qt5WidgetGenerator {},
			data_processor_list: Vec::new(),
		};
	}

	fn layout(&mut self) {
		match self.main_widget.as_mut() {
			Some(widget) => {
				match widget.to_native(&mut self.widget_generator) {
					Ok(native_main_widget) => {
						unsafe {
							uniui_gui_widget_show(native_main_widget);
						};
					},
					Err(_) => log::error!("to_native failed"),
				}
			},
			None => log::error!("main widget not set"),
		}
	}

	fn on_draw(&mut self) {
		match &mut self.main_widget {
			Some(l) => l.draw(),
			None => log::error!("no layout"),
		}
	}

	fn process_data(&mut self) {
		let start = std::time::SystemTime::now();
		let msec_since_app_epoch = start
			.duration_since(std::time::UNIX_EPOCH)
			.expect("Time went backwards")
			.as_millis() as i64;

		// FIXME(#131) layout are not accessable during data processing of layout and
		// it's subcomponents
		self.main_widget = match self.main_widget.take() {
			Some(mut l) => {
				l.process_data(msec_since_app_epoch, self);
				Some(l)
			},
			None => None,
		};

		let mut data_processor_list = self.data_processor_list.clone();
		data_processor_list.iter_mut().for_each(|holder| {
			match holder.write() {
				Ok(mut item) => item.process_data(msec_since_app_epoch, self),
				Err(e) => log::error!("can't lock data processor e:{:?}", e),
			}
		});
	}
}
