use std::ffi::c_void;

extern "C" {
	fn uniui_gui_qpointer_for_qwidget_create(qwidget: *mut c_void) -> *mut c_void;
	fn uniui_gui_qpointer_is_alive(qpointer_to_qwidget: *mut c_void) -> bool;
	fn uniui_gui_qpointer_get_widget(qpointer_to_qwidget: *mut c_void) -> *mut c_void;
	fn uniui_gui_qpointer_drop(qpointer_to_qwidget: *mut c_void);
}

pub struct Qt5OptionalNative {
	native: Option<*mut c_void>,
}

impl Qt5OptionalNative {
	pub fn new() -> Qt5OptionalNative {
		return Qt5OptionalNative {
			native: None,
		};
	}

	pub unsafe fn new_with(pointer_to_qwidget: *mut c_void) -> Qt5OptionalNative {
		let mut result = Qt5OptionalNative::new();
		result.replace(pointer_to_qwidget);
		return result;
	}

	pub unsafe fn replace(
		&mut self,
		pointer_to_qwidget: *mut c_void,
	) {
		let p = uniui_gui_qpointer_for_qwidget_create(pointer_to_qwidget);
		let old = self.native.replace(p);
		Qt5OptionalNative::drop_element(old);
	}

	pub fn as_ref(&self) -> Option<*mut c_void> {
		return match self.native.as_ref() {
			Some(qpointer) => {
				let qpointer = qpointer.clone();
				let is_alive = unsafe { uniui_gui_qpointer_is_alive(qpointer) };
				match is_alive {
					true => {
						let qwidget = unsafe { uniui_gui_qpointer_get_widget(qpointer) };
						Some(qwidget)
					},
					false => None,
				}
			},
			None => None,
		};
	}

	pub fn as_mut(&mut self) -> Option<*mut c_void> {
		return self.as_ref();
	}

	fn drop_element(mut qpointer_to_qwidget: Option<*mut c_void>) {
		if let Some(p) = qpointer_to_qwidget.take() {
			unsafe {
				uniui_gui_qpointer_drop(p);
			};
		};
	}
}

impl Drop for Qt5OptionalNative {
	fn drop(&mut self) {
		Qt5OptionalNative::drop_element(self.native);
	}
}
