use super::Qt5NativeWidget;
use crate::WidgetGenerator;

pub struct Qt5WidgetGenerator {}

impl WidgetGenerator for Qt5WidgetGenerator {
	fn create_element(
		&mut self,
		_: &str,
	) -> Result<Qt5NativeWidget, ()> {
		Err(())
	}
}
