use crate::{
	NativeWidget,
	WidgetGenerator,
};


use wasm_bindgen::JsCast;
use web_sys::{
	Document,
	HtmlElement,
};

pub struct WasmWidgetGenerator {
	pub document: Document,
}

impl WidgetGenerator for WasmWidgetGenerator {
	fn create_element(
		&mut self,
		s: &str,
	) -> Result<NativeWidget, ()> {
		match self.document.create_element(s) {
			Ok(e) => {
				match e.dyn_into::<HtmlElement>() {
					Ok(e) => Ok(e),
					Err(_) => Err(()),
				}
			},
			Err(_) => Err(()),
		}
	}
}
