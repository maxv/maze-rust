pub type WasmNativeWidget = web_sys::HtmlElement;

mod wasm_application;
pub use self::wasm_application::WasmApplication;

mod wasm_widget_generator;
pub use self::wasm_widget_generator::WasmWidgetGenerator;

mod wasm_optional_native;
pub use self::wasm_optional_native::WasmOptionalNative;
