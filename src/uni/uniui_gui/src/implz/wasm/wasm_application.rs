use std::{
	cell::RefCell,
	collections::HashMap,
	rc::Rc,
	sync::{
		Arc,
		RwLock,
		Weak,
	},
};

use wasm_bindgen::{
	closure::Closure,
	JsCast,
};

use web_sys::{
	DocumentFragment,
	Window,
};

use url::Url;

use uni_components::CallError;

use crate::{
	utils::SilentCssPropertySetter,
	DataProcessor,
	UiPageContext,
	Widget,
	WidgetGenerator,
};

use super::wasm_widget_generator::WasmWidgetGenerator;


#[derive(Debug)]
pub enum Error {
	NoWindowFound,
	UnknownError,
	AlreadyExist,
	KeyEventRegistrationFailed,
}


pub struct WasmApplication {
	main_widget: Option<Box<dyn Widget>>,
	window: Window,
	this: Option<Rc<RefCell<WasmApplication>>>, // we are undead
	cyclic_animation: bool,
	animation_routine: Option<Closure<dyn FnMut()>>,
	process_data_routine: Option<Closure<dyn FnMut()>>,
	data_processor_list: Vec<Arc<RwLock<dyn DataProcessor>>>,
	epoch: i64,
	widget_generator: WasmWidgetGenerator,
	urls_data: HashMap<String, Vec<u8>>,
	base_address: String,
}

impl crate::UiPageContext for WasmApplication {
	fn get_env_var(
		&self,
		var_name: &str,
	) -> Option<String> {
		return self.get_env_var(var_name);
	}

	fn set_title(
		&self,
		title: &str,
	) {
		self.set_title(title);
	}

	fn set_main_widget(
		&mut self,
		widget: Box<dyn crate::Widget + 'static>,
	) {
		self.set_main_widget(widget);
	}

	fn get_resource_path(
		&self,
		link: &str,
	) -> String {
		return self.get_resource_path(link);
	}

	fn add_data_processor_shared(
		&mut self,
		dp: Arc<RwLock<dyn DataProcessor>>,
	) {
		self.data_processor_list.push(dp);
	}

	fn widget_generator(&mut self) -> &mut dyn WidgetGenerator {
		return self.widget_generator();
	}

	fn open(
		&self,
		path: &str,
		data: &str,
	) -> Result<(), CallError> {
		let path = format!("{}?{}", path, data);
		uni_net::open(&path)?;
		return Ok(());
	}

	fn get_launch_string(&self) -> Result<String, CallError> {
		return match self.window.location().search() {
			Err(e) => {
				log::error!("get_launch_string can't get search e:{:?}", e);
				Err(CallError::IncorrectInput)
			},
			Ok(s) => {
				match s.len() {
					0 | 1 => {
						log::error!("get_launch_string input is to short");
						Err(CallError::IncorrectInput)
					},
					_ => Ok(s[1..].to_owned()),
				}
			},
		};
	}

	fn get_host(&self) -> String {
		return self.get_host();
	}
}

impl WasmApplication {
	pub fn start(f: &dyn Fn(&mut dyn UiPageContext)) -> Result<(), ()> {
		let app = WasmApplication::try_new();
		return match app {
			Ok(app) => {
				let app = &mut *app.borrow_mut();
				f(app);
				app.layout();
				app.redraw();
				Ok(())
			},
			Err(e) => {
				log::error!("can't create app:{:?}", e);
				Err(())
			},
		};
	}

	pub fn set_title(
		&self,
		title: &str,
	) {
		self.window.document().unwrap().set_title(title);
	}

	pub fn get_host(&self) -> String {
		return web_sys::window().unwrap().location().host().unwrap();
	}

	pub fn set_local_path(
		&mut self,
		module_address: &str,
	) -> Result<(), ()> {
		let mut pathname = web_sys::window().unwrap().location().pathname().unwrap();
		return match pathname.ends_with(module_address) {
			true => {
				let letters = pathname.len() - module_address.len();
				pathname.truncate(letters);
				if !pathname.ends_with("/") {
					pathname += "/";
				};

				self.base_address = pathname;
				log::info!("base_address:{}", self.base_address);
				Ok(())
			},
			false => {
				log::warn!(
					"pathname:{} doesn't match module_address:{}",
					pathname,
					module_address
				);
				Err(())
			},
		};
	}

	pub fn get_resource_path(
		&self,
		link: &str,
	) -> String {
		return format!("{}{}", self.base_address, link);
	}

	pub fn get_env_var(
		&self,
		var_name: &str,
	) -> Option<String> {
		let location = web_sys::window().unwrap().location().href().unwrap();
		let url = Url::parse(&location).unwrap();
		let mut result = None;
		for (that_var_name, that_var_value) in url.query_pairs() {
			if var_name == that_var_name {
				result = Some(that_var_value.to_string());
				break;
			}
		}

		return result;
	}

	pub fn replace_local_ref(
		&self,
		s: &str,
	) {
		let location = web_sys::window().unwrap().location();
		match location.set_href(s) {
			Ok(()) => log::trace!("location changed:{}", s),
			Err(e) => log::error!("location change error:{:?}", e),
		};
	}

	pub fn push_state_with_new_env_var(
		&mut self,
		var_name: &str,
		var_value: &str,
		title: &str,
	) {
		let location = web_sys::window().unwrap().location().href().unwrap();
		let current_url = url::Url::parse(&location).unwrap();

		let mut new_url = current_url.clone();
		{
			let mut pairs = new_url.query_pairs_mut();
			pairs.clear();
			pairs.append_pair(var_name, var_value);
			for (that_var_name, that_var_value) in current_url.query_pairs() {
				if var_name != that_var_name {
					pairs.append_pair(&that_var_name, &that_var_value);
					break;
				}
			}
		}

		web_sys::window()
			.unwrap()
			.history()
			.unwrap()
			.push_state_with_url(
				&wasm_bindgen::JsValue::NULL,
				title,
				Some(&new_url.to_string()),
			)
			.unwrap();

		log::info!("new state:{}", current_url);
	}

	pub fn try_new() -> Result<Rc<RefCell<WasmApplication>>, Error> {
		let result = match web_sys::window() {
			Some(window) => {
				let app = Rc::new(RefCell::new(WasmApplication::new(window)));
				app.borrow_mut().this = Some(app.clone());
				app.borrow_mut().animation_routine =
					Some(WasmApplication::create_animation_routine(app.clone()));
				app.borrow_mut().process_data_routine =
					Some(WasmApplication::create_process_data_routine(app.clone()));

				Ok(app)
			},
			None => {
				log::error!("try_new:window:err:not_found");
				Err(Error::NoWindowFound)
			},
		};

		return result;
	}

	pub fn redraw(&mut self) {
		match &self.animation_routine {
			Some(r) => {
				let result =
					self.window.request_animation_frame(r.as_ref().unchecked_ref());

				match result {
					Ok(id) => log::trace!("request_next_frame:ok:{}", id),
					Err(e) => log::error!("request_next_frame:request_failed:{:?}", e),
				}
			},
			None => log::error!("request_next_frame:err:routine is None"),
		}
	}

	pub fn set_main_widget(
		&mut self,
		widget: Box<dyn crate::Widget + 'static>,
	) {
		self.main_widget = Some(widget);
	}

	pub fn widget_generator(&mut self) -> &mut dyn WidgetGenerator {
		return &mut self.widget_generator;
	}

	pub fn layout(&mut self) {
		let fragment = Rc::new(DocumentFragment::new().unwrap());
		let fragment_clone = Rc::clone(&fragment);
		match self.main_widget.as_mut() {
			Some(l) => {
				match l.to_native(&mut self.widget_generator) {
					Ok(element) => {
						let style = element.style();
						style.set_property_silent("width", "100%");
						style.set_property_silent("height", "100vh");
						style.set_property_silent("max-height", "100vh");

						match fragment.append_child(&element) {
							Ok(_) => log::trace!("child added"),
							Err(e) => log::error!("can't add child e:{:?}", e),
						};
					},
					Err(e) => log::error!("element to_native error:{:?}", e),
				};
			},
			None => log::error!("No layout"),
		}

		let document = self.window.document().unwrap();
		let body = document.body();
		match body {
			Some(body) => {
				let append_result = body.append_child(&fragment_clone);
				match append_result {
					Ok(_) => log::trace!("fragment added"),
					Err(e) => log::error!("couldn't add fragment e:{:?}", e),
				}
			},
			None => {
				log::error!("document w/o body");
			},
		}
	}

	pub fn set_cyclic_animation(
		&mut self,
		val: bool,
	) {
		self.cyclic_animation = val;
	}

	pub fn add_data_processor<T>(
		&mut self,
		processor: T,
	) -> Weak<RwLock<T>>
	where
		T: 'static + DataProcessor,
	{
		let holder = Arc::new(RwLock::new(processor));
		let result = Arc::downgrade(&holder);
		self.data_processor_list.push(holder);
		return result;
	}

	pub fn data_by_url(
		&self,
		s: &str,
	) -> Option<&Vec<u8>> {
		return self.urls_data.get(s);
	}

	pub fn slice_to_url(data: &[u8]) -> Result<String, ()> {
		let uint8array = js_sys::Uint8Array::from(data);
		let array_to_save = js_sys::Array::new();
		array_to_save.set(0, uint8array.into());
		return match web_sys::Blob::new_with_u8_array_sequence(&array_to_save) {
			Ok(blob) => {
				match web_sys::Url::create_object_url_with_blob(&blob) {
					Ok(url) => {
						let mut v = Vec::new();
						v.extend_from_slice(data);
						Ok(url)
					},
					Err(e) => {
						log::error!("url failed e:{:?}", e);
						Err(())
					},
				}
			},
			Err(e) => {
				log::error!("blob failed e:{:?}", e);
				Err(())
			},
		};
	}

	pub fn save_file(
		data: &[u8],
		file_name: &str,
	) {
		match WasmApplication::slice_to_url(data) {
			Ok(url) => {
				let document = web_sys::window().unwrap().document().unwrap();
				let a = document.create_element("a").unwrap();
				let a = a.dyn_into::<web_sys::HtmlAnchorElement>().unwrap();
				a.set_download(file_name);
				a.set_href(&url);

				match document.body() {
					Some(body) => {
						match body.append_child(&a) {
							Ok(_) => log::trace!("anchor added"),
							Err(e) => log::error!("couldn't add anchor e:{:?}", e),
						}
					},
					None => {
						log::error!("document w/o body");
					},
				}

				a.click();
			},
			Err(()) => log::error!("slice_to_url failed"),
		};
	}

	fn new(window: Window) -> WasmApplication {
		return WasmApplication {
			main_widget: None,
			this: None,
			cyclic_animation: false,
			animation_routine: None,
			process_data_routine: None,
			data_processor_list: Vec::new(),
			epoch: window.performance().expect("performance here").now() as i64,
			widget_generator: WasmWidgetGenerator {
				document: window.document().unwrap(),
			},
			window,
			urls_data: HashMap::new(),
			base_address: String::new(),
		};
	}

	fn create_animation_routine(
		app: Rc<RefCell<WasmApplication>>
	) -> Closure<dyn FnMut()> {
		let closure_ext = Closure::wrap(Box::new(move || {
			app.borrow_mut().on_draw();
		}) as Box<dyn FnMut()>);

		return closure_ext;
	}

	fn create_process_data_routine(
		app: Rc<RefCell<WasmApplication>>
	) -> Closure<dyn FnMut()> {
		let closure_ext = Closure::wrap(Box::new(move || {
			app.borrow_mut().process_data();
		}) as Box<dyn FnMut()>);

		return closure_ext;
	}

	fn request_data_processing(&mut self) {
		match &self.process_data_routine {
			Some(r) => {
				let r =
					self.window.set_timeout_with_callback_and_timeout_and_arguments_0(
						r.as_ref().unchecked_ref(),
						0,
					);

				match r {
					Ok(id) => log::trace!("request_data_processing:{}", id),
					Err(e) => {
						log::error!("request_data_processing:request_failed:{:?}", e)
					},
				}
			},
			None => log::error!("request_data_processing:routine is None"),
		}
	}

	fn on_draw(&mut self) {
		match &mut self.main_widget {
			Some(l) => l.draw(),
			None => log::error!("no layout"),
		}

		// https://gitlab.com/maxv/maze-rust/snippets/1881551
		self.request_data_processing();

		// Required since we not only draw in that method but also requests
		// data processing. At least before we found better way to keep our
		// main event loop runing.
		self.redraw();
	}

	fn msec_since_app_epoch(&self) -> i64 {
		return self.window.performance().expect("performance here").now() as i64 -
			self.epoch;
	}

	fn process_data(&mut self) {
		let msec_since_app_epoch = self.msec_since_app_epoch();

		// FIXME(#131) layout are not accessable during data processing of layout and
		// it's subcomponents
		self.main_widget = match self.main_widget.take() {
			Some(mut l) => {
				l.process_data(msec_since_app_epoch, self);
				Some(l)
			},
			None => None,
		};

		let mut data_processor_list = self.data_processor_list.clone();
		data_processor_list.iter_mut().for_each(|holder| {
			match holder.write() {
				Ok(mut item) => item.process_data(msec_since_app_epoch, self),
				Err(e) => log::error!("can't lock data processor e:{:?}", e),
			}
		});
	}
}
