use web_sys::{
	Element,
	HtmlElement,
};

use std::ops::Drop;


pub struct WasmOptionalNative<T>
where
	HtmlElement: std::convert::From<T>,
{
	native: Option<T>,
}

impl<T> WasmOptionalNative<T>
where
	HtmlElement: std::convert::From<T>,
{
	pub fn new() -> WasmOptionalNative<T> {
		return WasmOptionalNative {
			native: None,
		};
	}

	pub fn new_with(t: T) -> WasmOptionalNative<T> {
		return WasmOptionalNative {
			native: Some(t),
		};
	}

	pub fn replace(
		&mut self,
		new: T,
	) {
		let old = self.native.replace(new);
		WasmOptionalNative::drop_element(old);
	}

	pub fn as_mut(&mut self) -> Option<&mut T> {
		return self.native.as_mut();
	}

	pub fn as_ref(&self) -> Option<&T> {
		return self.native.as_ref();
	}

	fn drop_element(mut e: Option<T>) {
		match e.take() {
			Some(e) => {
				let e: HtmlElement = e.into();
				let e: Element = e.into();
				e.remove();
			},
			None => {},
		}
	}
}

impl<T> Drop for WasmOptionalNative<T>
where
	HtmlElement: std::convert::From<T>,
{
	fn drop(&mut self) {
		WasmOptionalNative::drop_element(self.native.take());
	}
}
