use std::sync::{
	Arc,
	RwLock,
};

use jni::objects::{
	JClass,
	JValue,
};

use uni_jnihelpers::{
	JniError,
	UniGlobalRef,
};

use uni_components::CallError;

use crate::{
	DataProcessor,
	UiPageContext,
	Widget,
	WidgetGenerator,
};

use super::{
	java_object::JavaObject,
	native_activity::{
		Activity,
		NativeActivity,
	},
};

pub struct AndroidApplication {
	main_widget: Option<Box<dyn Widget>>,
	data_processor_list: Vec<Arc<RwLock<dyn DataProcessor>>>,

	// That's OK to keep reference to activity there.
	// Whenever Activity will be destroyed it'll destroy application as well
	activity: NativeActivity,
}

impl UiPageContext for AndroidApplication {
	fn get_env_var(
		&self,
		var_name: &str,
	) -> Option<String> {
		return self.get_env_var(var_name);
	}

	fn set_title(
		&self,
		title: &str,
	) {
		self.set_title(title);
	}

	fn set_main_widget(
		&mut self,
		widget: Box<dyn crate::Widget + 'static>,
	) {
		self.set_main_widget(widget);
	}

	fn get_resource_path(
		&self,
		_link: &str,
	) -> String {
		log::error!(
			"UiPageContext::get_resource_path is not supported for android. Yet?"
		);
		return "".to_owned();
	}

	fn add_data_processor_shared(
		&mut self,
		dp: Arc<RwLock<dyn DataProcessor>>,
	) {
		self.data_processor_list.push(dp);
	}

	fn open(
		&self,
		path: &str,
		data: &str,
	) -> Result<(), CallError> {
		self.open_activity(path, data)?;
		return Ok(());
	}

	fn get_launch_string(&self) -> Result<String, CallError> {
		let r = self.activity.get_launch_parameters()?;
		return Ok(r);
	}

	fn widget_generator(&mut self) -> &mut dyn WidgetGenerator {
		return &mut self.activity;
	}
}

impl AndroidApplication {
	pub fn start(
		env: jni::JNIEnv,
		activity: jni::objects::JObject,
		f: &dyn Fn(&mut dyn UiPageContext),
	) -> Result<(), ()> {
		return match NativeActivity::try_from(&env, activity.clone()) {
			Err(e) => {
				log::error!("Native activity creation failed. Err:{:?}", e);
				Err(())
			},
			Ok(a) => {
				let mut app = Box::new(AndroidApplication {
					main_widget: None,
					data_processor_list: Vec::new(),
					activity: a,
				});

				f(app.as_mut());

				if let Err(e) = app.layout() {
					println!("Layout failed:{:?}", e);
					panic!("Layout failed");
				}

				let class = match env.get_object_class(activity) {
					Ok(c) => c,
					Err(e) => {
						log::error!("Can't get activity's calss name. Err:{:?}", e);
						panic!("Can't get activity's calss name");
					},
				};

				let set_native = env.get_method_id(class, "setNative", "(J)V");
				let set_native = match set_native {
					Ok(c) => c,
					Err(e) => {
						log::error!("Can't get activity's setNative method. Err:{:?}", e);
						panic!("Can't get activity's setnative method.");
					},
				};

				let app_ptr = Box::into_raw(app) as i64;
				let set_native_result = env.call_method_unchecked(
					activity,
					set_native,
					jni::signature::JavaType::Primitive(jni::signature::Primitive::Void),
					&[jni::objects::JValue::Long(app_ptr)],
				);
				if let Err(e) = set_native_result {
					log::error!("setNative failed err:{:?}", e);
					panic!("setNative failed");
				};


				Ok(())
			},
		};
	}

	pub fn get_env_var(
		&self,
		var_name: &str,
	) -> Option<String> {
		// let property_name = format!("log.uniui_android.{}", var_name);
		// Use java/util/Properties#getProperty(java.lang.String)

		return if var_name == "logs" {
			Some("console".to_owned())
		} else if var_name == "loglevel" {
			Some("debug".to_owned())
		} else {
			None
		};
	}

	pub fn set_title(
		&self,
		title: &str,
	) {
		if let Err(e) = self.activity.set_title(title) {
			log::error!("Can't set title. Err:{:?}", e);
		}
	}

	pub fn set_main_widget(
		&mut self,
		widget: Box<dyn crate::Widget + 'static>,
	) {
		self.main_widget = Some(widget);
	}

	fn get_activity_class(
		&self,
		env: &jni::JNIEnv,
	) -> Result<UniGlobalRef<JClass<'static>>, JniError> {
		let class = uni_jnihelpers::get_java_class!(env, "android/app/Activity")?;
		return Ok(class);
	}

	fn layout(&mut self) -> Result<(), JniError> {
		return match self.main_widget.as_mut() {
			Some(widget) => {
				match widget.to_native(&mut self.activity) {
					Ok(native_main_widget) => {
						self.activity.set_content_view(&native_main_widget)?;
						Ok(())
					},
					Err(_) => {
						log::error!("to_native failed");
						Ok(())
					},
				}
			},
			None => {
				log::error!("main widget not set");
				Ok(())
			},
		};
	}

	pub fn tick(
		_: jni::JNIEnv,
		_: jni::objects::JObject,
		addr: jni::sys::jlong,
	) -> jni::sys::jlong {
		let mut app = unsafe { Box::from_raw(addr as *mut AndroidApplication) };
		app.tick_int();
		return Box::into_raw(app) as i64;
	}

	pub fn cleanup(
		_: jni::JNIEnv,
		_: jni::objects::JObject,
		addr: jni::sys::jlong,
	) -> () {
		let _to_be_killed = unsafe { Box::from_raw(addr as *mut AndroidApplication) };
	}

	fn tick_int(&mut self) {
		let start = std::time::SystemTime::now();
		let msec_since_app_epoch = start
			.duration_since(std::time::UNIX_EPOCH)
			.expect("Time went backwards")
			.as_millis() as i64;

		// FIXME(#131) layout are not accessable during data processing of layout and
		// it's subcomponents
		self.main_widget = match self.main_widget.take() {
			Some(mut l) => {
				l.process_data(msec_since_app_epoch, self);
				Some(l)
			},
			None => None,
		};

		let mut data_processor_list = self.data_processor_list.clone();
		data_processor_list.iter_mut().for_each(|holder| {
			match holder.write() {
				Ok(mut item) => item.process_data(msec_since_app_epoch, self),
				Err(e) => log::error!("can't lock data processor e:{:?}", e),
			}
		});
	}

	fn open_activity(
		&self,
		path: &str,
		data: &str,
	) -> Result<(), JniError> {
		let a = &self.activity;
		use jni::signature::{
			JavaType,
			JavaType::Primitive,
			Primitive::Void,
		};

		let env = a.jni_env()?;
		let component_name = {
			let component_name_class =
				uni_jnihelpers::get_java_class!(&env, "android/content/ComponentName")?;

			let ctor = uni_jnihelpers::get_java_method!(
				env,
				component_name_class.as_obj(),
				"<init>",
				"(Landroid/content/Context;Ljava/lang/String;)V"
			);

			let activity_class_name = env.new_string(path)?;

			let local_object =
				env.new_object_unchecked(component_name_class.as_obj(), ctor, &[
					JValue::Object(a.jobject()),
					JValue::Object(activity_class_name.into()),
				])?;

			local_object
		};

		let intent_class =
			uni_jnihelpers::get_java_class!(&env, "android/content/Intent")?;

		let intent = {
			let ctor = uni_jnihelpers::get_java_method!(
				env,
				intent_class.as_obj(),
				"<init>",
				"()V"
			);

			let local_object =
				env.new_object_unchecked(intent_class.as_obj(), ctor, &[])?;

			local_object
		};

		let set_component = uni_jnihelpers::get_java_method!(
			env,
			intent_class.as_obj(),
			"setComponent",
			"(Landroid/content/ComponentName;)Landroid/content/Intent;"
		);

		let _ = env.call_method_unchecked(
			intent,
			set_component,
			JavaType::Object("android/content/Intent".to_owned()),
			&[JValue::Object(component_name.into())],
		)?;

		{
			let put_extra = uni_jnihelpers::get_java_method!(
				env,
				intent_class.as_obj(),
				"putExtra",
				"(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;"
			);

			let key = env.new_string(uni_components::ANDROID_INTENT_KEY)?;
			let data = env.new_string(data)?;

			let _ = env.call_method_unchecked(
				intent,
				put_extra,
				JavaType::Object("android/content/Intent".to_owned()),
				&[JValue::Object(key.into()), JValue::Object(data.into())],
			)?;
		}

		let ac = self.get_activity_class(&env)?;
		let start_activity = uni_jnihelpers::get_java_method!(
			env,
			ac.as_obj(),
			"startActivity",
			"(Landroid/content/Intent;)V"
		);

		let _ =
			env.call_method_unchecked(a.jobject(), start_activity, Primitive(Void), &[
				JValue::Object(intent),
			])?;

		Ok(())
	}
}
