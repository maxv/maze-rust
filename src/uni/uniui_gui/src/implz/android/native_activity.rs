use jni::{
	objects::{
		JClass,
		JObject,
		JValue,
	},
	JNIEnv,
	JavaVM,
};

use crate::{
	NativeWidget,
	WidgetGenerator,
};

use super::{
	android_context::Context,
	java_object::{
		JavaObject,
		JavaObjectConstructorable,
	},
	jni_error::JniError,
	uni_global_ref::UniGlobalRef,
	AndroidNativeWidget,
};

pub struct NativeActivity {
	global_ref: UniGlobalRef<JObject<'static>>,
	vm: JavaVM,
}

#[derive(Debug)]
struct NotActivity {}

impl std::fmt::Display for NotActivity {
	fn fmt(
		&self,
		f: &mut std::fmt::Formatter<'_>,
	) -> std::fmt::Result {
		write!(f, "jobject doesn't implement Activity class")
	}
}

impl std::error::Error for NotActivity {
}


impl NativeActivity {
	pub fn try_from(
		env: &jni::JNIEnv,
		jobject: JObject,
	) -> Result<NativeActivity, JniError> {
		let jobject_static: JObject<'static> = jobject.into_inner().into();
		let class_name = Activity::get_class(env)?;
		return match env.is_instance_of(jobject_static, class_name.as_obj())? {
			false => {
				log::error!("passed jobject doesn't implement Activity class");
				Err(JniError::Custom(Box::new(NotActivity {})))
			},
			true => {
				let result = NativeActivity {
					global_ref: UniGlobalRef::try_new(env, jobject_static)?,
					vm: env.get_java_vm()?,
				};
				Ok(result)
			},
		};
	}

	pub fn into_global_ref(self) -> UniGlobalRef<JObject<'static>> {
		return self.global_ref;
	}
}

impl JavaObject for NativeActivity {
	fn jobject(&self) -> JObject {
		return self.global_ref.as_obj();
	}

	fn jni_env(&self) -> Result<JNIEnv, JniError> {
		return Ok(self.vm.get_env()?);
	}
}

impl JavaObjectConstructorable for NativeActivity {
	fn construct(
		global_ref: UniGlobalRef<JObject<'static>>,
		vm: jni::JavaVM,
	) -> Self {
		return Self {
			global_ref,
			vm,
		};
	}
}

impl Activity for NativeActivity {
}

impl WidgetGenerator for NativeActivity {
	fn create_element(
		&mut self,
		_s: &str,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}

	fn context(&self) -> &dyn Context {
		return self;
	}
}

impl Context for NativeActivity {
}

pub trait Activity: JavaObject {
	fn set_title(
		&self,
		text: &str,
	) -> Result<(), JniError> {
		use jni::signature::{
			JavaType::Primitive,
			Primitive::Void,
		};

		let env = self.jni_env()?;
		let activity_class = Activity::get_class(&env)?;
		let set_title = crate::get_java_method!(
			env,
			activity_class.as_obj(),
			"setTitle",
			"(Ljava/lang/CharSequence;)V"
		);
		let text = env.new_string(text)?;

		let _ =
			env.call_method_unchecked(self.jobject(), set_title, Primitive(Void), &[
				JValue::Object(text.into()),
			])?;

		return Ok(());
	}

	fn set_content_view(
		&self,
		view: &AndroidNativeWidget,
	) -> Result<(), JniError> {
		let env = self.jni_env()?;
		let activity_class = Activity::get_class(&env)?;

		let set_content_view = crate::get_java_method!(
			&env,
			activity_class.as_obj(),
			"setContentView",
			"(Landroid/view/View;)V"
		);

		let widget = jni::objects::JValue::Object(view.as_obj());

		let _ = env.call_method_unchecked(
			self.jobject(),
			set_content_view,
			jni::signature::JavaType::Primitive(jni::signature::Primitive::Void),
			&[widget],
		)?;

		return Ok(());
	}

	fn get_launch_parameters(&self) -> Result<String, JniError> {
		use jni::signature::JavaType;

		let env = self.jni_env()?;
		let activity_class = Activity::get_class(&env)?;
		let get_intent = crate::get_java_method!(
			env,
			activity_class.as_obj(),
			"getIntent",
			"()Landroid/content/Intent;"
		);

		let intent = env.call_method_unchecked(
			self.jobject(),
			get_intent,
			JavaType::Object("Landroid/content/Intent;".to_owned()),
			&[],
		)?;

		return match intent {
			JValue::Object(intent) => {
				let intent_class =
					uni_jnihelpers::get_java_class!(&env, "android/content/Intent")?;

				let get_chars_extra = crate::get_java_method!(
					env,
					intent_class.as_obj(),
					"getCharSequenceExtra",
					"(Ljava/lang/String;)Ljava/lang/CharSequence;"
				);

				let key = env.new_string(uni_components::ANDROID_INTENT_KEY)?;

				let chars = env.call_method_unchecked(
					intent,
					get_chars_extra,
					JavaType::Object("Ljava/lang/CharSequence;".to_owned()),
					&[JValue::Object(key.into())],
				)?;

				match chars {
					JValue::Object(jo) => {
						let jst2 = env.get_string(jo.into())?;
						let s: String = jst2.into();

						Ok(s)
					},
					_ => {
						log::error!("wrong type returned from get_chars_extra");
						panic!("wrong type returned from get_chars_extra");
					},
				}
			},
			_ => {
				log::error!("intent is not an object");
				panic!("intent is not an object");
			},
		};
	}
}

impl dyn Activity {
	fn get_class(env: &JNIEnv) -> Result<UniGlobalRef<JClass<'static>>, JniError> {
		let class = crate::get_java_class!(env, "android/app/Activity")?;
		return Ok(class);
	}
}
