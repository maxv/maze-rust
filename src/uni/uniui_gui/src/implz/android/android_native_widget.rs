use jni::objects::JObject;

use super::uni_global_ref::UniGlobalRef;

pub type AndroidNativeWidget = UniGlobalRef<JObject<'static>>;
