#[macro_export]
macro_rules! get_java_class {
	($env:expr, $class_name:expr) => {{
		$crate::implz::android::reimport::uni_jnihelpers::get_java_class!(
			$env,
			$class_name
			)
		}};
}

#[macro_export]
macro_rules! get_java_method {
	($env:expr, $class:expr, $method_name:expr, $method_signature:expr) => {{
		$crate::implz::android::reimport::uni_jnihelpers::get_java_method!(
			$env,
			$class,
			$method_name,
			$method_signature
			)
		}};
}
