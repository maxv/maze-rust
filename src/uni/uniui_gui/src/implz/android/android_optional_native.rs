use jni::objects::WeakGlobalRef;

use super::{
	java_object::{
		JavaObject,
		JavaObjectConstructorable,
	},
	jni_error::JniError,
	uni_global_ref::UniGlobalRef,
};

pub struct AndroidOptionalNative<T: JavaObject + JavaObjectConstructorable> {
	native: Option<(WeakGlobalRef, jni::JavaVM)>,
	_p: std::marker::PhantomData<T>,
}

impl<T: JavaObject + JavaObjectConstructorable> AndroidOptionalNative<T> {
	pub fn new() -> Self {
		return Self {
			native: None,
			_p: std::marker::PhantomData,
		};
	}

	pub fn replace(
		&mut self,
		object: &T,
	) -> Result<(), JniError> {
		let env = object.jni_env()?;
		let jobject = object.jobject();
		let weak_ref = env.new_weak_global_ref(jobject)?;
		let java_vm = env.get_java_vm()?;
		self.native.replace((weak_ref, java_vm));
		return Ok(());
	}

	pub fn get(&self) -> Option<T> {
		return match self.native.as_ref() {
			None => None,
			Some((wgr, vm)) => {
				match vm.get_env() {
					Ok(env) => {
						let maybe_ugr = unsafe {
							// The call is safe because structure's type is
							// freezed by `_p`.
							UniGlobalRef::try_upgrade(wgr, &env)
						};
						let maybe_vm = unsafe {
							// the call is safe because vm is pointing
							// to the valid JavaVM
							jni::JavaVM::from_raw(vm.get_java_vm_pointer())
						};
						match (maybe_ugr, maybe_vm) {
							(Some(ugr), Ok(vm)) => {
								let result = T::construct(ugr, vm);
								Some(result)
							},
							_ => None,
						}
					},
					Err(_) => {
						log::error!(
							"Trying to access AndroidOptionalNative from non-attached \
							 thread"
						);
						None
					},
				}
			},
		};
	}
}
