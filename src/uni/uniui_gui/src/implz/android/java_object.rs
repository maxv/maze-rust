use super::{
	jni_error::JniError,
	uni_global_ref::UniGlobalRef,
};


/// Represents the JNI object
///
/// We are using traits to allow emulation of inheritance
pub trait JavaObject {
	fn jobject(&self) -> jni::objects::JObject;
	fn jni_env(&self) -> Result<jni::JNIEnv, JniError>;
}

/// Represents type which can construct itself from global_ref and JavaVM
pub trait JavaObjectConstructorable: Sized {
	fn construct(
		global_ref: UniGlobalRef<jni::objects::JObject<'static>>,
		vm: jni::JavaVM,
	) -> Self;
}
