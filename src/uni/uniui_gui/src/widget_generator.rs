use crate::NativeWidget;


pub trait WidgetGenerator {
	fn create_element(
		&mut self,
		s: &str,
	) -> Result<NativeWidget, ()>;

	#[cfg(target_os = "android")]
	fn context(&self) -> &dyn crate::implz::android::android_context::Context;
}
