use crate::Application;

/// May be inserted into [crate::Application] to regularry process the data.
pub trait DataProcessor {
	/// Will be called by [crate::Application] time to time
	///
	/// * _msec_since_app_start -- milliseconds since application start
	/// * _app -- mutable reference to the application
	fn process_data(
		&mut self,
		_msec_since_app_start: i64,
		_app: &mut dyn Application,
	) {
	}
}
