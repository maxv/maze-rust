#include <QApplication>
#include <QLabel>
#include <QPushButton>
#include <QWidget>
#include <QTimer>
#include <QPointer>

static int argc = 0;
static char** argv = NULL;


extern "C" void* uniui_gui_application_create() {
	QApplication* app = new QApplication(argc, argv);
	return app;
}

typedef void (*callback)(void*); 

extern "C" void uniui_gui_application_start(void* app, callback cb, void* cb_data) {
	QTimer timer;
	timer.setInterval(16); // 16 msec
	QObject::connect(
		&timer, 
		&QTimer::timeout, 
		[=]() {
			cb(cb_data); 
		}
	);
	timer.start();
	((QApplication*)app)->exec();
}

extern "C" void uniui_gui_widget_show(void* widget) {
	((QWidget*)widget)->show(); 
}

extern "C" void* uniui_gui_qpointer_for_qwidget_create(void* widget) {
	QPointer<QWidget>* pointer = new QPointer<QWidget>((QWidget*)widget);
	return pointer;
}

extern "C" bool uniui_gui_qpointer_is_alive(void* qpointer_to_qwidget) {
	QPointer<QWidget>* p = (QPointer<QWidget>*) qpointer_to_qwidget;
	return !p->isNull();
}

extern "C" void uniui_gui_qpointer_drop(void* qpointer_to_qwidget) {
	QPointer<QWidget>* p = (QPointer<QWidget>*) qpointer_to_qwidget;
	if (!p->isNull()) {
		(*p)->deleteLater();
	}
}

extern "C" void* uniui_gui_qpointer_get_widget(void* pointer) {
	QPointer<QWidget>* p = (QPointer<QWidget>*) pointer;
	return *p;
}

