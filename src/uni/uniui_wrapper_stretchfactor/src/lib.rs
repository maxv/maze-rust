use uniui_gui::prelude::*;

#[cfg(target_arch = "wasm32")]
use uniui_gui::utils::SilentCssPropertySetter;


pub struct StretchFactor<T: Widget> {
	pub factor: u32,
	pub widget: T,
}

impl<W: 'static + Widget> Widget for StretchFactor<W> {
	#[cfg(target_os = "linux")]
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		log::error!("StretchFactor is not supported for Qt5");
		return self.widget.to_native(widget_generator);
	}

	#[cfg(target_arch = "wasm32")]
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let inner_native = self.widget.to_native(widget_generator)?;

		inner_native.style().set_property_silent("flex-grow", &self.factor.to_string());

		return Ok(inner_native);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<W: 'static + Widget> DataProcessor for StretchFactor<W> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
