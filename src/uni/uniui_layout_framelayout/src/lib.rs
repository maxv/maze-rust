#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_gui;

mod frame_layout;
pub use self::frame_layout::FrameLayout;


#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_frame_layout;
	pub use self::wasm_frame_layout::WasmFrameLayout as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_frame_layout;
	pub use self::qt5_frame_layout::Qt5FrameLayout as Platform;
}
