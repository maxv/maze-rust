use crate::implz::Platform;

use uniui_gui::prelude::*;

pub struct FrameLayout {
	childs: Vec<Box<dyn Widget>>,
	platform: Platform,
}

impl FrameLayout {
	pub fn new() -> FrameLayout {
		return FrameLayout {
			childs: Vec::new(),
			platform: Platform::new(),
		};
	}

	pub fn push_widget<W: 'static + Widget>(
		&mut self,
		w: W,
	) {
		self.childs.push(Box::new(w));
	}
}

impl Widget for FrameLayout {
	fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(document, &mut self.childs);
	}

	fn draw(&mut self) {
		self.childs.iter_mut().for_each(|child| child.as_mut().draw());
	}
}

impl DataProcessor for FrameLayout {
	fn process_data(
		&mut self,
		msec_since_app_epoch: i64,
		app: &mut dyn Application,
	) {
		self.childs
			.iter_mut()
			.for_each(|child| child.as_mut().process_data(msec_since_app_epoch, app));
	}
}
