use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::HtmlDivElement;

use crate::implz::wasm_bindgen::JsCast;

pub struct WasmFrameLayout {
	native: OptionalNative<HtmlDivElement>,
}

impl WasmFrameLayout {
	pub fn new() -> WasmFrameLayout {
		return WasmFrameLayout {
			native: OptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
		childs: &mut Vec<Box<dyn Widget>>,
	) -> Result<NativeWidget, ()> {
		let node = document.create_element("div").unwrap();
		let div = node.dyn_into::<HtmlDivElement>().unwrap();

		childs.iter_mut().map(|c| c.to_native(document)).enumerate().for_each(
			|(id, e)| {
				match e {
					Ok(e) => {
						e.style().set_property_silent("position", "absolute");

						match div.append_child(&e) {
							Ok(_) => trace!("layout ok"),
							Err(e) => error!("layout inssert err:{:?}", e),
						}
					},
					Err(e) => error!("to_native:child({}):error:{:?}", id, e),
				}
			},
		);

		self.native.replace(div.clone());
		Ok(From::from(div))
	}
}
