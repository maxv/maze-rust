use uniui_gui::prelude::*;

pub struct Qt5FrameLayout {}

impl Qt5FrameLayout {
	pub fn new() -> Qt5FrameLayout {
		return Qt5FrameLayout {};
	}

	pub fn to_native(
		&mut self,
		_document: &mut dyn WidgetGenerator,
		_childs: &mut Vec<Box<dyn Widget>>,
	) -> Result<NativeWidget, ()> {
		error!("not implemented");
		return Err(());
	}
}
