pub use uniui_gui::Application;

pub mod canvas {
	// pub use uniui_widget_canvas::*;
}

pub mod core {
	pub use uniui_core::*;
}

pub mod layouts {
	pub mod prelude {
		pub use super::linearlayout::prelude::*;
	}

	pub mod linearlayout {
		pub use uniui_layout_linear_layout::*;
	}

	pub mod recycler_layout {
		// pub use uniui_layout_recyclerlayout::*;
	}
}

pub mod utils {
	pub use uniui_gui::utils::*;
}

pub mod widgets {
	pub mod button {
		pub use uniui_widget_button::*;
	}

	pub mod checkbox {
		// pub use uniui_widget_checkbox::*;
	}

	pub mod label {
		pub use uniui_widget_label::*;
	}

	pub mod textedit {
		pub use uniui_widget_textedit::*;
	}

	pub mod prelude {
		pub use super::{
			button::prelude::*,
			label::prelude::*,
			textedit::prelude::*,
		};
	}

	// pub use uniui_widget_color_picker::ColorPicker;
	// pub use uniui_widget_combobox::ComboBox;
	// pub use uniui_widget_fileloader::FileLoader;
	// pub use uniui_widget_fileselector::FileSelector;
	// pub use uniui_widget_groupbox::GroupBox;

	// pub use uniui_widget_radiobutton::RadioButton;
	// pub use uniui_widget_slider::Slider;
	// pub use uniui_widget_tabwidget::TabWidget;
}

pub mod gamepad {
	// pub use uniui_gamepad::*;
}

pub mod wrappers {
	pub mod minimalsize {
		pub use uniui_wrapper_minimalsize::*;
	}

	pub mod prelude {
		pub use super::minimalsize::prelude::*;
	}

	// pub use uniui_wrapper_fixedsize::*;
	// pub use uniui_wrapper_focusable::Focusable;
	// pub use uniui_wrapper_keyboardlistener::KeyboardListener;
	// pub use uniui_wrapper_mouseclicklistener::MouseClickListener;
	// pub use uniui_wrapper_stretchfactor::StretchFactor;
	// pub use uniui_wrapper_touchlistener::TouchListener;

	// pub use uniui_wrapper_visibility::{
	// 	Visibility,
	// 	VisibilityValue,
	// };
}

pub mod touch {
	// pub use uniui_wrapper_touchlistener::*;
}

pub mod graphic_scene {
	// pub use uniui_scene::*;
}

pub use uniui_gui::*;

pub mod prelude {
	pub use crate::{
		layouts::prelude::*,
		widgets::prelude::*,
		wrappers::prelude::*,
	};
	pub use uniui_gui::prelude::*;
}

pub mod files {
	// pub use uniui_file::File;
}

pub mod signal_mapper {
	// pub use uniui_signalmapper::*;
}
