#[doc(hidden)]
#[macro_export]
macro_rules! u_visibility_int {
	($b:ident,slot_set_visible: $va:expr) => {
		$va.connect_slot($b.slot_set_visible());
	};
	($b:ident,slot_set_visible_proxy: $va:expr) => {
		$va = $b.slot_set_visible().proxy();
	};
}

/// Simplifies [Visibility](crate::Visibility) creation
///
/// Parameters:
/// * widget (required): Widget,
/// * initially (required): [VisibilityValue](crate::VisibilityValue)
/// * slot_set_visible (optional): [Signal\<VisibilityValue\>](uniui_core::Signal), will
///   be connected to [Visibility::slot_set_visible],
/// * slot_set_visible_proxy (optional): [Visibility::slot_set_visible]'s proxy will be
///   assigned to provided identifier,
///
/// Example:
/// ```
/// TBD
/// ```
#[macro_export]
macro_rules! u_visibility {
	(widget: $w:expr, initially: $vis:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut v = $crate::Visibility::new($w, $vis);
		$($crate::u_visibility_int!(v, $param_name: $val);)*;
		v
	}};
}
