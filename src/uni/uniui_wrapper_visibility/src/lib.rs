use uniui_gui::prelude::*;

mod macros;

pub mod prelude {
	pub use crate::{
		u_visibility,
		Visibility,
		VisibilityValue,
	};
}

#[cfg(target_arch = "wasm32")]
use uniui_gui::utils::SilentCssPropertySetter;

#[cfg(target_arch = "wasm32")]
use web_sys::CssStyleDeclaration;

#[derive(Clone, Debug, Copy)]
pub enum VisibilityValue {
	Visible,
	Invisible,
	Collapse,
}


pub struct Visibility<T: 'static + Widget> {
	widget: T,
	visible: Property<VisibilityValue>,

	#[cfg(target_arch = "wasm32")]
	previous_display: Option<String>,

	#[cfg(target_arch = "wasm32")]
	style: Option<CssStyleDeclaration>,
}

impl<T: 'static + Widget> Visibility<T> {
	#[cfg(target_os = "linux")]
	pub fn new(
		widget: T,
		val: VisibilityValue,
	) -> Visibility<T> {
		return Visibility {
			widget,
			visible: Property::new(val),
		};
	}

	#[cfg(target_arch = "wasm32")]
	pub fn new(
		widget: T,
		val: VisibilityValue,
	) -> Visibility<T> {
		return Visibility {
			widget,
			visible: Property::new(val),
			previous_display: None,
			style: None,
		};
	}

	pub fn slot_set_visible(&self) -> &dyn Slot<VisibilityValue> {
		return self.visible.slot();
	}

	#[cfg(target_os = "linux")]
	fn update_style(&self) {
	}

	#[cfg(target_arch = "wasm32")]
	fn update_style(&mut self) {
		match self.style.as_ref() {
			Some(style) => {
				if let Some(v) = self.previous_display.take() {
					style.set_property_silent("display", &v);
				}

				match self.visible.as_ref() {
					VisibilityValue::Invisible => {
						style.set_property_silent("visibility", "hidden")
					},
					VisibilityValue::Visible => {
						style.set_property_silent("visibility", "inherit")
					},
					VisibilityValue::Collapse => {
						self.previous_display = style.get_property_value("display").ok();
						style.set_property_silent("display", "none");
					},
				}
			},
			None => {},
		}
	}
}

impl<T: 'static + Widget> Widget for Visibility<T> {
	#[cfg(target_os = "linux")]
	fn to_native(
		&mut self,
		_native_parent: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		Err(())
	}

	#[cfg(target_arch = "wasm32")]
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;

		self.style = Some(e.style());
		self.update_style();

		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for Visibility<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);

		if self.visible.try_update() {
			self.update_style();
		}
	}
}
