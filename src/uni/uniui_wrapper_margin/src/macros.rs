#[doc(hidden)]
#[macro_export]
macro_rules! u_margin_int {
	($b:ident,margin: $va:expr) => {
		$b.top = Some($va);
		$b.right = Some($va);
		$b.bottom = Some($va);
		$b.left = Some($va);
	};
	($b:ident,top: $va:expr) => {
		$b.top = Some($va);
	};
	($b:ident,right: $va:expr) => {
		$b.right = Some($va);
	};
	($b:ident,bottom: $va:expr) => {
		$b.bottom = Some($va);
	};
	($b:ident,left: $va:expr) => {
		$b.left = Some($va);
	};
}

/// Simplifies [Margin](crate::Margin) creation
///
/// Parameters:
/// * widget (required): widget to wrap,
/// * margin (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for
///   top/left/bottom/right margins,
/// * top (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for top margins,
/// * bottom (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for bottom margins,
/// * left (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for left margins,
/// * right (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for right margins.
///
/// Example:
/// ```
/// let widget_with_margins = u_margin! {
///     widget: widget_without_margins,
///     top: SizeUnit::PT(10.0),
///     bottom: SizeUnit::PT(20.0),
/// };
/// ```
#[macro_export]
macro_rules! u_margin {
	(widget: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut margin = $crate::Margin{
			widget: $w,
			top: None,
			right: None,
			bottom: None,
			left: None,
		};
		$($crate::u_margin_int!(margin, $param_name: $val);)*;
		margin
	}};
}
