mod margin;
pub use self::margin::Margin;


#[cfg(target_arch = "wasm32")]
mod implz {
	mod wasm_margin;
	pub use self::wasm_margin::WasmMargin as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_margin;
	pub use self::qt5_margin::Qt5Margin as Platform;
}

mod macros;

/// Contains [Margin], [u_margin!] and
/// [SizeUnit](uniui_gui::SizeUnit).
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_margin,
		Margin,
	};

	#[doc(hidden)]
	pub use uniui_gui::SizeUnit;
}
