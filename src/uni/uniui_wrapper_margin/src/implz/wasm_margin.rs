use uniui_gui::{
	NativeWidget,
	SizeUnit,
};

pub struct WasmMargin {}

impl WasmMargin {
	pub fn apply_to(
		e: &NativeWidget,
		top: &Option<SizeUnit>,
		right: &Option<SizeUnit>,
		bottom: &Option<SizeUnit>,
		left: &Option<SizeUnit>,
	) {
		let s = e.style();
		if let Some(val) = top.as_ref() {
			val.apply_as_property_value(&s, "margin-top");
		}

		if let Some(val) = right.as_ref() {
			val.apply_as_property_value(&s, "margin-right");
		}

		if let Some(val) = bottom.as_ref() {
			val.apply_as_property_value(&s, "margin-bottom");
		}

		if let Some(val) = left.as_ref() {
			val.apply_as_property_value(&s, "margin-left");
		}
	}
}
