use uniui_gui::{
	NativeWidget,
	SizeUnit,
};

pub struct Qt5Margin {}

impl Qt5Margin {
	pub fn apply_to(
		_e: &NativeWidget,
		_top: &Option<SizeUnit>,
		_right: &Option<SizeUnit>,
		_bottom: &Option<SizeUnit>,
		_left: &Option<SizeUnit>,
	) {
		log::error!("Margin not implemented for Qt5. Yet.");
	}
}
