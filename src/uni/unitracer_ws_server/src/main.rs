extern crate unitracer;
extern crate unitracer_jaeger;
extern crate unitracer_proto;

extern crate uniws;

use unitracer_proto::{
	Protocol,
	ProtocolV1,
};

use uniws::{
	Error,
	Format,
	UniWs,
	UniWsTrait,
};

use std::sync::{
	Arc,
	RwLock,
};

pub fn main() {
	let list = Arc::new(RwLock::new(Vec::<UniWs<Protocol>>::new()));
	let main_list = list.clone();
	UniWs::<Protocol>::listen(
		"127.0.0.1:8090".to_string(),
		Format::Binary,
		move |uniws, _| {
			return match list.write() {
				Ok(mut g) => {
					g.push(uniws);
					true
				},
				Err(_) => false,
			};
		},
	);

	let sleep_between_checks = std::time::Duration::from_millis(100);
	let jaeger_host = "127.0.0.1";
	let jaeger_port = "14268";
	loop {
		match main_list.write() {
			Ok(mut l) => {
				let v = l.split_off(0);
				for mut uniws in v {
					let keep = match uniws.try_recv() {
						Ok(Protocol::V1(ProtocolV1::Batch(batch))) => {
							unitracer_jaeger::send_batch(batch, jaeger_host, jaeger_port);
							true
						},
						Err(Error::Closed) => false,
						_ => true,
					};

					if keep {
						l.push(uniws);
					}
				}
			},
			Err(_) => break,
		}

		std::thread::sleep(sleep_between_checks);
	}

	println!("finished");
}
