use crate::implz::Platform;

use uniui_gui::{
	prelude::*,
	Color,
};

use std::sync::mpsc::{
	self,
	Receiver,
};

pub struct ColorPicker {
	selected_color: Color,
	receiver: Receiver<String>,
	signal_color_selected: Signal<Color>,
	platform: Platform,
}

impl ColorPicker {
	pub fn new(selected_color: Color) -> ColorPicker {
		let (sender, receiver) = mpsc::channel();
		return ColorPicker {
			selected_color,
			receiver,
			signal_color_selected: Signal::new(),
			platform: Platform::new(sender),
		};
	}

	pub fn selected_color(&self) -> Color {
		return self.selected_color;
	}

	pub fn signal_color_selected(&mut self) -> &mut Signal<Color> {
		return &mut self.signal_color_selected;
	}
}

impl Widget for ColorPicker {
	fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(document);
	}
}

impl DataProcessor for ColorPicker {
	fn process_data(
		&mut self,
		_msec_since_app_start: i64,
		_app: &mut dyn Application,
	) {
		match self.receiver.try_iter().last() {
			Some(value) => {
				match value.len() {
					7 => {
						let red = u8::from_str_radix(&value[1..=2], 16);
						let green = u8::from_str_radix(&value[3..=4], 16);
						let blue = u8::from_str_radix(&value[5..=6], 16);

						match (red, green, blue) {
							(Ok(red), Ok(green), Ok(blue)) => {
								self.selected_color = Color {
									red,
									green,
									blue,
								};
								self.signal_color_selected.emit(self.selected_color);
							},
							_ => {},
						}
					},
					_ => {},
				};
			},
			None => {},
		}
	}
}
