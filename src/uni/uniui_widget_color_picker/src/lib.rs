#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_gui;

mod color_picker;
pub use self::color_picker::ColorPicker;


#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_color_picker;
	pub use self::wasm_color_picker::WasmColorPicker as Platform;
}

pub const THEME_CLASS_NAME: &str = "u_colorpicker_theme_class";

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_color_picker;
	pub use self::qt5_color_picker::Qt5ColorPicker as Platform;
}
