use uniui_gui::prelude::*;

use std::sync::mpsc::Sender;

pub struct Qt5ColorPicker {}

impl Qt5ColorPicker {
	pub fn new(_sender: Sender<String>) -> Qt5ColorPicker {
		return Qt5ColorPicker {};
	}

	pub fn to_native(
		&mut self,
		_document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		error!("not implemented");
		return Err(());
	}
}
