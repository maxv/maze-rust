use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::HtmlInputElement;

use crate::implz::wasm_bindgen::closure::Closure;

use crate::implz::wasm_bindgen::JsCast;

use std::sync::mpsc::Sender;

pub struct WasmColorPicker {
	native: OptionalNative<HtmlInputElement>,
	closure: Option<Closure<dyn FnMut()>>,
	sender: Sender<String>,
}

impl WasmColorPicker {
	pub fn new(sender: Sender<String>) -> WasmColorPicker {
		return WasmColorPicker {
			native: OptionalNative::new(),
			closure: None,
			sender,
		};
	}

	pub fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let node = document.create_element("input").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let picker = node.dyn_into::<HtmlInputElement>().unwrap();
		picker.set_type("color");

		let sender = self.sender.clone();
		let closure_picker = picker.clone();
		let closure = Closure::wrap(Box::new(move || {
			match sender.send(closure_picker.value()) {
				Ok(()) => trace!("color sent"),
				Err(e) => error!("color send:err:{:?}", e),
			}
		}) as Box<dyn FnMut()>);

		picker.set_onchange(Some(closure.as_ref().unchecked_ref()));
		self.closure = Some(closure);

		self.native.replace(picker.clone());
		Ok(From::from(picker))
	}
}
