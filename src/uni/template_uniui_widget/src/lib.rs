extern crate uniui_core;
extern crate uniui_gui;

mod xxx;
pub use self::xxx::Xxx;

pub const THEME_CLASS_NAME: &str = "u_xxx_theme_class";

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_xxx;
	pub use self::wasm_xxx::WasmXxx as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_xxx;
	pub use self::qt5_xxx::Qt5Xxx as Platform;
}

#[cfg(target_os = "android")]
mod implz {
	mod android_xxx;
	pub use self::android_xxx::AndroidXxx as Platform;
}

