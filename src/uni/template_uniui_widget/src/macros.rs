#[doc(hidden)]
#[macro_export]
macro_rules! u_xxx_int {
	($b:ident,signal_xxx: $va:expr) => {
		$b.signal_clicked().connect_slot($va);
	};
	($b:ident,slot_set_xxx: $va:expr) => {
		$va.connect_slot($b.slot_set_xxx());
	};
	($b:ident,slot_set_xxx_proxy: $va:expr) => {
		$va = $b.slot_set_xxx().proxy();
	};
}

/// Simplifies [Xxx](crate::Xxx) creation
///
/// Parameters:
/// * xx_x (required): String,
/// * signal_xxx (optional): [&Slot\<()\>](uniui_core::Slot), will be connected to
///   [Xxx::signal_xxx],
/// * slot_set_xxx (optional): provided [Signal\<???\>](uniui_core::Signal) will be
///   connected to [Xxx::slot_set_xxx],
/// * slot_set_xxx_proxy (optional): [Xxx::slot_set_xxx]'s proxy will be assigned
///   to provided identifier,
///
/// Example:
/// ```
/// let slot_xxx = SlotImpl::new();
///
/// let x_xx = u_xxx! {
///     xx_x: "OK".to_owned(),
///     signal_xxx: &slot_xxx,
/// };
/// ```
#[macro_export]
macro_rules! u_xxx {
	(xx_x: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut r = $crate::Xxx::new($w);
		$($crate::u_button_int!(r, $param_name: $val);)*;
		r
	}};
}
