pub use crate::implz::shader::*;

#[derive(Copy, Clone, Debug)]
pub enum BindBufferTarget {
	ArrayBuffer,
	ElementArrayBuffer,
}

#[derive(Copy, Clone, Debug)]
pub enum BufferDataUsage {
	StaticDraw,
	DynamicDraw,
	StreamDraw,
}

#[derive(Copy, Clone, Debug)]
pub enum UGLType {
	Byte,
	Short,
	UnsignedByte,
	UnsignedShort,
	Float,
}

#[derive(Copy, Clone, Debug)]
pub enum DrawMode {
	Points,
	LineStrip,
	LineLoop,
	Lines,
	TriangleStrip,
	TriangleFan,
	Triangles,
}

#[derive(Copy, Clone, Debug)]
pub enum DepthFunc {
	Never,
	Less,
	Equal,
	LessOrEqual,
	Greater,
	NotEqual,
	GreateOrEqual,
	Always,
}
