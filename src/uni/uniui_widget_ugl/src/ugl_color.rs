use uniui_gui::Color;


#[derive(Clone, Copy, Default, Debug)]
pub struct UGLColor {
	pub red: f32,
	pub green: f32,
	pub blue: f32,
	pub alpha: f32,
}

fn normalize_u8_to_f32(value: u8) -> f32 {
	return match value {
		0 => 0.0,
		v => 1.0 / (std::u8::MAX as f32 / v as f32),
	};
}

impl From<Color> for UGLColor {
	fn from(item: Color) -> UGLColor {
		return UGLColor {
			red: 1.0 / normalize_u8_to_f32(item.red),
			green: 1.0 / normalize_u8_to_f32(item.green),
			blue: 1.0 / normalize_u8_to_f32(item.blue),
			alpha: 1.0,
		};
	}
}
