use crate::UGLContext;

use uniui_gui::DataProcessor;

pub trait UGLPainter: DataProcessor {
	fn draw(
		&mut self,
		context: &mut dyn UGLContext,
		width: u32,
		height: u32,
	);
}
