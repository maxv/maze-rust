use crate::UGLColor;

use crate::shader::*;

pub trait UGLContext {
	fn clear_color(
		&self,
		color: &UGLColor,
	);

	fn get_shader_compile_status(
		&self,
		shader: &UGLVertexShader,
	) -> Result<(), ()>;

	fn compile_shader(
		&self,
		shader: &UGLVertexShader,
	);

	fn shader_add_sources(
		&self,
		shader: &UGLVertexShader,
		source: &str,
	);

	fn create_vertex_shader(&self) -> Option<UGLVertexShader>;

	fn create_fragment_shader(&self) -> Option<UGLFragmentShader>;

	fn create_program(&self) -> Option<UGLProgram>;

	fn attach_fragment_shader(
		&self,
		program: &UGLProgram,
		shader: &UGLFragmentShader,
	);

	fn attach_vertex_shader(
		&self,
		program: &UGLProgram,
		shader: &UGLVertexShader,
	);

	fn link_program(
		&self,
		program: &UGLProgram,
	);

	fn get_program_link_status(
		&self,
		program: &UGLProgram,
	) -> Result<(), ()>;

	fn get_attrib_location(
		&self,
		program: &UGLProgram,
		name: &str,
	) -> Option<UGLAttribLocation>;

	fn get_uniform_location(
		&self,
		program: &UGLProgram,
		name: &str,
	) -> Option<UGLUniformLocation>;

	fn create_buffer(&self) -> Option<UGLBuffer>;

	fn bind_buffer(
		&self,
		target: BindBufferTarget,
		buffer: Option<&UGLBuffer>,
	);

	fn buffer_data_f32(
		&self,
		target: BindBufferTarget,
		data: &[f32],
		usage: BufferDataUsage,
	);

	fn vertex_attrib_pointer(
		&self,
		location: &UGLAttribLocation,
		size: i32,
		gl_type: UGLType,
		normalize: bool,
		stride: i32,
		offset: i32,
	);

	fn enable_vertex_attrib_array(
		&self,
		location: &UGLAttribLocation,
	);

	fn use_program(
		&self,
		program: &UGLProgram,
	);

	fn draw_arrays(
		&self,
		mode: DrawMode,
		first: i32,
		count: i32,
	);

	fn depth_func(
		&self,
		func: DepthFunc,
	);

	fn get_error(&self) -> u32;

	/// transpose - missed since for WebGL transpose have to be false all the
	/// time, count - missed since WebGL doesn't support it at all
	fn uniform_matrix4fv(
		&self,
		uniform_location: &UGLUniformLocation,
		data: &[f32; 16],
	);
}
