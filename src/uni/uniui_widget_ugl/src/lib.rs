mod ugl;
pub use self::ugl::UGL;

mod ugl_context;
pub use self::ugl_context::UGLContext;

mod ugl_color;
pub use self::ugl_color::UGLColor;

mod ugl_painter;
pub use self::ugl_painter::UGLPainter;

pub mod shader;

#[cfg(target_arch = "wasm32")]
mod implz {
	pub extern crate js_sys;
	pub extern crate wasm_bindgen;
	pub extern crate web_sys;

	mod wasm;
	pub use self::wasm::{
		wasm_shader as shader,
		WasmUGL as Platform,
	};
}

#[cfg(target_os = "linux")]
mod implz {
	uniui_gui::desktop_qt! {
		mod qt5;
		pub use self::qt5::{
			qt5_shader as shader,
			Qt5UGL as Platform,
		};
	}

	uniui_gui::desktop_noop! {
		mod noop;
		pub use self::noop::{
			noop_shader as shader,
			NoopUGL as Platform,
		};
	}
}
