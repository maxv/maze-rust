use crate::UGLPainter;

use crate::implz::Platform;

use uniui_gui::prelude::*;


pub struct UGL<P: UGLPainter> {
	platform: Platform,
	width: u32,
	height: u32,

	painter: Option<P>,
}

impl<P: UGLPainter> UGL<P> {
	pub fn new() -> UGL<P> {
		return UGL {
			platform: Platform::new(),
			width: 0,
			height: 0,
			painter: None,
		};
	}

	pub fn set_painter(
		&mut self,
		painter: Option<P>,
	) {
		self.painter = painter;
	}

	fn resize_if_needed(&mut self) {
		match self.platform.actual_size() {
			Some((width, height)) => {
				if self.width != width || self.height != height {
					log::warn!("resize:(w,h):({},{})", width, height);
					self.width = width;
					self.height = height;
					self.platform.resize_if_needed(self.width, self.height);
				}
			},
			None => {},
		}
	}
}

impl<P: UGLPainter> Widget for UGL<P> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(widget_generator);
	}

	fn draw(&mut self) {
		if let Some(context) = self.platform.get_context() {
			if let Some(painter) = self.painter.as_mut() {
				painter.draw(context, self.width, self.height);
			}
			self.platform.rendering_done();
		}
	}
}

impl<P: UGLPainter> DataProcessor for UGL<P> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.resize_if_needed();
		if let Some(p) = self.painter.as_mut() {
			p.process_data(msec_since_app_start, app);
		}
	}
}
