use super::native;

use crate::{
	shader::*,
	UGLColor,
	UGLContext,
};

use std::{
	convert::TryInto,
	ffi::{
		c_void,
		CString,
	},
};

pub struct Qt5Context {
	qt_functions: *mut c_void,
}

impl Qt5Context {
	pub unsafe fn new(qt_functions: *mut c_void) -> Qt5Context {
		return Qt5Context {
			qt_functions,
		};
	}

	unsafe fn convert_target(target: BindBufferTarget) -> u32 {
		return match target {
			BindBufferTarget::ArrayBuffer => native::UGL_ARRAY_BUFFER,
			BindBufferTarget::ElementArrayBuffer => native::UGL_ELEMENT_ARRAY_BUFFER,
		};
	}

	unsafe fn convert_usage(usage: BufferDataUsage) -> u32 {
		return match usage {
			BufferDataUsage::StaticDraw => native::UGL_STATIC_DRAW,
			BufferDataUsage::DynamicDraw => native::UGL_DYNAMIC_DRAW,
			BufferDataUsage::StreamDraw => native::UGL_STREAM_DRAW,
		};
	}

	unsafe fn convert_type(gl_type: UGLType) -> u32 {
		return match gl_type {
			UGLType::Byte => native::UGL_BYTE,
			UGLType::Short => native::UGL_SHORT,
			UGLType::UnsignedByte => native::UGL_UNSIGNED_BYTE,
			UGLType::UnsignedShort => native::UGL_UNSIGNED_SHORT,
			UGLType::Float => native::UGL_FLOAT,
		};
	}

	unsafe fn convert_draw_mode(mode: DrawMode) -> u32 {
		return match mode {
			DrawMode::Points => native::UGL_POINTS,
			DrawMode::LineStrip => native::UGL_LINE_STRIP,
			DrawMode::LineLoop => native::UGL_LINE_LOOP,
			DrawMode::Lines => native::UGL_LINES,
			DrawMode::TriangleStrip => native::UGL_TRIANGLE_STRIP,
			DrawMode::TriangleFan => native::UGL_TRIANGLE_FAN,
			DrawMode::Triangles => native::UGL_TRIANGLES,
		};
	}

	unsafe fn convert_depth_func(func: DepthFunc) -> u32 {
		return match func {
			DepthFunc::Never => native::UGL_NEVER,
			DepthFunc::Less => native::UGL_LESS,
			DepthFunc::Equal => native::UGL_EQUAL,
			DepthFunc::LessOrEqual => native::UGL_LEQUAL,
			DepthFunc::Greater => native::UGL_GREATER,
			DepthFunc::NotEqual => native::UGL_NOTEQUAL,
			DepthFunc::GreateOrEqual => native::UGL_GEQUAL,
			DepthFunc::Always => native::UGL_ALWAYS,
		};
	}
}

impl UGLContext for Qt5Context {
	fn clear_color(
		&self,
		color: &UGLColor,
	) {
		unsafe {
			native::uniui_widget_ugl_clear_color(
				self.qt_functions,
				color.red,
				color.green,
				color.blue,
				color.alpha,
			);
		};
	}

	fn get_shader_compile_status(
		&self,
		shader: &UGLVertexShader,
	) -> Result<(), ()> {
		let status = unsafe {
			native::uniui_widget_ugl_functions_get_shader_compile_status(
				self.qt_functions,
				*shader,
			)
		};
		return match status {
			true => Ok(()),
			false => Err(()),
		};
	}

	fn compile_shader(
		&self,
		shader: &UGLVertexShader,
	) {
		unsafe {
			native::uniui_widget_ugl_functions_compile_shader(self.qt_functions, *shader);
		};
	}

	fn shader_add_sources(
		&self,
		shader: &UGLVertexShader,
		source: &str,
	) {
		unsafe {
			let cstring_text = CString::new(source).unwrap_or(CString::default());
			let raw_char = cstring_text.into_raw();

			native::uniui_widget_ugl_functions_shader_add_source(
				self.qt_functions,
				*shader,
				raw_char,
			);

			CString::from_raw(raw_char);
		};
	}

	fn create_vertex_shader(&self) -> Option<UGLVertexShader> {
		let shader = unsafe {
			native::uniui_widget_ugl_functions_create_vertex_shader(self.qt_functions)
		};
		return Some(shader);
	}

	fn create_fragment_shader(&self) -> Option<UGLFragmentShader> {
		let shader = unsafe {
			native::uniui_widget_ugl_functions_create_fragment_shader(self.qt_functions)
		};
		return Some(shader);
	}

	fn create_program(&self) -> Option<UGLProgram> {
		let program = unsafe {
			native::uniui_widget_ugl_functions_create_program(self.qt_functions)
		};
		return Some(program);
	}

	fn attach_fragment_shader(
		&self,
		program: &UGLProgram,
		shader: &UGLFragmentShader,
	) {
		unsafe {
			native::uniui_widget_ugl_functions_attach_shader(
				self.qt_functions,
				*program,
				*shader,
			)
		};
	}

	fn attach_vertex_shader(
		&self,
		program: &UGLProgram,
		shader: &UGLVertexShader,
	) {
		unsafe {
			native::uniui_widget_ugl_functions_attach_shader(
				self.qt_functions,
				*program,
				*shader,
			)
		};
	}

	fn link_program(
		&self,
		program: &UGLProgram,
	) {
		unsafe {
			native::uniui_widget_ugl_functions_link_program(self.qt_functions, *program)
		};
	}

	fn get_program_link_status(
		&self,
		program: &UGLProgram,
	) -> Result<(), ()> {
		let status = unsafe {
			native::uniui_widget_ugl_functions_get_program_link_status(
				self.qt_functions,
				*program,
			)
		};
		return match status {
			true => Ok(()),
			false => Err(()),
		};
	}

	fn get_attrib_location(
		&self,
		program: &UGLProgram,
		name: &str,
	) -> Option<UGLAttribLocation> {
		let location;
		unsafe {
			let cstring_text = CString::new(name).unwrap_or(CString::default());
			let raw_char = cstring_text.into_raw();

			location = native::uniui_widget_ugl_functions_get_attrib_location(
				self.qt_functions,
				*program,
				raw_char,
			);

			CString::from_raw(raw_char);
		};

		return location.try_into().ok();
	}

	fn get_uniform_location(
		&self,
		program: &UGLProgram,
		name: &str,
	) -> Option<UGLUniformLocation> {
		let location;
		unsafe {
			let cstring_text = CString::new(name).unwrap_or(CString::default());
			let raw_char = cstring_text.into_raw();

			location = native::uniui_widget_ugl_functions_get_uniform_location(
				self.qt_functions,
				*program,
				raw_char,
			);

			CString::from_raw(raw_char);
		};

		return location.try_into().ok();
	}

	fn create_buffer(&self) -> Option<UGLBuffer> {
		let buffer = unsafe {
			native::uniui_widget_ugl_functions_create_buffer(self.qt_functions)
		};
		return Some(buffer);
	}

	fn bind_buffer(
		&self,
		target: BindBufferTarget,
		buffer: Option<&UGLBuffer>,
	) {
		unsafe {
			let target = Qt5Context::convert_target(target);
			let buffer = buffer.map(|b| *b).unwrap_or(0);
			native::uniui_widget_ugl_functions_bind_buffer(
				self.qt_functions,
				target,
				buffer,
			);
		};
	}

	fn buffer_data_f32(
		&self,
		target: BindBufferTarget,
		data: &[f32],
		usage: BufferDataUsage,
	) {
		unsafe {
			let target = Qt5Context::convert_target(target);
			let usage = Qt5Context::convert_usage(usage);
			native::uniui_widget_ugl_functions_buffer_data_f32(
				self.qt_functions,
				target,
				data.len() as u32,
				data.as_ptr(),
				usage,
			);
		};
	}

	fn vertex_attrib_pointer(
		&self,
		location: &UGLAttribLocation,
		size: i32,
		gl_type: UGLType,
		normalize: bool,
		stride: i32,
		offset: i32,
	) {
		unsafe {
			let gl_type = Qt5Context::convert_type(gl_type);
			native::uniui_widget_ugl_functions_vertex_attrib_pointer(
				self.qt_functions,
				*location,
				size,
				gl_type,
				normalize,
				stride,
				offset,
			)
		};
	}

	fn enable_vertex_attrib_array(
		&self,
		location: &UGLAttribLocation,
	) {
		unsafe {
			native::uniui_widget_ugl_functions_enable_vertex_attrib_array(
				self.qt_functions,
				*location,
			)
		};
	}

	fn use_program(
		&self,
		program: &UGLProgram,
	) {
		unsafe {
			native::uniui_widget_ugl_functions_use_program(self.qt_functions, *program)
		};
	}

	fn draw_arrays(
		&self,
		mode: DrawMode,
		first: i32,
		count: i32,
	) {
		unsafe {
			let mode = Qt5Context::convert_draw_mode(mode);
			native::uniui_widget_ugl_functions_draw_arrays(
				self.qt_functions,
				mode,
				first,
				count,
			)
		};
	}

	fn depth_func(
		&self,
		func: DepthFunc,
	) {
		unsafe {
			let func = Qt5Context::convert_depth_func(func);
			native::uniui_widget_ugl_functions_depth_func(self.qt_functions, func)
		};
	}

	fn get_error(&self) -> u32 {
		return unsafe {
			native::uniui_widget_ugl_functions_get_error(self.qt_functions)
		};
	}

	fn uniform_matrix4fv(
		&self,
		uniform_location: &UGLUniformLocation,
		data: &[f32; 16],
	) {
		unsafe {
			native::uniui_widget_ugl_functions_uniform_matrix4fv(
				self.qt_functions,
				*uniform_location,
				data.as_ptr(),
			)
		};
	}
}
