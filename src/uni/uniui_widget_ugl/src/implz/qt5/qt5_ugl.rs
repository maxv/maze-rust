use super::{
	native,
	qt5_context::Qt5Context,
};

use crate::UGLContext;

use uniui_gui::{
	implz::qt5::Qt5OptionalNative,
	NativeWidget,
	WidgetGenerator,
};

pub struct Qt5UGL {
	native: Qt5OptionalNative,
	context: Option<Qt5Context>,
}


impl Qt5UGL {
	pub fn new() -> Qt5UGL {
		return Qt5UGL {
			native: Qt5OptionalNative::new(),
			context: None,
		};
	}

	pub fn rendering_done(&mut self) {
		if let Some(w) = self.native.as_mut() {
			unsafe {
				native::uniui_widget_ugl_update(w);
			}
		}

		self.context = None;
	}

	pub fn get_context(&mut self) -> Option<&mut dyn UGLContext> {
		match self.context.is_some() {
			true => log::warn!("context was not freed"),
			false => {
				if let Some(ugl) = self.native.as_mut() {
					unsafe {
						let context = native::uniui_widget_ugl_get_functions(ugl);
						self.context = Some(Qt5Context::new(context));
					}
				}
			},
		}

		return match self.context.as_mut() {
			Some(c) => Some(c),
			None => None,
		};
	}

	pub fn actual_size(&self) -> Option<(u32, u32)> {
		return match self.native.as_ref() {
			Some(n) => {
				let width = unsafe { native::uniui_widget_ugl_width(n) };
				let height = unsafe { native::uniui_widget_ugl_height(n) };
				Some((width as u32, height as u32))
			},
			None => None,
		};
	}

	pub fn resize_if_needed(
		&mut self,
		_width: u32,
		_height: u32,
	) {
		// not needed since Qt automatically updates viewport size
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let ptr = unsafe {
			let ptr = native::uniui_widget_ugl_create();
			self.native.replace(ptr);
			ptr
		};

		return Ok(ptr);
	}
}
