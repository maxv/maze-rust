use std::{
	ffi::c_void,
	os::raw::c_char,
};

extern "C" {
	pub static UGL_ARRAY_BUFFER: u32;
	pub static UGL_ELEMENT_ARRAY_BUFFER: u32;
	pub static UGL_STATIC_DRAW: u32;
	pub static UGL_DYNAMIC_DRAW: u32;
	pub static UGL_STREAM_DRAW: u32;
	pub static UGL_BYTE: u32;
	pub static UGL_SHORT: u32;
	pub static UGL_UNSIGNED_BYTE: u32;
	pub static UGL_UNSIGNED_SHORT: u32;
	pub static UGL_FLOAT: u32;
	pub static UGL_POINTS: u32;
	pub static UGL_LINE_STRIP: u32;
	pub static UGL_LINE_LOOP: u32;
	pub static UGL_LINES: u32;
	pub static UGL_TRIANGLE_STRIP: u32;
	pub static UGL_TRIANGLE_FAN: u32;
	pub static UGL_TRIANGLES: u32;
	pub static UGL_NEVER: u32;
	pub static UGL_LESS: u32;
	pub static UGL_EQUAL: u32;
	pub static UGL_LEQUAL: u32;
	pub static UGL_GREATER: u32;
	pub static UGL_NOTEQUAL: u32;
	pub static UGL_GEQUAL: u32;
	pub static UGL_ALWAYS: u32;

	pub fn uniui_widget_ugl_create() -> *mut c_void;

	pub fn uniui_widget_ugl_get_functions(ugl: *mut c_void) -> *mut c_void;

	pub fn uniui_widget_ugl_clear_color(
		qt_function: *mut c_void,
		red: f32,
		green: f32,
		blue: f32,
		alpha: f32,
	);
	pub fn uniui_widget_ugl_update(ugl: *mut c_void);

	pub fn uniui_widget_ugl_width(ugl: *mut c_void) -> i32;
	pub fn uniui_widget_ugl_height(ugl: *mut c_void) -> i32;

	pub fn uniui_widget_ugl_functions_get_shader_compile_status(
		qt_function: *mut c_void,
		shader: u32,
	) -> bool;

	pub fn uniui_widget_ugl_functions_create_vertex_shader(
		qt_function: *mut c_void
	) -> u32;
	pub fn uniui_widget_ugl_functions_create_fragment_shader(
		qt_function: *mut c_void
	) -> u32;

	pub fn uniui_widget_ugl_functions_shader_add_source(
		qt_function: *mut c_void,
		shader: u32,
		string: *const c_char,
	);

	pub fn uniui_widget_ugl_functions_compile_shader(
		qt_function: *mut c_void,
		shader: u32,
	);

	pub fn uniui_widget_ugl_functions_create_program(qt_function: *mut c_void) -> u32;

	pub fn uniui_widget_ugl_functions_attach_shader(
		qt_function: *mut c_void,
		program: u32,
		shader: u32,
	);

	pub fn uniui_widget_ugl_functions_link_program(
		qt_function: *mut c_void,
		program: u32,
	);

	pub fn uniui_widget_ugl_functions_get_program_link_status(
		qt_function: *mut c_void,
		program: u32,
	) -> bool;

	pub fn uniui_widget_ugl_functions_get_attrib_location(
		qt_function: *mut c_void,
		program: u32,
		name: *const c_char,
	) -> i32;

	pub fn uniui_widget_ugl_functions_get_uniform_location(
		qt_function: *mut c_void,
		program: u32,
		name: *const c_char,
	) -> i32;

	pub fn uniui_widget_ugl_functions_create_buffer(qt_function: *mut c_void) -> u32;

	pub fn uniui_widget_ugl_functions_bind_buffer(
		qt_function: *mut c_void,
		target: u32,
		buffer: u32,
	);

	pub fn uniui_widget_ugl_functions_buffer_data_f32(
		qt_function: *mut c_void,
		target: u32,
		size: u32,
		data: *const f32,
		usage: u32,
	);

	pub fn uniui_widget_ugl_functions_vertex_attrib_pointer(
		qt_function: *mut c_void,
		index: u32,
		size: i32,
		ztype: u32,
		normalize: bool,
		stride: i32,
		offset: i32,
	);

	pub fn uniui_widget_ugl_functions_enable_vertex_attrib_array(
		qt_function: *mut c_void,
		index: u32,
	);

	pub fn uniui_widget_ugl_functions_use_program(
		qt_function: *mut c_void,
		program: u32,
	);

	pub fn uniui_widget_ugl_functions_draw_arrays(
		qt_function: *mut c_void,
		mode: u32,
		first: i32,
		count: i32,
	);

	pub fn uniui_widget_ugl_functions_uniform_matrix4fv(
		qt_function: *mut c_void,
		location: u32,
		value: *const f32,
	);

	pub fn uniui_widget_ugl_functions_get_error(qt_function: *mut c_void) -> u32;

	pub fn uniui_widget_ugl_functions_depth_func(
		qt_function: *mut c_void,
		func: u32,
	);
}
