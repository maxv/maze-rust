pub type UGLProgram = u32;
pub type UGLVertexShader = u32;
pub type UGLFragmentShader = u32;
pub type UGLAttribLocation = u32;
pub type UGLUniformLocation = u32;
pub type UGLBuffer = u32;
