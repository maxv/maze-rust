use crate::UGLContext;

pub mod noop_shader {
	pub type UGLProgram = ();
	pub type UGLVertexShader = ();
	pub type UGLFragmentShader = ();
	pub type UGLAttribLocation = ();
	pub type UGLUniformLocation = ();
	pub type UGLBuffer = ();
}

use uniui_gui::{
	NativeWidget,
	WidgetGenerator,
};

pub struct NoopUGL {
}


impl NoopUGL {
	pub fn new() -> NoopUGL {
		return NoopUGL {};
	}

	pub fn rendering_done(&mut self) {
	}

	pub fn get_context(&mut self) -> Option<&mut dyn UGLContext> {
		return None;
	}

	pub fn actual_size(&self) -> Option<(u32, u32)> {
		return None;
	}

	pub fn resize_if_needed(
		&mut self,
		_width: u32,
		_height: u32,
	) {
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}
}
