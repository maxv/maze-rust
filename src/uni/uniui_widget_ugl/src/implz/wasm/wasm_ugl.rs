use super::wasm_context::WasmContext;

use crate::UGLContext;

use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	NativeWidget,
	WidgetGenerator,
};

use crate::implz::web_sys::{
	HtmlCanvasElement,
	WebGlRenderingContext,
};

use crate::implz::wasm_bindgen::JsCast;

pub struct WasmUGL {
	native: OptionalNative<HtmlCanvasElement>,
	context: Option<WasmContext>,
}

impl WasmUGL {
	pub fn new() -> WasmUGL {
		return WasmUGL {
			native: OptionalNative::new(),
			context: None,
		};
	}

	pub fn rendering_done(&mut self) {
	}

	pub fn get_context(&mut self) -> Option<&mut dyn UGLContext> {
		return match self.context.as_mut() {
			Some(c) => Some(c),
			None => None,
		};
	}

	pub fn actual_size(&self) -> Option<(u32, u32)> {
		return self
			.native
			.as_ref()
			.map(|c| c.get_bounding_client_rect())
			.map(|rect| (rect.width() as u32, rect.height() as u32));
	}

	pub fn resize_if_needed(
		&mut self,
		width: u32,
		height: u32,
	) {
		if let Some(c) = self.native.as_mut() {
			c.set_width(width);
			c.set_height(height);
		}

		if let Some(c) = self.context.as_mut() {
			c.viewport(0, 0, width as i32, height as i32);
		}
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("canvas").unwrap();
		let canvas = node.dyn_into::<HtmlCanvasElement>().unwrap();

		let context = canvas
			.get_context("webgl")
			.unwrap()
			.unwrap()
			.dyn_into::<WebGlRenderingContext>()
			.unwrap();
		self.context = Some(WasmContext::new(context));

		self.native.replace(canvas.clone());
		return Ok(From::from(canvas));
	}
}
