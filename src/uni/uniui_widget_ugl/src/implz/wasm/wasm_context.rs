use crate::{
	shader::*,
	UGLColor,
	UGLContext,
};

use crate::implz::{
	js_sys::Float32Array,
	web_sys::WebGlRenderingContext,
};

use std::convert::TryInto;

pub struct WasmContext {
	context: WebGlRenderingContext,
}

impl WasmContext {
	pub fn new(context: WebGlRenderingContext) -> WasmContext {
		return WasmContext {
			context,
		};
	}

	pub fn viewport(
		&self,
		x: i32,
		y: i32,
		width: i32,
		height: i32,
	) {
		self.context.viewport(x, y, width, height);
	}

	fn convert_target(target: BindBufferTarget) -> u32 {
		return match target {
			BindBufferTarget::ArrayBuffer => WebGlRenderingContext::ARRAY_BUFFER,
			BindBufferTarget::ElementArrayBuffer => {
				WebGlRenderingContext::ELEMENT_ARRAY_BUFFER
			},
		};
	}

	fn convert_usage(usage: BufferDataUsage) -> u32 {
		return match usage {
			BufferDataUsage::StaticDraw => WebGlRenderingContext::STATIC_DRAW,
			BufferDataUsage::DynamicDraw => WebGlRenderingContext::DYNAMIC_DRAW,
			BufferDataUsage::StreamDraw => WebGlRenderingContext::STREAM_DRAW,
		};
	}

	fn convert_type(gl_type: UGLType) -> u32 {
		return match gl_type {
			UGLType::Byte => WebGlRenderingContext::BYTE,
			UGLType::Short => WebGlRenderingContext::SHORT,
			UGLType::UnsignedByte => WebGlRenderingContext::UNSIGNED_BYTE,
			UGLType::UnsignedShort => WebGlRenderingContext::UNSIGNED_SHORT,
			UGLType::Float => WebGlRenderingContext::FLOAT,
		};
	}

	fn convert_draw_mode(mode: DrawMode) -> u32 {
		return match mode {
			DrawMode::Points => WebGlRenderingContext::POINTS,
			DrawMode::LineStrip => WebGlRenderingContext::LINE_STRIP,
			DrawMode::LineLoop => WebGlRenderingContext::LINE_LOOP,
			DrawMode::Lines => WebGlRenderingContext::LINES,
			DrawMode::TriangleStrip => WebGlRenderingContext::TRIANGLE_STRIP,
			DrawMode::TriangleFan => WebGlRenderingContext::TRIANGLE_FAN,
			DrawMode::Triangles => WebGlRenderingContext::TRIANGLES,
		};
	}

	fn convert_depth_func(func: DepthFunc) -> u32 {
		return match func {
			DepthFunc::Never => WebGlRenderingContext::NEVER,
			DepthFunc::Less => WebGlRenderingContext::LESS,
			DepthFunc::Equal => WebGlRenderingContext::EQUAL,
			DepthFunc::LessOrEqual => WebGlRenderingContext::LEQUAL,
			DepthFunc::Greater => WebGlRenderingContext::GREATER,
			DepthFunc::NotEqual => WebGlRenderingContext::NOTEQUAL,
			DepthFunc::GreateOrEqual => WebGlRenderingContext::GEQUAL,
			DepthFunc::Always => WebGlRenderingContext::ALWAYS,
		};
	}
}

impl UGLContext for WasmContext {
	fn clear_color(
		&self,
		c: &UGLColor,
	) {
		self.context.clear_color(c.red, c.green, c.blue, c.alpha);
		self.context.clear(WebGlRenderingContext::COLOR_BUFFER_BIT);
	}

	fn get_shader_compile_status(
		&self,
		shader: &UGLVertexShader,
	) -> Result<(), ()> {
		let status = self
			.context
			.get_shader_parameter(shader, WebGlRenderingContext::COMPILE_STATUS);
		return match status.as_bool() {
			Some(true) => Ok(()),
			_ => Err(()),
		};
	}

	fn compile_shader(
		&self,
		shader: &UGLVertexShader,
	) {
		self.context.compile_shader(shader);
	}

	fn shader_add_sources(
		&self,
		shader: &UGLVertexShader,
		source: &str,
	) {
		self.context.shader_source(shader, source);
	}

	fn create_vertex_shader(&self) -> Option<UGLVertexShader> {
		return self.context.create_shader(WebGlRenderingContext::VERTEX_SHADER);
	}

	fn create_fragment_shader(&self) -> Option<UGLFragmentShader> {
		return self.context.create_shader(WebGlRenderingContext::FRAGMENT_SHADER);
	}

	fn create_program(&self) -> Option<UGLProgram> {
		return self.context.create_program();
	}

	fn attach_fragment_shader(
		&self,
		program: &UGLProgram,
		shader: &UGLFragmentShader,
	) {
		self.context.attach_shader(program, shader);
	}

	fn attach_vertex_shader(
		&self,
		program: &UGLProgram,
		shader: &UGLVertexShader,
	) {
		self.context.attach_shader(program, shader);
	}

	fn link_program(
		&self,
		program: &UGLProgram,
	) {
		self.context.link_program(program);
	}

	fn get_program_link_status(
		&self,
		program: &UGLProgram,
	) -> Result<(), ()> {
		let status = self
			.context
			.get_program_parameter(program, WebGlRenderingContext::LINK_STATUS);
		return match status.as_bool() {
			Some(true) => Ok(()),
			_ => Err(()),
		};
	}

	fn get_attrib_location(
		&self,
		program: &UGLProgram,
		name: &str,
	) -> Option<UGLAttribLocation> {
		return self.context.get_attrib_location(program, name).try_into().ok();
	}

	fn get_uniform_location(
		&self,
		program: &UGLProgram,
		name: &str,
	) -> Option<UGLUniformLocation> {
		return self.context.get_uniform_location(program, name);
	}

	fn create_buffer(&self) -> Option<UGLBuffer> {
		return self.context.create_buffer();
	}

	fn bind_buffer(
		&self,
		target: BindBufferTarget,
		buffer: Option<&UGLBuffer>,
	) {
		let target = WasmContext::convert_target(target);
		self.context.bind_buffer(target, buffer);
	}

	fn buffer_data_f32(
		&self,
		target: BindBufferTarget,
		data: &[f32],
		usage: BufferDataUsage,
	) {
		let target = WasmContext::convert_target(target);
		let usage = WasmContext::convert_usage(usage);
		unsafe {
			// it's safe because we don't have any Box::new calls while
			// buffer exists and data lives long enougth (in outside block)
			let buffer = Float32Array::view(data);

			self.context.buffer_data_with_array_buffer_view(target, &buffer, usage);
		};
	}

	fn vertex_attrib_pointer(
		&self,
		location: &UGLAttribLocation,
		size: i32,
		gl_type: UGLType,
		normalize: bool,
		stride: i32,
		offset: i32,
	) {
		let gl_type = WasmContext::convert_type(gl_type);
		self.context.vertex_attrib_pointer_with_i32(
			*location, size, gl_type, normalize, stride, offset,
		);
	}

	fn enable_vertex_attrib_array(
		&self,
		location: &UGLAttribLocation,
	) {
		self.context.enable_vertex_attrib_array(*location);
	}

	fn use_program(
		&self,
		program: &UGLProgram,
	) {
		self.context.use_program(Some(program));
	}

	fn draw_arrays(
		&self,
		mode: DrawMode,
		first: i32,
		count: i32,
	) {
		let mode = WasmContext::convert_draw_mode(mode);
		self.context.draw_arrays(mode, first, count);
	}

	fn depth_func(
		&self,
		func: DepthFunc,
	) {
		let func = WasmContext::convert_depth_func(func);
		self.context.depth_func(func)
	}

	fn get_error(&self) -> u32 {
		return self.context.get_error();
	}

	fn uniform_matrix4fv(
		&self,
		uniform_location: &UGLUniformLocation,
		data: &[f32; 16],
	) {
		self.context.uniform_matrix4fv_with_f32_array(
			Some(uniform_location),
			false,
			data,
		);
	}
}
