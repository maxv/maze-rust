pub type UGLProgram = crate::implz::web_sys::WebGlProgram;
pub type UGLVertexShader = crate::implz::web_sys::WebGlShader;
pub type UGLFragmentShader = crate::implz::web_sys::WebGlShader;
pub type UGLAttribLocation = u32;
pub type UGLUniformLocation = crate::implz::web_sys::WebGlUniformLocation;
pub type UGLBuffer = crate::implz::web_sys::WebGlBuffer;
