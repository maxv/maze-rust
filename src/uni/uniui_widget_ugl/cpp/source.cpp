#include <QOpenGLWidget>
#include <QOpenGLContext>
#include <QOpenGLFunctions>


namespace uniui_widget_ugl {

class UGL: public QOpenGLWidget {
public:
	int m_width;
	int m_height;

	UGL():
		QOpenGLWidget(),
		m_width(0),
		m_height(0)
	{
	}
protected:
    void resizeGL(int w, int h) override
    {
        m_width = w;
        m_height = h;
    }
};

}

extern "C" {
	extern const GLenum UGL_ARRAY_BUFFER = GL_ARRAY_BUFFER;
	extern const GLenum UGL_ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER;
	extern const GLenum UGL_STATIC_DRAW = GL_STATIC_DRAW;
	extern const GLenum UGL_DYNAMIC_DRAW = GL_DYNAMIC_DRAW;
	extern const GLenum UGL_STREAM_DRAW = GL_STREAM_DRAW;
	extern const GLenum UGL_BYTE = GL_BYTE;
	extern const GLenum UGL_SHORT = GL_SHORT;
	extern const GLenum UGL_UNSIGNED_BYTE = GL_UNSIGNED_BYTE;
	extern const GLenum UGL_UNSIGNED_SHORT = GL_UNSIGNED_SHORT;
	extern const GLenum UGL_FLOAT = GL_FLOAT;
	extern const GLenum UGL_POINTS = GL_POINTS;
	extern const GLenum UGL_LINE_STRIP = GL_LINE_STRIP;
	extern const GLenum UGL_LINE_LOOP = GL_LINE_LOOP;
	extern const GLenum UGL_LINES = GL_LINES;
	extern const GLenum UGL_TRIANGLE_STRIP = GL_TRIANGLE_STRIP;
	extern const GLenum UGL_TRIANGLE_FAN = GL_TRIANGLE_FAN;
	extern const GLenum UGL_TRIANGLES = GL_TRIANGLES;
	extern const GLenum UGL_NEVER = GL_NEVER;
	extern const GLenum UGL_LESS = GL_LESS;
	extern const GLenum UGL_EQUAL = GL_EQUAL;
	extern const GLenum UGL_LEQUAL = GL_LEQUAL;
	extern const GLenum UGL_GREATER = GL_GREATER;
	extern const GLenum UGL_NOTEQUAL = GL_NOTEQUAL;
	extern const GLenum UGL_GEQUAL = GL_GEQUAL;
	extern const GLenum UGL_ALWAYS = GL_ALWAYS;
}

using namespace uniui_widget_ugl;


extern "C" void* uniui_widget_ugl_create() {
	UGL* result = new UGL();
	result->setUpdateBehavior(QOpenGLWidget::PartialUpdate);
	return result;
}

extern "C" int uniui_widget_ugl_width(void* _ugl) {
	return ((UGL*)_ugl)->m_width;
}

extern "C" int uniui_widget_ugl_height(void* _ugl) {
	return ((UGL*)_ugl)->m_height;
}


extern "C" void* uniui_widget_ugl_get_functions(void* _ugl) {
	UGL* ugl = ((UGL*)_ugl);
	ugl->makeCurrent();
	return ugl->context()->functions();
}

extern "C" void uniui_widget_ugl_update(void* _ugl) {
	UGL* ugl = ((UGL*)_ugl);
	ugl->doneCurrent();
	ugl->update();
}


extern "C" void uniui_widget_ugl_clear_color(void* functions, float red, float green, float blue, float alpha) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glClearColor(red, green, blue, alpha);
	f->glClear(GL_COLOR_BUFFER_BIT);
}

extern "C" bool uniui_widget_ugl_functions_get_shader_compile_status(void* functions, unsigned int shader) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	int result = 0;
	f->glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	return result == GL_TRUE;
}

extern "C" unsigned int uniui_widget_ugl_functions_create_vertex_shader(void* functions) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	return f->glCreateShader(GL_VERTEX_SHADER);
}

extern "C" unsigned int uniui_widget_ugl_functions_create_fragment_shader(void* functions) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	return f->glCreateShader(GL_FRAGMENT_SHADER);
}

extern "C" void uniui_widget_ugl_functions_shader_add_source(
	void* functions,
	unsigned int shader,
	const char* string
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glShaderSource(shader, 1, &string, NULL);
}

extern "C" void uniui_widget_ugl_functions_compile_shader(
	void* functions,
	unsigned int shader
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glCompileShader(shader);
}

extern "C" unsigned int uniui_widget_ugl_functions_create_program(void* functions) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	return f->glCreateProgram();
}

extern "C" void uniui_widget_ugl_functions_attach_shader(
	void* functions,
	unsigned int program,
	unsigned int shader
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glAttachShader(program, shader);
}

extern "C" void uniui_widget_ugl_functions_link_program(
	void* functions,
	unsigned int program
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glLinkProgram(program);
}

extern "C" bool uniui_widget_ugl_functions_get_program_link_status(void* functions, unsigned int program) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	int result = 0;
	f->glGetProgramiv(program, GL_LINK_STATUS, &result);
	return result == GL_TRUE;
}

extern "C" int uniui_widget_ugl_functions_get_attrib_location(
	void* functions,
	unsigned int program,
	const char* name
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	return f->glGetAttribLocation(program, name);
}


extern "C" int uniui_widget_ugl_functions_get_uniform_location(
	void* functions,
	unsigned int program,
	const char *name
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	return f->glGetUniformLocation(program, name);
}

extern "C" unsigned int uniui_widget_ugl_functions_create_buffer(
	void* functions
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	unsigned int buffers = 0;
	f->glGenBuffers(1, &buffers);
	return buffers;
}

extern "C" void uniui_widget_ugl_functions_bind_buffer(
	void* functions,
	unsigned int target,
	unsigned int buffer
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glBindBuffer(target, buffer);
}

extern "C" void uniui_widget_ugl_functions_buffer_data_f32(
	void* functions,
	unsigned int target,
	unsigned int size,
	const float* data,
	unsigned int usage
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glBufferData(target, size*sizeof(float), data, usage);
}

extern "C" void uniui_widget_ugl_functions_vertex_attrib_pointer(
	void* functions,
	unsigned int index,
	int size,
	unsigned int type,
	bool normalize,
	int stride,
	void* offset
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glVertexAttribPointer(
		index,
		size,
		type,
		normalize ? GL_TRUE: GL_FALSE,
		stride,
		offset
	);
}

extern "C" void uniui_widget_ugl_functions_enable_vertex_attrib_array(
	void* functions,
	unsigned int index
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glEnableVertexAttribArray(index);
}

extern "C" void uniui_widget_ugl_functions_use_program(
	void* functions,
	unsigned int program
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glUseProgram(program);
}

extern "C" void uniui_widget_ugl_functions_draw_arrays(
	void* functions,
	unsigned int mode,
	int first,
	int count
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glDrawArrays(mode, first, count);
}

extern "C" void uniui_widget_ugl_functions_uniform_matrix4fv(
	void* functions,
	unsigned int location,
	const float* value
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glUniformMatrix4fv(location, 1, GL_FALSE, value);
}

extern "C" unsigned int uniui_widget_ugl_functions_get_error(
	void* functions
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	return f->glGetError();
}

extern "C" void uniui_widget_ugl_functions_depth_func(
	void* functions,
	unsigned int func
) {
	QOpenGLFunctions* f = (QOpenGLFunctions*) functions;
	f->glDepthFunc(func);
}
