use uniui_gui::{
	NativeWidget,
	WidgetGenerator,
};

use std::sync::mpsc::Sender;

pub struct Qt5RadioButton {}

impl Qt5RadioButton {
	pub fn new(_sender_changed: Sender<()>) -> Qt5RadioButton {
		return Qt5RadioButton {};
	}

	pub fn set_checked(
		&self,
		_checked: bool,
	) {
		log::error!("Qt5RadioButton::set_checked not implemented");
	}

	pub fn to_native(
		&mut self,
		_document: &mut dyn WidgetGenerator,
		_group_name: &String,
		_checked: bool,
	) -> Result<NativeWidget, ()> {
		log::error!("Qt5RadioButton not implemented");
		return Err(());
	}
}
