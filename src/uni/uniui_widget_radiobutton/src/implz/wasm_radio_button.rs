use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	utils::*,
	NativeWidget,
	WidgetGenerator,
};

use crate::implz::web_sys::HtmlInputElement;

use crate::implz::wasm_bindgen::closure::Closure;

use crate::implz::wasm_bindgen::JsCast;

use std::sync::mpsc::Sender;


pub struct WasmRadioButton {
	native: OptionalNative<HtmlInputElement>,
	closure: Closure<dyn FnMut()>,
}

impl WasmRadioButton {
	pub fn new(sender_changed: Sender<()>) -> WasmRadioButton {
		let closure = Closure::wrap(Box::new(move || {
			sender_changed.send_silent(());
		}) as Box<dyn FnMut()>);

		return WasmRadioButton {
			native: OptionalNative::new(),
			closure,
		};
	}

	pub fn set_checked(
		&self,
		checked: bool,
	) {
		if let Some(n) = self.native.as_ref() {
			n.set_checked(checked);
		}
	}

	pub fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
		group_name: &String,
		checked: bool,
	) -> Result<NativeWidget, ()> {
		let node = document.create_element("input").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let input = node.dyn_into::<HtmlInputElement>().unwrap();
		input.set_type("radio");
		input.set_name(group_name);

		input.set_checked(checked);
		input.set_onchange(Some(self.closure.as_ref().unchecked_ref()));

		self.native.replace(input.clone());
		Ok(From::from(input))
	}
}
