use crate::implz::Platform;

use uniui_core::{
	Signal,
	Slot,
	SlotImpl,
};

use uniui_gui::{
	Application,
	DataProcessor,
	NativeWidget,
	Widget,
	WidgetGenerator,
};

use std::sync::mpsc::{
	self,
	Receiver,
};

pub struct RadioButton {
	group_name: String,
	signal_checked: Signal<()>,
	slot_set_checked: SlotImpl<bool>,
	receiver_changed: Receiver<()>,
	platform: Platform,
}

impl RadioButton {
	pub fn new(group_name: String) -> RadioButton {
		let (sender_changed, receiver_changed) = mpsc::channel();

		return RadioButton {
			group_name,
			signal_checked: Signal::new(),
			slot_set_checked: SlotImpl::new(),
			receiver_changed,
			platform: Platform::new(sender_changed),
		};
	}

	pub fn signal_checked(&mut self) -> &mut Signal<()> {
		return &mut self.signal_checked;
	}

	pub fn slot_checked(&self) -> &dyn Slot<bool> {
		return &self.slot_set_checked;
	}
}

impl Widget for RadioButton {
	fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let checked = self.slot_set_checked.last().unwrap_or(false);
		return self.platform.to_native(document, &self.group_name, checked);
	}
}

impl DataProcessor for RadioButton {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		if let Some(checked) = self.slot_set_checked.last() {
			self.platform.set_checked(checked);
		}

		if let Some(()) = self.receiver_changed.try_iter().last() {
			self.signal_checked.emit(());
		};
	}
}
