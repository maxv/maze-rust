#[doc(hidden)]
#[macro_export]
macro_rules! u_radiobutton_int {
	($b:ident,signal_checked: $va:expr) => {
		$b.signal_checked().connect_slot($va);
	};
	($b:ident,initially: $va:expr) => {
		$b.slot_checked().exec_for($va);
	};
	($b:ident,slot_checked: $va:expr) => {
		$va.connect_slot($b.slot_checked());
	};
	($b:ident,slot_checked_proxy: $va:expr) => {
		$va = $b.slot_checked_porxy();
	};
}

/// Simplifies [RadioButton](crate::RadioButton) creation
///
/// Parameters:
/// * group (required): String,
/// * signal_checked (optional): [&Slot\<()\>](uniui_core::Slot), will be connected to
///   [RadioButton::signal_checked],
/// * initially (optional): bool,
/// * slot_checked (optional): [&Signal\<bool\>](uniui_core::Signal), will be connected to
///   [RadioButton::slot_checked],
/// * slot_checked_proxy (optional): [RadioButton::slot_checked]'s proxy.
///
/// Example:
/// ```
/// let slot_button_checked = SlotImpl::new();
///
/// let button = u_buton! {
///     group: "group_name".to_owned(),
///     signal_checked: &slot_button_checked,
/// };
/// ```
#[macro_export]
macro_rules! u_radiobutton {
	(group: $group:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut button = $crate::RadioButton::new($group);
		$($crate::u_radiobutton_int!(button, $param_name: $val);)*;
		button
	}};
}
