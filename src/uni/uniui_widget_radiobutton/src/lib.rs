extern crate uniui_core;
extern crate uniui_gui;

mod radio_button;
pub use self::radio_button::RadioButton;

mod macros;

/// Contains [RadioButton] and [u_radiobutton!].
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_radiobutton,
		RadioButton,
	};
}

pub const THEME_CLASS_NAME: &str = "u_radiobutton_theme_class";


#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_radio_button;
	pub use self::wasm_radio_button::WasmRadioButton as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_radio_button;
	pub use self::qt5_radio_button::Qt5RadioButton as Platform;
}
