pub use crate::TraceSender as TraceSenderWs;

use unitracer::{
	TraceSender,
	TraceSenderFactory,
};

use std::{
	cell::RefCell,
	rc::Rc,
};


pub struct Factory {
	pub url: String,
}

impl Factory {
	fn create_sender(&self) -> Rc<RefCell<dyn TraceSender>> {
		return Rc::new(RefCell::new(TraceSenderWs::new(self.url.clone())));
	}
}

impl TraceSenderFactory for Factory {
	fn call(&mut self) -> Rc<RefCell<dyn TraceSender>> {
		return self.create_sender();
	}
}
