use unitracer::{
	Batch,
	TraceSender as TS,
};

use uniws::{
	Format,
	State,
	UniWs,
	UniWsTrait,
};

use std::collections::LinkedList;


pub struct TraceSender {
	ws: UniWs<unitracer_proto::Protocol>,
	scheduled_batches: LinkedList<unitracer_proto::Protocol>,
}

impl TraceSender {
	pub fn new(url: String) -> TraceSender {
		let result = TraceSender {
			ws: UniWs::connect(&url, Format::Binary),
			scheduled_batches: LinkedList::new(),
		};

		return result;
	}

	fn send_all_scheduled(&mut self) {
		let mut list = LinkedList::new();
		for s in self.scheduled_batches.iter() {
			match self.ws.send(&s) {
				Ok(()) => {},
				Err(e) => {
					println!("send failed e:{:?}", e);
					list.push_front(s.clone());
				},
			};
		}

		self.scheduled_batches = list;
	}
}

impl TS for TraceSender {
	fn send_batch(
		&mut self,
		batch: Batch,
	) {
		let msg = unitracer_proto::ProtocolV1::Batch(batch);
		let msg = unitracer_proto::Protocol::V1(msg);
		let sended = match self.ws.ready_state() {
			State::Open => {
				match self.ws.send(&msg) {
					Ok(()) => true,
					Err(e) => {
						println!("message was not sended e:{:?}", e);
						false
					},
				}
			},
			_ => {
				// error!("socket not opened state:{:?}", s);
				false
			},
		};

		if !sended {
			self.scheduled_batches.push_back(msg);
		} else {
			self.send_all_scheduled();
		};
	}
}
