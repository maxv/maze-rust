extern crate unitracer;
extern crate unitracer_proto;
extern crate uniws;


mod trace_sender;
pub use trace_sender::TraceSender;


mod factory;


pub fn init(url: String) {
	let f = Box::new(factory::Factory {
		url,
	});
	unitracer::set_trace_sender_factory(f);
}
