//! The crate with helpful proc macros for uniui_gui.
//!
//! It's better to use that functionality via uniui_gui since the crate itself
//! is not ready to be used directly anythere.

extern crate proc_macro;
use proc_macro::TokenStream;

fn the_crate_name() -> proc_macro2::TokenStream {
	quote::quote! {
		uniui_gui
	}
}

mod derives;
mod u_main;

/// Macros allows to mark function as main entry point for UniUi Application
///
/// uniui_gui crate have to be presented in the scope.
///
/// wasm_bindgen should not be presented in crate's dependencies list.
///
/// Example
/// ```
/// #[uniui_gui::uni_main]
/// pub fn app_main(app: &mut uniui_gui::Application) {
///     // ...
/// }
/// ```
#[proc_macro_attribute]
pub fn u_main(
	attr: TokenStream,
	input: TokenStream,
) -> TokenStream {
	return u_main::u_main(attr, input);
}

/// DataProcessor derive macro
///
/// The DataProcessor trait will be automatically implemented for the structure.
///
/// There is few attributes which will help you to configure the implementation.
///
/// # #[public_slot]
/// Attribute may be added to private member which
/// implements [uniui_core::Slot] trait. For the structure new method will be generated
/// with he same name as memeber's name. The method will give public access to the
/// member as `&dyn Slot`.
///
/// Example:
/// ```
/// mod string_processor {
///     #[derive(uniui_gui::DataProcessor)]
///     pub struct StringProcessor {
///         #[public_slot]
///         slot_process_string: uniui_core::SlotImpl<String>,
///     }
///
///     // That kind of code will be generated automatically by derive macos:
///     //
///     // impl StringProcessor {
///     //     pub fn slot_process_string(&self) -> &dyn Slot<String> {
///     //         return &self.slot_process_string;
///     //     }
///     // }
/// }
///
/// // Usage:
///
/// use string_processor::StringProcessor;
///
/// fn send_empty_string(string_processor: StringProcessor, s: String) {
///     let slot: &dyn Slot<String> = string_processor.slot_process_string();
///     slot.exec_for(s);
/// }
/// ```
///
/// # #\[public_signal\]
/// Attribute may be added to private member which
/// implements [uniui_core::Signal] trait. For the structure new method will be generated
/// with he same name as memeber's name. The method will give public access to the
/// member as `&mut dyn Signal`.
///
/// Example:
/// ```
/// mod string_provider {
///     #[derive(uniui_gui::DataProcessor)]
///     pub struct StringProvider {
///         #[public_signal]
///         signal_new_string: uniui_core::Signal<String>,
///     }
///
///     // That kind of code will be generated automatically by derive macos:
///     //
///     // impl StringProvider {
///     //     pub fn signal_new_string(&mut self) -> &mut dyn Signal<String> {
///     //         return &mut self.signal_new_string;
///     //     }
///     // }
/// }
///
/// // Usage:
///
/// use string_provider::StringProvider;
///
/// fn listen_for_new_string(provider: StringProvider, s: &dyn Slot<String>) {
///     provider.signal_new_string().connect_slot(s);
/// }
/// ```
///
/// # #\[uprocess_self(<method_name>)\]
/// The structure itself may be marked by the attribute. Structure's method should
/// be provided as parameter to attribue. Then the method will be called every
/// tick (see [uniui_gui::Application] and [uniui_gui::DataProcessor#process_data]
/// for more info about ticks).
///
/// Example
/// ```
/// #[derive(uniui_gui::DataProcessor)]
/// #[uprocess_self(tick)]
/// pub struct SelfProcessor {
/// }
///
/// impl SelfProcessor {
///     pub tick(&mut self, _: i64, _: &mut uniui_gui::Application) {
///         log:trace!("tick");
///     }
/// }
/// ```
///
/// # #\[uprocess_each(<method_name>)\]
/// Structure's [uniui_core::SlotImpl] may be marked. Then each value arrived
/// to the slot will be processed by the method `<method_name>`.
///
/// Example:
/// ```
/// #[derive(uniui_gui::DataProcessor)]
/// pub struct StringLogger {
///     #[public_slot]
///     #[uprocess_each(log_string)]
///     slot_log_string: uniui_gui::core::SlotImpl<String>,
/// }
///
/// impl StringLogger{
///     pub fn new() -> Self {
///         slot_lot_string: uniui_gui::core::SlotImpl::new(),
///     }
///
///     // The method will be called for each value posted to the slot_log_string
///     fn log_string(&self, s: String) {
///         log::info!("new string:{}", s);
///     }
/// }
/// ```
///
/// # #\[uprocess_last(<method_name>)\]
/// Structure's [uniui_core::SlotImpl] may be marked. Then each tick the last arrived
/// value will be processed by the method. See
/// [uprocess_each](derive.DataProcessor.html#uprocess_each) for usage example
///
/// # #\[uprocess_each_app(<method_name>)\]
/// The same as [uprocess_each](derive.DataProcessor.html#uprocess_each) but
/// reference to the [uniui_gui::Application] will be provided as an extra
/// parameter for the method.
///
/// Example:
/// ```
/// #[derive(uniui_gui::DataProcessor)]
/// pub struct StringLogger {
///     #[public_slot]
///     #[uprocess_each_app(log_string)]
///     slot_log_string: uniui_gui::core::SlotImpl<String>,
/// }
///
/// impl StringLogger{
///     pub fn new() -> Self {
///         slot_lot_string: uniui_gui::core::SlotImpl::new(),
///     }
///
///     // The method will be called for each value posted to the slot_log_string
///     fn log_string(&self, s: String, _app: &mut uniui_gui::core::Application) {
///         log::info!("new string:{}", s);
///     }
/// }
/// ```
///
///
/// # #\[uprocess_last_app(<method_name>)\]
/// The same as [uprocess_last](derive.DataProcessor.html#uprocess_last) but
/// reference to the [uniui_gui::Application] will be provided as an extra
/// parameter for the method.
///
/// # #\[uproperty_process\]
/// Structure's member of type [uniui_core::Property] may be marked by attribute.
/// Then every tick the member will process all incoming values. The method name
/// may be provided as a parameter of the attribute. Then the method will be called
/// every time (Property)[uniui_core::Property]'s value will be changed.
///
/// # #\[uproperty_public_slot(<method_name>)\]
/// Create public method to access [uniui_core::Property]'s slot. New method with
/// provided name will be generated.
///
/// # #\[uproperty_public_signal(<method_name>)\]
/// Create public method to access [uniui_core::Property]'s signal. New method with
/// provided name will be generated.
///
/// # #\[data_processor\]
/// Structure's member of type [DataProcessor] may be marked by that attribute.
/// Then the member's [process_data](DataProcessor#process_data) method
/// will be called every time when [process_data](DataProcessor#process_data)
/// called for the Structure.
#[proc_macro_derive(
	DataProcessor,
	attributes(
		public_slot,
		public_signal,
		uprocess_self,
		uprocess_each,
		uprocess_last,
		uprocess_each_app,
		uprocess_last_app,
		uproperty_process,
		uproperty_public_slot,
		uproperty_public_signal,
		data_processor,
	)
)]
pub fn derive_data_processor(input: TokenStream) -> TokenStream {
	return derives::derive_uobject(input);
}

/// Widget derive macro
///
/// The Widget (and DataProcessor) trait will be automatically implemented for the
/// structure.
///
/// There is few attributes which will help you to configure the implementation.
///
/// # #\[base_widget\]
/// Exactly one structure's member with implemented [Widget] trait have to be
/// marked by the attribute. All [Widget] stuff will be delegated to the member.
///
/// # #\[public_slot\]
/// see [DataProcessor's public_slot](derive.DataProcessor.html#public_slot)
///
/// # #\[public_signal\]
/// see [DataProcessor's public_signal](derive.DataProcessor.html#public_signal)
///
/// # #\[uprocess_self(<method_name>)\]
/// see [DataProcessor's uprocess_self](derive.DataProcessor.html#uprocess_self)
///
/// # #\[uprocess_each(<method_name>)\]
/// see [DataProcessor's uprocess_each](derive.DataProcessor.html#uprocess_each)
///
/// # #\[uprocess_last(<method_name>)\]
/// see [DataProcessor's uprocess_last](derive.DataProcessor.html#uprocess_last)
///
/// # #\[uprocess_each_app(<method_name>)\]
/// see [DataProcessor's uprocess_each_app](derive.DataProcessor.html#uprocess_each_app)
///
/// # #\[uprocess_last_app(<method_name>)\]
/// see [DataProcessor's uprocess_last_app](derive.DataProcessor.html#uprocess_last_app)
///
/// # #\[uproperty_process\]
/// see [DataProcessor's uproperty_process](derive.DataProcessor.html#uproperty_process)
///
/// # #\[uproperty_public_slot(<method_name>)\]
/// see [DataProcessor's
/// uproperty_public_slot](derive.DataProcessor.html#uproperty_public_slot)
///
/// # #\[uproperty_public_signal(<method_name>)\]
/// see [DataProcessor's
/// uproperty_public_signal](derive.DataProcessor.html#uproperty_public_signal)
///
/// # #\[data_processor\]
/// see [DataProcessor's data_processor](derive.DataProcessor.html#data_processor)
#[proc_macro_derive(
	Widget,
	attributes(
		public_slot,
		public_signal,
		uprocess_self,
		uprocess_each,
		uprocess_last,
		uprocess_each_app,
		uprocess_last_app,
		uproperty_process,
		uproperty_public_slot,
		uproperty_public_signal,
		base_widget,
		data_processor,
	)
)]
pub fn derive_widget(input: TokenStream) -> TokenStream {
	return derives::derive_widget(input);
}
