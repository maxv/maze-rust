extern crate proc_macro;
use proc_macro::TokenStream;

use syn::{
	parse_macro_input,
	ItemFn,
};

use quote::{
	format_ident,
	quote,
};


pub fn u_main(
	_attr: TokenStream,
	input: TokenStream,
) -> TokenStream {
	let input_c = input.clone();
	let ast = parse_macro_input!(input_c as ItemFn);

	let fn_name = &ast.sig.ident;

	let crate_name = crate::the_crate_name();
	let wasm_mod = format_ident!("{}_wasm_bindgen_start", fn_name);

	let android_mod = format_ident!("{}", uniui_build::android_module_name(fn_name));
	let run_fn = format_ident!("{}", uniui_build::RUN_FUNCTION_NAME);
	let tick_fn = format_ident!("{}", uniui_build::TICK_FUNCTION_NAME);
	let cleanup_fn = format_ident!("{}", uniui_build::CLEANUP_FUNCTION_NAME);

	let linux_mod = format_ident!("linux");

	let result = quote! {
		#ast

		#[cfg(target_arch = "wasm32")]
		pub mod #wasm_mod {
			use #crate_name::log;

			mod wasm_bindgen {
				pub use #crate_name::wasm_bindgen::*;
			}


			#[self::wasm_bindgen::prelude::wasm_bindgen(start)]
			pub fn run() -> Result<(), self::wasm_bindgen::JsValue> {
				match #crate_name::implz::wasm::WasmApplication::start(
					& super::#fn_name
				) {
					Ok(()) => log::trace!("App started"),
					Err(()) => log::error!("App start failed"),
				}

				Ok(())
			}
		}

		#[cfg(target_os = "android")]
		pub mod #android_mod {
			use #crate_name::implz::jni;

			pub fn #run_fn (
				env: jni::JNIEnv,
				activity: jni::objects::JObject,
			) {
				let start_result = #crate_name::implz::android::AndroidApplication::start(
					env,
					activity,
					& super::#fn_name,
				);

				match start_result {
					Ok(()) => #crate_name::log::trace!("App started"),
					Err(e) => #crate_name::log::error!("App start failed. Err:{:?}", e),
				}
			}

			pub fn #tick_fn (
				env: jni::JNIEnv,
				app: jni::objects::JObject,
				addr: jni::sys::jlong,
			) -> jni::sys::jlong {
				return #crate_name::implz::android::AndroidApplication::tick(
					env,
					app,
					addr
				);
			}

			pub fn #cleanup_fn (
				env: jni::JNIEnv,
				app: jni::objects::JObject,
				addr: jni::sys::jlong,
			) {
				return #crate_name::implz::android::AndroidApplication::cleanup(
					env,
					app,
					addr
				);
			}
		}

		#[cfg(target_os = "linux")]
		pub mod #linux_mod {
			pub fn main() {
				#crate_name::desktop_qt!{
					use #crate_name::implz::qt5::Qt5Application;
					match Qt5Application::start(& super::#fn_name) {
						Ok(()) => #crate_name::log::trace!("App started"),
						Err(e) => #crate_name::log::error!(
							"App start failed. Err:{:?}",
							e
						),
					}
				};

				#crate_name::desktop_noop!{
					let _ = super::#fn_name;
				};
			}
		}
	};

	return result.into();
}
