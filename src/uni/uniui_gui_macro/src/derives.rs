use proc_macro::TokenStream;

use proc_macro2::TokenStream as TokenStream2;

use quote::quote;

mod delegate_data_processor;
mod delegate_slot;
mod delegate_widget;
mod public_signal;
mod public_slot;
mod uprocess_self;
mod utils;

fn gui_crate_name() -> TokenStream2 {
	quote! {
		uniui_gui
	}
}

fn core_crate_name() -> TokenStream2 {
	quote! {
		uniui_gui::core
	}
}

fn print_if_debug<T: std::fmt::Display>(v: &T) {
	if let Ok(_) = std::env::var("UOBJECT_DEBUG") {
		println!("generated:{}", v);
	}
}

pub fn derive_uobject(input: TokenStream) -> TokenStream {
	let ast = syn::parse(input).expect("Correct input");

	let public_slots_gen = public_slot::generate_for(&ast);
	let public_signals_gen = public_signal::generate_for(&ast);
	let delegate_slots = delegate_slot::generate_for(&ast);
	let self_process = uprocess_self::generate_for(&ast);
	let data_processors = delegate_data_processor::generate_for(&ast);

	let gen = quote! {
		#public_slots_gen


		#public_signals_gen


	};

	let result = match delegate_slots.is_empty() &&
		self_process.is_empty() &&
		data_processors.is_empty()
	{
		true => gen,
		false => {
			let name = &ast.ident;
			let generics = &ast.generics;
			let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

			let crate_name = gui_crate_name();

			quote! {
				#gen

				impl #impl_generics #crate_name::DataProcessor for #name #ty_generics
				#where_clause
				{
					fn process_data(
						&mut self,
						msec_since_app_start: i64,
						app: &mut #crate_name::Application,
					) {
						#delegate_slots

						#self_process

						#data_processors
					}
				}
			}
		},
	};

	print_if_debug(&result);
	return result.into();
}

pub fn derive_widget(input: TokenStream) -> TokenStream {
	let ast = syn::parse(input).expect("Correct input");


	let public_slots_gen = public_slot::generate_for(&ast);
	let public_signals_gen = public_signal::generate_for(&ast);
	let delegate_slots = delegate_slot::generate_for(&ast);
	let (widget_impl, widget_processor) = delegate_widget::generate_for(&ast);
	let self_process = uprocess_self::generate_for(&ast);
	let data_processors = delegate_data_processor::generate_for(&ast);

	let name = &ast.ident;
	let generics = &ast.generics;
	let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
	let crate_name = gui_crate_name();

	let gen = quote! {
		#public_slots_gen


		#public_signals_gen

		#widget_impl

		impl #impl_generics #crate_name::DataProcessor for #name #ty_generics
		#where_clause
		{
			fn process_data(
				&mut self,
				msec_since_app_start: i64,
				app: &mut #crate_name::Application,
			) {
				#widget_processor

				#data_processors

				#delegate_slots

				#self_process
			}
		}
	};

	print_if_debug(&gen);
	return gen.into();
}
