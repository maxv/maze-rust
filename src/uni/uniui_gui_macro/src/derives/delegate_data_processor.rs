use super::utils;

use quote::quote;

use syn::{
	DeriveInput,
	Ident,
};

use proc_macro2::TokenStream;

pub fn generate_for(ast: &DeriveInput) -> TokenStream {
	let widgets: Vec<_> = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("data_processor").ok().as_ref(),
	)
	.iter()
	.map(|t| t.0.clone())
	.collect();

	let data_processor_call = quote! {
		#(
			self. #widgets .process_data(msec_since_app_start, app);
		)*
	};

	return data_processor_call;
}
