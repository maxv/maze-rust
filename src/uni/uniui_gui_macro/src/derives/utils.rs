use syn::{
	Data,
	DeriveInput,
	Fields,
	GenericArgument,
	Ident,
	Type,
};

use proc_macro2::{
	TokenStream,
	TokenTree,
};

pub fn collect_fields_with_attr(
	ast: &DeriveInput,
	ident: Option<&Ident>,
) -> Vec<(Ident, Type, proc_macro2::TokenStream)> {
	return match &ast.data {
		Data::Struct(data_struct) => {
			match &data_struct.fields {
				Fields::Named(fields) => {
					fields
						.named
						.iter()
						.filter_map(|f| {
							let attrs = f
								.attrs
								.iter()
								.filter(|a| a.path.get_ident() == ident)
								.collect::<Vec<_>>();

							attrs.get(0).map(|a| (f, a.clone()))
						})
						.map(|(f, a)| {
							(f.ident.clone().unwrap(), f.ty.clone(), a.tokens.clone())
						})
						.collect()
				},
				_ => panic!("only named fields supported"),
			}
		},
		_ => panic!("only structs allowed"),
	};
}

pub fn extract_single_generic_from_type(the_type: &Type) -> &Type {
	return match &the_type {
		Type::Path(t) => {
			match &t.path.segments.last().unwrap().arguments {
				syn::PathArguments::AngleBracketed(aa) => {
					match aa.args.iter().len() {
						1 => {
							match aa.args.first() {
								Some(GenericArgument::Type(t)) => t,
								Some(_) => panic!("another ga"),
								None => panic!("internal error"),
							}
						},
						_ => panic!("more than one type not supported"),
					}
				},
				syn::PathArguments::None => panic!("another arg: None"),
				syn::PathArguments::Parenthesized(_) => {
					panic!("another arg: Parent-blah")
				},
			}
		},
		_ => panic!("unsupported type"),
	};
}

pub fn split(
	slots: Vec<(Ident, Type, TokenStream)>
) -> (Vec<Ident>, Vec<TokenStream>, Vec<Type>) {
	let mut idents = Vec::new();
	let mut tss = Vec::new();
	let mut types = Vec::new();
	for (ident, the_type, ts) in slots {
		idents.push(ident);
		types.push(the_type);

		tss.push(remove_brackets(ts));
	}

	return (idents, tss, types);
}

pub fn remove_brackets(ts: TokenStream) -> TokenStream {
	return match ts.into_iter().next() {
		Some(TokenTree::Group(group)) => group.stream(),
		_ => panic!("Incorrect syntaxis. The correct syntacsys is ???"),
	};
}
