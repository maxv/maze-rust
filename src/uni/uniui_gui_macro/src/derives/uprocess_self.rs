use super::utils;

use quote::quote;

use syn::{
	Attribute,
	DeriveInput,
};

use proc_macro2::{
	Ident,
	TokenStream,
};


fn is_process_self(a: &&Attribute) -> bool {
	let ident = syn::parse_str::<Ident>("uprocess_self").ok();
	return a.path.get_ident() == ident.as_ref();
}

pub fn generate_for(ast: &DeriveInput) -> TokenStream {
	let result = match ast.attrs.iter().find(&is_process_self) {
		None => quote! {},
		Some(attr) => {
			let tokens = utils::remove_brackets(attr.tokens.clone());
			quote! {
				self.#tokens(msec_since_app_start, app);
			}
		},
	};


	return result;
}
