use super::{
	gui_crate_name,
	utils,
};

use quote::quote;

use syn::{
	DeriveInput,
	Ident,
};

use proc_macro2::TokenStream;

pub fn generate_for(ast: &DeriveInput) -> (TokenStream, TokenStream) {
	let name = &ast.ident;
	let generics = &ast.generics;
	let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
	let widgets = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("base_widget").ok().as_ref(),
	);

	let widget = match widgets.len() {
		1 => widgets.first().expect("always works").0.clone(),

		0 => panic!("field with #[base_widget] not found"),
		_ => panic!("only one field may be #[base_widget]"),
	};

	let crate_name = gui_crate_name();

	let widget_impl = quote! {
		impl #impl_generics #crate_name::Widget for #name #ty_generics #where_clause {
			fn to_native(
				&mut self,
				widget_generator: &mut dyn #crate_name::WidgetGenerator,
			) -> Result<#crate_name::NativeWidget, ()> {
				return self.#widget.to_native(widget_generator);
			}

			fn draw(&mut self) {
				self.#widget.draw();
			}
		}
	};

	let data_processor_impl = quote! {
		self.#widget.process_data(msec_since_app_start, app);
	};

	return (widget_impl, data_processor_impl);
}
