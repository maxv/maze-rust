use super::utils;

use quote::quote;

use syn::{
	DeriveInput,
	Ident,
};

use proc_macro2::TokenStream;


pub fn generate_for(ast: &DeriveInput) -> TokenStream {
	let each_slots = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("uprocess_each").ok().as_ref(),
	);

	let last_slots = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("uprocess_last").ok().as_ref(),
	);

	let each_slots_app = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("uprocess_each_app").ok().as_ref(),
	);

	let last_slots_app = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("uprocess_last_app").ok().as_ref(),
	);

	let mut properties_process = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("uproperty_process").ok().as_ref(),
	);

	let mut properties = Vec::new();
	properties_process.retain(|(p, _, s)| {
		let no_tss = s.is_empty();

		if no_tss {
			properties.push(p.clone());
		}

		!no_tss
	});

	return match (
		each_slots.len(),
		last_slots.len(),
		each_slots_app.len(),
		last_slots_app.len(),
		properties.len(),
		properties_process.len(),
	) {
		(0, 0, 0, 0, 0, 0) => quote! {},
		_ => {
			let (each_idents, each_tss, _) = utils::split(each_slots);
			let (last_idents, last_tss, _) = utils::split(last_slots);

			let (each_idents_app, each_tss_app, _) = utils::split(each_slots_app);
			let (last_idents_app, last_tss_app, _) = utils::split(last_slots_app);

			let (prop_idents, prop_tss, _) = utils::split(properties_process);

			quote! {
				#(
					self. #properties .try_update();
				)*

				#(
					if self. #prop_idents.try_update() {
						self. #prop_tss ();
					}
				)*

				#(
					while let Some(t) = self.#each_idents . next() {
						self.#each_tss (t);
					}
				)*

				#(
					if let Some(t) = self.#last_idents . last() {
						self.#last_tss (t);
					}
				)*

				#(
					while let Some(t) = self.#each_idents_app . next() {
						self.#each_tss_app (app, t);
					}
				)*

				#(
					if let Some(t) = self.#last_idents_app . last() {
						self.#last_tss_app (app, t);
					}
				)*
			}
		},
	};
}
