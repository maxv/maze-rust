use super::utils;

use quote::quote;

use syn::{
	DeriveInput,
	Ident,
};

use proc_macro2::TokenStream;


pub fn generate_for(ast: &DeriveInput) -> TokenStream {
	let crate_name = super::core_crate_name();
	let name = &ast.ident;
	let generics = &ast.generics;
	let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

	let public_slots = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("public_slot").ok().as_ref(),
	);


	let mut result = quote! {};

	for (ident, the_type, _) in public_slots {
		let the_type = utils::extract_single_generic_from_type(&the_type);

		result = quote! {
			#result

			impl #impl_generics #name #ty_generics #where_clause {
				pub fn #ident(&self) -> &dyn #crate_name::Slot<#the_type> {
					return &self.#ident;
				}
			}
		};
	}

	let property_public_signals: Vec<_> = utils::collect_fields_with_attr(
		ast,
		syn::parse_str::<Ident>("uproperty_public_slot").ok().as_ref(),
	)
	.iter()
	.map(|(i, tt, ts)| {
		(i.clone(), ts.clone(), utils::extract_single_generic_from_type(tt).clone())
	})
	.collect();

	for (ident, ts, the_type) in property_public_signals {
		let ts = utils::remove_brackets(ts);

		result = quote! {
			#result

			impl #impl_generics #name #ty_generics #where_clause {
				pub fn #ts(&self) -> &dyn #crate_name::Slot<#the_type> {
					return self.#ident.slot();
				}
			}
		};
	}

	return result;
}
