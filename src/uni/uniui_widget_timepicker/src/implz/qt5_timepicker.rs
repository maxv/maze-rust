use std::sync::mpsc::Sender;

use chrono::naive::NaiveTime;

use uniui_gui::prelude::*;


pub struct Qt5Timepicker {}

impl Qt5Timepicker {
	pub fn new() -> Qt5Timepicker {
		return Qt5Timepicker {};
	}

	pub fn set_value(
		&mut self,
		_time: NaiveTime,
	) {
		log::error!("Qt5Timepicker is not implemented. Yet.");
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_sender: Sender<NaiveTime>,
	) -> Result<NativeWidget, ()> {
		log::error!("Qt5Timepicker is not implemented. Yet.");
		return Err(());
	}
}
