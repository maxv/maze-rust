use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::HtmlInputElement;

use crate::implz::wasm_bindgen::{
	closure::Closure,
	JsCast,
};

use chrono::{
	NaiveTime,
	Timelike,
};

use std::sync::mpsc::Sender;


pub struct WasmTimepicker {
	closure: Option<Closure<dyn FnMut()>>,
	native: OptionalNative<HtmlInputElement>,
}

impl WasmTimepicker {
	pub fn new() -> WasmTimepicker {
		return WasmTimepicker {
			closure: None,
			native: OptionalNative::new(),
		};
	}

	pub fn set_value(
		&mut self,
		time: NaiveTime,
	) {
		match self.native.as_mut() {
			Some(native) => {
				let time_string = format!("{:0>2}:{:0>2}", time.hour(), time.minute());
				native.set_value(&time_string);
			},
			None => {},
		};
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		sender: Sender<NaiveTime>,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("input").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);

		let picker = node.dyn_into::<HtmlInputElement>().unwrap();
		picker.set_type("time");

		let closure_picker = picker.clone();
		let closure = Closure::wrap(Box::new(move || {
			let time_string = closure_picker.value();
			match NaiveTime::parse_from_str(&time_string, "%H:%M") {
				Ok(time) => sender.send_silent(time),
				Err(_) => log::warn!("incorrect time format:{}", time_string),
			}
		}) as Box<dyn FnMut()>);

		picker.set_onchange(Some(closure.as_ref().unchecked_ref()));
		self.closure = Some(closure);

		self.native.replace(picker.clone());
		Ok(From::from(picker))
	}
}
