use crate::implz::Platform;

use uniui_gui::prelude::*;

use chrono::naive::NaiveTime;

use std::sync::mpsc::{
	self,
	Receiver,
	Sender,
};

/// Widget for Time selection. Seconds will be ignored.
pub struct Timepicker {
	platform: Platform,
	sender: Sender<NaiveTime>,
	receiver: Receiver<NaiveTime>,
	signal_time_selected: Signal<NaiveTime>,
	slot_set_time: SlotImpl<NaiveTime>,
}

impl Timepicker {
	pub fn new() -> Timepicker {
		let (sender, receiver) = mpsc::channel();
		return Timepicker {
			platform: Platform::new(),
			sender,
			receiver,
			signal_time_selected: Signal::new(),
			slot_set_time: SlotImpl::new(),
		};
	}

	pub fn signal_time_selected(&mut self) -> &mut Signal<NaiveTime> {
		return &mut self.signal_time_selected;
	}

	pub fn slot_set_time(&self) -> &dyn Slot<NaiveTime> {
		return &self.slot_set_time;
	}

	fn update_selected_value(&mut self) {
		if let Some(v) = self.slot_set_time.last() {
			self.platform.set_value(v);
		}
	}
}

impl Widget for Timepicker {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let result = self.platform.to_native(widget_generator, self.sender.clone());
		self.update_selected_value();
		return result;
	}
}

impl DataProcessor for Timepicker {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		self.update_selected_value();

		for time in self.receiver.try_recv() {
			self.signal_time_selected.emit(time);
		}

		if let Some(time) = self.slot_set_time.last() {
			self.platform.set_value(time);
		}
	}
}
