#[doc(hidden)]
#[macro_export]
macro_rules! u_timepicker_int {
	($b:ident,signal_time_selected: $va:expr) => {
		$b.signal_time_selected().connect_slot($va);
	};
	($b:ident,slot_set_time: $va:expr) => {
		$va.connect_slot($b.slot_set_time());
	};
	($b:ident,slot_set_time_proxy: $va:expr) => {
		$va = $b.slot_set_time().proxy();
	};
}

/// Simplifies [Timepicker](crate::Timepicker) creation
///
/// Parameters:
/// * signal_time_selected (optional): [&Slot\<NaiveTime\>](uniui_core::Slot), will be
///   connected to [Timepicker::signal_time_selected] to receive
///   [NaiveTime](chrono::NaiveTime),
/// * slot_set_time (optional): [&Signal\<NaiveTime\>](uniui_core::Signal) -
///   [NaiveTime](chrono::NaiveTime),
/// * slot_set_time_proxy (optional): identifier,
///
/// Example:
/// ```
/// TBD
/// ```
#[macro_export]
macro_rules! u_timepicker {
	($($param_name:ident: $val:expr),*$(,)*) => {{
		let mut dp = $crate::Timepicker::new();
		$($crate::u_timepicker_int!(dp, $param_name: $val);)*;
		dp
	}};
}
