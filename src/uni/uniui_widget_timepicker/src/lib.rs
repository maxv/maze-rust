extern crate uniui_core;
extern crate uniui_gui;

mod timepicker;
pub use self::timepicker::Timepicker;

mod macros;

pub mod prelude {
	pub use crate::{
		u_timepicker,
		Timepicker,
	};
	pub use chrono::{
		naive::NaiveTime,
		Timelike,
	};
}

pub const THEME_CLASS_NAME: &str = "u_timepicker_theme_class";

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_timepicker;
	pub use self::wasm_timepicker::WasmTimepicker as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_timepicker;
	pub use self::qt5_timepicker::Qt5Timepicker as Platform;
}
