extern crate uniui_gui;

use uniui_gui::prelude::*;

pub struct SignalMapper<T, U> {
	slots: Vec<(SlotImpl<T>, Box<dyn FnMut(T) -> U>)>,
	signal: Signal<U>,
}

impl<T, U> SignalMapper<T, U>
where
	T: 'static + std::clone::Clone,
	U: 'static + std::clone::Clone,
{
	pub fn new() -> SignalMapper<T, U> {
		return SignalMapper {
			slots: Vec::new(),
			signal: Signal::new(),
		};
	}

	pub fn signal(&mut self) -> &mut Signal<U> {
		return &mut self.signal;
	}

	pub fn map<F: 'static + FnMut(T) -> U>(
		&mut self,
		signal: &mut Signal<T>,
		func: F,
	) {
		let slot = SlotImpl::new();
		signal.connect_slot(&slot);
		self.slots.push((slot, Box::new(func)));
	}

	pub fn map_slot<F: 'static + FnMut(T) -> U>(
		&mut self,
		slot: SlotImpl<T>,
		func: F,
	) {
		self.slots.push((slot, Box::new(func)));
	}
}

impl<T, U> DataProcessor for SignalMapper<T, U>
where
	T: std::clone::Clone,
	U: 'static + std::clone::Clone,
{
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		for (slot, func) in self.slots.iter_mut() {
			for t in slot.data_iter() {
				let u = func(t);
				self.signal.emit(u);
			}
		}
	}
}
