use crate::{
	implz::Platform,
	DataHolder,
	DataHolderUpdate,
};

use uniui_gui::{
	prelude::*,
	Orientation,
};

pub struct RecyclerLayout<W, H>
where
	W: Widget,
	H: DataHolder<W>,
{
	data_holder: H,
	widgets: Vec<W>,
	data_holder_updates_slot: SlotImpl<DataHolderUpdate>,
	direction: Orientation,

	platform: Platform,
}

impl<W, H> RecyclerLayout<W, H>
where
	W: Widget,
	H: DataHolder<W>,
{
	pub fn new(
		mut data_holder: H,
		direction: Orientation,
	) -> RecyclerLayout<W, H> {
		let data_holder_updates_slot = SlotImpl::new();
		data_holder.setup_update_slot(&data_holder_updates_slot);
		return RecyclerLayout {
			platform: Platform::new(),
			data_holder,
			data_holder_updates_slot,
			widgets: Vec::new(),
			direction,
		};
	}

	pub fn data_holder(&self) -> &H {
		return &self.data_holder;
	}

	pub fn data_holder_mut(&mut self) -> &mut H {
		return &mut self.data_holder;
	}

	fn update_whole_data(
		&mut self,
		app: &mut dyn Application,
	) {
		self.platform.update_whole_data(app, &mut self.data_holder, &mut self.widgets);
		let holder_count = self.data_holder.count();
		self.fill_widgets(0, holder_count);
	}

	fn fill_widgets(
		&mut self,
		start: usize,
		count: usize,
	) {
		for i in start..(start + count) {
			match self.widgets.get_mut(i) {
				Some(w) => self.data_holder.fill(i, w),
				None => error!("couldn't found widget for id:{}", i),
			}
		}
	}
}

impl<W, H> DataProcessor for RecyclerLayout<W, H>
where
	W: Widget,
	H: DataHolder<W>,
{
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widgets.iter_mut().for_each(|w| w.process_data(msec_since_app_start, app));
		self.data_holder.process_data(msec_since_app_start, app);

		let data: Vec<_> = self.data_holder_updates_slot.data_iter().collect();
		for update in data {
			match update {
				DataHolderUpdate::WholeData => {
					self.update_whole_data(app);
					break;
				},
				DataHolderUpdate::UpdatedElement(id) => {
					self.fill_widgets(id, 1);
				},
			}
		}
	}
}

impl<W, H> Widget for RecyclerLayout<W, H>
where
	W: Widget,
	H: DataHolder<W>,
{
	fn to_native(
		&mut self,
		native_parent: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let result = self.platform.to_native(native_parent, &self.direction);

		if result.is_ok() {
			self.data_holder_updates_slot.exec_for(DataHolderUpdate::WholeData);
		}

		return result;
	}

	fn draw(&mut self) {
		self.widgets.iter_mut().for_each(|w| w.draw());
	}
}
