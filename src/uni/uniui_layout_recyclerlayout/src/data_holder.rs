use crate::DataHolderUpdate;

use uniui_gui::prelude::*;

/// Trait provides information to [RecyclerLayout](crate::RecyclerLayout) information
/// about elements count, creates Widgets and fill widgets with data
///
/// More infor will be provided later
pub trait DataHolder<W: Widget>: DataProcessor {
	/// Returns count of holding elements
	fn count(&self) -> usize;

	/// Provides slot for notificatoins about element changes
	///
	/// The method will be called once by [RecyclerLayout](crate::RecyclerLayout)
	///
	/// Provided slot have to be used to inform [RecyclerLayout](crate::RecyclerLayout)
	/// whenever holding elements changes: new elements comes, existent elements updated,
	/// elements removal, etc.
	fn setup_update_slot(
		&mut self,
		slot: &dyn Slot<DataHolderUpdate>,
	);

	/// [RecyclerLayout](crate::RecyclerLayout) request holder to fill up provided
	/// widget by data for element with provided id
	fn fill(
		&self,
		id: usize,
		w: &mut W,
	);

	/// [RecyclerLayout](crate::RecyclerLayout) request holder to generate new widget
	fn new_widget(&mut self) -> W;
}
