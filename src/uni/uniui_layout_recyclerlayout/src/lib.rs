#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_gui;

#[derive(Clone, Debug, Copy)]
pub enum DataHolderUpdate {
	WholeData,
	UpdatedElement(usize),
}

mod data_holder;
pub use self::data_holder::DataHolder;

mod recycler_layout;
pub use self::recycler_layout::RecyclerLayout;

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_recycler_layout;
	pub use self::wasm_recycler_layout::WasmRecyclerLayout as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_recycler_layout;
	pub use self::qt5_recycler_layout::Qt5RecyclerLayout as Platform;
}

pub mod prelude {
	pub use crate::{
		DataHolder,
		RecyclerLayout,
	};
	pub use uniui_gui::Orientation;
	pub use DataHolderUpdate;
}
