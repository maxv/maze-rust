use crate::DataHolder;

use uniui_gui::{
	prelude::*,
	Orientation as Direction,
};

pub struct Qt5RecyclerLayout {}

impl Qt5RecyclerLayout {
	pub fn new() -> Qt5RecyclerLayout {
		return Qt5RecyclerLayout {};
	}

	pub fn update_whole_data<W: Widget, H: DataHolder<W>>(
		&mut self,
		_app: &mut dyn Application,
		_data_holder: &mut H,
		_widgets: &mut Vec<W>,
	) {
	}

	pub fn to_native(
		&mut self,
		_native_parent: &mut dyn WidgetGenerator,
		_direction: &Direction,
	) -> Result<NativeWidget, ()> {
		Err(())
	}
}
