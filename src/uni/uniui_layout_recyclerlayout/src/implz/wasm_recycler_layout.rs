use crate::DataHolder;

use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
	Orientation as Direction,
};

use crate::implz::web_sys::HtmlDivElement;

use crate::implz::wasm_bindgen::JsCast;

use std::cmp::Ordering;

pub struct WasmRecyclerLayout {
	native: OptionalNative<HtmlDivElement>,
}

impl WasmRecyclerLayout {
	pub fn new() -> WasmRecyclerLayout {
		return WasmRecyclerLayout {
			native: OptionalNative::new(),
		};
	}

	pub fn update_whole_data<W: Widget, H: DataHolder<W>>(
		&mut self,
		app: &mut dyn Application,
		data_holder: &mut H,
		widgets: &mut Vec<W>,
	) {
		let holder_count = data_holder.count();
		let widgets_count = widgets.len();
		match widgets_count.cmp(&holder_count) {
			Ordering::Less => {
				let additional_count = holder_count - widgets_count;
				widgets.reserve_exact(additional_count);
				let widget_generator = app.widget_generator();
				for _ in 0..additional_count {
					let mut w = data_holder.new_widget();
					let native = w.to_native(widget_generator);
					match native {
						Ok(e) => {
							match self.native.as_mut() {
								Some(div) => {
									match div.append_child(&e) {
										Ok(_) => trace!("new widget ok"),
										Err(e) => {
											error!("new widget inssert err:{:?}", e)
										},
									}
								},
								None => {},
							}
						},
						Err(e) => error!("to native err:{:?}", e),
					}

					widgets.push(w);
				}
			},
			Ordering::Greater => widgets.truncate(holder_count),
			Ordering::Equal => {},
		}
	}

	pub fn to_native(
		&mut self,
		native_parent: &mut dyn WidgetGenerator,
		direction: &Direction,
	) -> Result<NativeWidget, ()> {
		let node = native_parent.create_element("div").unwrap();
		let div = node.dyn_into::<HtmlDivElement>().unwrap();
		let style = div.style();
		style.set_property_silent("display", "flex");
		match direction {
			Direction::Horizontal => style.set_property_silent("flex-direction", "row"),
			Direction::Vertical => style.set_property_silent("flex-direction", "column"),
		};
		style.set_property_silent("overflow", "auto");
		style.set_property_silent("flex-wrap", "nowrap");

		self.native.replace(div.clone());
		return Ok(From::from(div));
	}
}
