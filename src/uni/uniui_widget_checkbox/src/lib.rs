mod check_box;
pub use self::check_box::CheckBox;


#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_check_box;
	pub use self::wasm_check_box::WasmCheckBox as Platform;
}

pub const THEME_CLASS_NAME: &str = "u_checkbox_theme_class";

#[cfg(target_os = "linux")]
mod implz {
	uniui_gui::desktop_qt! {
		mod qt5_check_box;
		pub use self::qt5_check_box::Qt5CheckBox as Platform;
	}

	uniui_gui::desktop_noop! {
		mod noop_checkbox;
		pub use self::noop_checkbox::NoopCheckbox as Platform;
	}
}

#[doc(hidden)]
#[cfg(target_os = "android")]
extern crate uni_tmp_jni as jni;

#[doc(hidden)]
#[cfg(target_os = "android")]
pub mod implz {
	// it's pub to provide access to inner functions
	// it's necessary to reexport them from the main library until
	// https://github.com/rust-lang/rfcs/issues/2771
	pub mod android;
	pub use self::android::AndroidCheckBox as Platform;
}
