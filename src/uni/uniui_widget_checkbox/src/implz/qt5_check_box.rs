use uniui_gui::{
	implz::qt5::Qt5OptionalNative,
	NativeWidget,
	WidgetGenerator,
};

use std::{
	ffi::{
		c_void,
		CString,
	},
	os::raw::c_char,
	sync::mpsc::Sender,
};

extern "C" {
	fn uniui_widget_checkbox_create(
		placehoder: *const c_char,
		state: bool,
	) -> *mut c_void;

	fn uniui_widget_checkbox_set_checked_state_listener(
		qcheckbox: *mut c_void,
		sender: *mut c_void,
		callback: extern "C" fn(bool, *mut c_void),
	);

	fn uniui_widget_checkbox_set_checked(
		qcheckbox: *mut c_void,
		checked_state: bool,
	);

	fn uniui_widget_checkbox_set_text(
		qcheckbox: *mut c_void,
		text: *const c_char,
	);
}

extern "C" fn callback(
	state: bool,
	sender: *mut std::ffi::c_void,
) {
	let sender: &mut Sender<bool> = unsafe { &mut *(sender as *mut Sender<bool>) };
	match sender.send(state) {
		Ok(()) => log::trace!("send ok"),
		Err(e) => log::error!("send err:{:?}", e),
	};
}

pub struct Qt5CheckBox {
	checked_state_changed_sender: Sender<bool>,
	native: Qt5OptionalNative,
}

impl Qt5CheckBox {
	pub fn new(sender_changed: Sender<bool>) -> Qt5CheckBox {
		return Qt5CheckBox {
			checked_state_changed_sender: sender_changed,
			native: Qt5OptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		text: &str,
		checked: &bool,
		_document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let ptr = unsafe {
			let cstring_text =
				CString::new(text.to_string()).unwrap_or(CString::default());
			let raw_char = cstring_text.into_raw();
			let native = uniui_widget_checkbox_create(raw_char, *checked);
			self.native.replace(native);
			CString::from_raw(raw_char);

			uniui_widget_checkbox_set_checked_state_listener(
				native,
				&mut self.checked_state_changed_sender as *mut _ as *mut std::ffi::c_void,
				callback,
			);

			native
		};

		return Ok(ptr);
	}

	pub fn set_is_checked(
		&mut self,
		val: bool,
	) {
		if let Some(native) = self.native.as_mut() {
			unsafe {
				uniui_widget_checkbox_set_checked(native, val);
			}
		}
	}

	pub fn set_title(
		&mut self,
		title: &str,
	) {
		if let Some(native) = self.native.as_mut() {
			unsafe {
				let cstring_text =
					CString::new(title.to_string()).unwrap_or(CString::default());
				let raw_char = cstring_text.into_raw();
				uniui_widget_checkbox_set_text(native, raw_char);
				CString::from_raw(raw_char);
			}
		}
	}
}
