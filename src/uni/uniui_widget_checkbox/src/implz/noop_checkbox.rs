use uniui_gui::{
	NativeWidget,
	WidgetGenerator,
};

use std::{
	sync::mpsc::Sender,
};

pub struct NoopCheckbox {}

impl NoopCheckbox {
	pub fn new(_sender_changed: Sender<bool>) -> NoopCheckbox {
		return NoopCheckbox {};
	}

	pub fn to_native(
		&mut self,
		_text: &str,
		_checked: &bool,
		_document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}

	pub fn set_is_checked(
		&mut self,
		_val: bool,
	) {
	}

	pub fn set_title(
		&mut self,
		_title: &str,
	) {
	}
}
