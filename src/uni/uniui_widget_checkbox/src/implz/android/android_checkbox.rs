use std::sync::mpsc::Sender;

use uniui_gui::{
	implz::android::{
		jni_error::JniError,
		AndroidOptionalNative,
	},
	NativeWidget,
	WidgetGenerator,
};

use super::native_checkbox::{
	CheckBox,
	NativeCheckBox,
};

pub struct AndroidCheckBox {
	checked_state_changed_sender: Sender<bool>,
	native: AndroidOptionalNative<NativeCheckBox>,
}

impl AndroidCheckBox {
	pub fn new(checked_state_changed_sender: Sender<bool>) -> AndroidCheckBox {
		return AndroidCheckBox {
			checked_state_changed_sender,
			native: AndroidOptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		text: &str,
		checked: &bool,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let mut ton = || -> Result<NativeCheckBox, JniError> {
			let checkbox = CheckBox::new_for_context(document.context())?;
			checkbox.set_native(self.checked_state_changed_sender.clone())?;
			checkbox.set_text(text)?;
			checkbox.set_checked(*checked)?;
			self.native.replace(&checkbox)?;
			Ok(checkbox)
		};
		return match ton() {
			Ok(b) => Ok(b.into_global_ref()),
			Err(e) => {
				// clear exception?
				log::error!("to_native err:{:?}", e);
				Err(())
			},
		};
	}

	pub fn set_is_checked(
		&mut self,
		is_checked: bool,
	) {
		if let Some(n) = self.native.get() {
			if let Err(e) = n.set_checked(is_checked) {
				log::error!("set_is_checked failed err:{:?}", e);
			}
		}
	}

	pub fn set_title(
		&mut self,
		title: &str,
	) {
		if let Some(n) = self.native.get() {
			if let Err(e) = n.set_text(title) {
				log::error!("set_text failed err:{:?}", e);
			}
		}
	}
}
