mod android_checkbox;
pub use android_checkbox::AndroidCheckBox;

mod native_checkbox;

pub fn checked_changed(
	_: jni::JNIEnv,
	_: jni::objects::JClass,
	sender_ptr: jni::sys::jlong,
	is_checked: jni::sys::jboolean,
) -> jni::sys::jlong {
	use std::sync::mpsc::Sender;

	let sender = unsafe { Box::from_raw(sender_ptr as *mut Sender<bool>) };
	let _ = sender.send(match is_checked {
		0 => false,
		_ => true,
	});
	return Box::into_raw(sender) as i64;
}

pub fn free(
	_: jni::JNIEnv,
	_: jni::objects::JClass,
	sender_ptr: jni::sys::jlong,
) {
	use std::sync::mpsc::Sender;
	let _to_be_killed = unsafe { Box::from_raw(sender_ptr as *mut Sender<bool>) };
}
