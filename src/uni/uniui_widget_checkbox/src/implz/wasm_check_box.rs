use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::{
	HtmlInputElement,
	HtmlLabelElement,
	Node,
	Text,
};

use crate::implz::wasm_bindgen::closure::Closure;

use crate::implz::wasm_bindgen::JsCast;

use std::sync::mpsc::Sender;


pub struct WasmCheckBox {
	text: Option<Text>,
	native_label: OptionalNative<HtmlLabelElement>,
	native_checkbox: OptionalNative<HtmlInputElement>,
	closure: Option<Closure<dyn FnMut()>>,
	sender: Sender<bool>,
}

impl WasmCheckBox {
	pub fn new(sender_changed: Sender<bool>) -> WasmCheckBox {
		return WasmCheckBox {
			native_checkbox: OptionalNative::new(),
			native_label: OptionalNative::new(),
			closure: None,
			text: None,
			sender: sender_changed,
		};
	}

	pub fn to_native(
		&mut self,
		text: &str,
		is_checked: &bool,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let node = document.create_element("input").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let input = node.dyn_into::<HtmlInputElement>().unwrap();
		input.set_type("checkbox");
		input.set_checked(*is_checked);

		let closure = {
			let input_clone = input.clone();
			let sender_clone = self.sender.clone();
			Closure::wrap(Box::new(move || {
				sender_clone.send_silent(input_clone.checked());
			}) as Box<dyn FnMut()>)
		};
		input.set_onchange(Some(closure.as_ref().unchecked_ref()));
		self.closure = Some(closure);
		self.native_checkbox.replace(input.clone());

		let label = document.create_element("label").unwrap();
		let node = input.dyn_into::<Node>().unwrap();
		label.append_child(&node).unwrap();

		let text = Text::new_with_data(text).unwrap();
		self.text = Some(text.clone());
		let node = text.dyn_into::<Node>().unwrap();
		label.append_child(&node).unwrap();

		let label = label.dyn_into::<HtmlLabelElement>().unwrap();
		self.native_label.replace(label.clone());

		Ok(From::from(label))
	}

	pub fn set_is_checked(
		&mut self,
		val: bool,
	) {
		match self.native_checkbox.as_mut() {
			Some(n) => n.set_checked(val),
			None => log::warn!("set_is_checked: native is empty"),
		}
	}

	pub fn set_title(
		&mut self,
		title: &str,
	) {
		self.text.iter().for_each(|t| t.set_data(title));
	}
}
