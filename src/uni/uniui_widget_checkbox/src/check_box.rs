use crate::implz::Platform;

use uniui_gui::prelude::*;

use std::sync::mpsc::{
	self,
	Receiver,
};

#[derive(uniui_gui::DataProcessor)]
#[uprocess_self(data_process)]
pub struct CheckBox {
	#[uproperty_public_slot(slot_set_title)]
	#[uproperty_process(on_title_changed)]
	title: Property<String>,

	#[uproperty_process(on_checked_changed)]
	#[uproperty_public_signal(signal_checkstate_changed)]
	#[uproperty_public_slot(slot_set_checked)]
	checked: Property<bool>,

	receiver_changed: Receiver<bool>,
	platform: Platform,
}

impl CheckBox {
	pub fn new(
		title: String,
		default_value: bool,
	) -> CheckBox {
		let (sender_changed, receiver_changed) = mpsc::channel();

		return CheckBox {
			checked: Property::new(default_value),
			receiver_changed,
			platform: Platform::new(sender_changed),
			title: Property::new(title),
		};
	}

	pub fn data_process(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		if let Some(checked) = self.receiver_changed.try_iter().last() {
			self.checked.set(checked);
		};
	}

	fn on_checked_changed(&mut self) {
		self.platform.set_is_checked(*self.checked.as_ref());
	}

	fn on_title_changed(&mut self) {
		self.platform.set_title(self.title.as_ref());
	}
}

impl Widget for CheckBox {
	fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(
			self.title.as_ref(),
			self.checked.as_ref(),
			document,
		);
	}
}
