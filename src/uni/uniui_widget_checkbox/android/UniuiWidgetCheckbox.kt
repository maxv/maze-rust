// Buildsystem will put all our files into the package with the 
// same name as our crate's name is.
package uniui_widget_checkbox

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.widget.CompoundButton
import android.widget.CheckBox

@SuppressLint("AppCompatCustomView")
class UniuiWidgetCheckbox(context: Context?) : CheckBox(context) {
    init {
        setOnCheckedChangeListener(
            MyWatcher()
        )
    }

    inner class MyWatcher: CompoundButton.OnCheckedChangeListener {

        override fun onCheckedChanged(
            buttonView: CompoundButton,
            isChecked: Boolean
        ): Unit {
         if (native != 0L) {
                native = checkedChanged(native, isChecked);
            }
        }

    }

    protected fun finalize() {
        val old_native = native
        native = 0
        free(old_native)
    }

    private external fun checkedChanged(native: Long, isChecked: Boolean): Long
    private external fun free(native: Long)

    // public to simplify JNI access
    public var native: Long = 0
}

