#include <QApplication>
#include <QCheckBox>
#include <QWidget>


extern "C" void* uniui_widget_checkbox_create(char* text, bool state) {
	QCheckBox* result = new QCheckBox(text);
	result->setCheckState(state? Qt::Unchecked : Qt::Checked);
	return result;
}

namespace uniui_widget_checkbox{
typedef void (*sender_callback)(bool, void*);
}

extern "C" void uniui_widget_checkbox_set_checked_state_listener(
	void* qlineedit,
	void* data,
	uniui_widget_checkbox::sender_callback callback
) {
	QObject::connect(
		((QCheckBox*)qlineedit),
		&QCheckBox::stateChanged,
		[=](const int state){
			callback(
				state == Qt::Checked,
				data
			);
		}
	);
}

extern "C" void uniui_widget_checkbox_set_checked(void* qcheckbox, bool state) {
	((QCheckBox*)qcheckbox)->setCheckState(state? Qt::Unchecked : Qt::Checked);
}

extern "C" void uniui_widget_checkbox_set_text(void* qcheckbox, const char* text) {
	((QCheckBox*)qcheckbox)->setText(text);
}

