#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_file;
extern crate uniui_gui;

mod file_selector;
pub use self::file_selector::FileSelector;

pub const THEME_CLASS_NAME: &str = "u_fileselector_theme_class";

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_file_selector;
	pub use self::wasm_file_selector::WasmFileSelector as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_file_selector;
	pub use self::qt5_file_selector::Qt5FileSelector as Platform;
}
