use uniui_file::File;

use uniui_gui::prelude::*;

use std::sync::mpsc::Sender;


pub struct Qt5FileSelector {}

impl Qt5FileSelector {
	pub fn new(_sender: Sender<Vec<File>>) -> Qt5FileSelector {
		return Qt5FileSelector {};
	}

	pub fn to_native(
		&mut self,
		_document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		error!("not implemented");
		return Err(());
	}
}
