use uniui_file::File;

use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::HtmlInputElement;

use crate::implz::wasm_bindgen::closure::Closure;

use crate::implz::wasm_bindgen::JsCast;

use std::sync::mpsc::Sender;


pub struct WasmFileSelector {
	native: OptionalNative<HtmlInputElement>,
	closure: Option<Closure<dyn FnMut()>>,
	sender: Sender<Vec<File>>,
}

impl WasmFileSelector {
	pub fn new(sender: Sender<Vec<File>>) -> WasmFileSelector {
		return WasmFileSelector {
			closure: None,
			sender,
			native: OptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let node = document.create_element("input").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);

		let input = node.dyn_into::<HtmlInputElement>().unwrap();
		input.set_type("file");

		let input_clone = input.clone();
		let sender = self.sender.clone();
		let closure = Closure::wrap(Box::new(move || {
			match input_clone.files() {
				Some(file_list) => {
					let files_count = file_list.length();
					let mut result = Vec::with_capacity(files_count as usize);
					for id in 0..files_count {
						match file_list.get(id) {
							Some(web_file) => {
								let f = File::new(web_file);
								result.push(f);
							},
							None => error!("no file for id:{}", id),
						}
					}
					sender.send_silent(result);
				},
				None => warn!("files is NONE"),
			};
		}) as Box<dyn FnMut()>);

		input.set_onchange(Some(closure.as_ref().unchecked_ref()));

		self.native.replace(input.clone());
		self.closure = Some(closure);
		Ok(From::from(input))
	}
}
