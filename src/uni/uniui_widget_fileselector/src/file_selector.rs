use crate::implz::Platform;

use uniui_file::File;

use uniui_gui::prelude::*;

use std::sync::mpsc::{
	self,
	Receiver,
};

pub struct FileSelector {
	file_selected_receiver: Receiver<Vec<File>>,
	signal_files_selected: Signal<Vec<File>>,
	platform: Platform,
}

impl FileSelector {
	pub fn new() -> FileSelector {
		let (file_selected_sender, file_selected_receiver) = mpsc::channel();

		return FileSelector {
			file_selected_receiver,
			signal_files_selected: Signal::new(),
			platform: Platform::new(file_selected_sender),
		};
	}

	pub fn signal_files_selected(&mut self) -> &mut Signal<Vec<File>> {
		return &mut self.signal_files_selected;
	}
}

impl Widget for FileSelector {
	fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(document);
	}
}

impl DataProcessor for FileSelector {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		if let Some(v) = self.file_selected_receiver.try_iter().last() {
			self.signal_files_selected.emit(v);
		}
	}
}
