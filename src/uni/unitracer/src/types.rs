use serde::{
	Deserialize,
	Serialize,
};

#[derive(Serialize, Deserialize, Copy, Clone, Debug, PartialEq, PartialOrd)]
pub enum TagType {
	String = 0,
	Double = 1,
	Bool = 2,
	Long = 3,
	Binary = 4,
}

#[derive(Serialize, Deserialize, Copy, Clone, Debug, PartialEq, PartialOrd)]
pub enum SpanRefType {
	ChildOf = 0,
	FollowsFrom = 1,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, PartialOrd)]
pub struct Tag {
	pub key: String,
	pub v_type: TagType,
	pub v_str: Option<String>,
	pub v_double: Option<f64>,
	pub v_bool: Option<bool>,
	pub v_long: Option<i64>,
	pub v_binary: Option<Vec<u8>>,
}

impl Tag {
	pub fn new<F3, F4, F5, F6, F7>(
		key: String,
		v_type: TagType,
		v_str: F3,
		v_double: F4,
		v_bool: F5,
		v_long: F6,
		v_binary: F7,
	) -> Tag
	where
		F3: Into<Option<String>>,
		F4: Into<Option<f64>>,
		F5: Into<Option<bool>>,
		F6: Into<Option<i64>>,
		F7: Into<Option<Vec<u8>>>,
	{
		Tag {
			key,
			v_type,
			v_str: v_str.into(),
			v_double: v_double.into(),
			v_bool: v_bool.into(),
			v_long: v_long.into(),
			v_binary: v_binary.into(),
		}
	}
}


#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, PartialOrd)]
pub struct Log {
	pub timestamp: i64,
	pub fields: Vec<Tag>,
}

impl Log {
	pub fn new(
		timestamp: i64,
		fields: Vec<Tag>,
	) -> Log {
		Log {
			timestamp,
			fields,
		}
	}
}

// SpanRef
//

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, PartialOrd)]
pub struct SpanRef {
	pub ref_type: SpanRefType,
	pub trace_id_low: i64,
	pub trace_id_high: i64,
	pub span_id: i64,
}

impl SpanRef {
	pub fn new(
		ref_type: SpanRefType,
		trace_id_low: i64,
		trace_id_high: i64,
		span_id: i64,
	) -> SpanRef {
		SpanRef {
			ref_type,
			trace_id_low,
			trace_id_high,
			span_id,
		}
	}
}

// Span
//

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, PartialOrd)]
pub struct Span {
	pub trace_id_low: i64,
	pub trace_id_high: i64,
	pub span_id: i64,
	pub parent_span_id: i64,
	pub operation_name: String,
	pub references: Option<Vec<SpanRef>>,
	pub flags: i32,
	pub start_time: i64,
	pub duration: i64,
	pub tags: Option<Vec<Tag>>,
	pub logs: Option<Vec<Log>>,
}

impl Span {
	pub fn new<F6, F10, F11>(
		trace_id_low: i64,
		trace_id_high: i64,
		span_id: i64,
		parent_span_id: i64,
		operation_name: String,
		references: F6,
		flags: i32,
		start_time: i64,
		duration: i64,
		tags: F10,
		logs: F11,
	) -> Span
	where
		F6: Into<Option<Vec<SpanRef>>>,
		F10: Into<Option<Vec<Tag>>>,
		F11: Into<Option<Vec<Log>>>,
	{
		Span {
			trace_id_low,
			trace_id_high,
			span_id,
			parent_span_id,
			operation_name,
			references: references.into(),
			flags,
			start_time,
			duration,
			tags: tags.into(),
			logs: logs.into(),
		}
	}
}

// Process
//

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, PartialOrd)]
pub struct Process {
	pub service_name: String,
	pub tags: Option<Vec<Tag>>,
}

impl Process {
	pub fn new<F2>(
		service_name: String,
		tags: F2,
	) -> Process
	where
		F2: Into<Option<Vec<Tag>>>,
	{
		Process {
			service_name,
			tags: tags.into(),
		}
	}
}

// Batch
//

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, PartialOrd)]
pub struct Batch {
	pub process: Process,
	pub spans: Vec<Span>,
}

impl Batch {
	pub fn new(
		process: Process,
		spans: Vec<Span>,
	) -> Batch {
		Batch {
			process,
			spans,
		}
	}
}
