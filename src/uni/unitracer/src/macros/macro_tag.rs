#[macro_export]
macro_rules! tag_empty {
	($key:expr) => {
		unitracer::TRACER.with(|t| {
			t.borrow_mut().add_empty_tag_to_current_span($key);
			});
	};
}

#[macro_export]
macro_rules! tag_bool {
	($key:expr, $value:expr) => {
		unitracer::TRACER.with(|t| {
			t.borrow_mut().add_bool_tag_to_current_span($key, $value);
			});
	};
}

#[macro_export]
macro_rules! tag_binary {
	($key:expr, $value:expr) => {
		unitracer::TRACER.with(|t| {
			t.borrow_mut().add_binary_tag_to_current_span($key, $value);
			});
	};
}

#[macro_export]
macro_rules! tag_f64 {
	($key:expr, $value:expr) => {
		unitracer::TRACER.with(|t| {
			t.borrow_mut().add_f64_tag_to_current_span($key, $value);
			});
	};
}

#[macro_export]
macro_rules! tag_string {
	($key:expr, $value:expr) => {
		unitracer::TRACER.with(|t| {
			t.borrow_mut().add_string_tag_to_current_span($key, $value);
			});
	};
}

#[macro_export]
macro_rules! tag_i64 {
	($key:expr, $value:expr) => {
		unitracer::TRACER.with(|t| {
			t.borrow_mut().add_i64_tag_to_current_span($key, $value as i64);
			});
	};
}
