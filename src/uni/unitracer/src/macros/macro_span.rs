#[macro_export]
macro_rules! start_trace {
	($x:ident) => {
		let span = unitracer::TRACER.with(|t| {
			let span = {
				// to avoid span destruction before borrow_mut is done
				t.borrow_mut().start_new_span(stringify!($x).to_string(), true)
				};
			span
			});
	};
}

#[macro_export]
macro_rules! span {
	($x:ident) => {
		let span = unitracer::TRACER.with(|t| {
			let span = {
				// to avoid span destruction before borrow_mut is done
				t.borrow_mut().start_new_span(stringify!($x).to_string(), false)
				};
			span
			});
	};
}

#[macro_export]
macro_rules! get_span_context {
	() => {{
		let span_context = unitracer::TRACER.with(|t| t.borrow_mut().get_span_context());
		span_context
		}};
}

#[macro_export]
macro_rules! span_from_context {
	($context:expr, $x:ident) => {
		let span = unitracer::TRACER.with(|t| {
			let span = {
				// to avoid span destruction before borrow_mut is done
				t.borrow_mut()
					.start_new_span_from_context(stringify!($x).to_string(), $context)
				};
			span
			});
	};
}
