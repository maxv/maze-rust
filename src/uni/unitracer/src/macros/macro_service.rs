#[macro_export]
macro_rules! service_global {
	($x:ident) => {
		match unitracer::SERVICE_NAME_GLOBAL.lock() {
			Ok(mut v) => *v = stringify!($x),
			Err(e) => println!("Couldn't setyp global service name e:{:?}", e),
			}
	};
}

#[macro_export]
macro_rules! service_for_thread {
	($x:ident) => {
		unitracer::TRACER.with(|t| t.borrow_mut().set_service_name(stringify!($x)));
	};
}
