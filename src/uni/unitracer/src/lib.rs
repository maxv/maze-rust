#[macro_use]
extern crate lazy_static;

extern crate unitime;

extern crate log;
extern crate rand;
extern crate serde;

use std::{
	cell::RefCell,
	rc::Rc,
};

mod trace_logger;
use trace_logger::TraceLogger;

mod types;
pub use types::{
	Batch,
	Log,
	Process,
	Span,
	SpanRef,
	SpanRefType,
	Tag,
	TagType,
};


mod tracer;
pub use tracer::{
	SpanContext,
	SpanWrapper,
	Tracer,
};

mod trace_sender;
pub use trace_sender::{
	EmptyTraceSender,
	TraceSender,
};

mod macros;

mod global_variables;
pub use global_variables::{
	DEFAULT_SERVICE_NAME,
	SERVICE_NAME_GLOBAL,
	TRACER,
	TRACE_SENDER,
	TRACE_SENDER_GLOBAL_FACTORY,
};

pub trait TraceSenderFactory {
	fn call(&mut self) -> Rc<RefCell<dyn TraceSender>>;
}

pub type BoxedTraceSenderFactory =
	Box<dyn TraceSenderFactory + 'static + std::marker::Send>;


pub fn init_logger(log_level: log::LevelFilter) {
	let r = log::set_boxed_logger(Box::new(TraceLogger {}));
	if r.is_ok() {
		log::set_max_level(log_level);
	}
	println!("logger activated");
}

pub fn set_trace_sender_factory(f: BoxedTraceSenderFactory) {
	match TRACE_SENDER_GLOBAL_FACTORY.lock() {
		Ok(mut mg) => *mg = Some(f),
		Err(e) => println!("<unitracer> set_factory failed e:{:?}", e),
	}
}
