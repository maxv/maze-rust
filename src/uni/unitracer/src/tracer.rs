use crate::{
	Batch,
	Log,
	Process,
	Span,
	Tag,
	TagType,
	TRACE_SENDER,
};

use std::collections::LinkedList;


pub struct SpanContext {
	pub trace_id_low: i64,
	pub trace_id_high: i64,
	pub span_id: i64,
}

pub struct SpanWrapper {}

impl Drop for SpanWrapper {
	fn drop(&mut self) {
		crate::TRACER.with(|t| {
			t.borrow_mut().drop_last();
		});
	}
}

static HIGH_ID_AVAILABLE: bool = false;

pub struct Tracer {
	spans: LinkedList<Span>,
	service_name: &'static str,
}

impl Tracer {
	pub fn new() -> Tracer {
		let service_name = match crate::SERVICE_NAME_GLOBAL.lock() {
			Ok(v) => *v,
			Err(e) => {
				println!("couldn't get global service name e:{:?}", e);
				crate::DEFAULT_SERVICE_NAME
			},
		};

		return Tracer {
			spans: LinkedList::new(),
			service_name,
		};
	}

	pub fn start_new_span(
		&mut self,
		operation_name: String,
		force_new_trace: bool,
	) -> SpanWrapper {
		let parent_span_context = match force_new_trace {
			false => self.get_span_context(),
			true => None,
		};

		let result =
			self.start_new_span_from_context(operation_name, parent_span_context);
		return result;
	}

	pub fn get_span_context(&self) -> Option<SpanContext> {
		return match self.spans.back() {
			Some(span) => {
				Some(SpanContext {
					trace_id_low: span.trace_id_low,
					trace_id_high: span.trace_id_high,
					span_id: span.span_id,
				})
			},
			None => None,
		};
	}

	pub fn start_new_span_from_context(
		&mut self,
		operation_name: String,
		parent_span_context: Option<SpanContext>,
	) -> SpanWrapper {
		let span = self.create_span(operation_name, parent_span_context);
		self.spans.push_back(span);
		return SpanWrapper {};
	}

	pub fn set_service_name(
		&mut self,
		s: &'static str,
	) {
		self.service_name = s;
	}

	pub fn add_log(
		&mut self,
		record: &log::Record,
	) {
		let timestamp = unitime::msec_since_epoch();
		let tags = self.log_record_to_tags(record);
		let new_log = Log::new(timestamp, tags);

		match self.spans.back_mut() {
			Some(span) => Self::add_log_to_span(span, new_log),
			None => {
				let mut tmp_span = self.create_span("missed logs".to_string(), None);
				Self::add_log_to_span(&mut tmp_span, new_log);
				self.drop_span(tmp_span);
			},
		};
	}

	pub fn add_empty_tag_to_current_span(
		&mut self,
		key: String,
	) {
		let new_tag = Tag::new(key, TagType::Bool, None, None, None, None, None);
		self.add_tag_to_current_span(new_tag);
	}

	pub fn add_bool_tag_to_current_span(
		&mut self,
		key: String,
		value: bool,
	) {
		let new_tag = Tag::new(key, TagType::Bool, None, None, Some(value), None, None);
		self.add_tag_to_current_span(new_tag);
	}

	pub fn add_f64_tag_to_current_span(
		&mut self,
		key: String,
		value: f64,
	) {
		let new_tag = Tag::new(key, TagType::Double, None, Some(value), None, None, None);
		self.add_tag_to_current_span(new_tag);
	}

	pub fn add_binary_tag_to_current_span(
		&mut self,
		key: String,
		value: Vec<u8>,
	) {
		let new_tag = Tag::new(key, TagType::Binary, None, None, None, None, Some(value));
		self.add_tag_to_current_span(new_tag);
	}

	pub fn add_string_tag_to_current_span(
		&mut self,
		key: String,
		value: String,
	) {
		let new_tag = Tag::new(key, TagType::String, Some(value), None, None, None, None);
		self.add_tag_to_current_span(new_tag);
	}

	pub fn add_i64_tag_to_current_span(
		&mut self,
		key: String,
		value: i64,
	) {
		let new_tag = Tag::new(key, TagType::Long, None, None, None, Some(value), None);
		self.add_tag_to_current_span(new_tag);
	}

	pub fn drop_last(&mut self) {
		match self.spans.pop_back() {
			Some(span) => self.drop_span(span),
			None => println!("Drop NONE!"),
		}
	}

	fn drop_span(
		&self,
		mut span: Span,
	) {
		let end_time = unitime::msec_since_epoch();
		span.duration = end_time - span.start_time;
		let process = Process::new(
			self.service_name.to_string(), // service name
			None,                          // tags
		);
		let batch = Batch::new(process, vec![span]);
		TRACE_SENDER.with(move |s| {
			s.borrow_mut().send_batch(batch);
		});
	}

	fn add_log_to_span(
		span: &mut Span,
		new_log: Log,
	) {
		match span.logs.as_mut() {
			Some(logs) => {
				logs.push(new_log);
			},
			None => {
				// FIXME(#92)
				println!("no logs");
			},
		}
	}

	fn log_record_to_tags(
		&self,
		record: &log::Record,
	) -> Vec<Tag> {
		let mut tags = Vec::new();

		tags.push(Tag::new(
			"level".to_string(),
			TagType::String,
			Some(record.level().to_string()),
			None,
			None,
			None,
			None,
		));
		tags.push(Tag::new(
			"message".to_string(),
			TagType::String,
			Some(record.args().to_string()),
			None,
			None,
			None,
			None,
		));
		match record.module_path() {
			Some(v) => {
				tags.push(Tag::new(
					"module".to_string(),
					TagType::String,
					Some(v.to_string()),
					None,
					None,
					None,
					None,
				))
			},
			None => {},
		}

		match record.file() {
			Some(v) => {
				tags.push(Tag::new(
					"file".to_string(),
					TagType::String,
					Some(v.to_string()),
					None,
					None,
					None,
					None,
				))
			},
			None => {},
		}

		match record.line() {
			Some(v) => {
				tags.push(Tag::new(
					"line".to_string(),
					TagType::Long,
					None,
					None,
					None,
					Some(v as i64),
					None,
				))
			},
			None => {},
		}

		return tags;
	}

	fn add_tag_to_current_span(
		&mut self,
		new_tag: Tag,
	) {
		match self.spans.back_mut() {
			Some(s) => {
				let mut tags = match s.tags.take() {
					Some(t) => t,
					None => Vec::new(),
				};
				tags.push(new_tag);
				s.tags = Some(tags);
			},
			None => {},
		}
	}

	fn create_span(
		&self,
		operation_name: String,
		parent_span_context: Option<SpanContext>,
	) -> Span {
		let span_id = rand::random::<i64>();
		let flags_fix_me = 1;

		let start_time = unitime::msec_since_epoch();

		let (parent_span_id, (trace_id_low, trace_id_high)) = match parent_span_context {
			Some(s) => (s.span_id, (s.trace_id_low, s.trace_id_high)),
			_ => (0, Self::generate_trace_id()),
		};

		let references = None;
		let duration = 0;
		let tags = None;
		return Span::new(
			trace_id_low,
			trace_id_high,
			span_id,
			parent_span_id,
			operation_name,
			references,
			flags_fix_me,
			start_time,
			duration,
			tags,
			Some(Vec::new()),
		);
	}

	fn generate_trace_id() -> (i64, i64) {
		let trace_id_low = rand::random::<i64>();
		let trace_id_high = match HIGH_ID_AVAILABLE {
			true => rand::random::<i64>(),
			false => 0,
		};

		return (trace_id_low, trace_id_high);
	}
}
