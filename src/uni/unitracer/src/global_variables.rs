use crate::{
	BoxedTraceSenderFactory,
	EmptyTraceSender,
	TraceSender,
	Tracer,
};

use std::{
	cell::RefCell,
	rc::Rc,
	sync::Mutex,
};

thread_local!(pub static TRACER: Rc<RefCell<Tracer>> =  {
	Rc::new(RefCell::new(Tracer::new()))
});

lazy_static! {
	pub static ref TRACE_SENDER_GLOBAL_FACTORY: Mutex<Option<BoxedTraceSenderFactory>> =
		Mutex::new(None);
}

thread_local!(pub static TRACE_SENDER: Rc<RefCell<dyn TraceSender>> = {
		match TRACE_SENDER_GLOBAL_FACTORY.lock() {
			Ok(mut mg) => match mg.as_mut() {
				Some(factory) => (*factory).call(),
				None => {
					println!("<unitracer> tracer sender not setted up propery");
					Rc::new(RefCell::new(EmptyTraceSender{}))
				}
			},
			Err(e) => {
				println!("<unitracer> TRACE SENDER init failed e:{:?}", e);
				Rc::new(RefCell::new(EmptyTraceSender{}))
			}
		}
});

pub static DEFAULT_SERVICE_NAME: &str = "UnknownService";

lazy_static! {
	pub static ref SERVICE_NAME_GLOBAL: Mutex<&'static str> =
		Mutex::new(DEFAULT_SERVICE_NAME);
}
