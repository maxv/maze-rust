use crate::Batch;

pub trait TraceSender {
	fn send_batch(
		&mut self,
		batch: Batch,
	);
}

pub struct EmptyTraceSender {}

impl TraceSender for EmptyTraceSender {
	fn send_batch(
		&mut self,
		_: Batch,
	) {
	}
}
