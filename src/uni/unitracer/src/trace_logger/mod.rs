pub struct TraceLogger;

impl log::Log for TraceLogger {
	fn enabled(
		&self,
		_metadata: &log::Metadata,
	) -> bool {
		true
	}

	fn log(
		&self,
		record: &log::Record,
	) {
		if !self.enabled(record.metadata()) {
			return;
		}

		crate::TRACER.with(|t| {
			match t.try_borrow_mut() {
				Ok(mut t) => t.add_log(record),
				Err(_e) => {}, // FIXME(#97)
			}
		});
	}

	fn flush(&self) {
	}
}
