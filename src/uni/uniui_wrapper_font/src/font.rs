use crate::implz::Platform;

use uniui_gui::{
	prelude::*,
	AlignmentHorizontal,
	Color,
	SizeUnit,
};

/// Customize widget's font
pub struct Font<W: Widget> {
	pub widget: W,
	pub weight: Option<u32>,
	pub size: Option<u32>,
	pub color: Option<Color>,
	pub family: Option<String>,
	pub shadow_color: Option<Color>,
	pub shadow_diff: Option<(SizeUnit, SizeUnit)>,
	pub alignment_horizontal: Option<AlignmentHorizontal>,
	pub line_height: Option<SizeUnit>,
}


impl<W: Widget> Widget for Font<W> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let inner_native = self.widget.to_native(widget_generator)?;
		let outer_native = Platform::to_native(
			inner_native,
			&self.size,
			&self.weight,
			&self.color,
			&self.family,
			&self.shadow_color,
			&self.shadow_diff,
			&self.alignment_horizontal,
			&self.line_height,
		);
		return Ok(outer_native);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<W: Widget> DataProcessor for Font<W> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
