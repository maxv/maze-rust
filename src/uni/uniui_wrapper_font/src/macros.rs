#[doc(hidden)]
#[macro_export]
macro_rules! u_font_int {
	($b:ident,size: $va:expr) => {
		$b.size = Some($va);
	};
	($b:ident,color: $va:expr) => {
		$b.color = Some($va);
	};
	($b:ident,weight: $va:expr) => {
		$b.weight = Some($va);
	};
	($b:ident,family: $va:expr) => {
		$b.family = Some($va);
	};
	($b:ident,shadow_color: $va:expr) => {
		$b.shadow_color = Some($va);
	};
	($b:ident,shadow_diff: $va:expr) => {
		$b.shadow_diff = Some($va);
	};
	($b:ident,alignment_horizontal: $va:expr) => {
		$b.alignment_horizontal = Some($va);
	};
	($b:ident,line_height: $va:expr) => {
		$b.line_height = Some($va);
	};
}

/// Simplifies [Font] creation/configuration.
///
/// Parameters:
/// * widget (required): [Widget](uniui_gui::Widget) for which custom font will be
///   created,
/// * size (optional): u32,
/// * color (optional): [Color](uniui_gui::Color),
/// * weight (optional): u32 (effective values 0..1000)
/// * family (optional): String
/// * shadow_color (optional): [Color](uniui_gui::Color),
/// * shadow_diff (optional): ([SizeUnit](uniui_gui::SizeUnit),
///   [SizeUnit](uniui_gui::SizeUnit)) // (x-diff, y-diff)
/// * alignment_horizontal (optional):
///   [AlignmentHorizontal](uniui_gui::AlignmentHorizontal)
/// * line_height (optional): [SizeUnit](uniui_gui::SizeUnit)
///
/// Example:
/// ```
/// let label = u_font!{
///     widget: u_label!{
///         text: "Submarin".to_owned(),
///     },
///     size: 16,
///     weight: 800,
///     color: Color{
///         red: 220,
///         green: 220,
///         blue: 25,
///     },
/// };
/// ```
#[macro_export]
macro_rules! u_font {
	(widget: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut font = $crate::Font{
			widget: $w,
			size: None,
			color: None,
			weight: None,
			family: None,
			shadow_color: None,
			shadow_diff: None,
			alignment_horizontal: None,
			line_height: None,
		};
		$($crate::u_font_int!(font, $param_name: $val);)*;
		font
	}};
}
