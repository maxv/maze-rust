extern crate uniui_core;
extern crate uniui_gui;

mod font;
mod macros;
pub use self::font::Font;


#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_font;
	pub use self::wasm_font::WasmFont as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	uniui_gui::desktop_qt! {
		mod qt5_font;
		pub use self::qt5_font::Qt5Font as Platform;
	}

	uniui_gui::desktop_noop! {
		mod noop_font;
		pub use self::noop_font::NoopFont as Platform;
	}
}

pub mod prelude {
	pub use crate::{
		font::Font,
		u_font,
	};
	pub use uniui_gui::{
		AlignmentHorizontal,
		Color,
		SizeUnit,
	};
}
