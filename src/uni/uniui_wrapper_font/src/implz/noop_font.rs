use uniui_gui::{
	prelude::*,
	AlignmentHorizontal,
	Color,
	SizeUnit,
};

pub struct NoopFont {}

impl NoopFont {
	pub fn to_native(
		widget: NativeWidget,
		_size: &Option<u32>,
		_weight: &Option<u32>,
		_color: &Option<Color>,
		_family: &Option<String>,
		_shadow_color: &Option<Color>,
		_shadow_diff: &Option<(SizeUnit, SizeUnit)>,
		_alignment_horizontal: &Option<AlignmentHorizontal>,
		_line_height: &Option<SizeUnit>,
	) -> NativeWidget {
		return widget;
	}
}
