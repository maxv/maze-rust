use uniui_gui::{
	prelude::SilentCssPropertySetter,
	AlignmentHorizontal,
	Color,
	NativeWidget,
	SizeUnit,
};

pub struct WasmFont {}

impl WasmFont {
	pub fn to_native(
		widget: NativeWidget,
		size: &Option<u32>,
		weight: &Option<u32>,
		color: &Option<Color>,
		family: &Option<String>,
		shadow_color: &Option<Color>,
		shadow_diff: &Option<(SizeUnit, SizeUnit)>,
		alignment_horizontal: &Option<AlignmentHorizontal>,
		line_height: &Option<SizeUnit>,
	) -> NativeWidget {
		let style = widget.style();

		if let Some(s) = size.as_ref() {
			style.set_property_silent("font-size", &format!("{}pt", s));
		}

		if let Some(w) = weight.as_ref() {
			style.set_property_silent("font-weight", &format!("{}", w));
		}

		if let Some(f) = family.as_ref() {
			style.set_property_silent("font-family", &f);
		};

		if let Some(c) = color.as_ref() {
			style.set_property_silent("color", &c.html_string());
		}

		if let Some(c) = shadow_color.as_ref() {
			let (x, y) = shadow_diff.unwrap_or((SizeUnit::PX(0.0), SizeUnit::PX(0.0)));
			let val = format!(
				"{} {} {}",
				c.html_string(),
				x.to_htmlstring(),
				y.to_htmlstring()
			);
			style.set_property_silent("text-shadow", &val);
		}

		let aligment_str = match alignment_horizontal.as_ref() {
			None => "",
			Some(AlignmentHorizontal::Center) => "center",
			Some(AlignmentHorizontal::Start) => "start",
			Some(AlignmentHorizontal::End) => "end",
			Some(AlignmentHorizontal::SpaceAround) => "center",
			Some(AlignmentHorizontal::SpaceBetween) => "center",
			Some(AlignmentHorizontal::Stretch) => "justify",
		};
		style.set_property_silent("text-align", aligment_str);

		if let Some(lh) = line_height.as_ref() {
			lh.apply_as_property_value(&style, "line-height");
		}

		return widget;
	}
}
