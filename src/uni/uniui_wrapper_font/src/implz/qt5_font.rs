use uniui_gui::{
	prelude::*,
	AlignmentHorizontal,
	Color,
	SizeUnit,
};

use std::{
	ffi::{
		c_void,
		CString,
	},
	os::raw::c_char,
};

extern "C" {
	fn uniui_wrapper_font_set_fount(
		style: *const c_char,
		widget_pointer: *mut c_void,
	);
}

pub struct Qt5Font {}

impl Qt5Font {
	pub fn to_native(
		widget: NativeWidget,
		size: &Option<u32>,
		weight: &Option<u32>,
		color: &Option<Color>,
		family: &Option<String>,
		_shadow_color: &Option<Color>,
		_shadow_diff: &Option<(SizeUnit, SizeUnit)>,
		_alignment_horizontal: &Option<AlignmentHorizontal>,
		_line_height: &Option<SizeUnit>,
	) -> NativeWidget {
		let family_string = match family.as_ref() {
			Some(f) => format!("font-family:{};", f),
			None => format!(""),
		};

		let color_string = match color.as_ref() {
			Some(c) => format!("color:{};", c.html_string()),
			None => format!(""),
		};

		let size_string = match size.as_ref() {
			Some(s) => format!("font-size:{}pt;", s),
			None => format!(""),
		};

		let weight_string = match weight.as_ref() {
			Some(w) => format!("font-weight:{};", w),
			None => format!(""),
		};

		let style = format!(
			"*{{
				{size_string}
				{weight_string}
				{font_family}
				{color_string}
			}}",
			size_string = size_string,
			weight_string = weight_string,
			font_family = family_string,
			color_string = color_string,
		);

		unsafe {
			let cstring_style =
				CString::new(style.to_string()).unwrap_or(CString::default());
			let raw_char = cstring_style.into_raw();
			uniui_wrapper_font_set_fount(raw_char, widget);
			CString::from_raw(raw_char);
		};

		return widget;
	}
}
