#include <QWidget>

extern "C" void uniui_wrapper_font_set_fount(const char* style, void* w) {
	QString qstyle = ((QWidget*)w)->styleSheet();
	qstyle += style;
	((QWidget*)w)->setStyleSheet(qstyle);
}
