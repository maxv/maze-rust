#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_gui;
extern crate uniui_layout_linear_layout;
extern crate uniui_widget_button;
extern crate uniui_wrapper_fixedsize;
extern crate uniui_wrapper_visibility;


mod tab_widget;
pub use self::tab_widget::TabWidget;

pub const THEME_CLASS_NAME: &str = "u_tabwidget_theme_class";

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_tab_widget;
	pub use self::wasm_tab_widget::WasmTabWidget as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_tab_widget;
	pub use self::qt5_tab_widget::Qt5TabWidget as Platform;
}
