use crate::implz::Platform;

use uniui_layout_linear_layout::LinearLayout;
use uniui_widget_button::Button;
use uniui_wrapper_fixedsize::u_fixedsize;
use uniui_wrapper_visibility::{
	Visibility,
	VisibilityValue,
};

use uniui_gui::{
	prelude::*,
	Orientation,
	SizeUnit,
};


pub struct TabWidget {
	current_element: usize,
	elements: Vec<(Signal<VisibilityValue>, SlotImpl<()>)>,
	buttons_layout: LinearLayout,
	content_layout: LinearLayout,
	platform: Platform,
}

impl TabWidget {
	pub fn new() -> TabWidget {
		let buttons_layout = LinearLayout::new(Orientation::Horizontal);
		let content_layout = LinearLayout::new(Orientation::Horizontal);

		return TabWidget {
			current_element: 0,
			elements: Vec::new(),
			buttons_layout,
			content_layout,
			platform: Platform::new(),
		};
	}

	pub fn push_widget<T: 'static + Widget>(
		&mut self,
		name: String,
		w: T,
	) {
		let slot_button_clicked = SlotImpl::new();

		self.buttons_layout.push_widget({
			let mut button = Button::new(name);
			button.signal_clicked().connect_slot(&slot_button_clicked);
			button
		});

		let already_visible = match self.elements.len() == 0 {
			true => VisibilityValue::Visible,
			false => VisibilityValue::Collapse,
		};

		let vis = Visibility::new(w, already_visible);
		let mut signal_set_visible = Signal::new();
		signal_set_visible.connect_slot(vis.slot_set_visible());
		self.content_layout.push_widget(u_fixedsize! {
			widget: vis,
			width: SizeUnit::Percent(100.0),
			height: SizeUnit::Percent(100.0),
		});

		self.elements.push((signal_set_visible, slot_button_clicked));
	}
}

impl Widget for TabWidget {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(
			widget_generator,
			&mut self.buttons_layout,
			&mut self.content_layout,
		);
	}

	fn draw(&mut self) {
		self.buttons_layout.draw();
		self.content_layout.draw();
	}
}

impl DataProcessor for TabWidget {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.buttons_layout.process_data(msec_since_app_start, app);
		self.content_layout.process_data(msec_since_app_start, app);
		let mut new_selected = None;
		for (n, (_, s)) in self.elements.iter_mut().enumerate() {
			if let Some(()) = s.last() {
				new_selected = Some(n);
			}
		}

		match new_selected {
			Some(new_selected) => {
				if self.current_element != new_selected {
					match self.elements.get_mut(self.current_element) {
						Some((signal, _)) => signal.emit(VisibilityValue::Collapse),
						None => {},
					};

					self.current_element = new_selected;
					match self.elements.get_mut(self.current_element) {
						Some((signal, _)) => signal.emit(VisibilityValue::Visible),
						None => {},
					};
				}
			},
			None => {},
		}
	}
}
