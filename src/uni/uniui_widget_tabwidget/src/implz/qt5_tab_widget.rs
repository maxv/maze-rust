use uniui_gui::prelude::*;

pub struct Qt5TabWidget {}

impl Qt5TabWidget {
	pub fn new() -> Qt5TabWidget {
		return Qt5TabWidget {};
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_buttons_layout: &mut dyn Widget,
		_content_layout: &mut dyn Widget,
	) -> Result<NativeWidget, ()> {
		error!("not implemented");
		return Err(());
	}
}
