use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::HtmlDivElement;

use crate::implz::wasm_bindgen::JsCast;


pub struct WasmTabWidget {
	native: OptionalNative<HtmlDivElement>,
}

impl WasmTabWidget {
	pub fn new() -> WasmTabWidget {
		return WasmTabWidget {
			native: OptionalNative::new(),
		};
	}

	fn add_widget_to_div(
		w: &mut dyn Widget,
		div: &HtmlDivElement,
		widget_generator: &mut dyn WidgetGenerator,
		flex_grow: String,
	) -> Result<(), ()> {
		let native = w.to_native(widget_generator)?;
		let style = native.style();
		style.set_property_silent("flex-grow", &flex_grow);
		style.set_property_silent("max-height", "100%");
		style.set_property_silent("max-width", "100%");

		return match div.append_child(&native) {
			Ok(_) => {
				trace!("add_widget_to_div:layout_insert:ok");
				Ok(())
			},
			Err(e) => {
				error!("add_widget_to_div:layout_insert:err:{:?}", e);
				Err(())
			},
		};
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		buttons_layout: &mut dyn Widget,
		content_layout: &mut dyn Widget,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("div").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let div = node.dyn_into::<HtmlDivElement>().unwrap();
		self.native.replace(div.clone());

		let style = div.style();
		style.set_property_silent("display", "flex");

		style.set_property_silent("flex-direction", "column");
		style.set_property_silent("flex-wrap", "nowrap");
		style.set_property_silent("align-content", "stretch");
		style.set_property_silent("justify-content", "stretch");


		WasmTabWidget::add_widget_to_div(
			buttons_layout,
			&div,
			widget_generator,
			0.to_string(),
		)?;
		WasmTabWidget::add_widget_to_div(
			content_layout,
			&div,
			widget_generator,
			1.to_string(),
		)?;
		self.native.replace(div.clone());

		return Ok(div.into());
	}
}
