use uniui_gui::{
	prelude::*,
	SizeUnit,
};

#[cfg(target_arch = "wasm32")]
use web_sys::CssStyleDeclaration;

/// Contains [FixedSize], [u_fixedsize!] and
/// [SizeUnit](uniui_gui::SizeUnit).
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_fixedsize,
		FixedSize,
	};

	#[doc(hidden)]
	pub use uniui_gui::SizeUnit;
}

mod macros;

pub struct FixedSize<T: 'static + Widget> {
	pub widget: T,

	pub width_min: Option<SizeUnit>,
	pub width: Option<SizeUnit>,
	pub width_max: Option<SizeUnit>,

	pub height_min: Option<SizeUnit>,
	pub height: Option<SizeUnit>,
	pub height_max: Option<SizeUnit>,
}


#[cfg(target_arch = "wasm32")]
fn apply_property(
	property_name: &str,
	value: &Option<SizeUnit>,
	s: &CssStyleDeclaration,
) {
	if let Some(value) = value.as_ref() {
		value.apply_as_property_value(s, property_name);
	};
}

impl<T: 'static + Widget> Widget for FixedSize<T> {
	#[cfg(target_os = "linux")]
	fn to_native(
		&mut self,
		wg: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		log::error!("Fixed size is not implemented for Qt5. Yet.");
		return self.widget.to_native(wg);
	}

	#[cfg(target_arch = "wasm32")]
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;

		let s = e.style();

		apply_property("min-width", &self.width_min, &s);
		apply_property("width", &self.width, &s);
		apply_property("max-width", &self.width_max, &s);

		apply_property("min-height", &self.height_min, &s);
		apply_property("height", &self.height, &s);
		apply_property("max-height", &self.height_max, &s);

		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for FixedSize<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
