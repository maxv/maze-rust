#[doc(hidden)]
#[macro_export]
macro_rules! u_fixedsize_int {
	($b:ident,width_min: $va:expr) => {
		$b.width_min = Some($va);
	};
	($b:ident,width: $va:expr) => {
		$b.width = Some($va);
	};
	($b:ident,width_max: $va:expr) => {
		$b.width_max = Some($va);
	};
	($b:ident,width_strict: $va:expr) => {
		$b.width_min = Some($va);
		$b.width = Some($va);
		$b.width_max = Some($va);
	};
	($b:ident,height_min: $va:expr) => {
		$b.height_min = Some($va);
	};
	($b:ident,height: $va:expr) => {
		$b.height = Some($va);
	};
	($b:ident,height_max: $va:expr) => {
		$b.height_max = Some($va);
	};
	($b:ident,height_strict: $va:expr) => {
		$b.height_min = Some($va);
		$b.height = Some($va);
		$b.height_max = Some($va);
	};
}

/// Simplifies [FixedSize](crate::FixedSize) creation
///
/// Parameters:
/// * widget (required): widget to wrap,
/// * width_min (optional): [SizeUnit](uniui_gui::SizeUnit),
/// * width (optional): [SizeUnit](uniui_gui::SizeUnit),
/// * width_max (optional): [SizeUnit](uniui_gui::SizeUnit),
/// * width_strict (optional): [SizeUnit](uniui_gui::SizeUnit) for
///   width_min/width/width_max,
/// * height_min (optional): [SizeUnit](uniui_gui::SizeUnit),
/// * height (optional): [SizeUnit](uniui_gui::SizeUnit),
/// * height_max (optional): [SizeUnit](uniui_gui::SizeUnit),
/// * height_strict (optional): [SizeUnit](uniui_gui::SizeUnit) for
///   height_min/height/height_max,
///
/// Example:
/// ```
/// let widget_with_size_limits = u_fixedsize! {
///     widget: widget,
///     width_min: SizeUnit::PT(10.0),
///     height_strict: SizeUnit::PT(20.0),
/// };
/// ```
#[macro_export]
macro_rules! u_fixedsize {
	(widget: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut fixed_size = $crate::FixedSize{
			widget: $w,
			width_max: None,
			width: None,
			width_min: None,
			height_max: None,
			height: None,
			height_min: None,
		};
		$($crate::u_fixedsize_int!(fixed_size, $param_name: $val);)*;
		fixed_size
	}};
}
