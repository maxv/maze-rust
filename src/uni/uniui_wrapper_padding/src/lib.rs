mod padding;
pub use self::padding::Padding;


#[cfg(target_arch = "wasm32")]
mod implz {
	mod wasm_padding;
	pub use self::wasm_padding::WasmPadding as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_padding;
	pub use self::qt5_padding::Qt5Padding as Platform;
}

mod macros;

/// Contains [Padding], [u_padding!] and
/// [SizeUnit](uniui_gui::SizeUnit).
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_padding,
		Padding,
	};

	#[doc(hidden)]
	pub use uniui_gui::SizeUnit;
}
