use uniui_gui::{
	prelude::*,
	NativeWidget,
	SizeUnit,
};

pub struct WasmPadding {}

impl WasmPadding {
	pub fn apply_to(
		e: &NativeWidget,
		top: &Option<SizeUnit>,
		right: &Option<SizeUnit>,
		bottom: &Option<SizeUnit>,
		left: &Option<SizeUnit>,
	) {
		let s = e.style();
		if let Some(top) = top.as_ref() {
			top.apply_as_property_value(&s, "padding-top");
		}

		if let Some(top) = right.as_ref() {
			top.apply_as_property_value(&s, "padding-right");
		}

		if let Some(top) = bottom.as_ref() {
			top.apply_as_property_value(&s, "padding-bottom");
		}

		if let Some(top) = left.as_ref() {
			top.apply_as_property_value(&s, "padding-left");
		}
	}
}
