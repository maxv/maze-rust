use uniui_gui::{
	NativeWidget,
	SizeUnit,
};

pub struct Qt5Padding {}

impl Qt5Padding {
	pub fn apply_to(
		_e: &NativeWidget,
		_top: &Option<SizeUnit>,
		_right: &Option<SizeUnit>,
		_bottom: &Option<SizeUnit>,
		_left: &Option<SizeUnit>,
	) {
		log::error!("Padding not implemented for Qt5. Yet.");
	}
}
