use crate::implz::Platform;

use uniui_gui::{
	prelude::*,
	SizeUnit,
};

pub struct Padding<T: 'static + Widget> {
	pub widget: T,
	pub top: Option<SizeUnit>,
	pub right: Option<SizeUnit>,
	pub bottom: Option<SizeUnit>,
	pub left: Option<SizeUnit>,
}

impl<T: 'static + Widget> Widget for Padding<T> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;
		Platform::apply_to(&e, &self.top, &self.right, &self.bottom, &self.left);

		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for Padding<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
