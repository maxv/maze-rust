#[doc(hidden)]
#[macro_export]
macro_rules! u_padding_int {
	($b:ident,padding: $va:expr) => {
		$b.top = Some($va);
		$b.right = Some($va);
		$b.bottom = Some($va);
		$b.left = Some($va);
	};
	($b:ident,top: $va:expr) => {
		$b.top = Some($va);
	};
	($b:ident,right: $va:expr) => {
		$b.right = Some($va);
	};
	($b:ident,bottom: $va:expr) => {
		$b.bottom = Some($va);
	};
	($b:ident,left: $va:expr) => {
		$b.left = Some($va);
	};
}

/// Simplifies [Padding](crate::Padding) creation
///
/// Parameters:
/// * widget (required): widget to wrap,
/// * padding (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for
///   top/left/bottom/right paddings,
/// * top (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for top paddings,
/// * bottom (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for bottom paddings,
/// * left (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for left paddingsv,
/// * right (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for right paddings.
///
/// Example:
/// ```
/// let widget_with_margins = u_padding! {
///     widget: widget_without_margins,
///     top: SizeUnit::PT(10.0),
///     bottom: SizeUnit::PT(20.0),
/// };
/// ```
#[macro_export]
macro_rules! u_padding {
	(widget: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut padding = $crate::Padding{
			widget: $w,
			top: None,
			right: None,
			bottom: None,
			left: None,
		};
		$($crate::u_padding_int!(padding, $param_name: $val);)*;
		padding
	}};
}
