use uniui_widget_fileselector::FileSelector;

use uniui_file::File;

use uniui_gui::prelude::*;


pub struct FileLoader {
	file_selector: FileSelector,
	signal_file_loaded: Signal<Vec<u8>>,
	slot_file_selected: SlotImpl<Vec<File>>,
	slot_file_readed: SlotImpl<Result<Vec<u8>, ()>>,
}

impl FileLoader {
	pub fn new() -> FileLoader {
		let slot_file_selected = SlotImpl::new();
		let mut file_selector = FileSelector::new();
		file_selector.signal_files_selected().connect_slot(&slot_file_selected);
		return FileLoader {
			file_selector,
			signal_file_loaded: Signal::new(),
			slot_file_selected,
			slot_file_readed: SlotImpl::new(),
		};
	}

	pub fn signal_file_loaded(&mut self) -> &mut Signal<Vec<u8>> {
		return &mut self.signal_file_loaded;
	}
}

impl Widget for FileLoader {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.file_selector.to_native(widget_generator);
	}

	fn draw(&mut self) {
		self.file_selector.draw();
	}
}

impl DataProcessor for FileLoader {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.file_selector.process_data(msec_since_app_start, app);

		for vec_of_files in self.slot_file_selected.data_iter() {
			for file in vec_of_files {
				file.read_all(self.slot_file_readed.proxy());
			}
		}

		for res in self.slot_file_readed.data_iter() {
			match res {
				Ok(v) => self.signal_file_loaded.emit(v),
				Err(e) => error!("file loading error:{:?}", e),
			}
		}
	}
}
