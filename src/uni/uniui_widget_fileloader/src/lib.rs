#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_file;
extern crate uniui_gui;
extern crate uniui_widget_fileselector;

mod file_loader;
pub use self::file_loader::FileLoader;
