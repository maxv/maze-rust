use std::{
	cell::RefCell,
	rc::Rc,
	sync::mpsc::{
		self,
		Receiver,
	},
};

use wasm_bindgen::{
	closure::Closure,
	JsCast,
};

use web_sys::window as js_window;

use crate::{
	implz::GamepadEvent,
	Gamepad,
};


pub struct WasmFactory {}

impl WasmFactory {
	pub fn receiver() -> Result<Receiver<Gamepad>, ()> {
		let (sender, receiver) = mpsc::channel();

		let rc = Rc::new(RefCell::new(None));
		let rc_clone = rc.clone();
		let closure = Closure::wrap(Box::new(move |e: GamepadEvent| {
			let gamepad = Gamepad::new(&e);
			match gamepad {
				Some(g) => {
					match sender.send(g) {
						Ok(()) => log::trace!("gamepad sended"),
						Err(_) => {
							log::warn!("Couldn't send gamepad. Disconnect");
							rc_clone.replace(None);
						},
					}
				},
				None => {},
			};
		}) as Box<dyn FnMut(GamepadEvent)>);

		return match js_window() {
			None => Err(()),
			Some(w) => {
				let listener_added = w.add_event_listener_with_callback(
					"gamepadconnected",
					closure.as_ref().unchecked_ref(),
				);

				match listener_added {
					Err(_) => Err(()),
					Ok(_) => {
						rc.replace(Some(closure));
						Ok(receiver)
					},
				}
			},
		};
	}
}
