use crate::Gamepad;

use std::sync::mpsc::{
	self,
	Receiver,
};


pub struct Qt5Factory {}

impl Qt5Factory {
	pub fn receiver() -> Result<Receiver<Gamepad>, ()> {
		let (_, receiver) = mpsc::channel();
		return Ok(receiver);
	}
}
