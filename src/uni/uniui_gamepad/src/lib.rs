mod gamepad;
pub use self::gamepad::Gamepad;

mod gamepad_state;
pub use self::gamepad_state::GamepadState;

pub const BUTTONS_COUNT: usize = 8;
pub const AXES_COUNT: usize = 8;

#[cfg(target_arch = "wasm32")]
mod implz {
	pub use web_sys::GamepadEvent;

	mod wasm_factory;
	pub use self::wasm_factory::WasmFactory as Factory;
}


#[cfg(target_os = "linux")]
mod implz {
	mod qt5_factory;
	pub type GamepadEvent = u32;
	pub use self::qt5_factory::Qt5Factory as Factory;
}

pub use implz::Factory;
