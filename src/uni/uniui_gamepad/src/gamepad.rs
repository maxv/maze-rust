use super::{
	GamepadState,
	AXES_COUNT,
	BUTTONS_COUNT,
};

use crate::implz::GamepadEvent;

#[cfg(target_arch = "wasm32")]
use web_sys::{
	window as js_window,
	Gamepad as JsGamepad,
	GamepadButton as JsGamepadButton,
};

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::JsValue;

#[cfg(target_arch = "wasm32")]
use js_sys::Array as JsArray;

#[derive(Clone)]
pub struct Gamepad {
	#[cfg(target_arch = "wasm32")]
	index: u32,
}

#[cfg(target_os = "linux")]
impl Gamepad {
	pub fn new(_event: &GamepadEvent) -> Option<Gamepad> {
		return None;
	}

	pub fn state(&self) -> GamepadState {
		let buttons = [false; BUTTONS_COUNT];
		let axes = [0.0; AXES_COUNT];
		return GamepadState {
			buttons,
			axes,
		};
	}
}


#[cfg(target_arch = "wasm32")]
impl Gamepad {
	pub fn new(event: &GamepadEvent) -> Option<Gamepad> {
		let js_gamepad = event.gamepad()?;
		return Some(Gamepad {
			index: js_gamepad.index(),
		});
	}

	pub fn state(&self) -> GamepadState {
		let mut buttons = [false; BUTTONS_COUNT];
		let mut axes = [0.0; AXES_COUNT];

		let self_id = self.index;
		match js_window().unwrap().navigator().get_gamepads() {
			Err(_) => {},
			Ok(array) => {
				array.for_each(&mut |js_gamepad: JsValue,
				                     gamepad_id: u32,
				                     _: JsArray| {
					if gamepad_id == self_id {
						let js_gamepad: JsGamepad = js_gamepad.into();
						js_gamepad.buttons().for_each(
							&mut |button: JsValue, id: u32, _: JsArray| {
								let id = id as usize;
								let button: JsGamepadButton = button.into();
								match id < BUTTONS_COUNT {
									true => buttons[id] = button.pressed(),
									false => {},
								}
							},
						);

						js_gamepad.axes().for_each(
							&mut |value: JsValue, axe_id: u32, _: JsArray| {
								let axe_id = axe_id as usize;
								match (axe_id < AXES_COUNT, value.as_f64()) {
									(true, Some(value)) => axes[axe_id] = value,
									_ => {},
								}
							},
						);
					}
				});
			},
		}

		return GamepadState {
			buttons,
			axes,
		};
	}
}
