use crate::{
	AXES_COUNT,
	BUTTONS_COUNT,
};

pub struct GamepadState {
	pub buttons: [bool; BUTTONS_COUNT],
	pub axes: [f64; AXES_COUNT],
}

impl GamepadState {
	pub fn empty() -> GamepadState {
		return GamepadState {
			buttons: [false; BUTTONS_COUNT],
			axes: [0.0; AXES_COUNT],
		};
	}
}
