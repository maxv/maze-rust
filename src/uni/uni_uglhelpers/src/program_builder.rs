use uniui_widget_ugl::UGLContext;

use uniui_widget_ugl::shader::{
	UGLFragmentShader,
	UGLProgram,
	UGLVertexShader,
};

pub struct ProgramBuilder<'a> {
	context: &'a dyn UGLContext,
	fragment_shader: Option<UGLFragmentShader>,
	vertex_shader: Option<UGLVertexShader>,
}

impl<'a> ProgramBuilder<'a> {
	pub fn new(context: &'a dyn UGLContext) -> ProgramBuilder<'a> {
		return ProgramBuilder {
			context,
			fragment_shader: None,
			vertex_shader: None,
		};
	}

	pub fn add_fragment_shader(
		&mut self,
		sources: &[&str],
	) -> Result<(), ()> {
		let shader = self.context.create_fragment_shader().unwrap();
		for s in sources {
			self.context.shader_add_sources(&shader, s);
		}
		self.context.compile_shader(&shader);
		return match self.context.get_shader_compile_status(&shader) {
			Ok(_) => {
				self.fragment_shader = Some(shader);
				Ok(())
			},
			Err(_) => Err(()),
		};
	}

	pub fn add_vertex_shader(
		&mut self,
		sources: &[&str],
	) -> Result<(), ()> {
		let shader = self.context.create_vertex_shader().unwrap();
		for s in sources {
			self.context.shader_add_sources(&shader, s);
		}
		self.context.compile_shader(&shader);
		return match self.context.get_shader_compile_status(&shader) {
			Ok(_) => {
				self.vertex_shader = Some(shader);
				Ok(())
			},
			Err(_) => Err(()),
		};
	}

	pub fn build(self) -> Result<UGLProgram, ()> {
		let program = self.context.create_program().unwrap();
		self.context.attach_fragment_shader(&program, &self.fragment_shader.unwrap());
		self.context.attach_vertex_shader(&program, &self.vertex_shader.unwrap());
		self.context.link_program(&program);
		return match self.context.get_program_link_status(&program) {
			Ok(_) => Ok(program),
			Err(_) => {
				// self.context.delete_shader(..)
				// self.context.delete_program(...)
				Err(())
			},
		};
	}
}
