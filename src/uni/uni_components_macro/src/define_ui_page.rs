use proc_macro::TokenStream;

use quote::quote;

use syn::{
	parse::Parse,
	Error as SynError,
	Token,
};

const INCORRECT_FORMAT: &str = "
define_ui_page! - incorrect format.

Expected format:
define_ui_page!(
	name: <identifier>,
	parameter: <type>,
	path: <&str>,
);

Example:
```
define_ui_page!(
    name: PAGE_INDEX,
    parameter: String,
    path: \"/\",
);
```
";

#[derive(Debug)]
struct UiPageDefinition {
	name: syn::Ident,

	parameter: syn::Type,

	path: syn::Expr,
}

impl Parse for UiPageDefinition {
	fn parse(parser: syn::parse::ParseStream<'_>) -> syn::parse::Result<Self> {
		let mut name = None;
		let mut parameter = None;
		let mut path = None;

		while parser.peek(syn::Ident) {
			let ident = parser.parse::<syn::Ident>().expect(INCORRECT_FORMAT);

			let _ = parser.parse::<Token![:]>().expect(INCORRECT_FORMAT);
			if ident == "name" {
				let error = format!(
					"{}\n{}",
					"Expected *identifier* for 'name'", INCORRECT_FORMAT
				);
				name = Some(parser.parse::<syn::Ident>().expect(&error));
			} else if ident == "parameter" {
				let error =
					format!("{}\n{}", "Expected *type* for 'input'", INCORRECT_FORMAT);
				parameter = Some(parser.parse::<syn::Type>().expect(&error));
			} else if ident == "path" {
				let error =
					format!("{}\n{}", "Expected &str for 'path'", INCORRECT_FORMAT);
				path = Some(parser.parse::<syn::Expr>().expect(&error));
			} else {
				Err(SynError::new(ident.span(), "Unexpected parameter name"))?;
			}

			let _ = parser.parse::<Token![,]>();
		}

		return Ok(UiPageDefinition {
			name: name.expect(&format!("{} not found.\n{}", "name", INCORRECT_FORMAT)),
			parameter: parameter
				.expect(&format!("{} not found.\n{}", "input", INCORRECT_FORMAT)),
			path: path.expect(&format!("{} not found.\n{}", "path", INCORRECT_FORMAT)),
		});
	}
}

pub fn define_ui_page(input_tokens: TokenStream) -> TokenStream {
	let ui_page_definition = syn::parse_macro_input!(input_tokens as UiPageDefinition);

	let name = &ui_page_definition.name;
	let parameter = &ui_page_definition.parameter;
	let path = &ui_page_definition.path;

	let crate_name = crate::the_crate_name();
	let result = quote! {
		pub const #name:
			&dyn #crate_name::ui_page::UiPage<Data = #parameter> = {
			struct Struct {};

			impl #crate_name::ui_page::UiPageData for Struct {
				fn path(&self) -> &str {
					return #path;
				}
			}

			impl #crate_name::ui_page::UiPage for Struct {
				type Data = #parameter;
			};

			const CONST: Struct = Struct {};

			&CONST
		};
	};

	return result.into();
}
