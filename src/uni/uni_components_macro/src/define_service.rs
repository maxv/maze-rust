use proc_macro::{
	Span,
	TokenStream,
};

use quote::quote;

use syn::{
	parse::Parse,
	Error as SynError,
	Token,
};

const INCORRECT_FORMAT: &str = "
define_service! - incorrect format.

Expected format:
define_service!(
    name: <identifier>,
    input: <type>,
    outputs: [
    	<identifier> {
    		code: <http status code literal>,
    		body: <type>,
    	},
    	<...>
    ],
    path: <&str>,
    module_name: <&str>,
    kind: <uni_components::Kind>,
);

Example:
```
define_service!(
    name: STRING_LENGTH,
    input: String,
    outputs: [
    	Length{
    		code: 200,
    		body: usize,
    	},
    	InternalError{
    		code: 500,
    	},
    ],
    path: \"/api/string_length\",
    module_name: \"string_length\",
    kind: uni_components::Kind::Get,
);
```
";


#[derive(Debug)]
struct ServiceOutput {
	name: syn::Ident,
	code: syn::LitInt,
	body_type: Option<syn::Expr>,
}

#[derive(Debug)]
struct ServiceDefinition {
	name: syn::Ident,

	input: syn::Type,

	path: syn::Expr,
	kind: syn::Expr,

	outputs_data: Option<syn::Expr>,
	output_type: Option<syn::Type>,

	doc: Option<syn::LitStr>,
}

const NAME: &str = "name";
const INPUT: &str = "input";
const DOC: &str = "doc";
const OUTPUTS_DATA: &str = "outputs";
const OUTPUT_TYPE: &str = "output_type";
const PATH: &str = "path";
const KIND: &str = "kind";

impl Parse for ServiceDefinition {
	fn parse(parser: syn::parse::ParseStream<'_>) -> syn::parse::Result<Self> {
		let mut name =
			Err(SynError::new(Span::call_site().into(), format!("{} not found", NAME)));
		let mut input =
			Err(SynError::new(Span::call_site().into(), format!("{} not found", INPUT)));

		let mut path =
			Err(SynError::new(Span::call_site().into(), format!("{} not found", PATH)));

		let mut kind =
			Err(SynError::new(Span::call_site().into(), format!("{} not found", KIND)));

		let mut outputs_data = None;
		let mut output_type = None;
		let mut doc = None;

		while parser.peek(syn::Ident) {
			let ident = parser.parse::<syn::Ident>()?;

			let _ = parser.parse::<Token![:]>().expect(INCORRECT_FORMAT);
			if ident == NAME {
				name = Ok(parser.parse::<syn::Ident>()?);
			} else if ident == DOC {
				doc = Some(parser.parse::<syn::LitStr>()?);
			} else if ident == INPUT {
				input = Ok(parser.parse::<syn::Type>()?);
			} else if ident == OUTPUTS_DATA {
				outputs_data = Some(parser.parse::<syn::Expr>()?);
			} else if ident == OUTPUT_TYPE {
				output_type = Some(parser.parse::<syn::Type>()?);
			} else if ident == PATH {
				path = Ok(parser.parse::<syn::Expr>()?);
			} else if ident == KIND {
				kind = Ok(parser.parse::<syn::Expr>()?);
			} else {
				Err(SynError::new(ident.span(), "Unexpected parameter name"))?;
			}

			let _ = parser.parse::<Token![,]>();
		}

		return Ok(ServiceDefinition {
			name: name.expect(&format!("{} not found.\n{}", "name", INCORRECT_FORMAT)),
			input: input?,
			outputs_data,
			output_type,
			path: path?,
			kind: kind.expect(&format!("{} not found.\n{}", "kind", INCORRECT_FORMAT)),
			doc,
		});
	}
}

pub fn define_service(input_tokens: TokenStream) -> TokenStream {
	let service_definition = syn::parse_macro_input!(input_tokens as ServiceDefinition);

	let name = &service_definition.name;
	let input = &service_definition.input;
	let path = &service_definition.path;
	let kind = &service_definition.kind;
	let outputs_data = &service_definition.outputs_data;
	let output_type = &service_definition.output_type;
	let doc = &service_definition.doc;

	let crate_name = crate::the_crate_name();

	let enum_name = quote! {Out};

	let output_enum = match (outputs_data, output_type) {
		(Some(d), None) => {
			quote! {
				#crate_name::define_type!{
					name: #enum_name,
					variants: #d,
				}
			}
		},
		(None, Some(t)) => {
			let doc = format!("Type alias to [{}]", quote! {#t});
			quote! {
				#[doc = #doc]
				pub type #enum_name = #t;
			}
		},
		(None, None) => panic!("outputs_data and outputs_type is None"),
		(Some(_), Some(_)) => panic!("Both outputs_data and outputs_type specified."),
	};

	let doc = match doc {
		Some(d) => {
			quote! {#[doc = #d]}
		},
		None => {
			quote! {}
		},
	};

	let in_doc = format!("Type alias to [{}]", quote! {#input});

	let result = quote! {
		#doc
		pub mod #name {
			use super::*;
			use #crate_name::uniui_core::Slot;

			#[doc = #in_doc]
			pub type In = #input;

			#output_enum

			pub struct Service{}

			impl Service {
				pub fn exec(
					&self,
					p: &#input,
					s: &dyn Slot<Result<#enum_name, #crate_name::Response>>,
				) -> Result<(), #crate_name::CallError> {
					return <Self as #crate_name::service::Service>::exec(&self, p, s);
				}
			}

			impl #crate_name::service::Service for Service {
				type In = #input;
				type Out = #enum_name;

				fn path(&self) -> &str {
					return PATH;
				}

				fn kind(&self) -> #crate_name::Kind {
					return KIND;
				}

				fn from_response(
					&self
				) -> fn(
					response: #crate_name::Response
				) -> Result<Self::Out, #crate_name::Response> {
					return #enum_name::from_response;
				}
			}

			pub const PATH: &'static str = #path;
			pub const KIND: #crate_name::Kind = #kind;

			pub const INSTANCE: Service = Service{};
			pub const REF: &'static Service = &INSTANCE;
		}
	};

	return result.into();
}
