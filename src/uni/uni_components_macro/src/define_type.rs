use proc_macro::{
	Span,
	TokenStream,
};

use quote::quote;

use syn::{
	parse::Parse,
	Error as SynError,
	Token,
};

#[derive(Debug)]
struct ServiceOutput {
	name: syn::Ident,
	code: syn::LitInt,
	body_type: Option<syn::Type>,
	doc: Option<syn::ExprLit>,
}

const CODE: &str = "code";
const BODY: &str = "body";
const DOC: &str = "doc";

fn parse_variants(
	parser: &syn::parse::ParseStream<'_>
) -> syn::parse::Result<Vec<ServiceOutput>> {
	let mut result = Vec::new();

	let content;
	syn::bracketed!(content in parser);

	while content.peek(syn::Ident) {
		let name = content.parse::<syn::Ident>()?;

		let fields;
		syn::braced! {fields in content};

		let mut code = Err(SynError::new(content.span(), format!("{} not found", CODE)));
		let mut doc = None;
		let mut body_type = None;

		while fields.peek(syn::Ident) {
			let ident = fields.parse::<syn::Ident>()?;
			fields.parse::<Token![:]>()?;
			if ident == CODE {
				code = Ok(fields.parse::<syn::LitInt>()?);
			} else if ident == DOC {
				doc = Some(fields.parse::<syn::ExprLit>()?);
			} else if ident == BODY {
				body_type = Some(fields.parse::<syn::Type>()?);
			} else {
				Err(SynError::new(
					ident.span(),
					format!("Unexpected parameter:{}", ident),
				))?;
			}

			let _ = fields.parse::<Token![,]>();
		}

		result.push(ServiceOutput {
			code: code?,
			doc,
			body_type,
			name,
		});

		let _ = content.parse::<Token![,]>();
	}

	return Ok(result);
}

struct ServiceOutputVec {
	pub outputs: Vec<ServiceOutput>,
	pub type_name: syn::Ident,
	pub type_generic: Option<syn::AngleBracketedGenericArguments>,
	pub doc: Option<syn::LitStr>,
	pub where_clause:
		Option<syn::punctuated::Punctuated<syn::WherePredicate, syn::token::Comma>>,
}

const NAME: &str = "name";
const VARIANTS: &str = "variants";

impl Parse for ServiceOutputVec {
	fn parse(parser: syn::parse::ParseStream<'_>) -> syn::parse::Result<Self> {
		let mut type_name =
			Err(SynError::new(Span::call_site().into(), format!("{} not found", NAME)));
		let mut doc = None;
		let mut outputs = Err(SynError::new(
			Span::call_site().into(),
			format!("{} not found", VARIANTS),
		));
		let mut type_generic = None;
		let mut where_clause = None;

		while parser.peek(syn::Ident) {
			let ident = parser.parse::<syn::Ident>()?;

			let _ = parser.parse::<Token![:]>()?;
			if ident == NAME {
				type_name = Ok(parser.parse::<syn::Ident>()?);
				type_generic = parser.parse::<syn::AngleBracketedGenericArguments>().ok();
				where_clause =
					parser.parse::<syn::WhereClause>().map(|w| w.predicates).ok();
				let _ = parser.parse::<syn::Block>();
			} else if ident == DOC {
				doc = Some(parser.parse::<syn::LitStr>()?);
			} else if ident == VARIANTS {
				outputs = parse_variants(&parser);
			} else {
				Err(SynError::new(
					ident.span(),
					format!("Unexpected parameter:{}", ident),
				))?;
			}

			let _ = parser.parse::<Token![,]>();
		}

		return Ok(Self {
			type_name: type_name?,
			outputs: outputs?,
			doc,
			type_generic,
			where_clause,
		});
	}
}

pub fn define_type(input_tokens: TokenStream) -> TokenStream {
	let ServiceOutputVec {
		outputs,
		type_name,
		type_generic,
		doc,
		where_clause,
	} = syn::parse_macro_input!(input_tokens as ServiceOutputVec);

	let serde_bound = match where_clause.is_some() {
		false => None,
		true => {
			let where_string = format!("{}", quote! {#where_clause});
			Some(quote! {
				#[serde(bound = #where_string)]
			})
		},
	};

	let outputs_enum_elements: Vec<proc_macro2::TokenStream> = outputs
		.iter()
		.map(|m| {
			let ident = &m.name;
			let body_type = &m.body_type;

			let val = match &body_type {
				Some(t) => {
					quote! { #ident(#t)}
				},
				None => {
					quote! {#ident}
				},
			};

			match m.doc.as_ref() {
				Some(doc) => {
					quote! {
						#[doc = #doc]
						#val
					}
				},
				None => val,
			}
		})
		.collect();

	let one_line_doc = outputs
		.iter()
		.map(|m| format!("[{}::{}]", quote! {#type_name}, m.name))
		.collect::<Vec<_>>()
		.join(", ");
	let one_line_doc = format!(
		"Contains {}. {}",
		one_line_doc,
		doc.map(|d| d.value()).unwrap_or("".to_owned())
	);


	let response_input_name = quote! {response};
	let crate_name = crate::the_crate_name();

	let output_enum_to_responses: Vec<proc_macro2::TokenStream> = outputs
		.iter()
		.map(|m| {
			let ident = &m.name;
			let code = &m.code;

			match m.body_type.as_ref() {
				Some(_) => {
					quote! {
						#ident(s) => {
						match #crate_name::serde_json::to_vec(&s) {
								std::result::Result::Ok(v) => {
									#crate_name::Response{
										code: #code,
										body: Some(v),
										// cookies,
										// headers,
									}
								},
								std::result::Result::Err(e) => {
									#crate_name::log::error!(
										"serde_json::to_vec failed err:{:?}",
										e
									);

									#crate_name::Response{
										code: 500,
										body: None,
										// cookies
										// headeres
									}
								},
							}
						}
					}
				},
				None => {
					quote! {
						#ident => {
							#crate_name::Response {
								code: #code,
								body: None,
								// cookies,
								// headers,
							}
						}
					}
				},
			}
		})
		.collect();

	let outputs_enum_from_response: Vec<proc_macro2::TokenStream> = outputs
		.iter()
		.map(|m| {
			let code = &m.code;
			let body_type = &m.body_type;
			let name = &m.name;

			match body_type {
				Some(_) => {
					quote! {
						#code => {
							match #response_input_name.body.as_ref() {
								Some(b) =>  {
									use std::result::Result as StdResult;
									match #crate_name::serde_json::from_slice(b) {
										StdResult::Ok(d) => StdResult::Ok(#name(d)),
										StdResult::Err(e) => {
											#crate_name::log::error!(
												"deserialization error:{:?}",
												e
											);
											StdResult::Err(#response_input_name)
										},
									}
								},
								None => {
									#crate_name::log::error!("body not found");
									std::result::Result::Err(#response_input_name)
								},
							}
						}
					}
				},
				None => {
					quote! {#code => std::result::Result::Ok(#name)}
				},
			}
		})
		.collect();

	let path_to_serde = format!("{}", quote! {
		#crate_name::serde
	});

	let result = quote! {
		#[derive(
			Debug,
			#crate_name::serde::Deserialize,
			#crate_name::serde::Serialize
		)]
		#[doc = #one_line_doc]
		#[serde(crate = #path_to_serde)]
		#serde_bound
		pub enum #type_name#type_generic
		where #where_clause
		{
			#(#outputs_enum_elements),*
		}

		impl#type_generic #type_name#type_generic
		where #where_clause
		{
			pub fn from_response(
				#response_input_name: #crate_name::Response
			) -> std::result::Result<Self, #crate_name::Response> {
				use #type_name::*;
				match #response_input_name.code {
					#(#outputs_enum_from_response),*,
					_ => Err(#response_input_name),
				}
			}
		}

		impl#type_generic #crate_name::AsResponse for #type_name#type_generic
		where #where_clause
		{
			fn to_response(self) -> #crate_name::Response {
				return match self {
					#( #type_name::#output_enum_to_responses),*,
				}
			}
		}
	};

	if let Ok(_) = std::env::var("UNI_COMPONENTS_MACRO_DEBUG") {
		println!("Result:{:#}", result);
	}

	return result.into();
}
