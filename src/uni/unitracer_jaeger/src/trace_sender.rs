use crate::jaeger_sender::JaegerSender;

use unitracer::{
	Batch,
	TraceSender as TS,
	*,
};


pub struct TraceSender {
	host: String,
	port: String,
}

impl TraceSender {
	pub fn new(
		host: String,
		port: String,
	) -> TraceSender {
		return TraceSender {
			host,
			port,
		};
	}
}

impl TS for TraceSender {
	fn send_batch(
		&mut self,
		batch: Batch,
	) {
		send_batch(batch, &self.host, &self.port);
	}
}

pub fn send_batch(
	batch: Batch,
	host: &str,
	port: &str,
) {
	println!("send bytes");
	use std::io::{
		Read,
		Write,
	};

	let bytes = as_bytes(batch);

	let target = format!("{}:{}", host, port);
	match std::net::TcpStream::connect(&target) {
		Ok(mut stream) => {
			println!("stream");
			let mut request: Vec<&[u8]> = Vec::new();

			let s = format!("POST /api/traces?format=jaeger.thrift HTTP/1.0\r\n");
			request.push(&s.as_bytes());

			let s = format!("Host: {}\r\n", host);
			request.push(&s.as_bytes());

			let s = format!("Content-Type: application/x-thrift\r\n");
			request.push(&s.as_bytes());

			let s = format!("Content-Length: {}\r\n", bytes.len());
			request.push(&s.as_bytes());

			let s = format!("\r\n");
			request.push(&s.as_bytes());
			request.push(&bytes);
			request.push(&s.as_bytes());
			request.push(&s.as_bytes());

			request.iter().for_each(|b| {
				match stream.write_all(b) {
					Ok(()) => {},
					Err(e) => {
						println!("write chunk failed e:{:?}", e);
					},
				}
			});

			let mut resp = Vec::new();
			match stream.read_to_end(&mut resp) {
				Ok(_) => {},
				Err(e) => println!("response error:{:?}", e),
			}
		},
		Err(e) => {
			println!("Stream not opened e:{:?}", e);
		},
	}
}

struct MyWriter {
	pub sender: std::sync::mpsc::Sender<u8>,
}

impl std::io::Write for MyWriter {
	fn write(
		&mut self,
		buf: &[u8],
	) -> std::io::Result<usize> {
		use std::io::{
			Error,
			ErrorKind,
		};

		let size = buf.len();
		let mut sended = 0;
		for b in buf {
			match self.sender.send(*b) {
				Ok(()) => {},
				Err(_) => break,
			}
			sended += 1;
		}

		return match sended == size {
			true => Ok(size),
			false => Err(Error::new(ErrorKind::Other, "sender failed")),
		};
	}

	fn flush(&mut self) -> std::io::Result<()> {
		Ok(()) // nothing to do on flush
	}
}

pub fn as_bytes(batch: Batch) -> Vec<u8> {
	use thrift::protocol::TBinaryOutputProtocol;

	let (sender, read) = std::sync::mpsc::channel();
	let my_write = MyWriter {
		sender,
	};

	let mut proto = TBinaryOutputProtocol::new(my_write, true);
	match batch.write_to_out_protocol(&mut proto) {
		Ok(()) => {},
		Err(e) => println!("proto send err:{:?}", e),
	}

	return read.try_iter().collect();
}


use thrift::{
	protocol::{
		field_id,
		verify_required_field_exists,
		TFieldIdentifier,
		TInputProtocol,
		TListIdentifier,
		TOutputProtocol,
		TStructIdentifier,
		TType,
	},
	ProtocolError,
	ProtocolErrorKind,
};

impl JaegerSender for TagType {
	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		o_prot.write_i32(*self as i32)
	}

	fn read_from_in_protocol(i_prot: &mut dyn TInputProtocol) -> thrift::Result<TagType> {
		let enum_value = i_prot.read_i32()?;
		tag_type_try_from(enum_value)
	}
}

fn tag_type_try_from(i: i32) -> Result<TagType, thrift::Error> {
	match i {
		0 => Ok(TagType::String),
		1 => Ok(TagType::Double),
		2 => Ok(TagType::Bool),
		3 => Ok(TagType::Long),
		4 => Ok(TagType::Binary),
		_ => {
			Err(thrift::Error::Protocol(ProtocolError::new(
				ProtocolErrorKind::InvalidData,
				format!("cannot convert enum constant {} to TagType", i),
			)))
		},
	}
}

impl JaegerSender for SpanRefType {
	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		o_prot.write_i32(*self as i32)
	}

	fn read_from_in_protocol(
		i_prot: &mut dyn TInputProtocol
	) -> thrift::Result<SpanRefType> {
		let enum_value = i_prot.read_i32()?;
		span_ref_type_try_from(enum_value)
	}
}

fn span_ref_type_try_from(i: i32) -> Result<SpanRefType, thrift::Error> {
	match i {
		0 => Ok(SpanRefType::ChildOf),
		1 => Ok(SpanRefType::FollowsFrom),
		_ => {
			Err(thrift::Error::Protocol(ProtocolError::new(
				ProtocolErrorKind::InvalidData,
				format!("cannot convert enum constant {} to SpanRefType", i),
			)))
		},
	}
}

impl JaegerSender for Tag {
	fn read_from_in_protocol(i_prot: &mut dyn TInputProtocol) -> thrift::Result<Tag> {
		i_prot.read_struct_begin()?;
		let mut f_1: Option<String> = None;
		let mut f_2: Option<TagType> = None;
		let mut f_3: Option<String> = None;
		let mut f_4: Option<f64> = None;
		let mut f_5: Option<bool> = None;
		let mut f_6: Option<i64> = None;
		let mut f_7: Option<Vec<u8>> = None;
		loop {
			let field_ident = i_prot.read_field_begin()?;
			if field_ident.field_type == TType::Stop {
				break;
			}
			let field_id = field_id(&field_ident)?;
			match field_id {
				1 => {
					let val = i_prot.read_string()?;
					f_1 = Some(val);
				},
				2 => {
					let val = TagType::read_from_in_protocol(i_prot)?;
					f_2 = Some(val);
				},
				3 => {
					let val = i_prot.read_string()?;
					f_3 = Some(val);
				},
				4 => {
					let val = i_prot.read_double()?;
					f_4 = Some(val);
				},
				5 => {
					let val = i_prot.read_bool()?;
					f_5 = Some(val);
				},
				6 => {
					let val = i_prot.read_i64()?;
					f_6 = Some(val);
				},
				7 => {
					let val = i_prot.read_bytes()?;
					f_7 = Some(val);
				},
				_ => {
					i_prot.skip(field_ident.field_type)?;
				},
			};
			i_prot.read_field_end()?;
		}
		i_prot.read_struct_end()?;
		verify_required_field_exists("Tag.key", &f_1)?;
		verify_required_field_exists("Tag.v_type", &f_2)?;
		let ret = Tag {
			key: f_1.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			v_type: f_2.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			v_str: f_3,
			v_double: f_4,
			v_bool: f_5,
			v_long: f_6,
			v_binary: f_7,
		};
		Ok(ret)
	}

	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		let struct_ident = TStructIdentifier::new("Tag");
		o_prot.write_struct_begin(&struct_ident)?;
		o_prot.write_field_begin(&TFieldIdentifier::new("key", TType::String, 1))?;
		o_prot.write_string(&self.key)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("vType", TType::I32, 2))?;
		self.v_type.write_to_out_protocol(o_prot)?;
		o_prot.write_field_end()?;
		if let Some(ref fld_var) = self.v_str {
			o_prot.write_field_begin(&TFieldIdentifier::new("vStr", TType::String, 3))?;
			o_prot.write_string(fld_var)?;
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		if let Some(fld_var) = self.v_double {
			o_prot.write_field_begin(&TFieldIdentifier::new(
				"vDouble",
				TType::Double,
				4,
			))?;
			o_prot.write_double(fld_var.into())?;
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		if let Some(fld_var) = self.v_bool {
			o_prot.write_field_begin(&TFieldIdentifier::new("vBool", TType::Bool, 5))?;
			o_prot.write_bool(fld_var)?;
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		if let Some(fld_var) = self.v_long {
			o_prot.write_field_begin(&TFieldIdentifier::new("vLong", TType::I64, 6))?;
			o_prot.write_i64(fld_var)?;
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		if let Some(ref fld_var) = self.v_binary {
			o_prot.write_field_begin(&TFieldIdentifier::new(
				"vBinary",
				TType::String,
				7,
			))?;
			o_prot.write_bytes(fld_var)?;
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		o_prot.write_field_stop()?;
		o_prot.write_struct_end()
	}
}

impl JaegerSender for Log {
	fn read_from_in_protocol(i_prot: &mut dyn TInputProtocol) -> thrift::Result<Log> {
		i_prot.read_struct_begin()?;
		let mut f_1: Option<i64> = None;
		let mut f_2: Option<Vec<Tag>> = None;
		loop {
			let field_ident = i_prot.read_field_begin()?;
			if field_ident.field_type == TType::Stop {
				break;
			}
			let field_id = field_id(&field_ident)?;
			match field_id {
				1 => {
					let val = i_prot.read_i64()?;
					f_1 = Some(val);
				},
				2 => {
					let list_ident = i_prot.read_list_begin()?;
					let mut val: Vec<Tag> = Vec::with_capacity(list_ident.size as usize);
					for _ in 0..list_ident.size {
						let list_elem_0 = Tag::read_from_in_protocol(i_prot)?;
						val.push(list_elem_0);
					}
					i_prot.read_list_end()?;
					f_2 = Some(val);
				},
				_ => {
					i_prot.skip(field_ident.field_type)?;
				},
			};
			i_prot.read_field_end()?;
		}
		i_prot.read_struct_end()?;
		verify_required_field_exists("Log.timestamp", &f_1)?;
		verify_required_field_exists("Log.fields", &f_2)?;
		let ret = Log {
			timestamp: f_1.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			fields: f_2.expect(
				"auto-generated code should have checked for presence of required fields",
			),
		};
		Ok(ret)
	}

	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		let struct_ident = TStructIdentifier::new("Log");
		o_prot.write_struct_begin(&struct_ident)?;
		o_prot.write_field_begin(&TFieldIdentifier::new("timestamp", TType::I64, 1))?;
		o_prot.write_i64(self.timestamp)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("fields", TType::List, 2))?;
		o_prot.write_list_begin(&TListIdentifier::new(
			TType::Struct,
			self.fields.len() as i32,
		))?;
		for e in &self.fields {
			e.write_to_out_protocol(o_prot)?;
			o_prot.write_list_end()?;
		}
		o_prot.write_field_end()?;
		o_prot.write_field_stop()?;
		o_prot.write_struct_end()
	}
}

impl JaegerSender for SpanRef {
	fn read_from_in_protocol(i_prot: &mut dyn TInputProtocol) -> thrift::Result<SpanRef> {
		i_prot.read_struct_begin()?;
		let mut f_1: Option<SpanRefType> = None;
		let mut f_2: Option<i64> = None;
		let mut f_3: Option<i64> = None;
		let mut f_4: Option<i64> = None;
		loop {
			let field_ident = i_prot.read_field_begin()?;
			if field_ident.field_type == TType::Stop {
				break;
			}
			let field_id = field_id(&field_ident)?;
			match field_id {
				1 => {
					let val = SpanRefType::read_from_in_protocol(i_prot)?;
					f_1 = Some(val);
				},
				2 => {
					let val = i_prot.read_i64()?;
					f_2 = Some(val);
				},
				3 => {
					let val = i_prot.read_i64()?;
					f_3 = Some(val);
				},
				4 => {
					let val = i_prot.read_i64()?;
					f_4 = Some(val);
				},
				_ => {
					i_prot.skip(field_ident.field_type)?;
				},
			};
			i_prot.read_field_end()?;
		}
		i_prot.read_struct_end()?;
		verify_required_field_exists("SpanRef.ref_type", &f_1)?;
		verify_required_field_exists("SpanRef.trace_id_low", &f_2)?;
		verify_required_field_exists("SpanRef.trace_id_high", &f_3)?;
		verify_required_field_exists("SpanRef.span_id", &f_4)?;
		let ret = SpanRef {
			ref_type: f_1.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			trace_id_low: f_2.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			trace_id_high: f_3.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			span_id: f_4.expect(
				"auto-generated code should have checked for presence of required fields",
			),
		};
		Ok(ret)
	}

	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		let struct_ident = TStructIdentifier::new("SpanRef");
		o_prot.write_struct_begin(&struct_ident)?;
		o_prot.write_field_begin(&TFieldIdentifier::new("refType", TType::I32, 1))?;
		self.ref_type.write_to_out_protocol(o_prot)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("traceIdLow", TType::I64, 2))?;
		o_prot.write_i64(self.trace_id_low)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("traceIdHigh", TType::I64, 3))?;
		o_prot.write_i64(self.trace_id_high)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("spanId", TType::I64, 4))?;
		o_prot.write_i64(self.span_id)?;
		o_prot.write_field_end()?;
		o_prot.write_field_stop()?;
		o_prot.write_struct_end()
	}
}

impl JaegerSender for Span {
	fn read_from_in_protocol(i_prot: &mut dyn TInputProtocol) -> thrift::Result<Span> {
		i_prot.read_struct_begin()?;
		let mut f_1: Option<i64> = None;
		let mut f_2: Option<i64> = None;
		let mut f_3: Option<i64> = None;
		let mut f_4: Option<i64> = None;
		let mut f_5: Option<String> = None;
		let mut f_6: Option<Vec<SpanRef>> = None;
		let mut f_7: Option<i32> = None;
		let mut f_8: Option<i64> = None;
		let mut f_9: Option<i64> = None;
		let mut f_10: Option<Vec<Tag>> = None;
		let mut f_11: Option<Vec<Log>> = None;
		loop {
			let field_ident = i_prot.read_field_begin()?;
			if field_ident.field_type == TType::Stop {
				break;
			}
			let field_id = field_id(&field_ident)?;
			match field_id {
				1 => {
					let val = i_prot.read_i64()?;
					f_1 = Some(val);
				},
				2 => {
					let val = i_prot.read_i64()?;
					f_2 = Some(val);
				},
				3 => {
					let val = i_prot.read_i64()?;
					f_3 = Some(val);
				},
				4 => {
					let val = i_prot.read_i64()?;
					f_4 = Some(val);
				},
				5 => {
					let val = i_prot.read_string()?;
					f_5 = Some(val);
				},
				6 => {
					let list_ident = i_prot.read_list_begin()?;
					let mut val: Vec<SpanRef> =
						Vec::with_capacity(list_ident.size as usize);
					for _ in 0..list_ident.size {
						let list_elem_1 = SpanRef::read_from_in_protocol(i_prot)?;
						val.push(list_elem_1);
					}
					i_prot.read_list_end()?;
					f_6 = Some(val);
				},
				7 => {
					let val = i_prot.read_i32()?;
					f_7 = Some(val);
				},
				8 => {
					let val = i_prot.read_i64()?;
					f_8 = Some(val);
				},
				9 => {
					let val = i_prot.read_i64()?;
					f_9 = Some(val);
				},
				10 => {
					let list_ident = i_prot.read_list_begin()?;
					let mut val: Vec<Tag> = Vec::with_capacity(list_ident.size as usize);
					for _ in 0..list_ident.size {
						let list_elem_2 = Tag::read_from_in_protocol(i_prot)?;
						val.push(list_elem_2);
					}
					i_prot.read_list_end()?;
					f_10 = Some(val);
				},
				11 => {
					let list_ident = i_prot.read_list_begin()?;
					let mut val: Vec<Log> = Vec::with_capacity(list_ident.size as usize);
					for _ in 0..list_ident.size {
						let list_elem_3 = Log::read_from_in_protocol(i_prot)?;
						val.push(list_elem_3);
					}
					i_prot.read_list_end()?;
					f_11 = Some(val);
				},
				_ => {
					i_prot.skip(field_ident.field_type)?;
				},
			};
			i_prot.read_field_end()?;
		}
		i_prot.read_struct_end()?;
		verify_required_field_exists("Span.trace_id_low", &f_1)?;
		verify_required_field_exists("Span.trace_id_high", &f_2)?;
		verify_required_field_exists("Span.span_id", &f_3)?;
		verify_required_field_exists("Span.parent_span_id", &f_4)?;
		verify_required_field_exists("Span.operation_name", &f_5)?;
		verify_required_field_exists("Span.flags", &f_7)?;
		verify_required_field_exists("Span.start_time", &f_8)?;
		verify_required_field_exists("Span.duration", &f_9)?;
		let ret = Span {
			trace_id_low: f_1.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			trace_id_high: f_2.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			span_id: f_3.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			parent_span_id: f_4.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			operation_name: f_5.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			references: f_6,
			flags: f_7.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			start_time: f_8.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			duration: f_9.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			tags: f_10,
			logs: f_11,
		};
		Ok(ret)
	}

	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		let struct_ident = TStructIdentifier::new("Span");
		o_prot.write_struct_begin(&struct_ident)?;
		o_prot.write_field_begin(&TFieldIdentifier::new("traceIdLow", TType::I64, 1))?;
		o_prot.write_i64(self.trace_id_low)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("traceIdHigh", TType::I64, 2))?;
		o_prot.write_i64(self.trace_id_high)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("spanId", TType::I64, 3))?;
		o_prot.write_i64(self.span_id)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new(
			"parentSpanId",
			TType::I64,
			4,
		))?;
		o_prot.write_i64(self.parent_span_id)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new(
			"operationName",
			TType::String,
			5,
		))?;
		o_prot.write_string(&self.operation_name)?;
		o_prot.write_field_end()?;
		if let Some(ref fld_var) = self.references {
			o_prot.write_field_begin(&TFieldIdentifier::new(
				"references",
				TType::List,
				6,
			))?;
			o_prot.write_list_begin(&TListIdentifier::new(
				TType::Struct,
				fld_var.len() as i32,
			))?;
			for e in fld_var {
				e.write_to_out_protocol(o_prot)?;
				o_prot.write_list_end()?;
			}
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		o_prot.write_field_begin(&TFieldIdentifier::new("flags", TType::I32, 7))?;
		o_prot.write_i32(self.flags)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("startTime", TType::I64, 8))?;
		o_prot.write_i64(self.start_time)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("duration", TType::I64, 9))?;
		o_prot.write_i64(self.duration)?;
		o_prot.write_field_end()?;
		if let Some(ref fld_var) = self.tags {
			o_prot.write_field_begin(&TFieldIdentifier::new("tags", TType::List, 10))?;
			o_prot.write_list_begin(&TListIdentifier::new(
				TType::Struct,
				fld_var.len() as i32,
			))?;
			for e in fld_var {
				e.write_to_out_protocol(o_prot)?;
				o_prot.write_list_end()?;
			}
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		if let Some(ref fld_var) = self.logs {
			o_prot.write_field_begin(&TFieldIdentifier::new("logs", TType::List, 11))?;
			o_prot.write_list_begin(&TListIdentifier::new(
				TType::Struct,
				fld_var.len() as i32,
			))?;
			for e in fld_var {
				e.write_to_out_protocol(o_prot)?;
				o_prot.write_list_end()?;
			}
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		o_prot.write_field_stop()?;
		o_prot.write_struct_end()
	}
}

impl JaegerSender for Process {
	fn read_from_in_protocol(i_prot: &mut dyn TInputProtocol) -> thrift::Result<Process> {
		i_prot.read_struct_begin()?;
		let mut f_1: Option<String> = None;
		let mut f_2: Option<Vec<Tag>> = None;
		loop {
			let field_ident = i_prot.read_field_begin()?;
			if field_ident.field_type == TType::Stop {
				break;
			}
			let field_id = field_id(&field_ident)?;
			match field_id {
				1 => {
					let val = i_prot.read_string()?;
					f_1 = Some(val);
				},
				2 => {
					let list_ident = i_prot.read_list_begin()?;
					let mut val: Vec<Tag> = Vec::with_capacity(list_ident.size as usize);
					for _ in 0..list_ident.size {
						let list_elem_4 = Tag::read_from_in_protocol(i_prot)?;
						val.push(list_elem_4);
					}
					i_prot.read_list_end()?;
					f_2 = Some(val);
				},
				_ => {
					i_prot.skip(field_ident.field_type)?;
				},
			};
			i_prot.read_field_end()?;
		}
		i_prot.read_struct_end()?;
		verify_required_field_exists("Process.service_name", &f_1)?;
		let ret = Process {
			service_name: f_1.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			tags: f_2,
		};
		Ok(ret)
	}

	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		let struct_ident = TStructIdentifier::new("Process");
		o_prot.write_struct_begin(&struct_ident)?;
		o_prot.write_field_begin(&TFieldIdentifier::new(
			"serviceName",
			TType::String,
			1,
		))?;
		o_prot.write_string(&self.service_name)?;
		o_prot.write_field_end()?;
		if let Some(ref fld_var) = self.tags {
			o_prot.write_field_begin(&TFieldIdentifier::new("tags", TType::List, 2))?;
			o_prot.write_list_begin(&TListIdentifier::new(
				TType::Struct,
				fld_var.len() as i32,
			))?;
			for e in fld_var {
				e.write_to_out_protocol(o_prot)?;
				o_prot.write_list_end()?;
			}
			o_prot.write_field_end()?;
			()
		} else {
			()
		}
		o_prot.write_field_stop()?;
		o_prot.write_struct_end()
	}
}

impl JaegerSender for Batch {
	fn read_from_in_protocol(i_prot: &mut dyn TInputProtocol) -> thrift::Result<Batch> {
		i_prot.read_struct_begin()?;
		let mut f_1: Option<Process> = None;
		let mut f_2: Option<Vec<Span>> = None;
		loop {
			let field_ident = i_prot.read_field_begin()?;
			if field_ident.field_type == TType::Stop {
				break;
			}
			let field_id = field_id(&field_ident)?;
			match field_id {
				1 => {
					let val = Process::read_from_in_protocol(i_prot)?;
					f_1 = Some(val);
				},
				2 => {
					let list_ident = i_prot.read_list_begin()?;
					let mut val: Vec<Span> = Vec::with_capacity(list_ident.size as usize);
					for _ in 0..list_ident.size {
						let list_elem_5 = Span::read_from_in_protocol(i_prot)?;
						val.push(list_elem_5);
					}
					i_prot.read_list_end()?;
					f_2 = Some(val);
				},
				_ => {
					i_prot.skip(field_ident.field_type)?;
				},
			};
			i_prot.read_field_end()?;
		}
		i_prot.read_struct_end()?;
		verify_required_field_exists("Batch.process", &f_1)?;
		verify_required_field_exists("Batch.spans", &f_2)?;
		let ret = Batch {
			process: f_1.expect(
				"auto-generated code should have checked for presence of required fields",
			),
			spans: f_2.expect(
				"auto-generated code should have checked for presence of required fields",
			),
		};
		Ok(ret)
	}

	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		let struct_ident = TStructIdentifier::new("Batch");
		o_prot.write_struct_begin(&struct_ident)?;
		o_prot.write_field_begin(&TFieldIdentifier::new("process", TType::Struct, 1))?;
		self.process.write_to_out_protocol(o_prot)?;
		o_prot.write_field_end()?;
		o_prot.write_field_begin(&TFieldIdentifier::new("spans", TType::List, 2))?;
		o_prot.write_list_begin(&TListIdentifier::new(
			TType::Struct,
			self.spans.len() as i32,
		))?;
		for e in &self.spans {
			e.write_to_out_protocol(o_prot)?;
			o_prot.write_list_end()?;
		}
		o_prot.write_field_end()?;
		o_prot.write_field_stop()?;
		o_prot.write_struct_end()
	}
}

// BatchSubmitResponse
//

#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub struct BatchSubmitResponse {
	pub ok: bool,
}

impl JaegerSender for BatchSubmitResponse {
	fn read_from_in_protocol(
		i_prot: &mut dyn TInputProtocol
	) -> thrift::Result<BatchSubmitResponse> {
		i_prot.read_struct_begin()?;
		let mut f_1: Option<bool> = None;
		loop {
			let field_ident = i_prot.read_field_begin()?;
			if field_ident.field_type == TType::Stop {
				break;
			}
			let field_id = field_id(&field_ident)?;
			match field_id {
				1 => {
					let val = i_prot.read_bool()?;
					f_1 = Some(val);
				},
				_ => {
					i_prot.skip(field_ident.field_type)?;
				},
			};
			i_prot.read_field_end()?;
		}
		i_prot.read_struct_end()?;
		verify_required_field_exists("BatchSubmitResponse.ok", &f_1)?;
		let ret = BatchSubmitResponse {
			ok: f_1.expect(
				"auto-generated code should have checked for presence of required fields",
			),
		};
		Ok(ret)
	}

	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn TOutputProtocol,
	) -> thrift::Result<()> {
		let struct_ident = TStructIdentifier::new("BatchSubmitResponse");
		o_prot.write_struct_begin(&struct_ident)?;
		o_prot.write_field_begin(&TFieldIdentifier::new("ok", TType::Bool, 1))?;
		o_prot.write_bool(self.ok)?;
		o_prot.write_field_end()?;
		o_prot.write_field_stop()?;
		o_prot.write_struct_end()
	}
}

// Collector service client
//

pub trait TCollectorSyncClient {
	fn submit_batches(
		&mut self,
		batches: Vec<Batch>,
	) -> thrift::Result<Vec<BatchSubmitResponse>>;
}

pub trait TCollectorSyncClientMarker {}
