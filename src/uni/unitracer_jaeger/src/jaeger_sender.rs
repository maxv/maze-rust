pub trait JaegerSender {
	fn write_to_out_protocol(
		&self,
		o_prot: &mut dyn thrift::protocol::TOutputProtocol,
	) -> thrift::Result<()>;

	fn read_from_in_protocol(
		i_prot: &mut dyn thrift::protocol::TInputProtocol
	) -> thrift::Result<Self>
	where
		Self: std::marker::Sized;
}
