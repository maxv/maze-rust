use crate::TraceSender as TraceSenderJaeger;

use unitracer::{
	TraceSender,
	TraceSenderFactory,
};


use std::{
	cell::RefCell,
	rc::Rc,
};

pub struct Factory {
	pub host: String,
	pub port: String,
}

impl TraceSenderFactory for Factory {
	fn call(&mut self) -> Rc<RefCell<dyn TraceSender>> {
		return Rc::new(RefCell::new(TraceSenderJaeger::new(
			self.host.clone(),
			self.port.clone(),
		)));
	}
}
