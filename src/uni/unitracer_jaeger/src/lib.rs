extern crate log;
extern crate thrift;
extern crate unitracer;

mod jaeger_sender;

mod trace_sender;
pub use trace_sender::{
	send_batch,
	TraceSender,
};

mod factory;

pub fn init(
	host: String,
	port: String,
) {
	let f = Box::new(factory::Factory {
		host,
		port,
	});
	unitracer::set_trace_sender_factory(f);
}
