#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_gui;

mod combo_box;
pub use self::combo_box::ComboBox;

mod macros;

pub mod prelude {
	pub use crate::{
		u_combobox,
		ComboBox,
	};
}

pub const THEME_CLASS_NAME: &str = "u_combobox_theme_class";


#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_combo_box;
	pub use self::wasm_combo_box::WasmComboBox as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_combo_box;
	pub use self::qt5_combo_box::Qt5ComboBox as Platform;
}
