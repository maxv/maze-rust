use uniui_gui::prelude::*;

use std::sync::mpsc::Sender;

pub struct Qt5ComboBox {}

impl Qt5ComboBox {
	pub fn new() -> Qt5ComboBox {
		return Qt5ComboBox {};
	}

	pub fn set_value(
		&mut self,
		_id: Option<usize>,
	) {
		log::error!("Qt5ComboBox is not implemented. Yet.");
	}

	pub fn update_items<T: 'static + Clone + Eq>(
		&mut self,
		_items: &Vec<(T, String)>,
	) {
		log::error!("Qt5ComboBox is not implemented. Yet.");
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_sender: Sender<usize>,
	) -> Result<NativeWidget, ()> {
		log::error!("Qt5ComboBox is not implemented. Yet.");
		return Err(());
	}
}
