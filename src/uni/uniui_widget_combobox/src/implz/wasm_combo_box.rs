use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::{
	Element,
	HtmlOptionElement,
	HtmlSelectElement,
};

use crate::implz::wasm_bindgen::{
	closure::Closure,
	JsCast,
};

use std::sync::mpsc::Sender;


pub struct WasmComboBox {
	closure: Option<Closure<dyn FnMut()>>,
	native: OptionalNative<HtmlSelectElement>,
}

impl WasmComboBox {
	pub fn new() -> WasmComboBox {
		return WasmComboBox {
			closure: None,
			native: OptionalNative::new(),
		};
	}

	pub fn set_value(
		&mut self,
		id: Option<usize>,
	) {
		match self.native.as_mut() {
			Some(native) => {
				native.set_value(&id.unwrap_or(0).to_string());
			},
			None => {},
		};
	}

	pub fn update_items<T: 'static + Clone + Eq>(
		&mut self,
		items: &Vec<(T, String)>,
	) {
		match self.native.as_mut() {
			Some(n) => {
				{
					let n = n.clone().dyn_into::<Element>().unwrap();
					while n.child_nodes().length() > 0 {
						match n.child_nodes().get(0) {
							Some(c) => {
								let remove_result = n.remove_child(&c);
								match remove_result {
									Ok(_) => {},
									Err(e) => error!("remove err:{:?}", e),
								}
							},
							None => error!("child not found"),
						}
					}
				}

				for (id, (_, description)) in items.iter().enumerate() {
					let option_element =
						HtmlOptionElement::new_with_text(&description).unwrap();
					option_element.set_value(&id.to_string());
					n.add_with_html_option_element(&option_element).unwrap();
				}
			},
			None => {},
		}
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		sender: Sender<usize>,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("select").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let element = node.dyn_into::<HtmlSelectElement>().unwrap();

		let element_clone = element.clone();
		let closure = Closure::wrap(Box::new(move || {
			match element_clone.value().parse::<usize>() {
				Ok(i) => sender.send_silent(i),
				Err(_) => {},
			};
		}) as Box<dyn FnMut()>);
		element.set_onchange(Some(closure.as_ref().unchecked_ref()));
		self.closure = Some(closure);

		self.native.replace(element.clone());

		Ok(From::from(element))
	}
}
