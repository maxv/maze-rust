use crate::implz::Platform;

use uniui_gui::prelude::*;

use std::sync::mpsc::{
	self,
	Receiver,
	Sender,
};

/// Widget with list of options where user can select one
pub struct ComboBox<T: 'static + Clone + Eq> {
	items: Vec<(T, String)>,
	platform: Platform,
	sender: Sender<usize>,
	receiver: Receiver<usize>,
	slot_set_items: SlotImpl<Vec<(T, String)>>,

	signal_selection_changed: Signal<T>,
	slot_set_selected_value: SlotImpl<T>,
}

impl<T: 'static + Clone + Eq> ComboBox<T> {
	pub fn new() -> ComboBox<T> {
		let (sender, receiver) = mpsc::channel();
		return ComboBox {
			items: Vec::new(),
			platform: Platform::new(),
			sender,
			receiver,
			slot_set_items: SlotImpl::new(),
			signal_selection_changed: Signal::new(),
			slot_set_selected_value: SlotImpl::new(),
		};
	}

	pub fn slot_set_items(&self) -> &dyn Slot<Vec<(T, String)>> {
		return &self.slot_set_items;
	}

	pub fn signal_selection_changed(&mut self) -> &mut Signal<T> {
		return &mut self.signal_selection_changed;
	}

	pub fn slot_set_selected_value(&self) -> &dyn Slot<T> {
		return &self.slot_set_selected_value;
	}

	pub fn add_item(
		&mut self,
		value: T,
		description: String,
	) {
		self.items.push((value, description));
	}

	fn update_selected_value(&mut self) {
		if let Some(v) = self.slot_set_selected_value.last() {
			let mut id = None;
			for (item_id, (item_value, _)) in self.items.iter().enumerate() {
				if *item_value == v {
					id = Some(item_id);
				}
			}
			self.platform.set_value(id);
		}
	}

	fn update_items(&mut self) {
		self.platform.update_items(&self.items);
	}
}

impl<T: 'static + Clone + Eq> Widget for ComboBox<T> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let result = self.platform.to_native(widget_generator, self.sender.clone());
		self.update_items();
		self.update_selected_value();
		return result;
	}
}

impl<T: 'static + Clone + Eq> DataProcessor for ComboBox<T> {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		self.update_selected_value();

		for id in self.receiver.try_recv() {
			match self.items.get(id) {
				Some((value, _)) => {
					self.signal_selection_changed.emit(value.clone());
				},
				None => error!("item for id:{} not found", id),
			}
		}

		if let Some(t) = self.slot_set_items.last() {
			self.items = t;
			self.update_items();
		}
	}
}
