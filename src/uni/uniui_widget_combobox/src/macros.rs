#[doc(hidden)]
#[macro_export]
macro_rules! u_combobox_int {
	($b:ident,signal_selection_changed: $va:expr) => {
		$b.signal_selection_changed().connect_slot($va);
	};
	($b:ident,slot_set_selected_value: $va:expr) => {
		$va.connect_slot($b.slot_set_selected_value());
	};
	($b:ident,slot_set_selected_value_proxy: $va:expr) => {
		$va = $b.slot_set_selected_value().proxy();
	};
	($b:ident,slot_set_items: $va:expr) => {
		$va.connect_slot($b.slot_set_items());
	};
	($b:ident,slot_set_items_proxy: $va:expr) => {
		$va = $b.slot_set_items().proxy();
	};
}

/// Simplifies [ComboBox\<T\>](crate::ComboBox) creation
///
/// Parameters:
/// * signal_selection_changed: Slot<T>,
/// * slot_set_selected_value: Signal<T>,
/// * slot_set_selected_value_proxy: identifier,
/// * slot_set_items: Signal<Vec<(T, String)>>,
/// * slot_set_items_proxy: identifier
///
/// Example:
/// ```
/// let slot_set_items;
/// let combo_box = u_combobox! {
///     default: Default::default(),
///     slot_set_items_proxy: slotproxy_set_items,
/// };
/// ```
#[macro_export]
macro_rules! u_combobox {
	($($param_name:ident: $val:expr),*$(,)*) => {{
		let mut result = $crate::ComboBox::new();
		$($crate::u_combobox_int!(result, $param_name: $val);)*;
		result
	}};
}
