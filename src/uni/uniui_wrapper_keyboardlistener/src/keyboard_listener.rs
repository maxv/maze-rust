use super::implz::Platform;

use uniui_wrapper_focusable::Focusable;

use uniui_gui::prelude::*;

use std::sync::mpsc::{
	self,
	Receiver,
};

pub struct KeyboardListener<T: 'static + Widget> {
	inner_element: Focusable<T>,
	keyboard_receiver: Receiver<u32>,
	keydown_signal: Signal<u32>,
	platform_listener: Platform,
}

impl<T: 'static + Widget> KeyboardListener<T> {
	pub fn new(inner_element: T) -> KeyboardListener<T> {
		let (keyboard_sender, keyboard_receiver) = mpsc::channel();

		return KeyboardListener {
			inner_element: Focusable {
				widget: inner_element,
			},
			platform_listener: Platform::new(keyboard_sender),
			keyboard_receiver,
			keydown_signal: Signal::new(),
		};
	}

	pub fn keydown_signal(&mut self) -> &mut Signal<u32> {
		return &mut self.keydown_signal;
	}
}


impl<T: 'static + Widget> Widget for KeyboardListener<T> {
	fn to_native(
		&mut self,
		document: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform_listener.to_native(document, &mut self.inner_element);
	}

	fn draw(&mut self) {
		self.inner_element.draw();
	}
}


impl<T: 'static + Widget> DataProcessor for KeyboardListener<T> {
	fn process_data(
		&mut self,
		msec_since_app_epoch: i64,
		app: &mut dyn Application,
	) {
		for d in self.keyboard_receiver.try_iter() {
			self.keydown_signal.emit(d);
		}

		self.inner_element.process_data(msec_since_app_epoch, app);
	}
}
