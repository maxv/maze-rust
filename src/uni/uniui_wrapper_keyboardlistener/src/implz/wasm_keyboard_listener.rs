use uniui_gui::prelude::*;

use std::sync::mpsc::Sender;

use crate::implz::web_sys::KeyboardEvent;

use crate::implz::wasm_bindgen::closure::Closure;

use crate::implz::wasm_bindgen::JsCast;

pub struct WasmKeyboardListener {
	closure: Closure<dyn FnMut(KeyboardEvent)>,
}

impl WasmKeyboardListener {
	pub fn new(keyboard_sender: Sender<u32>) -> WasmKeyboardListener {
		let closure = Closure::wrap(Box::new(move |event: KeyboardEvent| {
			keyboard_sender.send_silent(event.key_code());
		}) as Box<dyn FnMut(KeyboardEvent)>);

		return WasmKeyboardListener {
			closure,
		};
	}

	pub fn to_native<T: Widget>(
		&mut self,
		document: &mut dyn WidgetGenerator,
		inner_element: &mut T,
	) -> Result<NativeWidget, ()> {
		let inner_native = inner_element.to_native(document)?;

		let unchecked_ref = self.closure.as_ref().unchecked_ref();
		inner_native.add_event_listener_silent("keydown", unchecked_ref);

		Ok(inner_native)
	}
}
