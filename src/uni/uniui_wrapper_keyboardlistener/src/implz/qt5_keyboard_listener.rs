use uniui_gui::prelude::*;

use std::sync::mpsc::Sender;


pub struct Qt5KeyboardListener {}

impl Qt5KeyboardListener {
	pub fn new(_keyboard_sender: Sender<u32>) -> Qt5KeyboardListener {
		return Qt5KeyboardListener {};
	}

	pub fn to_native<T: Widget>(
		&mut self,
		_document: &mut dyn WidgetGenerator,
		_inner_element: &mut T,
	) -> Result<NativeWidget, ()> {
		Err(())
	}
}
