extern crate uniui_core;
extern crate uniui_gui;
extern crate uniui_wrapper_focusable;


mod keyboard_listener;
pub use self::keyboard_listener::KeyboardListener;

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_keyboard_listener;
	pub use self::wasm_keyboard_listener::WasmKeyboardListener as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_keyboard_listener;
	pub use self::qt5_keyboard_listener::Qt5KeyboardListener as Platform;
}
