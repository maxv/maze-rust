use crate::{
	implz::Platform,
	BorderStyle,
};

use uniui_gui::{
	prelude::*,
	Color,
	SizeUnit,
};

pub struct Border<T: 'static + Widget> {
	pub widget: T,
	pub top_left_radius: Option<SizeUnit>,
	pub top_right_radius: Option<SizeUnit>,
	pub bottom_left_radius: Option<SizeUnit>,
	pub bottom_right_radius: Option<SizeUnit>,
	pub color: Option<Color>,
	pub width: Option<SizeUnit>,
	pub style: Option<BorderStyle>,
}

impl<T: 'static + Widget> Widget for Border<T> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;
		Platform::apply_to(
			&e,
			&self.top_left_radius,
			&self.top_right_radius,
			&self.bottom_left_radius,
			&self.bottom_right_radius,
			&self.color,
			&self.width,
			&self.style,
		);

		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for Border<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
	}
}
