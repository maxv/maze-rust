use crate::BorderStyle;

use uniui_gui::{
	prelude::*,
	Color,
	NativeWidget,
	SizeUnit,
};

pub struct WasmBorder {}

impl WasmBorder {
	pub fn apply_to(
		e: &NativeWidget,
		top_left_radius: &Option<SizeUnit>,
		top_right_radius: &Option<SizeUnit>,
		bottom_left_radius: &Option<SizeUnit>,
		bottom_right_radius: &Option<SizeUnit>,
		color: &Option<Color>,
		width: &Option<SizeUnit>,
		style: &Option<BorderStyle>,
	) {
		let s = e.style();
		if let Some(su) = top_left_radius.as_ref() {
			su.apply_as_property_value(&s, "border-top-left-radius");
		}

		if let Some(su) = top_right_radius.as_ref() {
			su.apply_as_property_value(&s, "border-top-right-radius");
		}

		if let Some(su) = bottom_left_radius.as_ref() {
			su.apply_as_property_value(&s, "border-bottom-left-radius");
		}

		if let Some(su) = bottom_right_radius.as_ref() {
			su.apply_as_property_value(&s, "border-bottom-right-radius");
		}

		if let Some(su) = width.as_ref() {
			su.apply_as_property_value(&s, "border-width");
		}

		if let Some(color_str) = color.as_ref().map(Color::html_string) {
			s.set_property_silent("border-color", &color_str);
		}

		if let Some(style) = style.as_ref() {
			s.set_property_silent("border-style", &style.as_htmlstring());
		}
	}
}
