use crate::BorderStyle;

use uniui_gui::{
	Color,
	NativeWidget,
	SizeUnit,
};

pub struct Qt5Border {}

impl Qt5Border {
	pub fn apply_to(
		_e: &NativeWidget,
		_top_left_radius: &Option<SizeUnit>,
		_top_right_radius: &Option<SizeUnit>,
		_bottom_left_radius: &Option<SizeUnit>,
		_bottom_right_radius: &Option<SizeUnit>,
		_color: &Option<Color>,
		_width: &Option<SizeUnit>,
		_style: &Option<BorderStyle>,
	) {
		log::error!("Border not implemented for Qt5. Yet.");
	}
}
