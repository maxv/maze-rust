mod border;
pub use self::border::Border;


#[cfg(target_arch = "wasm32")]
mod implz {
	mod wasm_border;
	pub use self::wasm_border::WasmBorder as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_border;
	pub use self::qt5_border::Qt5Border as Platform;
}

mod macros;

pub enum BorderStyle {
	None,
	Solid,
}

impl BorderStyle {
	pub fn as_htmlstring(&self) -> &'static str {
		return match self {
			BorderStyle::None => "none",
			BorderStyle::Solid => "solid",
		};
	}
}

/// Contains [Border], [BorderStyle], [u_border!] and
/// [SizeUnit](uniui_gui::SizeUnit),
/// [Color](uniui_gui::Color).
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_border,
		Border,
		BorderStyle,
	};

	#[doc(hidden)]
	pub use uniui_gui::SizeUnit;

	#[doc(hidden)]
	pub use uniui_gui::Color;
}
