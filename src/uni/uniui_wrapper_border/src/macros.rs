#[doc(hidden)]
#[macro_export]
macro_rules! u_border_int {
	($b:ident,radius: $val:expr) => {
		$b.top_left_radius = Some($val);
		$b.top_right_radius = Some($val);
		$b.bottom_left_radius = Some($val);
		$b.bottom_right_radius = Some($val);
	};
	($b:ident,width: $val:expr) => {
		$b.width = Some($val);
	};
	($b:ident,color: $val:expr) => {
		$b.color = Some($val);
	};
	($b:ident,style: $val:expr) => {
		$b.style = Some($val);
	};
}

/// Simplifies [Border](crate::Border) creation
///
/// Parameters:
/// * widget (required): widget to wrap,
/// * radius (optional): [SizeUnit](uniui_gui::SizeUnit) to apply for
///   top/left/bottom/right corner radius,
/// * width (optional): [SizeUnit](uniui_gui::SizeUnit),
/// * color (optional): [Color](uniui_gui::Color),
/// * style (optional): [BorderStyle](crate::BorderStyle),
///
/// Example:
/// ```
/// let widget_with_margins = u_margin! {
///     widget: widget_without_margins,
///     top: SizeUnit::PT(10.0),
///     bottom: SizeUnit::PT(20.0),
/// };
/// ```
#[macro_export]
macro_rules! u_border {
	(widget: $w:expr, $($param_name:ident: $val:expr,)*$(,)*) => {{
		let mut border = $crate::Border{
			widget: $w,
			top_left_radius: None,
			top_right_radius: None,
			bottom_left_radius: None,
			bottom_right_radius: None,
			color: None,
			width: None,
			style: None,
		};
		$($crate::u_border_int!(border, $param_name: $val);)*;
		border
	}};
}
