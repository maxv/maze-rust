pub const BORDER_COLOR: &str = "BorderColor";
pub const BORDER_WIDTH: &str = "BorderWidth";

pub const SURFACE_PADDING: &str = "SurfacePadding";
pub const SURFACE_BORDER_RADIUS: &str = "SurfaceRadius";

pub const PRIMARY_COLOR: &str = "PrimaryColor";
pub const PRIMARY_VARIANT_COLOR: &str = "PrimaryVariantColor";
pub const ON_PRIMARY_COLOR: &str = "OnPrimaryColor";


pub const SECONDARY_COLOR: &str = "SecondaryColor";
pub const SECONDARY_VARIANT_COLOR: &str = "SecondaryVariantColor";
pub const ON_SECONDARY_COLOR: &str = "OnSecondaryColor";

pub const BACKGROUND_COLOR: &str = "BackgroundColor";
pub const ON_BACKGROUND_COLOR: &str = "OnBackgroundColor";

pub const SURFACE_COLOR: &str = "SurfaceColor";
pub const ON_SURFACE_COLOR: &str = "OnSurfaceColor";

pub const ERROR_COLOR: &str = "ErrorColor";
pub const ON_ERROR_COLOR: &str = "OnErrorColor";
