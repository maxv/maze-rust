pub mod default_attribute;

pub use uniui_theme::*;

pub const SURFACE_THEME_CLASS: &str = "u_surface_theme_class";

pub fn prepare_theme(theme: &mut uniui_theme::application_theme::ApplicationTheme) {
	use uniui_theme::application_theme::{
		Attribute,
		AttributeValue::{
			Custom,
			Default as Def,
		},
	};

	use crate::default_attribute::*;


	let class = uniui_theme::application_theme::ClassTheme {
		name: uniui_build::BODY_THEME_CLASS_NAME.to_owned(),
		attributes: vec![
			Attribute {
				name: "background-color".to_owned(),
				value: Def(BACKGROUND_COLOR.to_owned()),
			},
			Attribute {
				name: "color".to_owned(),
				value: Def(ON_BACKGROUND_COLOR.to_owned()),
			},
			Attribute {
				name: "border-color".to_owned(),
				value: Def(BORDER_COLOR.to_owned()),
			},
			Attribute {
				name: "border-width".to_owned(),
				value: Def(BORDER_WIDTH.to_owned()),
			},
		],
	};
	theme.add_class(class);

	theme.add_class(uniui_theme::application_theme::ClassTheme {
		name: SURFACE_THEME_CLASS.to_owned(),
		attributes: vec![
			Attribute {
				name: "background-color".to_owned(),
				value: Def(SURFACE_COLOR.to_owned()),
			},
			Attribute {
				name: "color".to_owned(),
				value: Def(ON_SURFACE_COLOR.to_owned()),
			},
			Attribute {
				name: "border-color".to_owned(),
				value: Def(ON_BACKGROUND_COLOR.to_owned()),
			},
			Attribute {
				name: "border-style".to_owned(),
				value: Custom("solid".to_owned()),
			},
			Attribute {
				name: "padding".to_owned(),
				value: Def(SURFACE_PADDING.to_owned()),
			},
			Attribute {
				name: "border-radius".to_owned(),
				value: Def(SURFACE_BORDER_RADIUS.to_owned()),
			},
		],
	});

	theme.set_default(SURFACE_PADDING.to_owned(), "5px".to_owned());
	theme.set_default(SURFACE_BORDER_RADIUS.to_owned(), "3px".to_owned());
}
