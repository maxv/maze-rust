use uniui_gui::prelude::*;

#[derive(Debug, Copy, Clone)]
pub enum Position {
	Center,
	Bottom,
	Left,
	Top,
	Right,
}

#[derive(Debug, Copy, Clone)]
pub enum Repeat {
	NoRepeat,
	Repeat,
	XRepeat,
	YRepeat,
	Round,
	Space,
	Revert,
}

pub struct BackgroundImage<T: 'static + Widget> {
	widget: T,
	image: Property<String>,
	position: Property<Option<Position>>,
	repeat: Property<Option<Repeat>>,
	native: Option<NativeWidget>,
}

impl<T: 'static + Widget> BackgroundImage<T> {
	pub fn new(widget: T) -> BackgroundImage<T> {
		return BackgroundImage {
			widget,
			image: Property::new(format!("")),
			position: Property::new(None),
			repeat: Property::new(None),
			native: None,
		};
	}

	pub fn slot_set_image(&self) -> &dyn Slot<String> {
		return self.image.slot();
	}

	pub fn slot_set_position(&self) -> &dyn Slot<Option<Position>> {
		return self.position.slot();
	}

	pub fn slot_set_repeat(&self) -> &dyn Slot<Option<Repeat>> {
		return self.repeat.slot();
	}

	#[cfg(target_os = "linux")]
	fn update_image(
		&mut self,
		_app: &dyn Application,
	) {
		log::error!("BackgroundImage not implemented for Qt5. Yet.");
	}

	#[cfg(target_arch = "wasm32")]
	fn update_image(
		&mut self,
		app: &Application,
	) {
		if let Some(n) = self.native.as_mut() {
			let style = n.style();

			let path = app.link_from_base(self.image.as_ref());
			let s = format!("url({})", path);
			style.set_property_silent("background-image", &s);

			let position_str = match self.position.as_ref() {
				Some(Position::Center) => "center",
				Some(Position::Left) => "left",
				Some(Position::Top) => "top",
				Some(Position::Right) => "right",
				Some(Position::Bottom) => "bottom",
				None => "",
			};
			style.set_property_silent("background-position", position_str);

			let repeat_str = match self.repeat.as_ref() {
				Some(Repeat::NoRepeat) => "no-repeat",
				Some(Repeat::Repeat) => "repeat",
				Some(Repeat::XRepeat) => "repeat-x",
				Some(Repeat::YRepeat) => "repeat-y",
				Some(Repeat::Round) => "round",
				Some(Repeat::Space) => "space",
				Some(Repeat::Revert) => "revert",
				None => "none",
			};
			style.set_property_silent("background-repeat", repeat_str);
		}
	}
}

impl<T: 'static + Widget> Widget for BackgroundImage<T> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let e = self.widget.to_native(widget_generator)?;
		self.native = Some(e.clone());
		return Ok(e);
	}

	fn draw(&mut self) {
		self.widget.draw();
	}
}

impl<T: 'static + Widget> DataProcessor for BackgroundImage<T> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.widget.process_data(msec_since_app_start, app);
		if self.image.try_update() ||
			self.position.try_update() ||
			self.repeat.try_update()
		{
			self.update_image(app);
		}
	}
}
