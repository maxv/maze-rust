#[cfg(target_arch = "wasm32")]
pub fn msec_since_epoch() -> i64 {
	return (js_sys::Date::now() * 1000.0) as i64;
}

#[cfg(not(target_arch = "wasm32"))]
pub fn msec_since_epoch() -> i64 {
	use std::time::{
		SystemTime,
		UNIX_EPOCH,
	};

	return match SystemTime::now().duration_since(UNIX_EPOCH) {
		Ok(t) => t.as_micros() as i64,
		Err(_) => 0,
	};
}
