//! The crate doesn't designed to be used itself anywhere. Outside
//! uniui_* crates familiy.

pub use implz::*;

use thiserror::Error;

/// Different kind of errors
#[derive(Error, Debug)]
#[non_exhaustive]
pub enum UniNetError {
	#[error("Complicated error w/o known reason")]
	Unknown,
}

#[doc(hidden)]
#[cfg(not(target_arch = "wasm32"))]
pub mod implz {
	use super::UniNetError;

	/// HTTP-POST data as JSON to provided url
	pub fn post_json<F: 'static + FnOnce(u16, String)>(
		url: &str,
		data: &str,
		_f: F,
	) -> Result<(), UniNetError> {
		log::info!("post not implemented. url:{}, data:{}", url, data);
		return Ok(());
	}

	/// HTTP-GET data from provided url
	pub fn get<F: 'static + FnOnce(u16, String)>(
		url: &str,
		_f: F,
	) -> Result<(), UniNetError> {
		log::info!("get not implemented. address:{}", url);
		return Ok(());
	}

	/// Open provided location
	pub fn open(location: &str) -> Result<(), UniNetError> {
		log::info!("open not implemented. location:{}", location);
		return Ok(());
	}
}

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
pub mod implz {
	use super::UniNetError;

	use wasm_bindgen::{
		closure::Closure,
		JsValue,
	};
	use web_sys::{
		Request,
		RequestInit,
	};

	use std::{
		cell::RefCell,
		rc::Rc,
	};

	#[allow(unused_must_use)]
	pub fn post_json<F: 'static + FnOnce(u16, String)>(
		address: &str,
		data: &str,
		f: F,
	) -> Result<(), UniNetError> {
		let mut opts = RequestInit::new();
		opts.method("POST");
		opts.mode(web_sys::RequestMode::SameOrigin);
		opts.body(Some(&JsValue::from_str(data)));
		opts.redirect(web_sys::RequestRedirect::Manual);

		let request = Request::new_with_str_and_init(address, &opts).unwrap();
		request.headers().set("Content-Type", "application/json").unwrap();

		let window = web_sys::window().unwrap();

		let promise = window.fetch_with_request(&request);

		let o = Rc::new(RefCell::new(None));
		let oclone = o.clone();

		let o2 = Rc::new(RefCell::new(None));
		let oclone2 = o2.clone();

		let cb = Closure::once(move |v: JsValue| {
			let response: web_sys::Response = v.into();
			extract_payload(response, f);
			oclone.replace(None);
			oclone2.replace(None);
		});


		let oclone_err = o.clone();
		let oclone_err2 = o2.clone();

		let rej = Closure::once(move |_v: JsValue| {
			// process error
			oclone_err.replace(None);
			oclone_err2.replace(None);
		});

		promise.then2(&cb, &rej);
		o.replace(Some(cb));
		o2.replace(Some(rej));

		return Ok(());
	}

	#[allow(unused_must_use)]
	pub fn get<F: 'static + FnOnce(u16, String)>(
		address: &str,
		f: F,
	) -> Result<(), UniNetError> {
		let mut opts = RequestInit::new();
		opts.method("GET");
		opts.mode(web_sys::RequestMode::SameOrigin);
		opts.redirect(web_sys::RequestRedirect::Manual);

		let request = Request::new_with_str_and_init(address, &opts).unwrap();
		request.headers().set("Content-Type", "application/json").unwrap();
		let window = web_sys::window().unwrap();

		let promise = window.fetch_with_request(&request);

		let o = Rc::new(RefCell::new(None));
		let oclone = o.clone();
		let cb = Closure::once(move |v: JsValue| {
			let response: web_sys::Response = v.into();
			extract_payload(response, f);
			oclone.replace(None);
		});

		// process error
		promise.then(&cb);
		o.replace(Some(cb));

		return Ok(());
	}

	#[allow(unused_must_use)]
	fn extract_payload<F: 'static + FnOnce(u16, String)>(
		response: web_sys::Response,
		f: F,
	) {
		let code = response.status();

		let o = Rc::new(RefCell::new(None));
		let oclone = o.clone();
		let cb = Closure::once(move |v: JsValue| {
			match v.as_string() {
				None => {
					// process error
				},
				Some(s) => {
					f(code, s);
				},
			};
			oclone.replace(None);
		});

		response.text().unwrap().then(&cb);
		o.replace(Some(cb));
	}

	pub fn open(location: &str) -> Result<(), UniNetError> {
		return match web_sys::window() {
			Some(window) => {
				match window.open_with_url_and_target(location, "_self") {
					Ok(_) => Ok(()),
					Err(_) => Err(UniNetError::Unknown),
				}
			},
			None => Err(UniNetError::Unknown),
		};
	}
}
