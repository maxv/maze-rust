/// Simplifies [GroupBox](crate::GroupBox) creation
///
/// Parameters:
/// * text (required): String,
/// * widget (required): Widget,
///
/// Example:
/// ```
/// TBD
/// ```
#[macro_export]
macro_rules! u_groupbox {
	(text: $t:expr,widget: $w:expr $(,)?) => {{
		let gbox = $crate::GroupBox::new($t, $w);
		gbox
		}};
}
