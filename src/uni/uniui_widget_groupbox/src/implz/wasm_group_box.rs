use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use crate::implz::web_sys::HtmlElement;


pub struct WasmGroupBox {
	native: OptionalNative<HtmlElement>,
}

impl WasmGroupBox {
	pub fn new() -> WasmGroupBox {
		return WasmGroupBox {
			native: OptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		inner: &mut dyn Widget,
		legend: &str,
	) -> Result<NativeWidget, ()> {
		let inner_native = inner.to_native(widget_generator)?;

		let legend_element = widget_generator.create_element("legend").unwrap();
		legend_element.set_inner_text(legend);

		let node = widget_generator.create_element("fieldset").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		node.append_child(&legend_element).unwrap();
		node.append_child(&inner_native).unwrap();

		self.native.replace(From::from(node.clone()));
		Ok(node)
	}
}
