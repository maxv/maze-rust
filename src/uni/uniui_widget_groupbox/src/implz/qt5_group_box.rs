use uniui_gui::prelude::*;

pub struct Qt5GroupBox {}

impl Qt5GroupBox {
	pub fn new() -> Qt5GroupBox {
		return Qt5GroupBox {};
	}

	pub fn to_native(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_inner: &mut dyn Widget,
		_legend: &str,
	) -> Result<NativeWidget, ()> {
		log::error!("GroupBox is not implemented for Qt. Yet.");
		return Err(());
	}
}
