extern crate uniui_core;
extern crate uniui_gui;

mod group_box;
pub use self::group_box::GroupBox;

mod macros;

pub mod prelude {
	pub use crate::{
		u_groupbox,
		GroupBox,
	};
}

pub const THEME_CLASS_NAME: &str = "u_groupbox_theme_class";

#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_group_box;
	pub use self::wasm_group_box::WasmGroupBox as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_group_box;
	pub use self::qt5_group_box::Qt5GroupBox as Platform;
}
