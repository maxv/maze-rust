use crate::implz::Platform;

use uniui_gui::prelude::*;


pub struct GroupBox<W: 'static + Widget> {
	inner: W,
	legend: String,
	platform: Platform,
}

impl<W: 'static + Widget> GroupBox<W> {
	pub fn new(
		legend: String,
		inner: W,
	) -> GroupBox<W> {
		return GroupBox {
			inner,
			legend,
			platform: Platform::new(),
		};
	}
}

impl<W: 'static + Widget> Widget for GroupBox<W> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(widget_generator, &mut self.inner, &self.legend);
	}

	fn draw(&mut self) {
		self.inner.draw();
	}
}

impl<W: 'static + Widget> DataProcessor for GroupBox<W> {
	fn process_data(
		&mut self,
		msec_since_app_start: i64,
		app: &mut dyn Application,
	) {
		self.inner.process_data(msec_since_app_start, app);
	}
}
