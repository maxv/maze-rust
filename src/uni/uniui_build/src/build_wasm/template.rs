pub const JS_FILE_NAME_PLACEHOLDER: &str = "JS_FILE_NAME";
pub const WASM_FILE_NAME_PLACEHOLDER: &str = "WASM_FILE_NAME";
pub const HTML_FILE_NAME_PLACEHOLDER: &str = "HTML_FILE_NAME";
pub const MODULE_NAME_PLACEHOLDER: &str = "MODULE_NAME";
pub const MODULE_PAGE_PATH_PLACEHOLDER: &str = "MODULE_PAGE_PATH";
pub const CUSTOM_THEME_PLACEHOLDER: &str = "CUSTOM_THEME";

pub const HTML: &str = "
<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'/>
	<meta
		name='viewport'
		content='
			width=device-width,
			initial-scale=1.0,
			maximum-scale=1.0,
			user-scalable=no
		'
	/>
	<title>Loading...</title>
	<style>
		* {
			margin: 0px;
			color: inherit;
			background-color: inherit;
			font-family: inherit;
			font-size: inherit;
			font-weight: inherit;
		}
		html {
			min-height: 100vh;
			max-height: 100vh;
			height: 100vh;
		}
		body {
			min-height: 100vh;
			max-height: 100vh;
			height: 100vh;
		}
	</style>

	<style>
		CUSTOM_THEME
	</style>
</head>
<body class='BODY_THEME_CLASS_NAME'>
</body>
	<script src='./JS_FILE_NAME'></script>

	<script>
		async function run() {
			await wasm_bindgen('./WASM_FILE_NAME');
		}
		run();
	</script>


</html>
";
