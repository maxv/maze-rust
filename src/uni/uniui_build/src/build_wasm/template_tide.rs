pub const MODULE: &str = r#"
pub mod MODULE_NAME {
	use tide::Response;
	use tide::Request;
	use tide::Route;

	async fn html<T>(_: Request<T>) -> tide::Result {
		let v = include_bytes!("HTML_FILE_NAME");
		let rv: &[u8] = v;
		let mut response = Response::new(200);
		response.set_body(rv);
		response.set_content_type(tide::http::mime::HTML);
		return Ok(response);
	}

	async fn js<T>(_: Request<T>) -> tide::Result {
		let v = include_bytes!("JS_FILE_NAME");
		let rv: &[u8] = v;
		let mut response = Response::new(200);
		response.set_body(rv);
		response.set_content_type(tide::http::mime::JAVASCRIPT);
		return Ok(response);
	}

	async fn wasm<T>(_: Request<T>) -> tide::Result {
		let v = include_bytes!("WASM_FILE_NAME");
		let rv: &[u8] = v;
		let mut response = Response::new(200);
		response.set_body(rv);
		response.set_content_type(tide::http::mime::WASM);
		return Ok(response);
	}

	pub fn attach<'a, T: Clone>(mut route: Route<'a, T>)
		where T:'static + std::marker::Sync + std::marker::Send
	{
		route.at("").get(html);
		route.at(".js").get(js);
		route.at("_bg.wasm").get(wasm);
	}
}
"#;

pub const GLOBAL_PRE: &str = "
#[allow(dead_code)]
pub fn attach<T: 'static + Send + Sync + Clone>(app: &mut tide::Server<T>) {
";

pub const GLOBAL_MODULE: &str = "
	MODULE_NAME::attach(app.at(\"MODULE_PAGE_PATH\"));
";

pub const GLOBAL_POST: &str = "
}
";
