pub const PROLOG: &str = "

#[derive(Responder)]
#[response(status = 200, content_type = \"application/wasm\")]
struct WasmResponder {
	#[response]
	data: &'static [u8],
}

#[derive(Responder)]
#[response(status = 200, content_type = \"js\")]
struct JsResponder {
	#[response]
	data: &'static str,
}


#[derive(Responder)]
#[response(status = 200, content_type = \"html\")]
struct HtmlResponder {
	#[response]
	data: &'static str,
}
";

pub const MODULE: &str = "
pub mod MODULE_NAME {
	use rocket::Route;
	use super::WasmResponder;
	use super::HtmlResponder;
	use super::JsResponder;

	pub struct RouterProvider {}

	#[get(\"/_bg.wasm\")]
	fn hello_ws() -> WasmResponder {
		let bytes = include_bytes!(\"WASM_FILE_NAME\");
		return WasmResponder {
			data: bytes,
		};
	}


	#[get(\"/.js\")]
	fn hello_js() -> JsResponder {
		let bytes = include_str!(\"JS_FILE_NAME\");
		return JsResponder {
			data: bytes,
		};
	}

	#[get(\"/\")]
	fn hello_index() -> HtmlResponder {
		let bytes = include_str!(\"HTML_FILE_NAME\");
		return HtmlResponder {
			data: bytes,
		};
	}


	impl RouterProvider {
		pub fn routes(&self) -> Vec<Route> {
			return routes![hello_js, hello_ws, hello_index];
		}
	}
}
";

pub const GLOBAL_PRE: &str = "
#[allow(dead_code)]
pub fn mount_to(rocket: rocket::Rocket) -> rocket::Rocket {
";

pub const GLOBAL_MODULE: &str = "
	let rocket = rocket.mount(
		\"MODULE_PAGE_PATH\",
		MODULE_NAME::RouterProvider {}.routes()
	);
";

pub const GLOBAL_POST: &str = "
	return rocket;
}
";
