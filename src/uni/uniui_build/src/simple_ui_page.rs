#[derive(Debug, Clone)]
pub struct SimpleUiPage {
	path: String,
	module_name: String,
	module_path: String,
}

impl SimpleUiPage {
	pub fn new(
		path: String,
		module_name: String,
		module_path: String,
	) -> SimpleUiPage {
		return match path.ends_with("/") {
			false => {
				panic!(
					"UiPage's path have to ends with `/`. Incorrect path provided `{}`",
					path
				)
			},
			true => {
				SimpleUiPage {
					path,
					module_name,
					module_path,
				}
			},
		};
	}

	pub fn path(&self) -> &str {
		return &self.path;
	}

	pub fn module_name(&self) -> &str {
		return &self.module_name;
	}

	pub fn module_path(&self) -> &str {
		return &self.module_path;
	}
}
