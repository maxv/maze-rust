#[derive(Clone)]
pub struct ManifestProcessor {
	metadata: cargo_metadata::Metadata,
	manifest_path: String,
}

impl ManifestProcessor {
	pub fn new() -> Self {
		let manifest_path = std::env::var("CARGO_MANIFEST_DIR").unwrap();

		let metadata = cargo_metadata::MetadataCommand::new()
			.manifest_path(format!("{}/Cargo.toml", manifest_path))
			.features(cargo_metadata::CargoOpt::AllFeatures)
			.exec()
			.expect(&format!(
				"Cargo metadata command failed. Manifest directory:{}",
				manifest_path
			));

		return Self {
			metadata,
			manifest_path,
		};
	}

	pub fn manifest_path(&self) -> &str {
		return &self.manifest_path;
	}

	/// Returns manifest path for the crate `crate_name` based on the info from
	/// current crate's Cargo.toml.
	pub fn get_manifest_path(
		&self,
		crate_name: &str,
	) -> Option<String> {
		let manifest_path = self.metadata.packages.iter().find_map(|p| {
			match p.name == crate_name {
				false => None,
				true => Some(p.manifest_path.to_string_lossy().to_string()),
			}
		});

		return manifest_path;
	}
}
