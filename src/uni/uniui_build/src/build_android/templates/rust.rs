const ADB: &'static str = "$ADB$";
const DIR: &'static str = "$DIR$";

#[cfg(debug_assertions)]
fn gradle_command() -> String {
	return "instRuDeb".to_owned();
}

#[cfg(not(debug_assertions))]
fn gradle_command() -> String {
	// TODO: fix the issue and install release version
	return "instRuDeb".to_owned();
}

pub fn start(tag_name: Option<&str>) {
	let status = std::process::Command::new(ADB)
		.arg("logcat")
		.arg("-c")
		.status()
		.expect(&format!("Adb failed. Command:{}", ADB));
		
	if !status.success() {
		panic!("adb cleanup failed");
	}
	
	let status = std::process::Command::new("./gradlew")
		.arg("--no-daemon")
		.arg(gradle_command())
		.current_dir(DIR)
		.status()
		.expect("Gradle build failed. Please see error above");
		
	if !status.success() {
		panic!("Gradle build failed. Please see error above");
	}
	
	let mut command = std::process::Command::new(ADB);
	command.arg("logcat");
	
	if let Some(tag_name) = tag_name.as_ref() {
		command.arg("-s");
		command.arg(tag_name);
	}
	
	command.status().expect(&format!("Adb failed. Command:{}", ADB));
}
