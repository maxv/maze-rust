package $PACKAGE$

import android.app.Application

class Application: Application() {
    companion object {
        init {
            System.loadLibrary("uni_android")
        }
    }
}
