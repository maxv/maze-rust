package $PACKAGE$

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log

import java.util.concurrent.TimeUnit

class $ACTIVITY_NAME$ : AppCompatActivity() {
	external fun onCreateNative()
	external fun onTickNative(app_ptr: Long): Long
	external fun onCleanupNative(app_ptr: Long)

	// public to simplify JNI integration
	public var native: Long = 0L
	
	private val delay: Long = TimeUnit.MILLISECONDS.toMillis(15)
	
	private var handler: Handler? = null
	private var running: Boolean = false
	
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		onCreateNative();
		
		handler = Handler(Looper.getMainLooper());
	}
	
	override fun onStart() {
		super.onStart()
		running = true
		handler?.postDelayed(object: Runnable {
			override fun run() {
				if (running) {
					if (native != 0L) {
						native = onTickNative(native)
					}

					handler?.postDelayed(this, delay)
				}
			}

		}, delay)
	}

	override fun onStop() {
		running = false
		super.onStop()
	}
	
	override fun onDestroy() {
		if (native != 0L) {
			val old_native = native
			native = 0L
			onCleanupNative(old_native)
		}
		super.onDestroy()
	}
}
