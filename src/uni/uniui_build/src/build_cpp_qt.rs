extern crate cc;
extern crate pkg_config;

const QT_5_NOT_FOUND_ERROR: &str = "
Qt5Widgets library not found with pkg-config
Please check that dev packages for Qt5Widgets installed properly and
available via pkg-config.

Debian-based: $ sudo apt install qtbase5-dev
Fedora: ???
";

#[doc(hidden)]
pub fn build_x86_64_cpp_qt5(
	file_name: &str,
	lib_name: &str,
) {
	let os = std::env::var("CARGO_CFG_TARGET_OS")
		.expect("CARGO_CFG_TARGET_OS is not specified");

	if os == "linux" {
		let config = pkg_config::Config::new();
		let library = config.probe("Qt5Widgets").expect(QT_5_NOT_FOUND_ERROR);

		let mut compiler = cc::Build::new();
		compiler.cpp(true).file(file_name).flag("-std=c++11");
		for path in library.include_paths {
			compiler.include(path);
		}


		compiler.compile(lib_name);
	}
}
