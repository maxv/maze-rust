use uniui_gui::prelude::*;

use crate::implz::web_sys::MouseEvent;

use crate::implz::wasm_bindgen::closure::Closure;

use crate::implz::wasm_bindgen::JsCast;

use std::sync::mpsc::Sender;


pub struct WasmMouseClickListener {
	closure: Closure<dyn FnMut(MouseEvent)>,
}

impl WasmMouseClickListener {
	pub fn new(mouse_click_sender: Sender<(i32, i32)>) -> WasmMouseClickListener {
		let closure = Closure::wrap(Box::new(move |event: MouseEvent| {
			mouse_click_sender.send_silent((event.offset_x(), event.offset_y()));
		}) as Box<dyn FnMut(MouseEvent)>);

		return WasmMouseClickListener {
			closure,
		};
	}

	pub fn to_native<T: Widget>(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		inner_element: &mut T,
	) -> Result<NativeWidget, ()> {
		let inner_native = inner_element.to_native(widget_generator)?;

		let unchecked_ref = self.closure.as_ref().unchecked_ref();
		inner_native.add_event_listener_silent("click", unchecked_ref);

		Ok(inner_native)
	}
}
