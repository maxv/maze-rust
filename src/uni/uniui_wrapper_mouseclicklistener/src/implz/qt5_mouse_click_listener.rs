use uniui_gui::prelude::*;

use std::sync::mpsc::Sender;

pub struct Qt5MouseClickListener {}

impl Qt5MouseClickListener {
	pub fn new(_mouse_click_sender: Sender<(i32, i32)>) -> Qt5MouseClickListener {
		return Qt5MouseClickListener {};
	}

	pub fn to_native<T: Widget>(
		&mut self,
		_widget_generator: &mut dyn WidgetGenerator,
		_inner_element: &mut T,
	) -> Result<NativeWidget, ()> {
		Err(())
	}
}
