#[macro_use]
extern crate log;

extern crate uniui_core;
extern crate uniui_gui;

mod mouse_click_listener;
pub use self::mouse_click_listener::MouseClickListener;

mod macros;

pub mod prelude {
	pub use crate::{
		u_mouse_click_listener,
		MouseClickListener,
	};
}


#[cfg(target_arch = "wasm32")]
mod implz {
	extern crate wasm_bindgen;
	extern crate web_sys;

	mod wasm_mouse_click_listener;
	pub use self::wasm_mouse_click_listener::WasmMouseClickListener as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	mod qt5_mouse_click_listener;
	pub use self::qt5_mouse_click_listener::Qt5MouseClickListener as Platform;
}
