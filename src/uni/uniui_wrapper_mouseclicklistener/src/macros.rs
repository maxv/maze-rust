#[doc(hidden)]
#[macro_export]
macro_rules! u_mouse_click_listener_int {
	($b:ident,signal_clicked: $va:expr) => {
		$b.signal_clicked().connect_slot($va);
	};
}


/// Simpliest way to create and setup MouseClickListener.
///
/// Parameters:
/// * widget: the widget to wrap (required)
/// * signal_clicked: the slot to receive click coordinates: Slot<(i32, i32)>
///
/// Example:
/// ```
/// let slot_clicked = SlotImpl::new();
/// let my_widget_with_listener = u_mouse_click_listener! {
///     widget: my_widget,
///     signal_clicked: slot_clicked,
/// };
/// ```
#[macro_export]
macro_rules! u_mouse_click_listener {
	(widget: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut listener = $crate::MouseClickListener::new($w);
		$($crate::u_mouse_click_listener_int!(listener, $param_name: $val);)*;
		listener
	}};
}
