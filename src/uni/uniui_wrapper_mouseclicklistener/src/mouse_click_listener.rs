use crate::implz::Platform;

use uniui_gui::prelude::*;

use std::sync::mpsc::{
	self,
	Receiver,
};

/// Allows listening for click events and click's x and y coordinates
pub struct MouseClickListener<T: 'static + Widget> {
	pub inner_element: T,
	mouse_click_receiver: Receiver<(i32, i32)>,
	signal_click: Signal<(i32, i32)>,
	platform_listener: Platform,
}

impl<T: 'static + Widget> MouseClickListener<T> {
	pub fn new(inner_element: T) -> MouseClickListener<T> {
		let (mouse_click_sender, mouse_click_receiver) = mpsc::channel();

		return MouseClickListener {
			inner_element,
			mouse_click_receiver,
			signal_click: Signal::new(),
			platform_listener: Platform::new(mouse_click_sender),
		};
	}

	pub fn signal_clicked(&mut self) -> &mut Signal<(i32, i32)> {
		return &mut self.signal_click;
	}

	pub fn set_inner_element(
		&mut self,
		inner: T,
	) {
		self.inner_element = inner;
	}
}

impl<T: 'static + Widget> Widget for MouseClickListener<T> {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self
			.platform_listener
			.to_native(widget_generator, &mut self.inner_element);
	}

	fn draw(&mut self) {
		self.inner_element.draw();
	}
}


impl<T: 'static + Widget> DataProcessor for MouseClickListener<T> {
	fn process_data(
		&mut self,
		msec_since_app_epoch: i64,
		app: &mut dyn Application,
	) {
		for d in self.mouse_click_receiver.try_iter() {
			info!("processed lick:{:?}", d);
			self.signal_click.emit(d);
		}

		self.inner_element.process_data(msec_since_app_epoch, app);
	}
}
