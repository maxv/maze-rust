// Buildsystem will put all our files into the package with the 
// same name as our crate's name is.
package uniui_widget_button

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Button

@SuppressLint("AppCompatCustomView")
class UniuiWidgetButton(context: Context?) : Button(context) {
    init {
        setOnClickListener {
            if (native != 0L) {
                native = clicked(native)
            }
        }
    }
    
    protected fun finalize() {
    	val old_native = native
    	native = 0
        free(old_native)
    }

    private external fun clicked(native: Long): Long
    private external fun free(native: Long)
    
    // public to simplify JNI access
    public var native: Long = 0
}

