#[doc(hidden)]
#[macro_export]
macro_rules! u_button_int {
	($b:ident,signal_clicked: $va:expr) => {
		$b.signal_clicked().connect_slot($va);
	};
}

/// Simplifies [Button](crate::Button) creation
///
/// Parameters:
/// * text (required): String,
/// * signal_clicked (optional): [&Slot\<()\>](uniui_core::Slot), will be connected to
///   [Button::signal_clicked].
///
/// Example:
/// ```
/// let slot_button_clicked = SlotImpl::new();
///
/// let button = u_buton! {
///     text: "OK".to_owned(),
///     signal_clicked: &slot_button_clicked,
/// };
/// ```
#[macro_export]
macro_rules! u_button {
	(text: $w:expr, $($param_name:ident: $val:expr),*$(,)*) => {{
		let mut button = $crate::Button::new($w);
		$($crate::u_button_int!(button, $param_name: $val);)*;
		button
	}};
}
