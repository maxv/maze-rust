mod button;
pub use self::button::Button;

mod macros;

/// Contains [Button] and [u_button!].
pub mod prelude {
	#[doc(hidden)]
	pub use crate::{
		u_button,
		Button,
	};
}

/// The class-name for theme configuration. Please check [uniui_theme] for
/// more info.
pub const THEME_CLASS_NAME: &str = "u_button_theme_class";

#[doc(hidden)]
#[cfg(target_arch = "wasm32")]
mod implz {
	mod wasm;
	pub use self::wasm::WasmButton as Platform;
}

#[doc(hidden)]
#[cfg(target_os = "linux")]
mod implz {
	uniui_gui::desktop_qt! {
		mod qt5;
		pub use self::qt5::Qt5Button as Platform;
	}

	uniui_gui::desktop_noop! {
		mod noop_button;
		pub use self::noop_button::NoopButton as Platform;
	}
}

#[doc(hidden)]
#[cfg(target_os = "android")]
extern crate uni_tmp_jni as jni;

#[doc(hidden)]
#[cfg(target_os = "android")]
pub mod implz {
	// it's pub to provide access to inner functions
	// it's necessary to reexport them from the main library until
	// https://github.com/rust-lang/rfcs/issues/2771
	pub mod android;
	pub use self::android::AndroidButton as Platform;
}
