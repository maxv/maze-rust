use std::sync::mpsc::{
	self,
	Receiver,
};

use uniui_core::Signal;

use uniui_gui::{
	Application,
	DataProcessor,
	NativeWidget,
	Widget,
	WidgetGenerator,
};

use super::implz::Platform;

/// Widget wich have a text and can be pressed by user.
///
/// Example (but you're better to use macros insted):
/// ```
/// let slot_button_clicked = SlotImpl::new();
/// let button = Button::new("PushMe".to_owned());
/// button.signal_clicked().connect_slot(&slot_button_clicked);
/// ```
/// # Macros
/// You may be interested in [u_button!](u_button!) macros
/// to simplify `Button` creation. Better example with macros:
/// ```
/// use uniui_widget_button::prelude::*;
///
/// let slot_button_clicked = SlotImpl::new();
///
/// let button = u_buton! {
///     text: "PushMe".to_owned(),
///     signal_clicked: &slot_button_clicked,
/// };
/// ```
pub struct Button {
	text: String,
	signal_clicked: Signal<()>,

	receiver: Receiver<()>,
	platform: Platform,
}

impl Button {
	/// Creates new `Button` with predefined text on it
	pub fn new(text: String) -> Self {
		let (sender, receiver) = mpsc::channel();
		return Self {
			signal_clicked: Signal::new(),
			text,
			receiver,
			platform: Platform::new(sender),
		};
	}

	/// The signal will be triggered every time user clicks the button.
	///
	/// You can connect your slots to the signal to receive notification
	/// about user clicks.
	pub fn signal_clicked(&mut self) -> &mut Signal<()> {
		return &mut self.signal_clicked;
	}
}

impl Widget for Button {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(widget_generator, self.text.clone());
	}
}

impl DataProcessor for Button {
	fn process_data(
		&mut self,
		_: i64,
		_: &mut dyn Application,
	) {
		if let Some(()) = self.receiver.try_iter().last() {
			self.signal_clicked.emit(());
		}
	}
}
