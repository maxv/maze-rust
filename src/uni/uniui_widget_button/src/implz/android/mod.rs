mod android_button;
pub use self::android_button::AndroidButton;

pub mod native_button;

pub fn clicked(
	_: jni::JNIEnv,
	_: jni::objects::JClass,
	sender_ptr: jni::sys::jlong,
) -> jni::sys::jlong {
	use std::sync::mpsc::Sender;

	let sender = unsafe { Box::from_raw(sender_ptr as *mut Sender<()>) };
	let _ = sender.send(());
	return Box::into_raw(sender) as i64;
}

pub fn free(
	_: jni::JNIEnv,
	_: jni::objects::JClass,
	sender_ptr: jni::sys::jlong,
) {
	use std::sync::mpsc::Sender;
	let _to_be_killed = unsafe { Box::from_raw(sender_ptr as *mut Sender<()>) };
}
