use std::sync::mpsc::Sender;

use uniui_gui::{
	implz::android::{
		jni_error::JniError,
		AndroidOptionalNative as OptionalNative,
	},
	NativeWidget,
	WidgetGenerator,
};

use super::native_button::{
	Button,
	NativeButton,
};

pub struct AndroidButton {
	native: OptionalNative<NativeButton>,
	sender: Sender<()>,
}

impl AndroidButton {
	pub fn new(sender: Sender<()>) -> Self {
		return Self {
			native: OptionalNative::new(),
			sender,
		};
	}

	pub fn to_native(
		&mut self,
		wg: &mut dyn WidgetGenerator,
		text: String,
	) -> Result<NativeWidget, ()> {
		let mut ton = || -> Result<NativeButton, JniError> {
			let button = Button::new_for_context(wg.context())?;
			button.set_text(&text)?;
			button.set_native(self.sender.clone())?;
			self.native.replace(&button)?;
			Ok(button)
		};

		return match ton() {
			Ok(b) => Ok(b.into_global_ref()),
			Err(e) => {
				// clear exception?
				log::error!("to_native err:{:?}", e);
				Err(())
			},
		};
	}
}
