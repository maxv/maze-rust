use std::sync::mpsc::Sender;

use uniui_gui::{
	NativeWidget,
	WidgetGenerator,
};

pub struct NoopButton {
}

impl NoopButton {
	pub fn new(_sender: Sender<()>) -> NoopButton {
		return NoopButton {
		};
	}

	pub fn to_native(
		&mut self,
		_: &mut dyn WidgetGenerator,
		_text: String,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}
}
