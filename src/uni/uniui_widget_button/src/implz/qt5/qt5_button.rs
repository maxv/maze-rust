use uniui_gui::{
	implz::qt5::Qt5OptionalNative,
	NativeWidget,
	WidgetGenerator,
};

use std::{
	ffi::{
		c_void,
		CString,
	},
	os::raw::c_char,
	sync::mpsc::Sender,
};

extern "C" {
	fn uniui_widget_button_create(text: *const c_char) -> *mut c_void;

	fn uniui_widget_button_set_global_click_callback(cb: extern "C" fn(*mut c_void));

	fn uniui_widget_button_set_click_listener(
		button: *mut c_void,
		sender: *mut c_void,
	);
}


extern "C" fn callback(sender: *mut std::ffi::c_void) {
	let sender: &mut Sender<()> = unsafe { &mut *(sender as *mut Sender<()>) };
	match sender.send(()) {
		Ok(()) => log::trace!("send ok"),
		Err(e) => log::error!("send err:{:?}", e),
	}
}


pub struct Qt5Button {
	sender: Sender<()>,
	native: Qt5OptionalNative,
}

impl Qt5Button {
	pub fn new(sender: Sender<()>) -> Qt5Button {
		return Qt5Button {
			sender,
			native: Qt5OptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		_: &mut dyn WidgetGenerator,
		text: String,
	) -> Result<NativeWidget, ()> {
		let ptr = unsafe {
			let cstring_text = CString::new(text).unwrap_or(CString::default());
			let raw_char = cstring_text.into_raw();
			let native = uniui_widget_button_create(raw_char);
			CString::from_raw(raw_char);

			uniui_widget_button_set_global_click_callback(callback);
			uniui_widget_button_set_click_listener(
				native,
				&mut self.sender as *mut _ as *mut std::ffi::c_void,
			);

			self.native.replace(native);

			native
		};

		return Ok(ptr);
	}
}
