use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	utils::*,
	NativeWidget,
	WidgetGenerator,
};

use web_sys::HtmlButtonElement;

use wasm_bindgen::{
	closure::Closure,
	JsCast,
};

use std::sync::mpsc::Sender;


pub struct WasmButton {
	native: OptionalNative<HtmlButtonElement>,
	closure: Closure<dyn FnMut()>,
}

impl WasmButton {
	pub fn new(sender: Sender<()>) -> WasmButton {
		let closure = Closure::wrap(Box::new(move || {
			sender.send_silent(());
		}) as Box<dyn FnMut()>);

		return WasmButton {
			native: OptionalNative::new(),
			closure,
		};
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
		text: String,
	) -> Result<NativeWidget, ()> {
		let may_be_button = widget_generator
			.create_element("button")
			.map(|e| {
				e.set_class_name(crate::THEME_CLASS_NAME);
				e
			})
			.and_then(|e| e.dyn_into::<HtmlButtonElement>().map_err(|_| ()));

		return match may_be_button {
			Ok(button) => {
				button.set_inner_text(&text);
				button.set_onclick(Some(self.closure.as_ref().unchecked_ref()));

				self.native.replace(button.clone());
				Ok(From::from(button))
			},
			Err(e) => {
				log::error!("couldn't create button err:{:?}", e);
				Err(())
			},
		};
	}
}
