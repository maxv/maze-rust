#include <QApplication>
#include <QLabel>
#include <QPushButton>
#include <QWidget>


extern "C" void* uniui_widget_button_create(char* text) {
	QPushButton* result = new QPushButton(text);
	return result;
}

namespace uniui_widget_button {
typedef void (*sender_callback)(void*); 
sender_callback SENDER_CALLBACK = nullptr;
}

extern "C" void uniui_widget_button_set_global_click_callback(
	uniui_widget_button::sender_callback callback
) {
	uniui_widget_button::SENDER_CALLBACK = callback;
}

extern "C" void uniui_widget_button_set_click_listener(void* button, void* data) {
	QObject::connect(
		((QPushButton*)button),
		&QPushButton::clicked,
		[=](){ uniui_widget_button::SENDER_CALLBACK(data); }
	);
}

