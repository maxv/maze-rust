//! The create provide basic functionality required for uniui_* crates family.
//!
//! If you are looking information for uniui then the best place to start is (???)[???]
//!
//! # Philosophy
//! The crate inspired by Qt's signal/slot system. [Signal] can emit data
//! and [Slot] can be connected to the [Signal] to receive data. There is
//! few new concepts like [SlotProxy] and [Property] as well.
//!
//! There is important difference between uniui and Qt's signal/slots system. [Slot]
//! doesn't activate any peace of code (function/method/etc.) by itself. Instead
//! it have to be pulled from the receiver side (via [SlotImpl::next] or
//! [SlotImpl::last] or etc.). Usually it'll be done by [uniui_gui::Application]'s
//! event loop. Please take a look at [uniui_gui::UWidget] or [uniui_gui::UObject]
//! for slot processing simplification.

#[macro_use]
extern crate log;

#[doc(hidden)]
pub mod signal;

pub use self::signal::Signal;

#[doc(hidden)]
pub mod slot;

pub use self::slot::{
	Slot,
	SlotImpl,
	SlotProxy,
};

#[doc(hidden)]
pub mod property;

pub use self::property::Property;
