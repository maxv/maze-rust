use super::implz::Platform;

use uniui_gui::prelude::*;

pub struct Image {
	platform: Platform,
	path: Property<String>,
	keepaspect: Property<bool>,
}

impl Image {
	pub fn new() -> Image {
		return Image {
			platform: Platform::new(),
			path: Property::new(format!("")),
			keepaspect: Property::new(false),
		};
	}

	pub fn slot_set_path(&self) -> &dyn Slot<String> {
		return self.path.slot();
	}

	pub fn slot_set_keepaspect(&self) -> &dyn Slot<bool> {
		return self.keepaspect.slot();
	}
}

impl Widget for Image {
	fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return self.platform.to_native(widget_generator);
	}

	fn draw(&mut self) {
	}
}

impl DataProcessor for Image {
	fn process_data(
		&mut self,
		_: i64,
		app: &mut dyn Application,
	) {
		if self.path.try_update() {
			let link = app.get_resource_path(self.path.as_ref());
			self.platform.update_path(&link);
		}

		if self.keepaspect.try_update() {
			self.platform.update_keepaspect(self.keepaspect.as_ref());
		}
	}
}
