use uniui_gui::{
	prelude::*,
};

pub struct NoopImage {
}

impl NoopImage {
	pub fn new() -> NoopImage {
		return NoopImage {
		};
	}

	pub fn to_native(
		&mut self,
		_: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		return Err(());
	}

	pub fn update_path(
		&mut self,
		_path: &str,
	) {
	}

	pub fn update_keepaspect(
		&mut self,
		_keepaspect: &bool,
	) {
	}
}
