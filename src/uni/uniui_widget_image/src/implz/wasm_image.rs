use uniui_gui::{
	implz::wasm::WasmOptionalNative as OptionalNative,
	prelude::*,
};

use wasm_bindgen::JsCast;
use web_sys::HtmlImageElement;


pub struct WasmImage {
	native: OptionalNative<HtmlImageElement>,
}

impl WasmImage {
	pub fn new() -> WasmImage {
		return WasmImage {
			native: OptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		widget_generator: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		let node = widget_generator.create_element("img").unwrap();
		node.set_class_name(crate::THEME_CLASS_NAME);
		let image = node.dyn_into::<HtmlImageElement>().unwrap();

		self.native.replace(From::from(image.clone()));
		return Ok(From::from(image));
	}

	pub fn update_path(
		&mut self,
		path: &str,
	) {
		if let Some(native) = self.native.as_mut() {
			native.set_src(path);
		}
	}

	pub fn update_keepaspect(
		&mut self,
		keepaspect: &bool,
	) {
		if let Some(native) = self.native.as_mut() {
			let val = match keepaspect {
				true => "contain",
				false => "",
			};

			native.style().set_property_silent("object-fit", val);
		}
	}
}
