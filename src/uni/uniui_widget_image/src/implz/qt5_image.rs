use uniui_gui::{
	implz::qt5::Qt5OptionalNative as OptionalNative,
	prelude::*,
};

pub struct Qt5Image {
	_native: OptionalNative,
}

impl Qt5Image {
	pub fn new() -> Qt5Image {
		return Qt5Image {
			_native: OptionalNative::new(),
		};
	}

	pub fn to_native(
		&mut self,
		_: &mut dyn WidgetGenerator,
	) -> Result<NativeWidget, ()> {
		log::error!("Image is not implemented for Qt5. Yet.");
		return Err(());
	}

	pub fn update_path(
		&mut self,
		_path: &str,
	) {
		log::error!("update_path is not implemented for Qt5. Yet.");
	}

	pub fn update_keepaspect(
		&mut self,
		_keepaspect: &bool,
	) {
		log::error!("update_keepaspect is not implemented for Qt5. Yet.");
	}
}
