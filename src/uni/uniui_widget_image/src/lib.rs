mod image;
pub use self::image::Image;

pub const THEME_CLASS_NAME: &str = "u_image_theme_class";

#[cfg(target_arch = "wasm32")]
mod implz {
	mod wasm_image;
	pub use self::wasm_image::WasmImage as Platform;
}

#[cfg(target_os = "linux")]
mod implz {
	uniui_gui::desktop_qt! {
		mod qt5_image;
		pub use self::qt5_image::Qt5Image as Platform;
	}

	uniui_gui::desktop_noop! {
		mod noop_image;
		pub use self::noop_image::NoopImage as Platform;
	}
}
