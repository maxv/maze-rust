#!/bin/bash


echo "Starting..."
mkdir -p /host/nginx
echo "Directory created"
/bin/jaeger --log-level=warn --collector.num-workers 1 --query.base-path "/jaeger"  > /host/jaeger.log 2>&1 & # --sampling.strategies-file
echo "Jaeger started"
# /bin/unitracer_ws_server > /host/tracer.log 2>&1 &
# echo "Unitracer started"
RUST_BACKTRACE=1 RUST_LOG=debug /bin/maze_server_websocket > /host/ws.log 2>&1 &
echo "Websocket started"
RUST_BACKTRACE=1 RUST_LOG=info /bin/maze_http_server > /host/http.log 2>&1 &
echo "HTTP started"
echo "Starting nginx..."
/usr/sbin/nginx
echo "Nginx finished"
