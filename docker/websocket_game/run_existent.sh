#!/bin/bash
branch_name=`git rev-parse --abbrev-ref HEAD`
image_name="registry.gitlab.com/maxv/maze-rust/websocket_game/$branch_name:latest"
docker pull $image_name
docker run -d -p 127.0.0.1:7000:80/tcp -v /tmp/host:/host  $image_name
