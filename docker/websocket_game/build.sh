#!/bin/bash

container_name=wgs

target_dir=../../src/target

RUSTFLAGS="-D warnings" cargo build --manifest-path ../../src/Cargo.toml -p maze_server_websocket --target-dir $target_dir --release || exit 1
cp $target_dir/release/maze_server_websocket bin/maze_server_websocket || exit 1

RUSTFLAGS="-D warnings" cargo build --manifest-path ../../src/Cargo.toml -p unitracer_ws_server --target-dir  $target_dir --release || exit 1
cp $target_dir/release/unitracer_ws_server bin/unitracer_ws_server || exit 1

RUSTFLAGS="-D warnings" TARGET_WASM="/zprojects/maze/src/target-wasm"  cargo build --manifest-path ../../src/Cargo.toml -p maze_http_server --target-dir  $target_dir --release || exit 1
cp $target_dir/release/maze_http_server bin/maze_http_server || exit 1

docker build -t $container_name . || exit 1

mount_dir=/tmp/host || exit 1

pid_c=`cat container.pid`
echo pid to kill $pid_c
docker container kill $pid_c
echo Kill:$?

mkdir -p $mount_dir/nginx
docker run -d  \
	-p 8080:80/tcp \
	-v $mount_dir:/host  \
	$container_name \
	> container.pid \
	 || exit 1

docker container ls
pid_c=`cat container.pid`
docker container logs $pid_c

