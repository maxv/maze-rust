echo "PREPARE ENV"

# Config:
# ssh-keygen -f /tmp/test2
# ssh-copy-id -i /tmp/test2 -o UserKnownHostsFile=/tmp/new_host  root@192.168.56.101
# ssh -i /tmp/test2 -o UserKnownHostsFile=/tmp/new_host root@192.168.56.101 --  sudo apt update
# ssh -i /tmp/test2 -o UserKnownHostsFile=/tmp/new_host root@192.168.56.101 --  sudo apt install python -y


HOSTS_PRODUCTION=''
KNOWN_HOSTS=''
SSH_KEY_SECRET=''


echo TEST IT

test_dir=/tmp/test-ansi
mkdir -p $test_dir

echo "$HOSTS_PRODUCTION" > $test_dir/hosts
echo "$KNOWN_HOSTS" > $test_dir/known_hosts

touch $test_dir/ansi
chmod a-rw $test_dir/ansi
chmod u+rw $test_dir/ansi
echo "$SSH_KEY_SECRET" > $test_dir/ansi

ansible pioneers -i $test_dir/hosts --private-key=$test_dir/ansi -u root --ssh-common-args "-o UserKnownHostsFile=$test_dir/known_hosts" -m ping


ansible_conf="-i $test_dir/hosts --private-key=$test_dir/ansi --ssh-common-args \"-o UserKnownHostsFile=$test_dir/known_hosts\""
image_conf="-e image_name=\"registry.gitlab.com/maxv/maze-rust/websocket_game/master:311b63d0d413c1150ffaf950fcd30070319cb713\""
eval "ansible-playbook $ansible_conf  ./production/prepare-production.yml"
eval "ansible-playbook $ansible_conf $image_conf  ./ws-service/deploy-ws.yml"

